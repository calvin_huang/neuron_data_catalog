import boto3
import sys
import hvac
import pymysql
import psycopg2
import os
import logging
from datetime import datetime, timedelta
import traceback
from neuron_data_utils.timestream_reader import Timestream_reader
import json
from pathlib import Path

logging.basicConfig(format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S',
                            level=20)

class Db_access():
    country_list = ['au', 'nz', 'gb', 'ca', 'kr']

    def __init__(self, Token='FAKE'):
        if os.path.isfile('/home/ubuntu/vault_config.py'):
            sys.path.insert(0, '/home/ubuntu')
            from vault_config import reader
            client = hvac.Client(
                url='https://vault-hq.neuron.sg',
                token=reader.VAULT_TOKEN)

        else:
            file = os.path.join(Path.home(), '.neuron_secret/vault')
            if os.path.isfile(file):
                with open(file) as f:
                    Token=f.readline()[:-1]
            client = hvac.Client(
                url='https://vault-hq.neuron.sg',
                token=os.getenv("VAULT_TOKEN", Token)
            )
        logging.info("------ Checking Started at: " + str(datetime.now()) + "------")
        logging.info(f'Vault client is authenticated = {client.is_authenticated()}')
        self.redshift_editor_auth = client.secrets.kv.v2.read_secret_version(
            mount_point='redshift-editor', path='database-editor')

        self.redshift_reader_auth = client.secrets.kv.v2.read_secret_version(
            mount_point='redshift-reader', path='database-reader')

        self.mysql_reader_auth = client.secrets.kv.v2.read_secret_version(
            mount_point='mysql-reader', path='database-reader')

        self.timestream_auth = client.secrets.kv.v2.read_secret_version(
            mount_point='neuron-data', path='timestream')

        self.vault_client=client

    def get_vault_client(self):
        return self.vault_client

    def get_redshift_editer_conn(self, db_name='neuron_pro'):
        return psycopg2.connect(dbname=db_name,
                                    host='redshift-cluster-1.cqvneaepqxlh.ap-southeast-1.redshift.amazonaws.com',
                                    port='5439',
                                    user=self.redshift_editor_auth['data']['data']['redshift.editor.username'],
                                    password=self.redshift_editor_auth['data']['data']['redshift.editor.password'])

    def get_redshift_test_editor_conn(self, db_name='data-raw'):
        return psycopg2.connect(dbname=db_name,
                                    host='redshift-cluster-test.cqvneaepqxlh.ap-southeast-1.redshift.amazonaws.com',
                                    port='5439',
                                    user=self.redshift_editor_auth['data']['data']['redshift.test.editor.username'],
                                    password=self.redshift_editor_auth['data']['data']['redshift.test.editor.password'])


    def get_redshift_reader_conn(self):
        return psycopg2.connect(dbname='neuron_pro',
                                    host='redshift-cluster-1.cqvneaepqxlh.ap-southeast-1.redshift.amazonaws.com',
                                    port='5439',
                                    user=self.mysql_reader_auth['data']['data']['redshift.editor.username'],
                                    password=self.mysql_reader_auth['data']['data']['redshift.editor.password'])



    def get_user_mysql_conn(self, is_kr=False):
        if is_kr:
            return pymysql.connect(host="kr-user-reader.c7ckiqj52mxh.ap-northeast-2.rds.amazonaws.com",
                                   user=self.mysql_reader_auth['data']['data']['rds.kr_user.reader.username'],
                                   password=self.mysql_reader_auth['data']['data']['rds.kr_user.reader.password'],
                                   database="neuron_user_kr")
        return pymysql.connect(host="user-sdy-reader.cdbyxqfptcdt.ap-southeast-1.rds.amazonaws.com",
                                       user=self.mysql_reader_auth['data']['data']['rds.user_syd.reader.username'],
                                       password=self.mysql_reader_auth['data']['data']['rds.user_syd.reader.password'], database="neuron_user")

    def get_mysql_conn(self, country):
        country = country.lower()
        if country not in self.country_list:
            logging.error("cannot get mysql connection for country {}".format(country))
            return None

        if country=='au':
            return pymysql.connect(host="neuron-pro-au-read.cdbyxqfptcdt.ap-southeast-1.rds.amazonaws.com",
                                 user=self.mysql_reader_auth['data']['data']['rds.au.reader.username'],
                                 password=self.mysql_reader_auth['data']['data']['rds.au.reader.password'],database='neuron_australia')
        if country=='nz':
            return pymysql.connect(host="neuron-pro-au-read.cdbyxqfptcdt.ap-southeast-1.rds.amazonaws.com",
                        user=self.mysql_reader_auth['data']['data']['rds.au.reader.username'],
                        password=self.mysql_reader_auth['data']['data']['rds.au.reader.password'], database='neuron_newzealand')
        if country=='gb':
            return pymysql.connect(host="gb-pro-reader.cdbyxqfptcdt.ap-southeast-1.rds.amazonaws.com",
                                 user=self.mysql_reader_auth['data']['data']['rds.gb.reader.username'],
                                 password=self.mysql_reader_auth['data']['data']['rds.gb.reader.password'],database='neuron_gb')
        if country=='kr':
            return pymysql.connect(host="kr-pro-reader.c7ckiqj52mxh.ap-northeast-2.rds.amazonaws.com",
                                user=self.mysql_reader_auth['data']['data']['rds.kr.reader.username'],
                                password=self.mysql_reader_auth['data']['data']['rds.kr.reader.password'],database="neuron_korea")
        if country=='ca':
            return pymysql.connect(host="ca-pro-slave.cdbyxqfptcdt.ap-southeast-1.rds.amazonaws.com",
                                       user=self.mysql_reader_auth['data']['data']['rds.ca.reader.username'],
                                       password=self.mysql_reader_auth['data']['data']['rds.ca.reader.password'], database="neuron_canada")

    def get_qa_mysql_conn(self):
        return pymysql.connect(host="neurondb-qa.neuron.sg",
                                       user=self.mysql_reader_auth['data']['data']['rds.qa.reader.username'],
                                       password=self.mysql_reader_auth['data']['data']['rds.qa.reader.password'], database="neuron_canada")
    def get_boto3_session(self):
        return boto3.Session(aws_access_key_id=self.timestream_auth['data']['data']['id'],
                                aws_secret_access_key=self.timestream_auth['data']['data']['secret'])

    def get_timestream_query_client(self):
        session= boto3.Session(aws_access_key_id=self.timestream_auth['data']['data']['id'], aws_secret_access_key=self.timestream_auth['data']['data']['secret'], region_name='us-east-1')
        query_client = session.client('timestream-query')
        return query_client

    def get_country_database_map(self):
        country_2_parameters = {
            'kr': {'mysql_database': 'neuron_korea',
                   'timestream_table': "kr-iot",
                   'red_stripe': 'neuron_stripe',
                   'paypal_schema': None},
            'gb': {'mysql_database': 'neuron_gb',
                   'timestream_table': "gb-iot",
                   'red_stripe': 'stripe_uk',
                   'paypal_schema': 'paypal_uk'},
            'nz': {'mysql_database': 'neuron_newzealand',
                   'timestream_table': "nz-iot",
                   'red_stripe': 'neuron_stripe_nz',
                   'paypal_schema': 'neuron_paypal_nz'},
            'au': {'mysql_database': 'neuron_australia',
                   'timestream_table': "au-iot",
                   'red_stripe': 'neuron_stripe_au',
                   'paypal_schema': 'neuron_paypal'},
            'ca': {'mysql_database': 'neuron_canada',
                   'timestream_table': "ca-iot",
                   'red_stripe': 'neuron_stripe_ca',
                   'paypal_schema': None}
            }
        return country_2_parameters

def test_mysql(client):
    client.get_qa_mysql_conn()
    client.get_mysql_conn("au")

def test_timestream(client):
    timestream_client=client.get_timestream_query_client()
    time_stream_reader=Timestream_reader(timestream_client)
    query1="""
    SELECT * FROM "{DATABASE_NAME}"."{TABLE_NAME}" WHERE time between ago(15000s) and now() 
    and imei='{imei}'
    ORDER BY time DESC
    """.format(DATABASE_NAME="datateam-timestream", TABLE_NAME="nz-iot", imei="862708041251179")
    result=time_stream_reader.run_query(query1)
    return result

def test_redshift(client):
    red_conn=client.get_redshift_test_editor_conn()
    query="""
        select s.nspname as table_schema, s.oid as schema_id,  u.usename as owner
        from pg_catalog.pg_namespace s
        join pg_catalog.pg_user u on u.usesysid = s.nspowner
        order by table_schema;
    """
    print(red_shift_exec(query, red_conn))

def red_shift_exec(command, red_conn):
    try:
        red_cur = red_conn.cursor()
        red_cur.execute(command)
        red_conn.commit()

    except Exception as err:
        traceback.print_exc()
        logging.error(err)


if __name__ == "__main__":
    my_db_access=Db_access("fake")
    test_redshift(my_db_access)


