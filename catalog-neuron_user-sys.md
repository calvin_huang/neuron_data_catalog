[TOC]

# card_block

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| account     | -            | character varying (24.0)    | -             | -        |
| blocked     | -            | smallint                    | -             | -        |
| comment     | -            | character varying (1536.0)  | -             | -        |
| count       | -            | integer                     | -             | -        |
| created_at  | -            | timestamp without time zone | -             | -        |
| created_by  | -            | bigint                      | -             | -        |
| fingerprint | -            | character varying (192.0)   | -             | -        |
| id          | -            | bigint                      | -             | -        |
| reason      | -            | character varying (192.0)   | -             | -        |
| source      | -            | character varying (192.0)   | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| updated_by  | -            | bigint                      | -             | -        |



# flyway_schema_history

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| checksum       | -            | integer                     | -             | -        |
| description    | -            | character varying (600.0)   | -             | -        |
| execution_time | -            | integer                     | -             | -        |
| installed_by   | -            | character varying (300.0)   | -             | -        |
| installed_on   | -            | timestamp without time zone | -             | -        |
| installed_rank | -            | integer                     | -             | -        |
| script         | -            | character varying (3000.0)  | -             | -        |
| success        | -            | smallint                    | -             | -        |
| type           | -            | character varying (60.0)    | -             | -        |
| version        | -            | character varying (150.0)   | -             | -        |



# password_update_record

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at      | -            | timestamp without time zone | -             | -        |
| created_by      | -            | bigint                      | -             | -        |
| id              | -            | bigint                      | -             | -        |
| password        | -            | character varying (768.0)   | -             | -        |
| password_length | -            | smallint                    | -             | -        |
| user_id         | -            | bigint                      | -             | -        |



# permission

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| name       | -            | character varying (192.0)   | -             | -        |



# role

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| id             | -            | bigint                      | -             | -        |
| parent_role_id | -            | bigint                      | -             | -        |
| role_code      | -            | character varying (150.0)   | -             | -        |
| updated_at     | -            | timestamp without time zone | -             | -        |
| updated_by     | -            | bigint                      | -             | -        |



# role_permission

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| id            | -            | bigint                      | -             | -        |
| permission_id | -            | bigint                      | -             | -        |
| role          | -            | character varying (150.0)   | -             | -        |



# role_resource_permission

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| id         | -            | bigint                      | -             | -        |
| permission | -            | character varying (1500.0)  | -             | -        |
| resource   | -            | character varying (150.0)   | -             | -        |
| role_code  | -            | character varying (150.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| updated_by | -            | bigint                      | -             | -        |



# user

|                        | definition   | data_type                   | related_key   | remark   |
|:-----------------------|:-------------|:----------------------------|:--------------|:---------|
| address                | -            | character varying (768.0)   | -             | -        |
| avatar                 | -            | character varying (768.0)   | -             | -        |
| city                   | -            | character varying (192.0)   | -             | -        |
| country                | -            | character varying (96.0)    | -             | -        |
| country_code           | -            | character varying (24.0)    | -             | -        |
| created_at             | -            | timestamp without time zone | -             | -        |
| deleted                | -            | smallint                    | -             | -        |
| disable_reason         | -            | character varying (3072.0)  | -             | -        |
| disabled               | -            | smallint                    | -             | -        |
| driver_license         | -            | smallint                    | -             | -        |
| email                  | -            | character varying (384.0)   | -             | -        |
| email_marketing        | -            | smallint                    | -             | -        |
| email_verified         | -            | smallint                    | -             | -        |
| full_name              | -            | character varying (384.0)   | -             | -        |
| id                     | -            | bigint                      | -             | -        |
| id_card                | -            | character varying (192.0)   | -             | -        |
| is_vip                 | -            | smallint                    | -             | -        |
| location_marketing     | -            | smallint                    | -             | -        |
| mobile                 | -            | character varying (96.0)    | -             | -        |
| nickname               | -            | character varying (384.0)   | -             | -        |
| notification_marketing | -            | smallint                    | -             | -        |
| password               | -            | character varying (768.0)   | -             | -        |
| slogan                 | -            | character varying (768.0)   | -             | -        |
| sms_marketing          | -            | smallint                    | -             | -        |
| updated_at             | -            | timestamp without time zone | -             | -        |
| updated_by             | -            | bigint                      | -             | -        |
| usage_tracking         | -            | smallint                    | -             | -        |
| user_protocol          | -            | smallint                    | -             | -        |
| username               | -            | character varying (768.0)   | -             | -        |



# user_country

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (192.0)   | -             | -        |
| country    | -            | character varying (24.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| is_vip     | -            | smallint                    | -             | -        |
| role_code  | -            | character varying (96.0)    | -             | -        |
| status     | -            | character varying (96.0)    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_mfa

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| secret     | -            | character varying (192.0)   | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_role

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (60.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| role_code  | -            | character varying (150.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| updated_by | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_social

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| created_at  | -            | timestamp without time zone | -             | -        |
| first_name  | -            | character varying (150.0)   | -             | -        |
| id          | -            | bigint                      | -             | -        |
| last_name   | -            | character varying (150.0)   | -             | -        |
| name        | -            | character varying (150.0)   | -             | -        |
| social_id   | -            | character varying (150.0)   | -             | -        |
| social_type | -            | character varying (60.0)    | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| user_id     | -            | bigint                      | -             | -        |



# user_stripe

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| account     | -            | character varying (96.0)    | -             | -        |
| country     | -            | character varying (24.0)    | -             | -        |
| created_at  | -            | timestamp without time zone | -             | -        |
| customer_id | -            | character varying (192.0)   | -             | -        |
| id          | -            | bigint                      | -             | -        |
| platform    | -            | character varying (96.0)    | -             | -        |
| primary     | -            | smallint                    | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| user_id     | -            | bigint                      | -             | -        |

