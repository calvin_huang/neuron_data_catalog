import os
from itertools import combinations
import numpy as np
import pandas as pd
import json
import time
import logging


class data_catalog_engin:
    def __init__(self, csv_file):
        self.filename = csv_file[:-4]   # when the file ends with '.csv'
        self.csv_file = csv_file
        self.cwd = os.getcwd() + '/' + self.csv_file
        self.raw_csv = pd.read_csv(self.cwd)
        self.raw_df = self.concatDataType()

    def concatDataType(self):
        df = self.raw_csv
        data_type = df['data_type'].values.tolist()
        length = df['character_maximum_length'].values.tolist()
        type_list = []
        for i, j in zip(data_type, length):
            if np.isnan(j):
                type_list.append(i)
            else:
                type_list.append(i+' ('+str(j)+')')
        df = df.drop(columns=['data_type', 'character_maximum_length'])
        df['data_type'] = type_list
        print('Data_type and max_length are concatenated')
        return df

    def writeJsonSchemas(self, schema_list, output_name, fill_empty=True):
        # reference: https://python.tutorialink.com/converting-a-multindex-dataframe-to-a-nested-dictionary-closed/
        df = self.raw_df
        df = df.loc[df['table_schema'].isin(schema_list)]
        d = df.groupby('table_schema').apply(
            lambda a: dict(a.groupby('table_name').apply(
                lambda b: dict(b.groupby('column_name').apply(
                    lambda x: {'data_type': list(x['data_type'])[0]})))))
        d = d.to_dict()

        # fill empty features
        if fill_empty:
            for schema in d:
                for table in d[schema]:
                    for col in d[schema][table]:
                        if 'data_type' not in d[schema][table][col].keys():
                            d[schema][table][col]['data_type'] = '-'
                        if 'definition' not in d[schema][table][col].keys():
                            d[schema][table][col]['definition'] = '-'
                        if 'related_key' not in d[schema][table][col].keys():
                            d[schema][table][col]['related_key'] = '-'
                        if 'remark' not in d[schema][table][col].keys():
                            d[schema][table][col]['remark'] = '-'

        json_object = json.dumps(d, indent=4)
        json_file = output_name
        with open(json_file, "w") as f:
            f.write(json_object)
        print("Json file '{}' is created.".format(json_file))

    def writeJsonCombinedSchemas(self, schema_list, combined_name, output_name, fill_empty=True):
        # reference: https://python.tutorialink.com/converting-a-multindex-dataframe-to-a-nested-dictionary-closed/
        df = self.raw_df
        df = df.loc[df['table_schema'].isin(schema_list)]
        df['table_schema'] = combined_name
        d = df.groupby('table_schema').apply(
            lambda a: dict(a.groupby('table_name').apply(
                lambda b: dict(b.groupby('column_name').apply(
                    lambda x: {'data_type': list(x['data_type'])[0]})))))
        d = d.to_dict()

        # fill empty features
        if fill_empty:
            for schema in d:
                for table in d[schema]:
                    for col in d[schema][table]:
                        if 'data_type' not in d[schema][table][col].keys():
                            d[schema][table][col]['data_type'] = '-'
                        if 'definition' not in d[schema][table][col].keys():
                            d[schema][table][col]['definition'] = '-'
                        if 'related_key' not in d[schema][table][col].keys():
                            d[schema][table][col]['related_key'] = '-'
                        if 'remark' not in d[schema][table][col].keys():
                            d[schema][table][col]['remark'] = '-'

        json_object = json.dumps(d, indent=4)
        json_file = output_name
        with open(json_file, "w") as f:
            f.write(json_object)
        print("Json file '{}' is created.".format(json_file))
        # with open(json_file+'-copy.json', "w") as f:
        #     f.write(json_object)
        # print("Json file '{}' is created.".format(json_file+'-copy.json'))

    def getJsonCatalog(self):
        # reference: https://python.tutorialink.com/converting-a-multindex-dataframe-to-a-nested-dictionary-closed/
        df = self.raw_df
        d = df.groupby('table_schema').apply(
            lambda a: dict(a.groupby('table_name').apply(
                lambda b: dict(b.groupby('column_name').apply(
                    lambda x: {'data_type': list(x['data_type'])[0]})))))
        d = d.to_dict()
        json_object = json.dumps(d, indent=4)
        json_file = self.filename+'.json'
        with open(json_file, "w") as f:
            f.write(json_object)
        print("Json file '{}' is created.".format(json_file))

    def table_to_md(self, md_name, table_df, table_name=None, purpose=None, status=None, comp_df=None, link=None, open_para='a'):
        # open_para = 'a' - Append - will append to the end of the file
        # open_para = 'w' - Write - will overwrite any existing content
        """
        Table_name
        Purpose:
        Status:
        |    | column_name            | data_type   | Definition   | Related key(s)   | Remarks   |
        |---:|:-----------------------|:------------|:-------------|:-----------------|:----------|
        |  0 | date                   | date        |              |                  |           |
        |  1 | hour                   | bigint      |              |                  |           |
        |  2 | minute                 | bigint      |              |                  |           |
        [Return to top]
        """
        with open(md_name, open_para) as f:
            if table_name is not None:
                f.write('\n\n## ' + table_name)
            if purpose is not None:
                f.write('\n\nPurpose: ' + purpose)
            if status is not None:
                f.write('\n\nStatus: ' + status)
            if comp_df is not None:
                f.write('\n\n')
                f.write(comp_df.to_markdown())
            f.write('\n\n')
            f.write(table_df.to_markdown(index=False))
            if link:
                f.write('\n\n[Return to top](' + link + '#top)\n')

    def df_to_md(self, md_name, table_name, info, df, link=None, open_para='a', show=False):
        # open_para = 'a' - Append - will append to the end of the file
        # open_para = 'w' - Write - will overwrite any existing content
        """
        ## Table name
        info
        |                                  | neuron_australia   | neuron_canada   | neuron_gb   | neuron_korea   |
        |:---------------------------------|:-------------------|:----------------|:------------|:---------------|
        | adl_cbd_status_count             | 1                  |                 |             |                |
        | adl_wa_status_count              | 1                  |                 |             |                |
        link
        """
        f = open(md_name, open_para)
        f.write('\n\n# ' + table_name)
        if info:
            f.write('\n\n' + info + '\n\n')
        else:
            f.write('\n\n')
        f.write(df.to_markdown())
        if link:
            f.write('\n\n[Return to top](' + link + '#top)\n')
        f.write('\n\n')
        f.close()
        if show:
            print('{} added to \'{}\''.format(table_name, md_name))

    def oneSchema_to_md(self, df, schema_name, md_name, keys=None, removed_table=None):
        if isinstance(schema_name, list):
            schema_name = schema_name[0]
        elif isinstance(schema_name, str):
            pass
        else:
            return print('Error: The schema_name is neither List or Str.')

        table_names, table_df_dic = self.getTablesOfASchema(df=df, schema_name=schema_name, keys=keys, removed_table=None)
        with open(md_name, 'a') as f:
            f.write('[TOC]')
            f.write('\n\n# ' + schema_name)
            f.write('\n\n')
        for table in table_names:
            table_df = table_df_dic[table]
            self.table_to_md(md_name=md_name, table_df=table_df, table_name=table)
        print('Markdown for scheme {} is created in {}.'.format(schema_name, md_name))

    def multiSchemas_to_md(self, df, schema_names, md_name, keys_dict=None, removed_table=None):
        for schema_name in schema_names:
            if isinstance(keys_dict, dict):
                keys = keys_dict[schema_name]
            elif isinstance(keys_dict, list):
                keys = keys_dict
            else:
                keys = None
            self.oneSchema_to_md(df, schema_name, md_name, keys, removed_table=None)

    def combinedSchemas_to_md(self, md_name, df, schemas_to_combine, link=None, open_para='a', keys=None,
                              removed_table=None, add_comp_table=True):
        # add [TOC]
        with open(md_name, open_para) as f:
            f.write('[TOC]')
        # add comparison table
        if add_comp_table:
            comp_df = self.getComparisonTable(df, schemas_to_combine)
            self.df_to_md(md_name=md_name, table_name='Schema Comparison Table',
                          info=None, df=comp_df, link=None, open_para=open_para)

        # add tables
        tables_to_add = []
        all_table_dict = {}
        for schema_name in schemas_to_combine:
            table_names, table_df_dic = self.getTablesOfASchema(df=df, schema_name=schema_name,
                                                                keys=keys, removed_table=removed_table)
            all_table_dict.update(table_df_dic)
            for table in table_names:
                if table not in tables_to_add:
                    tables_to_add.append(table)
        tables_to_add.sort()
        for table in tables_to_add:
            self.table_to_md(md_name=md_name, table_df=all_table_dict[table],
                             table_name=table, purpose=None, status=None, link=link, open_para=open_para)
        print('{} tables added to \'{}\''.format(len(tables_to_add), md_name))

    def getTablesOfASchema(self, df, schema_name, keys=None, remarks=None, removed_table=None):
        # get all tables of ONE given schema
        # df = all schemas
        """
        output:
            table_names = Array(table_name)
            table_df_dic = {table_name: col_name_df}
        """
        if removed_table is not None:
            temp = df.loc[df['table_schema'] == schema_name]
            schema_df = temp.loc[~temp['table_name'].isin(removed_table)].reset_index(drop=True)
        else:
            schema_df = df.loc[df['table_schema'] == schema_name].reset_index(drop=True)
        table_df = schema_df.drop('table_schema', axis=1)

        # add related keys
        if keys is not None:
            column_names = table_df['column_name'].values
            related_keys = []
            for name in column_names:
                if name in keys:
                    related_keys.append('.'.join(name.split('_')))
                else:
                    related_keys.append(' ')
            table_df['related_keys'] = related_keys
        else:
            table_df['related_keys'] = ' '

        if remarks == None:
            table_df['remarks'] = ' '

        table_names = table_df.table_name.unique()
        table_names.sort()
        table_df_dic = {}  # table_name: table
        for i in table_names:
            table_df_dic[i] = table_df.loc[table_df['table_name'] == i].reset_index(drop=True).drop('table_name',
                                                                                                    axis=1).fillna('')
        return table_names, table_df_dic

    def getTableNamesFromSchema(self, df, schema_name):
        """
        input : schema_name
        output: Array(table_names)
        """
        schema_df = df.loc[df['table_schema'] == schema_name].reset_index(drop=True)
        return schema_df.table_name.unique()

    def getColNamesFromSchemaTable(self, df, schema_name, table_name):
        """
        input : schema_name, table_name
        output: Array(column_names)
        """
        table_df = df.loc[(df['table_schema'] == schema_name) & (df['table_name'] == table_name)].reset_index(drop=True)
        return table_df.column_name.unique()

    def similarityRateBySchemaNames(self, df, schema_names, num, output_name=None, include_col=False):
        """
        schema_names:  [Array] schema_name
        num:           Length of the sequence
        output_name:   Name of the output csv file (=None if no need)
        include_col:   [Bool] True if need to get SR_column

        output:        [DataFrame] Similarity Rate result
        """
        schema_combs = list(combinations(schema_names, num))
        groupby_schema = df.groupby('table_schema')
        if include_col:
            groupby_schemaTable = df.groupby(['table_schema', 'table_name'])
        schema_table_dict = {}
        for schema_name in schema_names:
            temp = groupby_schema.get_group(schema_name).table_name.unique()
            schema_table_dict[schema_name] = temp

        columns = []
        for i in range(num):
            columns.append('Schema_{}'.format(i + 1))
        for i in range(num):
            columns.append('Percen_{}'.format(i + 1))
        columns.append('Average_tableSR')
        if include_col:
            columns.append('Average_colSR')
        comparison_df = pd.DataFrame(columns=columns)
        for schema_comb in list(schema_combs):
            names, sets, rates = [], [], []
            for i in range(num):
                names.append(schema_comb[i])
                sets.append(set(schema_table_dict[schema_comb[i]]))
            common_tables = sets[0].intersection(*sets[1:])  # use of *
            c = len(common_tables)
            for i in range(num):
                rates.append(c / len(sets[i]))
            average = sum(rates) / len(rates)
            avgColSR = 0
            if include_col:
                sr_col = []  # sr1, sr2, sr3
                for table in common_tables:
                    col_sets = []  # colnames1, colnames2, colnames3
                    for schema in schema_comb:
                        col_names = groupby_schemaTable.get_group((schema, table))['column_name'].unique()
                        col_sets.append(set(col_names))
                    common_cols = col_sets[0].intersection(*col_sets[1:])
                    srList = []
                    for col_names in col_sets:
                        srList.append(len(common_cols) / len(col_names))
                    avgColSR = sum(srList) / len(srList)
                d = dict(zip(columns, names + rates + list([average]) + list([avgColSR])))
            else:
                d = dict(zip(columns, names + rates + list([average])))
            # comparison_df = comparison_df.append(d, ignore_index=True)
            comparison_df = pd.concat([comparison_df, pd.DataFrame.from_dict([d])])
        if output_name is not None:
            comparison_df.to_csv(output_name)
            print("Similarity rate output in csv: ", output_name)
        return comparison_df

    def getComparisonTable(self, df, schemas_to_merge):
        all_table_names = []
        each_table_names = []
        for schema in schemas_to_merge:
            table_names, table_df_dic = self.getTablesOfASchema(df, schema)
            each_table_names.append(table_names)
            all_table_names += table_names.tolist()
        all_table_names = list(set(all_table_names))
        all_table_names.sort()

        rows = [[] for i in range(len(all_table_names))]
        for i in range(len(all_table_names)):
            for j in range(len(schemas_to_merge)):
                if all_table_names[i] in each_table_names[j]:
                    rows[i].append('1')
                else:
                    rows[i].append(' ')
        schema_comp_df = pd.DataFrame(np.array(rows), columns=schemas_to_merge, index=all_table_names)
        return schema_comp_df


def getCommonValues(csv_file):
    df = pd.read_csv(csv_file)
    cols = df.columns
    sets = []
    for col in cols:
        sets.append(set(df[col]))
    commons = sets[0].intersection(*sets[1:])
    return list(commons)


def show_jsonData(json_data):
    json_object = json.dumps(json_data, indent=4)
    print(json_object)


def csv_to_nestedDict(csv_data):
    df = csv_data
    three = list(df.columns[:3])
    others = list(df.columns)[3:]
    dic = {}
    for one in others:
        four = list(df.columns[:3])+[one]
        df_four = df[four]
        col1, col2, col3, att = four
        d = df_four.groupby(col1).apply(
            lambda a: dict(a.groupby(col2).apply(
                lambda b: dict(b.groupby(col3).apply(
                    lambda x: {att: list(x[att])[0]})))))
        dic = edit_dict(dic, d.to_dict(), show=False)
    return dic


def update_dict(ori, new, show=True):
    origin = dict(ori)
    update = dict(new)
    print('\n>----------< Modifications >----------<\n')
    for schema in update.keys():
        if schema not in origin.keys():
            logging.error("No schema '%s' in database.", schema)
        else:
            for table in update[schema].keys():
                if table not in origin[schema].keys():
                    logging.error("No table '%s' in (%s)'.", table, schema)
                else:
                    for column in update[schema][table].keys():
                        if column not in origin[schema][table].keys():
                            logging.error("No column '%s' in (%s -> %s).", column, schema, table)
                        else:
                            for att in update[schema][table][column].keys():
                                if att not in origin[schema][table][column].keys():
                                    logging.error("No feature '%s' in (%s -> %s -> %s).", att, schema, table, column)
                                else:
                                    temp_ori = origin[schema][table][column][att]
                                    temp_new = update[schema][table][column][att]
                                    if temp_ori != temp_new:
                                        if show:
                                            print(' - %s -> %s -> %s -> %s -> %s' %
                                                  (schema, table, column, att, temp_ori))
                                            print(' + %s -> %s -> %s -> %s -> %s' %
                                                  (schema, table, column, att, temp_new))
                                        origin[schema][table][column][att] = update[schema][table][column][att]
    print('\n>---------------< End >---------------<\n')
    return origin.copy()


def edit_dict(origin, update, show=True):
    for schema in update.keys():
        if schema not in origin.keys():
            origin[schema] = update[schema]
            if show:
                print(' + %s -> %s' % (schema, update[schema]))
        else:
            for table in update[schema].keys():
                if table not in origin[schema].keys():
                    origin[schema][table] = update[schema][table]
                    if show:
                        print(' + %s -> %s -> %s' % (schema, table, update[schema][table]))
                else:
                    for column in update[schema][table].keys():
                        if column not in origin[schema][table].keys():
                            origin[schema][table][column] = update[schema][table][column]
                            if show:
                                print(' + %s -> %s -> %s -> %s' % (schema, table, column,
                                                                   update[schema][table][column]))
                        else:
                            for att in update[schema][table][column].keys():
                                if att not in origin[schema][table][column].keys():
                                    origin[schema][table][column][att] = update[schema][table][column][att]
                                    if show:
                                        print(' + %s -> %s -> %s -> %s -> %s' % (schema, table, column, att,
                                                                                 update[schema][table][column][att]))
                                else:
                                    temp_ori = origin[schema][table][column][att]
                                    temp_new = update[schema][table][column][att]
                                    if temp_ori != temp_new:
                                        if show:
                                            print(' - %s -> %s -> %s -> %s -> %s' %
                                                  (schema, table, column, att, temp_ori))
                                            print(' + %s -> %s -> %s -> %s -> %s' %
                                                  (schema, table, column, att, temp_new))
                                        origin[schema][table][column][att] = update[schema][table][column][att]
    return origin


def getDictFromJson(json_file):
    with open(json_file, 'r') as f:
        json_dict = json.load(f)
    return json_dict


def getDictFromCsv(csv_file):
    csv_data = pd.read_csv(csv_file)
    csv_dict = csv_to_nestedDict(csv_data)
    return csv_dict


def writeJsonFromDict(d, json_file):
    json_object = json.dumps(d, indent=4)
    with open(json_file, "w") as f:
        f.write(json_object)
    print('Json file %s is created.' % json_file)


def dataframe_to_md(table_name, df, md_name, info=None, link=None, open_para='a', show=False):
    # open_para = 'a' - Append - will append to the end of the file
    # open_para = 'w' - Write - will overwrite any existing content
    """
    ## Table name
    info
    |                                  | neuron_australia   | neuron_canada   | neuron_gb   | neuron_korea   |
    |:---------------------------------|:-------------------|:----------------|:------------|:---------------|
    | adl_cbd_status_count             | 1                  |                 |             |                |
    | adl_wa_status_count              | 1                  |                 |             |                |
    link
    """
    f = open(md_name, open_para)
    f.write('\n\n# ' + table_name)
    if info:
        f.write('\n\n' + info + '\n\n')
    else:
        f.write('\n\n')
    f.write(df.to_markdown())
    if link:
        f.write('\n\n[Return to top](' + link + '#top)\n')
    f.write('\n\n')
    f.close()
    if show:
        print('{} added to \'{}\''.format(table_name, md_name))


def json_to_md(json_file, md_name, fill_empty=True):
    nested_dict = getDictFromJson(json_file)

    # fill empty features
    if fill_empty:
        for schema in nested_dict:
            for table in nested_dict[schema]:
                for col in nested_dict[schema][table]:
                    if 'data_type' not in nested_dict[schema][table][col].keys():
                        nested_dict[schema][table][col]['data_type'] = '-'
                    if 'definition' not in nested_dict[schema][table][col].keys():
                        nested_dict[schema][table][col]['definition'] = '-'
                    if 'related_key' not in nested_dict[schema][table][col].keys():
                        nested_dict[schema][table][col]['related_key'] = '-'
                    if 'remark' not in nested_dict[schema][table][col].keys():
                        nested_dict[schema][table][col]['remark'] = '-'

    # write to markdown
    for schema in nested_dict:
        for table in nested_dict[schema]:
            table_dict = {}
            for col in nested_dict[schema][table]:
                tmp = nested_dict[schema][table][col]
                table_dict[col] = [tmp['definition'], tmp['data_type'],
                                   tmp['related_key'], tmp['remark']]
            table_df = pd.DataFrame.from_dict(table_dict, orient='index',
                                              columns=['definition', 'data_type', 'related_key', 'remark'])
            dataframe_to_md(table_name=table, df=table_df, md_name=md_name)


def now():
    return time.strftime("%Y%m%d-%H%M%S")
