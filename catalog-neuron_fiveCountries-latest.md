[TOC]

# active_scooter_location

|                      | definition   | data_type                   | related_key   | remark   |
|:---------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at           | -            | timestamp without time zone | -             | -        |
| id                   | -            | bigint                      | -             | -        |
| imei                 | -            | character varying (96.0)    | -             | -        |
| is_no_parking        | -            | smallint                    | -             | -        |
| is_no_riding         | -            | smallint                    | -             | -        |
| is_outside_service   | -            | smallint                    | -             | -        |
| last_no_parking      | -            | bigint                      | -             | -        |
| last_no_riding       | -            | bigint                      | -             | -        |
| last_outside_service | -            | bigint                      | -             | -        |
| latitude             | -            | double precision            | -             | -        |
| longitude            | -            | double precision            | -             | -        |
| trip_id              | -            | bigint                      | -             | -        |
| updated_at           | -            | timestamp without time zone | -             | -        |
| user_id              | -            | bigint                      | -             | -        |



# activity

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at        | -            | timestamp without time zone | -             | -        |
| description       | -            | character varying (600.0)   | -             | -        |
| id                | -            | bigint                      | -             | -        |
| last_active_at    | -            | timestamp without time zone | -             | -        |
| related_device_id | -            | bigint                      | -             | -        |
| type              | -            | character varying (150.0)   | -             | -        |



# alarm_status_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| alarm_on   | -            | smallint                    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |



# app_config

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| data       | -            | character varying (1500.0)  | -             | -        |
| name       | -            | character varying (150.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# app_version

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| created_at   | -            | timestamp without time zone | -             | -        |
| force_update | -            | smallint                    | -             | -        |
| id           | -            | bigint                      | -             | -        |
| platform     | -            | character varying (150.0)   | -             | -        |
| update_log   | -            | character varying (765.0)   | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| version_code | -            | integer                     | -             | -        |
| version_name | -            | character varying (150.0)   | -             | -        |



# attractions_feedback

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| comment    | -            | character varying (900.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| rating     | -            | smallint                    | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# battery_info_record

|                               | definition   | data_type                   | related_key   | remark   |
|:------------------------------|:-------------|:----------------------------|:--------------|:---------|
| battery_number                | -            | character varying (192.0)   | -             | -        |
| bms_hardware_version          | -            | integer                     | -             | -        |
| bms_software_version          | -            | integer                     | -             | -        |
| charge_mos                    | -            | smallint                    | -             | -        |
| charge_overcurrent_protect    | -            | smallint                    | -             | -        |
| charge_overtemp_protect       | -            | smallint                    | -             | -        |
| charge_overtemp_warn          | -            | smallint                    | -             | -        |
| charge_undertemp_protect      | -            | smallint                    | -             | -        |
| charge_undertemp_warn         | -            | smallint                    | -             | -        |
| charger_connected             | -            | smallint                    | -             | -        |
| charging                      | -            | smallint                    | -             | -        |
| created_at                    | -            | timestamp without time zone | -             | -        |
| discharge_lowtemp_warn        | -            | smallint                    | -             | -        |
| discharge_mos                 | -            | smallint                    | -             | -        |
| discharge_overcurrent_protect | -            | smallint                    | -             | -        |
| discharge_overtemp_protect    | -            | smallint                    | -             | -        |
| discharge_overtemp_warn       | -            | smallint                    | -             | -        |
| discharge_undertemp_protect   | -            | smallint                    | -             | -        |
| discharging                   | -            | smallint                    | -             | -        |
| full_capacity                 | -            | integer                     | -             | -        |
| gps_id                        | -            | bigint                      | -             | -        |
| id                            | -            | bigint                      | -             | -        |
| milli_current                 | -            | integer                     | -             | -        |
| milli_voltage                 | -            | integer                     | -             | -        |
| overvoltage_protect           | -            | smallint                    | -             | -        |
| overvoltage_warn              | -            | smallint                    | -             | -        |
| powerlow_warn                 | -            | smallint                    | -             | -        |
| remaining_capacity            | -            | integer                     | -             | -        |
| remaining_percentage          | -            | integer                     | -             | -        |
| shortout                      | -            | smallint                    | -             | -        |
| temperature_1                 | -            | integer                     | -             | -        |
| temperature_2                 | -            | integer                     | -             | -        |
| undervoltage_protect          | -            | smallint                    | -             | -        |
| voltage_1                     | -            | integer                     | -             | -        |
| voltage_10                    | -            | integer                     | -             | -        |
| voltage_2                     | -            | integer                     | -             | -        |
| voltage_3                     | -            | integer                     | -             | -        |
| voltage_4                     | -            | integer                     | -             | -        |
| voltage_5                     | -            | integer                     | -             | -        |
| voltage_6                     | -            | integer                     | -             | -        |
| voltage_7                     | -            | integer                     | -             | -        |
| voltage_8                     | -            | integer                     | -             | -        |
| voltage_9                     | -            | integer                     | -             | -        |
| work_cycle                    | -            | integer                     | -             | -        |
| working_status                | -            | smallint                    | -             | -        |



# battery_inspection

|                | definition                                              | data_type                   | related_key                                                     | remark   |
|:---------------|:--------------------------------------------------------|:----------------------------|:----------------------------------------------------------------|:---------|
| battery_number | Unique number attached to battery                       | character varying (90.0)    | battery_inventory.battery_number, battery_inspection.battery_no | -        |
| city           | City of battery inspection                              | character varying (96.0)    | -                                                               | -        |
| created_at     | Timestamp of when row was created                       | timestamp without time zone | -                                                               | -        |
| id             | Unique ID for the table                                 | integer                     | -                                                               | -        |
| status         | Records the success of the activity                     | character varying (60.0)    | -                                                               | -        |
| user_id        | User_id of the Ops member that performed the inspection | bigint                      | battery_swap.user_id                                            | -        |



# battery_inventory

|                    | definition                                                  | data_type                   | related_key                       | remark   |
|:-------------------|:------------------------------------------------------------|:----------------------------|:----------------------------------|:---------|
| battery_number     | Unique number attached to battery                           | character varying (192.0)   | battery_inspection.battery_number | -        |
| city               | Location of battery                                         | character varying (60.0)    | -                                 | -        |
| created_at         | Timestamp of when row was created                           | timestamp without time zone | -                                 | -        |
| electricity        | Current                                                     | double precision            | -                                 | -        |
| full_capacity      | Whether the battery is full                                 | smallint                    | -                                 | -        |
| id                 | Unique ID for the table                                     | bigint                      | -                                 | -        |
| remaining_capacity | Remaining battery power                                     | smallint                    | -                                 | -        |
| scooter_id         | Id of scooter that the battery is in                        | bigint                      | scooter_inventory.id              | -        |
| status             | Status of battery                                           | character varying (90.0)    | -                                 | -        |
| updated_at         | Timestamp of when row was updated                           | timestamp without time zone | -                                 | -        |
| work_cycle         | Refers to the number of times the battery have been charged | smallint                    | -                                 | -        |



# battery_status_record

|                 | definition                        | data_type                   | related_key                      | remark   |
|:----------------|:----------------------------------|:----------------------------|:---------------------------------|:---------|
| battery_no      | Unique number attached to battery | character varying (150.0)   | battery_inventory.battery_number | -        |
| city            | Location of battery swap          | character varying (96.0)    | -                                | -        |
| created_at      | Timestamp of when row was created | timestamp without time zone | -                                | -        |
| id              | Unique ID for the table           | bigint                      | -                                | -        |
| previous_city   | -                                 | character varying (96.0)    | -                                | -        |
| previous_status | Old status of battery             | character varying (150.0)   | -                                | -        |
| status          | New status of battery             | character varying (150.0)   | -                                | -        |



# battery_swap

|                   | definition                                          | data_type                   | related_key          | remark   |
|:------------------|:----------------------------------------------------|:----------------------------|:---------------------|:---------|
| battery_in_level  | Amount of battery left (new)                        | numeric                     | -                    | -        |
| battery_in_no     | New battery number                                  | character varying (192.0)   | -                    | -        |
| battery_out_level | Amount of battery left (the one that   was removed) | numeric                     | -                    | -        |
| battery_out_no    | Battery number that was removed                     | character varying (192.0)   | -                    | -        |
| battery_swap_time | Timestamp of battery swap operation                 | timestamp without time zone | -                    | -        |
| city              | Location of battery swap                            | character varying (96.0)    | -                    | -        |
| created_at        | Timestamp of when row was created                   | timestamp without time zone | -                    | -        |
| id                | Unique ID for the table                             | bigint                      | -                    | -        |
| latitude          | -                                                   | numeric                     | -                    | -        |
| longitude         | -                                                   | numeric                     | -                    | -        |
| scooter_id        | Unique ID of scooter                                | bigint                      | scooter_inventory.id | -        |
| user_id           | Unique ID of the user that performed swap           | bigint                      | user.id              | -        |
| vehicle_type      | Type of vehicle                                     | character varying (96.0)    | -                    | -        |



# battery_swap_history

|                 | definition                                              | data_type                   | related_key                | remark                                    |
|:----------------|:--------------------------------------------------------|:----------------------------|:---------------------------|:------------------------------------------|
| battery_no      | Unique number attached to the battery                   | character varying (192.0)   | -                          | -                                         |
| created_at      | Timestamp of when the row is created at                 | timestamp without time zone | -                          | -                                         |
| current_battery | Scooter battery after the swap is completed             | double precision            | -                          | new row inserted when a swap is performed |
| id              | Unique ID of the table                                  | bigint                      | -                          | -                                         |
| scooter_id      | Unique ID of the scooter                                | bigint                      | -                          | -                                         |
| status          | Status of swap results                                  | character varying (60.0)    | -                          | -                                         |
| updated_at      | Timestamp of when the row is updated at                 | timestamp without time zone | -                          | -                                         |
| user_id         | User_id of the Ops member that performed the inspection | bigint                      | battery_inspection.user_id | -                                         |



# blacklist

|            | definition                        | data_type                   | related_key   | remark   |
|:-----------|:----------------------------------|:----------------------------|:--------------|:---------|
| created_at | Timestamp of when row was created | timestamp without time zone | -             | -        |
| id         | Unique ID for the table           | bigint                      | -             | -        |
| ip         | Blocked IP address                | character varying (192.0)   | -             | -        |



# campaign

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| active          | -            | smallint                    | -             | -        |
| base_fee        | -            | double precision            | -             | -        |
| city            | -            | character varying (765.0)   | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| created_by      | -            | bigint                      | -             | -        |
| deleted         | -            | smallint                    | -             | -        |
| end_date        | -            | character varying (48.0)    | -             | -        |
| end_time        | -            | character varying (48.0)    | -             | -        |
| end_timestamp   | -            | timestamp without time zone | -             | -        |
| id              | -            | bigint                      | -             | -        |
| name            | -            | character varying (765.0)   | -             | -        |
| start_date      | -            | character varying (48.0)    | -             | -        |
| start_time      | -            | character varying (48.0)    | -             | -        |
| start_timestamp | -            | timestamp without time zone | -             | -        |
| type            | -            | character varying (48.0)    | -             | -        |
| unit_fee        | -            | double precision            | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |
| updated_by      | -            | bigint                      | -             | -        |
| vehicle_type    | -            | character varying (96.0)    | -             | -        |
| weekdays        | -            | character varying (48.0)    | -             | -        |



# campaign_pass

|         | definition   | data_type                 | related_key   | remark   |
|:--------|:-------------|:--------------------------|:--------------|:---------|
| id      | -            | bigint                    | -             | -        |
| pass_id | -            | bigint                    | -             | -        |
| price   | -            | double precision          | -             | -        |
| source  | -            | character varying (150.0) | -             | -        |



# campaign_trip

|          | definition   | data_type        | related_key   | remark   |
|:---------|:-------------|:-----------------|:--------------|:---------|
| base_fee | -            | double precision | -             | -        |
| id       | -            | bigint           | -             | -        |
| unit_fee | -            | double precision | -             | -        |



# cmd_task

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| cmd_list   | -            | character varying (64512.0) | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| deleted    | -            | smallint                    | -             | -        |
| id         | -            | bigint                      | -             | -        |
| rule       | -            | character varying (600.0)   | -             | -        |
| task_type  | -            | character varying (60.0)    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| valid_time | -            | timestamp without time zone | -             | -        |



# command

|            | definition                            | data_type                   | related_key   | remark   |
|:-----------|:--------------------------------------|:----------------------------|:--------------|:---------|
| content    | Content of the command to the scooter | character varying (600.0)   | -             | -        |
| created_at | Timestamp of when row was created     | timestamp without time zone | -             | -        |
| id         | Unique ID for the table               | bigint                      | -             | -        |
| name       | Name of the command                   | character varying (150.0)   | -             | -        |
| updated_at | Timestamp of when row was updated     | timestamp without time zone | -             | -        |



# command_log

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| command    | -            | character varying (3072.0)  | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| succeed    | -            | smallint                    | -             | -        |



# command_response

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| message    | -            | character varying (765.0)   | -             | -        |



# coupon_task_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| coupon_id  | -            | bigint                      | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | character varying (192.0)   | -             | -        |
| remark     | -            | character varying (3072.0)  | -             | -        |
| type       | -            | character varying (96.0)    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# deposit

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| amount     | -            | double precision            | -             | -        |
| charge_id  | -            | character varying (300.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| refunded   | -            | smallint                    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# device_blacklist

|            | definition                            | data_type                   | related_key                  | remark   |
|:-----------|:--------------------------------------|:----------------------------|:-----------------------------|:---------|
| comment    | Comment made when blacklisting        | character varying (768.0)   | -                            | -        |
| created_at | Timestamp of when the row was created | timestamp without time zone | -                            | -        |
| created_by | User ID that   created the row        | bigint                      | -                            | -        |
| device_id  | Unique ID of device                   | character varying (192.0)   | user_device_record.device_id | -        |
| id         | Unique ID of table                    | bigint                      | -                            | -        |
| updated_at | Timestamp of when the row was updated | timestamp without time zone | -                            | -        |
| updated_by | User ID that   updated the row        | bigint                      | -                            | -        |



# dft_quiz_result

|            | definition                            | data_type                   | related_key   | remark   |
|:-----------|:--------------------------------------|:----------------------------|:--------------|:---------|
| created_at | Timestamp of when the row was created | timestamp without time zone | -             | -        |
| id         | Unique ID of table                    | bigint                      | -             | -        |
| quiz_id    | Unique ID of quiz                     | smallint                    | -             | -        |
| response   | User's response to quiz               | character varying (300.0)   | -             | -        |
| trip_id    | Unique ID of trip                     | bigint                      | trip.id       | -        |
| user_id    | Unique ID of the user                 | bigint                      | user.id       | -        |



# email_verification

|            | definition                           | data_type                   | related_key            | remark                                                                                                |
|:-----------|:-------------------------------------|:----------------------------|:-----------------------|:------------------------------------------------------------------------------------------------------|
| created_at | Timestamp of when the row is created | timestamp without time zone | -                      | when the user clicks 'verify now' this will change as well (anti-pattern of usual created_at columns) |
| email      | User's email                         | character varying (765.0)   | neuron_user.user.email | -                                                                                                     |
| id         | Unique ID of table                   | bigint                      | -                      | -                                                                                                     |
| status     | verification status                  | character varying (60.0)    | -                      | -                                                                                                     |
| token      | Unique API token sent                | character varying (150.0)   | -                      | -                                                                                                     |
| updated_at | Timestamp of when the row is updated | timestamp without time zone | -                      | -                                                                                                     |
| user_id    | Unique ID of the user                | bigint                      | user.id                | -                                                                                                     |



# feedback

|            | definition                                                  | data_type                   | related_key   | remark                                                                                                                                |
|:-----------|:------------------------------------------------------------|:----------------------------|:--------------|:--------------------------------------------------------------------------------------------------------------------------------------|
| city       | Taken from user_country.city when the user creates a ticket | character varying (60.0)    | -             | -                                                                                                                                     |
| content    | Updated feedback content in JSON format                     | character varying (65535.0) | -             | Refer to [feedback_content](https://bitbucket.org/neuroncn/data-dictionary/src/master/JSON_extract/feedback_content.json) for extract |
| created_at | Timestamp of when row was created                           | timestamp without time zone | -             | -                                                                                                                                     |
| id         | Unique ID for the table                                     | bigint                      | -             | -                                                                                                                                     |
| status     | Deprecated column                                           | character varying (150.0)   | -             | -                                                                                                                                     |
| subject    | Categorisation of user feedback                             | character varying (150.0)   | -             | -                                                                                                                                     |
| trip_id    | Unique ID of trip                                           | bigint                      | trip.id       | -                                                                                                                                     |
| updated_at | Timestamp of when row was updated                           | timestamp without time zone | -             | -                                                                                                                                     |
| user_id    | Unique ID of the user that provided feedback                | bigint                      | user.id       | -                                                                                                                                     |



# flyway_schema_history

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| checksum       | -            | integer                     | -             | -        |
| description    | -            | character varying (600.0)   | -             | -        |
| execution_time | -            | integer                     | -             | -        |
| installed_by   | -            | character varying (300.0)   | -             | -        |
| installed_on   | -            | timestamp without time zone | -             | -        |
| installed_rank | -            | integer                     | -             | -        |
| script         | -            | character varying (3000.0)  | -             | -        |
| success        | -            | smallint                    | -             | -        |
| type           | -            | character varying (60.0)    | -             | -        |
| version        | -            | character varying (150.0)   | -             | -        |



# geofence

|                 | definition                                 | data_type                   | related_key                | remark                                                                       |
|:----------------|:-------------------------------------------|:----------------------------|:---------------------------|:-----------------------------------------------------------------------------|
| city            | Acronym of the city                        | character varying (60.0)    | -                          | -                                                                            |
| created_at      | Timestamp of when row was created          | timestamp without time zone | -                          | -                                                                            |
| description     | -                                          | character varying (65535.0) | -                          | -                                                                            |
| end_date        | -                                          | character varying (60.0)    | -                          | -                                                                            |
| end_time        | Time-based geofence end time               | character varying (30.0)    | -                          | Local Time                                                                   |
| hidden          | Active geofence hidden in user and ops app | smallint                    | -                          | [Geofence hidden logic](https://neuronmobility.atlassian.net/browse/NS-1503) |
| id              | Unique ID for the table                    | bigint                      | geofence_position.fence_id | -                                                                            |
| max_speed       | Maximum speed allowed in Geofence          | integer                     | -                          | -                                                                            |
| name            | Name of the Geofence location              | character varying (450.0)   | -                          | -                                                                            |
| sidewalk_action | -                                          | integer                     | -                          | -                                                                            |
| start_date      | -                                          | character varying (60.0)    | -                          | -                                                                            |
| start_time      | Time-based geofence start time             | character varying (30.0)    | -                          | Local Time                                                                   |
| station_id      | Station that geofence is located at        | bigint                      | station.id                 | -                                                                            |
| status          | Whether Geofence is currently in used      | character varying (60.0)    | -                          | Active = Geofence is in used                                                 |
| type            | Type of Geofence                           | character varying (96.0)    | -                          | -                                                                            |
| updated_at      | Timestamp of when row was updated          | timestamp without time zone | -                          | -                                                                            |
| vehicle_type    | -                                          | character varying (96.0)    | -                          | -                                                                            |
| weekdays        | Time-based geofence day                    | character varying (96.0)    | -                          | -                                                                            |
| zone            | Zone that geofence lies within             | character varying (60.0)    | zone.code                  | -                                                                            |



# geofence_alert

|                   | definition                       | data_type                   | related_key   | remark   |
|:------------------|:---------------------------------|:----------------------------|:--------------|:---------|
| created_at        | Timestamp when row was created   | timestamp without time zone | -             | -        |
| geo_trigger_event | -                                | character varying (300.0)   | -             | -        |
| geofence_id       | -                                | bigint                      | -             | -        |
| id                | Unique ID for the table          | bigint                      | -             | -        |
| imei              | Unique id of GPS device          | character varying (96.0)    | -             | -        |
| source            | -                                | character varying (30.0)    | -             | -        |
| status            | Status of alert                  | character varying (48.0)    | -             | -        |
| trip_id           | Trip id that triggered the alert | bigint                      | -             | -        |
| updated_at        | Timestamp when row was updated   | timestamp without time zone | -             | -        |
| user_id           | User id that received alert      | bigint                      | -             | -        |



# geofence_position

|           | definition                   | data_type                | related_key   | remark   |
|:----------|:-----------------------------|:-------------------------|:--------------|:---------|
| fence_id  | Unique ID of Geofence        | bigint                   | geofence.id   | -        |
| id        | Unique ID for the table      | numeric                  | -             | -        |
| latitude  | y-coordinate of the Geofence | numeric                  | -             | -        |
| longitude | x-coordinate of the Geofence | numeric                  | -             | -        |
| type      | Representation type          | character varying (48.0) | -             | -        |



# getui_user_client

|            | definition                        | data_type                   | related_key   | remark   |
|:-----------|:----------------------------------|:----------------------------|:--------------|:---------|
| client_id  | Unique client ID of user          | character varying (1536.0)  | -             | -        |
| created_at | Timestamp of when row was created | timestamp without time zone | -             | -        |
| id         | Unique ID for the table           | bigint                      | -             | -        |
| type       | app type                          | character varying (48.0)    | -             | -        |
| updated_at | Timestamp of when row was updated | timestamp without time zone | -             | -        |
| user_id    | Unique ID of user                 | bigint                      | user.id       | -        |



# gps_brake

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| brake      | -            | smallint                    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| gps_id     | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |



# gps_inventory

|                      | definition                                                                                                | data_type                   | related_key   | remark                                                                           |
|:---------------------|:----------------------------------------------------------------------------------------------------------|:----------------------------|:--------------|:---------------------------------------------------------------------------------|
| alarm_on             | Flag if alarm is turned on                                                                                | smallint                    | -             | 1 = yes; 0 = no                                                                  |
| apn                  | Access Point Name. Every telecommunication carrier has their only APN for people to access their network. | character varying (300.0)   | -             | -                                                                                |
| blt_password         | -                                                                                                         | character varying (384.0)   | -             | -                                                                                |
| bluetooth_version    | Bluetooth version number                                                                                  | character varying (384.0)   | -             | -                                                                                |
| bt_mac               | The mac id of the bluetooth, unique for each bt                                                           | character varying (96.0)    | -             | -                                                                                |
| created_at           | Timestamp of when row was created                                                                         | timestamp without time zone | -             | -                                                                                |
| dashboard_version    | Dashboard version number                                                                                  | character varying (384.0)   | -             | -                                                                                |
| dbu_hw               | -                                                                                                         | character varying (30.0)    | -             | -                                                                                |
| engine_off           | Flag if the last received lock/unlock command response is 'LED:0' or 'LED:1'.                             | smallint                    | -             | LED:0 means scooter engine is on. LED: 1 means scooter engine is off.            |
| gps_battery_level    | GPS internal battery level, range of 0 - 6                                                                | integer                     | -             | 6 = highest, 0 = lowest                                                          |
| gps_external_power   | Denotes if external power is on or off (connected to the external big battery)                            | smallint                    | -             | 1 = connected; 0 = not connected                                                 |
| hdop                 | -                                                                                                         | double precision            | -             | -                                                                                |
| helmet_attached      | Whether there is a helmet inserted into the helmet lock                                                   | smallint                    | -             | 1 = yes ; 0 = no                                                                 |
| iccid                | Unique ID of the SIM card inside the GPS module.                                                          | character varying (300.0)   | -             | -                                                                                |
| id                   | Unique ID for the table                                                                                   | bigint                      | -             | -                                                                                |
| imei                 | Unique imei ID for the GPS                                                                                | character varying (300.0)   | -             | -                                                                                |
| iot_version          | -                                                                                                         | character varying (150.0)   | -             | -                                                                                |
| is_connected         | -                                                                                                         | smallint                    | -             | -                                                                                |
| latitude             | y-coordinate of the scooter                                                                               | double precision            | -             | Updated by scooter-gps, user-phone-gps, operator-phone-gps, or station-location. |
| local_geo_status     | -                                                                                                         | character varying (64512.0) | -             | -                                                                                |
| local_geofence       | -                                                                                                         | smallint                    | -             | -                                                                                |
| location_source      | Indicates how the scooter location is last updated.                                                       | character varying (48.0)    | -             | -                                                                                |
| longitude            | x-coordinate of the scooter                                                                               | double precision            | -             | Updated by scooter-gps, user-phone-gps, operator-phone-gps, or station-location. |
| lpwr_flag            | -                                                                                                         | smallint                    | -             | -                                                                                |
| mode                 | -                                                                                                         | smallint                    | -             | -                                                                                |
| motor_version        | Motor version number                                                                                      | character varying (384.0)   | -             | -                                                                                |
| no_parking_light     | Indicates whether the scooter is in parking area in trip                                                  | smallint                    | -             | 1 = yes (scooter enter zone in trip) ; 0 = no (left zone)                        |
| normal_speed         | Allowed Mode 1 speed                                                                                      | smallint                    | -             | -                                                                                |
| position_active_time | Time when GPS is last active                                                                              | timestamp without time zone | -             | -                                                                                |
| protocol             | V1 = Old IOT, V2 = Current IOT, V3 = ebike                                                                | character varying (90.0)    | -             | -                                                                                |
| remaining_battery    | Amount of battery left, range of 0 - 1                                                                    | double precision            | -             | Big battery's level                                                              |
| remaining_range      | -                                                                                                         | numeric                     | -             | -                                                                                |
| ride_mile            | -                                                                                                         | bigint                      | -             | -                                                                                |
| sport_speed          | Allowed Mode 2 speed                                                                                      | smallint                    | -             | -                                                                                |
| status               | Inventory status of GPS device                                                                            | character varying (60.0)    | -             | -                                                                                |
| topple               | -                                                                                                         | smallint                    | -             | -                                                                                |
| updated_at           | Timestamp of when row was updated                                                                         | timestamp without time zone | -             | -                                                                                |
| version              | Version of GPS device                                                                                     | character varying (765.0)   | -             | -                                                                                |
| version_list         | -                                                                                                         | character varying (64512.0) | -             | -                                                                                |
| vin                  | -                                                                                                         | double precision            | -             | -                                                                                |
| voltage              | Voltage of the external power, used to estimate external power                                            | numeric                     | -             | -                                                                                |
| wheel_size           | -                                                                                                         | smallint                    | -             | -                                                                                |
| white_noise          | -                                                                                                         | smallint                    | -             | -                                                                                |
| wrench_light         | Indicates when there is a manual issue (as reported by ops)                                               | smallint                    | -             | 1 = yes ; 0 = no/solved                                                          |



# gps_region

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| country    | -            | character varying (48.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| imei       | -            | character varying (105.0)   | -             | -        |



# gps_self_check

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| 4gstat       | -            | smallint                    | -             | -        |
| accelfault   | -            | smallint                    | -             | -        |
| baro         | -            | integer                     | -             | -        |
| bat_vs       | -            | smallint                    | -             | -        |
| batstat      | -            | smallint                    | -             | -        |
| batulck      | -            | smallint                    | -             | -        |
| brakefault   | -            | smallint                    | -             | -        |
| btu_sw       | -            | character varying (150.0)   | -             | -        |
| btu_vs       | -            | smallint                    | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| ctle         | -            | smallint                    | -             | -        |
| cyc          | -            | integer                     | -             | -        |
| dbstat       | -            | smallint                    | -             | -        |
| dbu_vs       | -            | smallint                    | -             | -        |
| description  | -            | character varying (768.0)   | -             | -        |
| fall         | -            | smallint                    | -             | -        |
| gps_id       | -            | bigint                      | -             | -        |
| gps_protocol | -            | character varying (24.0)    | -             | -        |
| hlu_vs       | -            | smallint                    | -             | -        |
| id           | -            | bigint                      | -             | -        |
| imei         | -            | character varying (96.0)    | -             | -        |
| iot_sw       | -            | character varying (150.0)   | -             | -        |
| mode         | -            | smallint                    | -             | -        |
| mpu          | -            | smallint                    | -             | -        |
| pitch        | -            | integer                     | -             | -        |
| roll         | -            | integer                     | -             | -        |
| rssi         | -            | integer                     | -             | -        |
| scu_vs       | -            | smallint                    | -             | -        |
| soc          | -            | smallint                    | -             | -        |
| status       | -            | character varying (96.0)    | -             | -        |
| sv_unum      | -            | smallint                    | -             | -        |
| temp         | -            | smallint                    | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| v18          | -            | integer                     | -             | -        |
| v50          | -            | integer                     | -             | -        |
| v90          | -            | integer                     | -             | -        |
| vbat         | -            | integer                     | -             | -        |
| vin          | -            | integer                     | -             | -        |
| yaw          | -            | integer                     | -             | -        |



# helmet_unlock_record

|                 | definition                             | data_type                   | related_key   | remark          |
|:----------------|:---------------------------------------|:----------------------------|:--------------|:----------------|
| created_at      | Timestamp of when row was created      | timestamp without time zone | -             | -               |
| helmet_attached | Whether helmet is attached to scooter  | smallint                    | -             | yes = 1; no = 0 |
| id              | Unique ID for the table                | bigint                      | -             | -               |
| result          | Result of unlock command               | smallint                    | -             | yes = 1; no =0  |
| scooter_online  | Whether scooter is online (ping <5min) | smallint                    | -             | yes = 1; no = 0 |
| stage           | Stage of Trip                          | character varying (60.0)    | -             | -               |
| trip_id         | Unique ID of trip                      | bigint                      | trip.id       | -               |
| unlock_channel  | Channel used to unlock                 | character varying (30.0)    | -             | -               |
| updated_at      | Timestamp of when row was updated      | timestamp without time zone | -             | -               |
| user_id         | Unique ID of user                      | bigint                      | user.id       | -               |



# ibeacon

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| major      | -            | character varying (48.0)    | -             | -        |
| minor      | -            | character varying (48.0)    | -             | -        |
| station_id | -            | bigint                      | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| uuid       | -            | character varying (150.0)   | -             | -        |



# incident_log

|               | definition                                | data_type                   | related_key   | remark   |
|:--------------|:------------------------------------------|:----------------------------|:--------------|:---------|
| aware_time    | Timestamp of when incident was made aware | timestamp without time zone | -             | -        |
| city          | City of shop                              | character varying (90.0)    | -             | -        |
| content       | Details of the incident                   | character varying (65535.0) | -             | -        |
| created_at    | Timestamp of when the row was   created   | timestamp without time zone | -             | -        |
| created_by    | User ID that created the row              | bigint                      | -             | -        |
| id            | Unique ID of table                        | bigint                      | -             | -        |
| incident_time | Timestamp of when the incident happened   | timestamp without time zone | -             | -        |
| risk_level    | Risk level of incident                    | character varying (90.0)    | -             | -        |
| status        | Status of incident                        | character varying (90.0)    | -             | -        |
| updated_at    | Timestamp of when the row was   updated   | timestamp without time zone | -             | -        |
| updated_by    | User ID that updated the row              | bigint                      | -             | -        |



# incident_log_image

|            | definition                            | data_type                   | related_key   | remark          |
|:-----------|:--------------------------------------|:----------------------------|:--------------|:----------------|
| created_at | Timestamp of when the row was created | timestamp without time zone | -             | -               |
| id         | Unique ID of table                    | bigint                      | -             | -               |
| image_id   | Unique ID of image                    | character varying (150.0)   | -             | neuron_file.id  |
| log_id     | Unique ID for the incident            | bigint                      | -             | incident_log.id |
| type       | Image type                            | character varying (90.0)    | -             | -               |



# incident_mechanic_report

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| comment           | -            | character varying (65535.0) | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| created_by        | -            | bigint                      | -             | -        |
| created_user      | -            | character varying (768.0)   | -             | -        |
| created_user_role | -            | character varying (768.0)   | -             | -        |
| deck_id           | -            | character varying (300.0)   | -             | -        |
| id                | -            | bigint                      | -             | -        |
| report_id         | -            | bigint                      | -             | -        |
| updated_at        | -            | timestamp without time zone | -             | -        |
| updated_by        | -            | bigint                      | -             | -        |
| updated_user      | -            | character varying (768.0)   | -             | -        |
| updated_user_role | -            | character varying (768.0)   | -             | -        |



# incident_report

|                      | definition   | data_type                   | related_key   | remark   |
|:---------------------|:-------------|:----------------------------|:--------------|:---------|
| aware_time           | -            | timestamp without time zone | -             | -        |
| business_risk_level  | -            | character varying (300.0)   | -             | -        |
| category             | -            | character varying (90.0)    | -             | -        |
| city                 | -            | character varying (90.0)    | -             | -        |
| created_at           | -            | timestamp without time zone | -             | -        |
| created_by           | -            | bigint                      | -             | -        |
| created_user         | -            | character varying (768.0)   | -             | -        |
| created_user_role    | -            | character varying (768.0)   | -             | -        |
| description          | -            | character varying (65535.0) | -             | -        |
| edm_sent             | -            | smallint                    | -             | -        |
| id                   | -            | bigint                      | -             | -        |
| incident_time        | -            | timestamp without time zone | -             | -        |
| latitude             | -            | numeric                     | -             | -        |
| location             | -            | character varying (768.0)   | -             | -        |
| longitude            | -            | numeric                     | -             | -        |
| parties              | -            | character varying (65535.0) | -             | -        |
| platform             | -            | character varying (300.0)   | -             | -        |
| police               | -            | smallint                    | -             | -        |
| reason               | -            | character varying (765.0)   | -             | -        |
| remarks              | -            | character varying (768.0)   | -             | -        |
| risk_level           | -            | character varying (90.0)    | -             | -        |
| short_description    | -            | character varying (3000.0)  | -             | -        |
| source               | -            | character varying (384.0)   | -             | -        |
| status               | -            | character varying (90.0)    | -             | -        |
| third_party_involved | -            | character varying (300.0)   | -             | -        |
| ticket_url           | -            | character varying (768.0)   | -             | -        |
| type                 | -            | character varying (90.0)    | -             | -        |
| updated_at           | -            | timestamp without time zone | -             | -        |
| updated_by           | -            | bigint                      | -             | -        |
| updated_user         | -            | character varying (768.0)   | -             | -        |
| updated_user_role    | -            | character varying (768.0)   | -             | -        |
| user_involved        | -            | character varying (300.0)   | -             | -        |
| users                | -            | character varying (65535.0) | -             | -        |
| vehicle_property     | -            | character varying (65535.0) | -             | -        |
| version              | -            | integer                     | -             | -        |



# incident_report_history

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at        | -            | timestamp without time zone | -             | -        |
| created_by        | -            | bigint                      | -             | -        |
| created_user      | -            | character varying (768.0)   | -             | -        |
| created_user_role | -            | character varying (768.0)   | -             | -        |
| current           | -            | character varying (65535.0) | -             | -        |
| id                | -            | bigint                      | -             | -        |
| platform          | -            | character varying (300.0)   | -             | -        |
| previous          | -            | character varying (65535.0) | -             | -        |
| report_id         | -            | bigint                      | -             | -        |
| source            | -            | character varying (384.0)   | -             | -        |
| updated_fields    | -            | character varying (1500.0)  | -             | -        |
| version           | -            | integer                     | -             | -        |



# incident_report_image

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| image_id   | -            | character varying (150.0)   | -             | -        |
| report_id  | -            | bigint                      | -             | -        |
| type       | -            | character varying (90.0)    | -             | -        |
| version    | -            | integer                     | -             | -        |



# incident_report_snapshot

|                      | definition   | data_type                   | related_key   | remark   |
|:---------------------|:-------------|:----------------------------|:--------------|:---------|
| aware_time           | -            | timestamp without time zone | -             | -        |
| business_risk_level  | -            | character varying (300.0)   | -             | -        |
| city                 | -            | character varying (90.0)    | -             | -        |
| created_at           | -            | timestamp without time zone | -             | -        |
| created_by           | -            | bigint                      | -             | -        |
| created_user         | -            | character varying (768.0)   | -             | -        |
| created_user_role    | -            | character varying (768.0)   | -             | -        |
| description          | -            | character varying (65535.0) | -             | -        |
| id                   | -            | bigint                      | -             | -        |
| incident_time        | -            | timestamp without time zone | -             | -        |
| location             | -            | character varying (768.0)   | -             | -        |
| parties              | -            | character varying (65535.0) | -             | -        |
| platform             | -            | character varying (300.0)   | -             | -        |
| police               | -            | smallint                    | -             | -        |
| reason               | -            | character varying (765.0)   | -             | -        |
| remarks              | -            | character varying (768.0)   | -             | -        |
| report_id            | -            | bigint                      | -             | -        |
| risk_level           | -            | character varying (90.0)    | -             | -        |
| short_description    | -            | character varying (3000.0)  | -             | -        |
| source               | -            | character varying (384.0)   | -             | -        |
| status               | -            | character varying (90.0)    | -             | -        |
| third_party_involved | -            | character varying (300.0)   | -             | -        |
| ticket_url           | -            | character varying (768.0)   | -             | -        |
| type                 | -            | character varying (90.0)    | -             | -        |
| user_involved        | -            | character varying (300.0)   | -             | -        |
| users                | -            | character varying (65535.0) | -             | -        |
| vehicle_property     | -            | character varying (65535.0) | -             | -        |
| version              | -            | integer                     | -             | -        |



# incident_report_timeline

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| content           | -            | character varying (65535.0) | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| created_by        | -            | bigint                      | -             | -        |
| created_user      | -            | character varying (768.0)   | -             | -        |
| created_user_role | -            | character varying (768.0)   | -             | -        |
| email_address     | -            | character varying (1500.0)  | -             | -        |
| email_template    | -            | character varying (384.0)   | -             | -        |
| id                | -            | bigint                      | -             | -        |
| local_time        | -            | smallint                    | -             | -        |
| report_id         | -            | bigint                      | -             | -        |
| touch_time        | -            | timestamp without time zone | -             | -        |
| type              | -            | character varying (150.0)   | -             | -        |
| updated           | -            | smallint                    | -             | -        |



# issue_subscribe_operator

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| created_at  | -            | timestamp without time zone | -             | -        |
| created_by  | -            | bigint                      | -             | -        |
| id          | -            | bigint                      | -             | -        |
| important   | -            | smallint                    | -             | -        |
| issue_id    | -            | bigint                      | -             | -        |
| mute        | -            | smallint                    | -             | -        |
| operator_id | -            | bigint                      | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| updated_by  | -            | bigint                      | -             | -        |



# location_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | character varying (1536.0)  | -             | -        |
| id         | -            | bigint                      | -             | -        |
| latitude   | -            | double precision            | -             | -        |
| longitude  | -            | double precision            | -             | -        |
| type       | -            | character varying (150.0)   | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# lock_response

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| card_no    | -            | character varying (300.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| locked     | -            | smallint                    | -             | -        |
| succeed    | -            | smallint                    | -             | -        |



# maintenance_work

|       | definition   | data_type                 | related_key                    | remark   |
|:------|:-------------|:--------------------------|:-------------------------------|:---------|
| id    | unique id    | integer                   | group_id.maintenance_work_item | -        |
| title | Group Type   | character varying (765.0) | -                              | -        |



# maintenance_work_item

|          | definition                                     | data_type                 | related_key                                  | remark          |
|:---------|:-----------------------------------------------|:--------------------------|:---------------------------------------------|:----------------|
| deleted  | whether the item has been removed from records | smallint                  | -                                            | yes = 1; no = 1 |
| group_id | maintenance work group                         | integer                   | id.maintenance_work                          | -               |
| id       | unique id                                      | integer                   | performed_item_id.maintenance_work_performed | -               |
| title    | Group Type                                     | character varying (765.0) | -                                            | -               |



# maintenance_work_performed

|                   | definition                               | data_type                   | related_key              | remark   |
|:------------------|:-----------------------------------------|:----------------------------|:-------------------------|:---------|
| action            | Status of work performed                 | character varying (765.0)   | -                        | -        |
| comment           | -                                        | character varying (6000.0)  | -                        | -        |
| created_at        | Timestamp of when the row was created    | timestamp without time zone | -                        | -        |
| created_by        | User Id of member who performed the work | bigint                      | -                        | -        |
| id                | Unique id of the table                   | bigint                      | -                        | -        |
| maintenance_id    | Unique id of maintenance job             | bigint                      | scooter_maintenance.id   | -        |
| performed_item_id | Item id                                  | integer                     | maintenance_work_item.id | -        |
| quantity          | -                                        | smallint                    | -                        | -        |
| updated_at        | Timestamp of when the row was updated    | timestamp without time zone | -                        | -        |
| updated_by        | User Id of member who updated (If any)   | bigint                      | -                        | -        |



# mechanic_report_image

|                    | definition   | data_type                   | related_key   | remark   |
|:-------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at         | -            | timestamp without time zone | -             | -        |
| id                 | -            | bigint                      | -             | -        |
| image_id           | -            | character varying (150.0)   | -             | -        |
| mechanic_report_id | -            | bigint                      | -             | -        |



# mobile_verification

|              | definition                                           | data_type                   | related_key   | remark   |
|:-------------|:-----------------------------------------------------|:----------------------------|:--------------|:---------|
| country_code | Country code at which verification is sent to mobile | character varying (48.0)    | -             | -        |
| created_at   | Timestamp of when row was created                    | timestamp without time zone | -             | -        |
| mobile       | Mobile number                                        | character varying (150.0)   | -             | -        |
| remote_ip    | Remote IP address of which verification is sent to   | character varying (192.0)   | -             | -        |
| sms_code     | Unique SMS codes                                     | character varying (30.0)    | -             | -        |
| updated_at   | Timestamp of when row was updated                    | timestamp without time zone | -             | -        |



# mqtt_btu

|                            | definition   | data_type                   | related_key   | remark   |
|:---------------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at                 | -            | timestamp without time zone | -             | -        |
| enabled                    | -            | smallint                    | -             | -        |
| gps_id                     | -            | bigint                      | -             | -        |
| hw                         | -            | character varying (96.0)    | -             | -        |
| id                         | -            | bigint                      | -             | -        |
| receive_failure_bytes      | -            | integer                     | -             | -        |
| receive_send_total_failure | -            | integer                     | -             | -        |
| receive_success_bytes      | -            | integer                     | -             | -        |
| send_failure_bytes         | -            | integer                     | -             | -        |
| send_success_bytes         | -            | integer                     | -             | -        |
| sn                         | -            | character varying (96.0)    | -             | -        |
| sw                         | -            | character varying (96.0)    | -             | -        |
| valid_count                | -            | integer                     | -             | -        |
| valid_status               | -            | smallint                    | -             | -        |
| working_status             | -            | smallint                    | -             | -        |



# mqtt_dbu

|                            | definition   | data_type                   | related_key   | remark   |
|:---------------------------|:-------------|:----------------------------|:--------------|:---------|
| accelerate                 | -            | smallint                    | -             | -        |
| bell_on                    | -            | smallint                    | -             | -        |
| braking                    | -            | smallint                    | -             | -        |
| created_at                 | -            | timestamp without time zone | -             | -        |
| gear                       | -            | smallint                    | -             | -        |
| gps_id                     | -            | bigint                      | -             | -        |
| headlight_on               | -            | smallint                    | -             | -        |
| hw                         | -            | character varying (96.0)    | -             | -        |
| id                         | -            | bigint                      | -             | -        |
| receive_failure_bytes      | -            | integer                     | -             | -        |
| receive_send_total_failure | -            | integer                     | -             | -        |
| receive_success_bytes      | -            | integer                     | -             | -        |
| screen_on                  | -            | smallint                    | -             | -        |
| send_failure_bytes         | -            | integer                     | -             | -        |
| send_success_bytes         | -            | integer                     | -             | -        |
| sn                         | -            | character varying (96.0)    | -             | -        |
| sw                         | -            | character varying (96.0)    | -             | -        |
| working_status             | -            | smallint                    | -             | -        |



# mqtt_hlu

|                            | definition   | data_type                   | related_key   | remark   |
|:---------------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at                 | -            | timestamp without time zone | -             | -        |
| gps_id                     | -            | bigint                      | -             | -        |
| helmet_locked              | -            | smallint                    | -             | -        |
| hw                         | -            | character varying (96.0)    | -             | -        |
| id                         | -            | bigint                      | -             | -        |
| physical_lock              | -            | smallint                    | -             | -        |
| receive_failure_bytes      | -            | integer                     | -             | -        |
| receive_send_total_failure | -            | integer                     | -             | -        |
| receive_success_bytes      | -            | integer                     | -             | -        |
| send_failure_bytes         | -            | integer                     | -             | -        |
| send_success_bytes         | -            | integer                     | -             | -        |
| sn                         | -            | character varying (96.0)    | -             | -        |
| sw                         | -            | character varying (96.0)    | -             | -        |
| working_status             | -            | smallint                    | -             | -        |



# mqtt_iot

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| baro              | -            | integer                     | -             | -        |
| bat_vc            | -            | smallint                    | -             | -        |
| bat_vs            | -            | smallint                    | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| dangerous_driving | -            | smallint                    | -             | -        |
| dbu_vc            | -            | smallint                    | -             | -        |
| dbu_vs            | -            | smallint                    | -             | -        |
| flow              | -            | integer                     | -             | -        |
| gps_id            | -            | bigint                      | -             | -        |
| hlu_vc            | -            | smallint                    | -             | -        |
| hlu_vs            | -            | smallint                    | -             | -        |
| hw                | -            | character varying (96.0)    | -             | -        |
| id                | -            | bigint                      | -             | -        |
| low_power_mode    | -            | smallint                    | -             | -        |
| lte_vc            | -            | smallint                    | -             | -        |
| lte_vs            | -            | smallint                    | -             | -        |
| mpu_status        | -            | smallint                    | -             | -        |
| ride_mile         | -            | integer                     | -             | -        |
| scu_vc            | -            | smallint                    | -             | -        |
| scu_vs            | -            | smallint                    | -             | -        |
| sn                | -            | character varying (96.0)    | -             | -        |
| sw                | -            | character varying (96.0)    | -             | -        |
| temperature       | -            | numeric                     | -             | -        |
| topple            | -            | smallint                    | -             | -        |
| total_mile        | -            | integer                     | -             | -        |
| uptime            | -            | integer                     | -             | -        |
| v18               | -            | integer                     | -             | -        |
| v50               | -            | integer                     | -             | -        |
| v90               | -            | integer                     | -             | -        |
| vbat              | -            | integer                     | -             | -        |
| vin               | -            | integer                     | -             | -        |
| water_leak        | -            | smallint                    | -             | -        |
| working_status    | -            | smallint                    | -             | -        |



# mqtt_lte

|                               | definition   | data_type                   | related_key   | remark   |
|:------------------------------|:-------------|:----------------------------|:--------------|:---------|
| ber                           | -            | smallint                    | -             | -        |
| brand                         | -            | character varying (192.0)   | -             | -        |
| chan                          | -            | character varying (192.0)   | -             | -        |
| channel                       | -            | character varying (192.0)   | -             | -        |
| connection_count              | -            | integer                     | -             | -        |
| created_at                    | -            | timestamp without time zone | -             | -        |
| dial_count                    | -            | integer                     | -             | -        |
| gps_id                        | -            | bigint                      | -             | -        |
| hw                            | -            | character varying (96.0)    | -             | -        |
| iccid                         | -            | character varying (96.0)    | -             | -        |
| id                            | -            | bigint                      | -             | -        |
| imsi                          | -            | character varying (192.0)   | -             | -        |
| join_count                    | -            | integer                     | -             | -        |
| join_status                   | -            | smallint                    | -             | -        |
| network_receive_failure_count | -            | integer                     | -             | -        |
| network_receive_success_count | -            | integer                     | -             | -        |
| network_send_failure_count    | -            | integer                     | -             | -        |
| network_send_success_count    | -            | integer                     | -             | -        |
| online_count                  | -            | integer                     | -             | -        |
| operator                      | -            | character varying (192.0)   | -             | -        |
| receive_failure_bytes         | -            | integer                     | -             | -        |
| receive_send_total_failure    | -            | integer                     | -             | -        |
| receive_success_bytes         | -            | integer                     | -             | -        |
| reset_count                   | -            | integer                     | -             | -        |
| rssi                          | -            | smallint                    | -             | -        |
| send_failure_bytes            | -            | integer                     | -             | -        |
| send_success_bytes            | -            | integer                     | -             | -        |
| sn                            | -            | character varying (96.0)    | -             | -        |
| sw                            | -            | character varying (96.0)    | -             | -        |
| tech                          | -            | character varying (192.0)   | -             | -        |
| working_status                | -            | smallint                    | -             | -        |



# mqtt_mpu

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| accelerate1 | -            | numeric                     | -             | -        |
| accelerate2 | -            | numeric                     | -             | -        |
| accelerate3 | -            | numeric                     | -             | -        |
| created_at  | -            | timestamp without time zone | -             | -        |
| gps_id      | -            | bigint                      | -             | -        |
| gyro1       | -            | numeric                     | -             | -        |
| gyro2       | -            | numeric                     | -             | -        |
| gyro3       | -            | numeric                     | -             | -        |
| id          | -            | bigint                      | -             | -        |
| pitch_angle | -            | numeric                     | -             | -        |
| roll_angle  | -            | numeric                     | -             | -        |
| ts          | -            | integer                     | -             | -        |
| yaw_angle   | -            | numeric                     | -             | -        |



# mqtt_position

|                  | definition   | data_type                   | related_key   | remark   |
|:-----------------|:-------------|:----------------------------|:--------------|:---------|
| altitude         | -            | numeric                     | -             | -        |
| course           | -            | numeric                     | -             | -        |
| created_at       | -            | timestamp without time zone | -             | -        |
| device_time      | -            | timestamp without time zone | -             | -        |
| gps_id           | -            | bigint                      | -             | -        |
| hdop             | -            | numeric                     | -             | -        |
| id               | -            | bigint                      | -             | -        |
| inview_st_signal | -            | character varying (384.0)   | -             | -        |
| inview_st_sn     | -            | character varying (384.0)   | -             | -        |
| latitude         | -            | numeric                     | -             | -        |
| longitude        | -            | numeric                     | -             | -        |
| mode             | -            | smallint                    | -             | -        |
| pdop             | -            | numeric                     | -             | -        |
| speed            | -            | numeric                     | -             | -        |
| status           | -            | smallint                    | -             | -        |
| using_st_num     | -            | smallint                    | -             | -        |
| vdop             | -            | numeric                     | -             | -        |
| visible_st_num   | -            | smallint                    | -             | -        |
| work_status      | -            | smallint                    | -             | -        |



# mqtt_scu

|                            | definition   | data_type                   | related_key   | remark   |
|:---------------------------|:-------------|:----------------------------|:--------------|:---------|
| battery_lock_failure       | -            | smallint                    | -             | -        |
| battery_lock_unlocked      | -            | smallint                    | -             | -        |
| battery_network_failure    | -            | smallint                    | -             | -        |
| controller_failure         | -            | smallint                    | -             | -        |
| created_at                 | -            | timestamp without time zone | -             | -        |
| current                    | -            | integer                     | -             | -        |
| current_limit              | -            | integer                     | -             | -        |
| gear                       | -            | smallint                    | -             | -        |
| gps_id                     | -            | bigint                      | -             | -        |
| hw                         | -            | character varying (96.0)    | -             | -        |
| id                         | -            | bigint                      | -             | -        |
| low_voltage                | -            | smallint                    | -             | -        |
| mhall_failure              | -            | smallint                    | -             | -        |
| mploss_failure             | -            | smallint                    | -             | -        |
| mstall_failure             | -            | smallint                    | -             | -        |
| over_current               | -            | smallint                    | -             | -        |
| receive_failure_bytes      | -            | integer                     | -             | -        |
| receive_send_total_failure | -            | integer                     | -             | -        |
| receive_success_bytes      | -            | integer                     | -             | -        |
| send_failure_bytes         | -            | integer                     | -             | -        |
| send_success_bytes         | -            | integer                     | -             | -        |
| sn                         | -            | character varying (96.0)    | -             | -        |
| speed                      | -            | numeric                     | -             | -        |
| speed_limit1               | -            | integer                     | -             | -        |
| speed_limit2               | -            | integer                     | -             | -        |
| sw                         | -            | character varying (96.0)    | -             | -        |
| unlocked                   | -            | smallint                    | -             | -        |
| voltage                    | -            | integer                     | -             | -        |
| working_status             | -            | smallint                    | -             | -        |



# n3_status

|                         | definition                                                                         | data_type                   | related_key      | remark                                                                                                                 |
|:------------------------|:-----------------------------------------------------------------------------------|:----------------------------|:-----------------|:-----------------------------------------------------------------------------------------------------------------------|
| a_phase_current         | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| accelerator             | The definition of these values are based on the IOT documentation from myway team. | smallint                    | -                | -                                                                                                                      |
| b_phase_current         | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| battery_percentage      | Scooter battery percentage                                                         | double precision            | -                | Refer to [logic](https://neuronmobility.atlassian.net/wiki/spaces/IW/pages/55083009/Scooter+Battery+Calculation+Logic) |
| ble_communication       | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| bms_communication       | However, they are merely saved and have yet to be strictly tested or used.         | smallint                    | -                | -                                                                                                                      |
| brake                   | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| c_phase_current         | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| created_at              | Timestamp of update                                                                | timestamp without time zone | -                | -                                                                                                                      |
| dashboard_communication | They are flagged when there is an error. (yes = 1; no = 1)                         | smallint                    | -                | -                                                                                                                      |
| dc_current              | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| device_id               | Unique GPS device ID                                                               | bigint                      | gps_inventory.id | -                                                                                                                      |
| electric_hall           | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| electricity             | Scooter electricity                                                                | double precision            | -                | -                                                                                                                      |
| helmet_attached         | Whether a helmet is attached to the scooter                                        | smallint                    | -                | Null (default): scooter did not report, yes = 1; no = 0                                                                |
| id                      | Unique ID for the table                                                            | bigint                      | -                | -                                                                                                                      |
| iot_communication       | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| lock_status             | Inaccurate (Bug for firmware below 1200)                                           | character varying (96.0)    | -                | locked = scooter engine is off; unlocked = scooter engine is on                                                        |
| low_battery             | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| low_battery_protection  | -                                                                                  | smallint                    | -                | -                                                                                                                      |
| single_range            | Deprecated                                                                         | double precision            | -                | value is inaccurate                                                                                                    |
| speed                   | m/s                                                                                | double precision            | -                | calculated based on the rotation of the scooter's wheel                                                                |
| topple                  | Flag when scooter topple                                                           | smallint                    | -                | yes = 1; no = 0                                                                                                        |
| voltage                 | Scooter voltage                                                                    | double precision            | -                | -                                                                                                                      |



# neuron_file

|            | definition                                                   | data_type                   | related_key                                        | remark                     |
|:-----------|:-------------------------------------------------------------|:----------------------------|:---------------------------------------------------|:---------------------------|
| audited    | -                                                            | smallint                    | -                                                  | -                          |
| bucket     | Bucket used to store file                                    | character varying (150.0)   | -                                                  | -                          |
| confidence | Level of confidence (recognition)                            | double precision            | -                                                  | -                          |
| created_at | Timestamp of when row was created                            | timestamp without time zone | -                                                  | -                          |
| id         | Unique ID for the table                                      | character varying (300.0)   | trip_helmet.image_id ; trip_finish_source.image_id | -                          |
| md5        | md5 hash                                                     | character varying (300.0)   | -                                                  | -                          |
| name       | Name of file                                                 | character varying (765.0)   | -                                                  | -                          |
| recognized | Whether it is recognized by AWS object recognization service | smallint                    | -                                                  | 1 = yes, 0 = no            |
| type       | Type of Image                                                | character varying (60.0)    | -                                                  | -                          |
| updated_at | Timestamp of when row was updated                            | timestamp without time zone | -                                                  | -                          |
| url        | URL of file                                                  | character varying (1500.0)  | -                                                  | full url: url + "/" + name |



# neuron_order

|               | definition                                                                                                                   | data_type                   | related_key                                                    | remark   |
|:--------------|:-----------------------------------------------------------------------------------------------------------------------------|:----------------------------|:---------------------------------------------------------------|:---------|
| amount        | Amount paid for order                                                                                                        | numeric                     | -                                                              | -        |
| city_base_fee | Base fee pricing of city                                                                                                     | numeric                     | -                                                              | -        |
| city_unit_fee | Additional fee per unit time (minute)                                                                                        | numeric                     | -                                                              | -        |
| created_at    | Timestamp of when row was created                                                                                            | timestamp without time zone | -                                                              | -        |
| currency      | Type of currency used                                                                                                        | character varying (48.0)    | -                                                              | -        |
| id            | Unique ID of the order                                                                                                       | bigint                      | order_detail.id ; receipt.source ; neuron_transaction.order_id | -        |
| pass_id       | Unique ID of type of pass used                                                                                               | bigint                      | pass.id                                                        | -        |
| status        | Status of order payment                                                                                                      | character varying (96.0)    | -                                                              | -        |
| trip_id       | Unique ID of the trip                                                                                                        | bigint                      | trip.id                                                        | -        |
| updated_at    | Timestamp of when row was updated                                                                                            | timestamp without time zone | -                                                              | -        |
| updated_by    | Unique ID of user (admin)                                                                                                    | bigint                      | user.id                                                        | -        |
| user_id       | Unique ID of user                                                                                                            | bigint                      | user.id                                                        | -        |
| version       | To ensure that the row was not updated by two different clients at the same time, thereby enabling stronger data consistency | integer                     | -                                                              | -        |



# neuron_sync_queue

|                     | definition   | data_type                   | related_key   | remark   |
|:--------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at          | -            | timestamp without time zone | -             | -        |
| id                  | -            | numeric                     | -             | -        |
| json_body           | -            | character varying (3000.0)  | -             | -        |
| original_created_at | -            | timestamp without time zone | -             | -        |
| original_id         | -            | bigint                      | -             | -        |
| succeed             | -            | smallint                    | -             | -        |
| type                | -            | character varying (150.0)   | -             | -        |
| updated_at          | -            | timestamp without time zone | -             | -        |



# neuron_sync_scooter_zego

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at      | -            | timestamp without time zone | -             | -        |
| id              | -            | numeric                     | -             | -        |
| scooter_id      | -            | bigint                      | -             | -        |
| zego_vehicle_id | -            | bigint                      | -             | -        |



# neuron_transaction

|                  | definition                                                            | data_type                   | related_key           | remark                                               |
|:-----------------|:----------------------------------------------------------------------|:----------------------------|:----------------------|:-----------------------------------------------------|
| account          | Country of account                                                    | character varying (96.0)    | -                     | -                                                    |
| amount           | Amount of transaction                                                 | numeric                     | -                     | -                                                    |
| automatic        | States if deduction was automatic                                     | smallint                    | -                     | yes = 1; no = 0                                      |
| brand            | Card brand                                                            | character varying (96.0)    | -                     | -                                                    |
| city             | City of user where transaction occured                                | character varying (96.0)    | -                     | -                                                    |
| client_ip        | User’s mobile IP when user make the purchase/generate the transaction | character varying (96.0)    | -                     | From user's device, null if it's from Stripe webhook |
| created_at       | Timestamp of when row was created                                     | timestamp without time zone | -                     | -                                                    |
| currency         | Type of currency used                                                 | character varying (48.0)    | -                     | -                                                    |
| dispute_amount   | Disputed amount                                                       | double precision            | -                     | -                                                    |
| dispute_status   | Payment dispute                                                       | character varying (96.0)    | -                     | -                                                    |
| id               | Unique ID for the table                                               | bigint                      | -                     | -                                                    |
| last4            | Last 4 digits of card number                                          | character varying (12.0)    | -                     | -                                                    |
| method           | Method of transaction                                                 | character varying (192.0)   | -                     | -                                                    |
| my_pass_id       | DEPRECATED                                                            | bigint                      | -                     | -                                                    |
| order_id         | Unique ID of individual order                                         | bigint                      | order_detail.order_id | -                                                    |
| platform         | Type of platform used                                                 | character varying (96.0)    | -                     | -                                                    |
| stripe_charge_id | Unique ID for Stripe charge                                           | character varying (300.0)   | Stripe Charge table   | -                                                    |
| stripe_refund_id | Unique refund ID for Stripe user                                      | character varying (300.0)   | -                     | -                                                    |
| trip_deposit_id  | -                                                                     | bigint                      | -                     | -                                                    |
| type             | Type of transaction made                                              | character varying (150.0)   | -                     | -                                                    |
| updated_at       | Timestamp when the row was updated                                    | timestamp without time zone | -                     | -                                                    |
| user_id          | Unique ID of user                                                     | bigint                      | user.id               | -                                                    |
| user_pass_id     | Unique pass id used                                                   | bigint                      | user_pass.id          | -                                                    |



# notification

|            | definition                                                         | data_type                   | related_key   | remark   |
|:-----------|:-------------------------------------------------------------------|:----------------------------|:--------------|:---------|
| content    | Content of notification                                            | character varying (65535.0) | -             | -        |
| created_at | Timestamp of when row was created                                  | timestamp without time zone | -             | -        |
| data       | App uses this field to trigger logic for certain push notification | character varying (65535.0) | -             | -        |
| id         | Unique ID for the table                                            | bigint                      | -             | -        |
| type       | Type of notification                                               | character varying (150.0)   | -             | -        |
| user_id    | Unique ID of user                                                  | bigint                      | user.id       | -        |



# notification_config

|               | definition                          | data_type                   | related_key   | remark   |
|:--------------|:------------------------------------|:----------------------------|:--------------|:---------|
| city          | City                                | character varying (60.0)    | -             | -        |
| created_at    | Timestamp when record was created   | timestamp without time zone | -             | -        |
| created_by    | User who created notification       | bigint                      | user.id       | -        |
| end_time      | End time of notification            | character varying (30.0)    | -             | -        |
| id            | Unique ID of table                  | bigint                      | -             | -        |
| message_id    | Unique ID of message                | character varying (150.0)   | -             | -        |
| message_type  | -                                   | character varying (60.0)    | -             | -        |
| start_time    | Start time of notification          | character varying (30.0)    | -             | -        |
| status        | Status of notification event        | character varying (60.0)    | -             | -        |
| title         | Title of notification               | character varying (765.0)   | -             | -        |
| trigger_event | Action that triggers   notification | character varying (150.0)   | -             | -        |
| updated_at    | Timestamp when record was updated   | timestamp without time zone | -             | -        |
| updated_by    | User who updated notification       | bigint                      | user.id       | -        |
| weekdays      | Days of notification                | character varying (90.0)    | -             | -        |



# okai_battery_sn

|            | definition                        | data_type                   | related_key   | remark   |
|:-----------|:----------------------------------|:----------------------------|:--------------|:---------|
| battery    | QR Code of the OKAI battery       | character varying (360.0)   | user.id       | -        |
| created_at | Timestamp when record was created | timestamp without time zone | -             | -        |
| id         | Unique ID of table                | bigint                      | -             | -        |
| sn         | OKAI battery serial number        | character varying (192.0)   | -             | -        |



# ops_live_location

|            | definition                        | data_type                   | related_key   | remark   |
|:-----------|:----------------------------------|:----------------------------|:--------------|:---------|
| avatar     | -                                 | character varying (768.0)   | -             | -        |
| city       | City of operation                 | character varying (150.0)   | -             | -        |
| created_at | Timestamp when record was created | timestamp without time zone | -             | -        |
| full_name  | -                                 | character varying (768.0)   | -             | -        |
| id         | Unique ID of table                | bigint                      | -             | -        |
| latitude   | y-coordinate of user's position   | double precision            | -             | -        |
| longitude  | x-coordinate of user's position   | double precision            | -             | -        |
| updated_at | Timestamp when record was updated | timestamp without time zone | -             | -        |
| user_id    | Unique ID of user (ops)           | bigint                      | user.id       | -        |
| username   | -                                 | character varying (768.0)   | -             | -        |



# ops_open_deck

|                   | definition                                 | data_type                   | related_key                       | remark                |
|:------------------|:-------------------------------------------|:----------------------------|:----------------------------------|:----------------------|
| battery_no        | Unique number attached to the battery      | character varying (192.0)   | battery_inventory.battery_number, | -                     |
| created_at        | Timestamp of when row was created          | timestamp without time zone | -                                 | -                     |
| id                | Unique ID for the table                    | bigint                      | -                                 | -                     |
| open_channel      | Channel used to open deck                  | character varying (60.0)    | -                                 | CHANNEL_3G/CHANNEL_BT |
| open_deck_time    | Timestamp of open command response         | timestamp without time zone | -                                 | -                     |
| remaining_battery | Amount of battery left, from 0 - 1         | numeric                     | -                                 | -                     |
| scooter_id        | Unique ID of scooter                       | bigint                      | scooter_inventory.id              | -                     |
| status            | Status of Operation                        | character varying (60.0)    | -                                 | -                     |
| user_id           | Unique ID of the user that opened the deck | bigint                      | user.id                           | -                     |



# order_detail

|                        | definition                                                                   | data_type                   | related_key     | remark                                                                                                                                                                      |
|:-----------------------|:-----------------------------------------------------------------------------|:----------------------------|:----------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| adjust_off             | Amount adjusted by customer service due to user complaint                    | numeric                     | -               | When a user sees the bill, he may not agree with the final amount, so he may contact customer service to get an adjustment. So adjust_off denotes the amount reduced by CS. |
| base_fee               | Base fee for using scooter                                                   | numeric                     | -               | -                                                                                                                                                                           |
| convenience_fee        | Convenience fee paid                                                         | numeric                     | -               | -                                                                                                                                                                           |
| coupon_amount          | Coupon amount used                                                           | numeric                     | -               | -                                                                                                                                                                           |
| created_at             | Timestamp of when row was created                                            | timestamp without time zone | -               | -                                                                                                                                                                           |
| credit_amount          | Final amount charged to credit card                                          | numeric                     | -               | -                                                                                                                                                                           |
| currency               | Currency used                                                                | character varying (48.0)    | -               | -                                                                                                                                                                           |
| deposit_captured       | -                                                                            | numeric                     | -               | -                                                                                                                                                                           |
| helmet_replacement_fee | helmet replacement amount                                                    | numeric                     | -               | -                                                                                                                                                                           |
| id                     | Unique ID for the table                                                      | bigint                      | -               | -                                                                                                                                                                           |
| is_short_trip          | less than 60 seconds                                                         | smallint                    | -               | -                                                                                                                                                                           |
| order_id               | Unique ID of the order                                                       | bigint                      | neuron_order.id | -                                                                                                                                                                           |
| pass_amount            | Pass amount used                                                             | numeric                     | -               | -                                                                                                                                                                           |
| pass_minutes           | trip_minutes, if pass is used (and my_pass_id is not null)                   | bigint                      | -               | -                                                                                                                                                                           |
| scooter_discount       | Amount discounted from using specific scooter                                | numeric                     | -               | -                                                                                                                                                                           |
| trip_cap_off           | DEPRECATED                                                                   | numeric                     | -               | Maximum trip amount; not configured for AU&NZ                                                                                                                               |
| trip_fare              | Total trip fare                                                              | numeric                     | -               | -                                                                                                                                                                           |
| trip_mileage           | Mileage of scooter trip                                                      | bigint                      | -               | -                                                                                                                                                                           |
| trip_minutes           | Minutes of scooter trip                                                      | bigint                      | -               | -                                                                                                                                                                           |
| unit_fee               | Amount per minute                                                            | numeric                     | -               | -                                                                                                                                                                           |
| updated_at             | Timestamp of when row was updated                                            | timestamp without time zone | -               | -                                                                                                                                                                           |
| updated_by             | Unique ID of user                                                            | bigint                      | user.id         | -                                                                                                                                                                           |
| user_coupon_id         | Unique ID of coupon used, linked to user_coupon table, id column             | bigint                      | -               | -                                                                                                                                                                           |
| user_id                | Unique ID of user                                                            | bigint                      | user.id         | -                                                                                                                                                                           |
| user_pass_id           | user_pass.id                                                                 | bigint                      | -               | user_pass_id is not null for orders during which users exceed the pass minutes, in another word, it is possible that pass_minutes=0 and user_pass_id is not null            |
| vip                    | Flag if user is VIP at the start of the trip                                 | smallint                    | -               | 1 = yes, 0 = no                                                                                                                                                             |
| wallet_amount          | Wallet amount used (final amount will be deducted from wallet before credit) | numeric                     | -               | 1. If the trip is done by a VIP, final_amount = 0 2. If the trip is not a VIP trip, final_amount = trip_fare + convenience_fee - pass_amount - coupon_amount - adjust_off   |



# organization

|              | definition                            | data_type                   | related_key                    | remark          |
|:-------------|:--------------------------------------|:----------------------------|:-------------------------------|:----------------|
| code         | Organization code                     | character varying (150.0)   | organization_discount.org_code | -               |
| created_at   | Timestamp of when the row was created | timestamp without time zone | -                              | -               |
| created_by   | -                                     | bigint                      | -                              | -               |
| created_user | -                                     | character varying (768.0)   | -                              | -               |
| deleted      | Flag if deleted                       | smallint                    | -                              | yes = 1; no = 0 |
| domains      | Organization domain                   | character varying (1500.0)  | -                              | -               |
| name         | Ogranization name                     | character varying (765.0)   | -                              | -               |
| type         | Organization type                     | character varying (150.0)   | -                              | -               |
| updated_at   | Timestamp of when the row was updated | timestamp without time zone | -                              | -               |
| updated_by   | -                                     | bigint                      | -                              | -               |
| updated_user | -                                     | character varying (768.0)   | -                              | -               |



# organization_discount

|            | definition                                                   | data_type                   | related_key       | remark          |
|:-----------|:-------------------------------------------------------------|:----------------------------|:------------------|:----------------|
| created_at | Timestamp of when the row was created                        | timestamp without time zone | -                 | -               |
| deleted    | Flag if deleted                                              | smallint                    | -                 | yes = 1; no = 0 |
| discount   | % of the usual price the user from the organization pays for | integer                     | -                 | -               |
| id         | Unique table id                                              | integer                     | -                 | -               |
| org_code   | Organization code                                            | character varying (765.0)   | organization.code | -               |
| pass_days  | number of days pass is applied to                            | integer                     | -                 | -               |
| pass_id    | -                                                            | bigint                      | -                 | -               |
| updated_at | Timestamp of when the row was updated                        | timestamp without time zone | -                 | -               |



# organization_email_verification

|            | definition                            | data_type                   | related_key         | remark   |
|:-----------|:--------------------------------------|:----------------------------|:--------------------|:---------|
| created_at | Timestamp of when the row was created | timestamp without time zone | -                   | -        |
| deleted    | -                                     | smallint                    | -                   | -        |
| email      | email used for verification           | character varying (765.0)   | -                   | -        |
| id         | Unique table id                       | bigint                      | -                   | -        |
| org_code   | -                                     | character varying (150.0)   | -                   | -        |
| status     | Verification status                   | character varying (60.0)    | -                   | -        |
| token      | Unique token                          | character varying (150.0)   | -                   | -        |
| updated_at | Timestamp of when the row was updated | timestamp without time zone | -                   | -        |
| user_id    | User id that is verified              | bigint                      | neuron_user.user.id | -        |



# organization_invitation

|            | definition                            | data_type                   | related_key       | remark   |
|:-----------|:--------------------------------------|:----------------------------|:------------------|:---------|
| code       | code used for the promotion           | character varying (96.0)    | -                 | -        |
| created_at | Timestamp of when the row was created | timestamp without time zone | -                 | -        |
| created_by | User id of admin who created          | bigint                      | -                 | -        |
| id         | Unique table id                       | bigint                      | -                 | -        |
| org_code   | organization code                     | character varying (765.0)   | organization.code | -        |
| status     | status of invitation process          | character varying (96.0)    | -                 | -        |
| updated_at | Timestamp of when the row was updated | timestamp without time zone | -                 | -        |
| updated_by | User id of admin who updated          | bigint                      | -                 | -        |



# organization_secret_key

|                  | definition   | data_type                   | related_key   | remark   |
|:-----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at       | -            | timestamp without time zone | -             | -        |
| created_by       | -            | bigint                      | -             | -        |
| encrypted_secret | -            | character varying (1536.0)  | -             | -        |
| id               | -            | bigint                      | -             | -        |
| org_code         | -            | character varying (765.0)   | -             | -        |
| status           | -            | character varying (96.0)    | -             | -        |
| updated_at       | -            | timestamp without time zone | -             | -        |
| updated_by       | -            | bigint                      | -             | -        |



# organization_user

|                  | definition                                                 | data_type                   | related_key       | remark   |
|:-----------------|:-----------------------------------------------------------|:----------------------------|:------------------|:---------|
| created_at       | Timestamp of when the row was created                      | timestamp without time zone | -                 | -        |
| deleted          | -                                                          | smallint                    | -                 | -        |
| external_user_id | -                                                          | character varying (384.0)   | -                 | -        |
| id               | Unique table id                                            | bigint                      | -                 | -        |
| org_code         | organization code                                          | character varying (765.0)   | organization.code | -        |
| updated_at       | Timestamp of when the row was updated                      | timestamp without time zone | -                 | -        |
| user_id          | User id that is verified as affiliated to the organization | bigint                      | -                 | -        |



# ota_status_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| imei       | -            | character varying (96.0)    | -             | -        |
| notes      | -            | character varying (300.0)   | -             | -        |
| scooter_id | -            | bigint                      | -             | -        |
| status     | -            | character varying (96.0)    | -             | -        |
| task_id    | -            | bigint                      | -             | -        |



# ota_task

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| description   | -            | character varying (300.0)   | -             | -        |
| end_time      | -            | character varying (48.0)    | -             | -        |
| enum_protocol | -            | character varying (48.0)    | -             | -        |
| firmware      | -            | character varying (384.0)   | -             | -        |
| fm_version    | -            | character varying (48.0)    | -             | -        |
| id            | -            | bigint                      | -             | -        |
| language      | -            | character varying (48.0)    | -             | -        |
| progress_info | -            | character varying (600.0)   | -             | -        |
| protocol      | -            | character varying (90.0)    | -             | -        |
| retry_times   | -            | smallint                    | -             | -        |
| scooter_id    | -            | bigint                      | -             | -        |
| start_time    | -            | character varying (48.0)    | -             | -        |
| status        | -            | character varying (96.0)    | -             | -        |
| type          | -            | character varying (96.0)    | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| upgrade_end   | -            | timestamp without time zone | -             | -        |
| upgrade_start | -            | timestamp without time zone | -             | -        |
| version       | -            | integer                     | -             | -        |
| voice         | -            | character varying (150.0)   | -             | -        |



# pass

|                          | definition                              | data_type                   | related_key   | remark                     |
|:-------------------------|:----------------------------------------|:----------------------------|:--------------|:---------------------------|
| cities                   | -                                       | character varying (1536.0)  | -             | -                          |
| created_at               | Timestamp of when row was created       | timestamp without time zone | -             | -                          |
| created_by               | User id of admin who created            | bigint                      | -             | -                          |
| currency                 | Currency used                           | character varying (48.0)    | -             | -                          |
| description              | Marketing text used in user's app       | character varying (65535.0) | -             | -                          |
| duration                 | Duration of pass (days)                 | bigint                      | -             | -                          |
| free_min                 | Free duration for scooter riding (mins) | bigint                      | -             | -                          |
| id                       | Unique ID for the table                 | bigint                      | -             | -                          |
| lang_description         | -                                       | character varying (6000.0)  | -             | -                          |
| lang_title               | -                                       | character varying (6000.0)  | -             | -                          |
| price                    | Discounted price of pass                | numeric                     | -             | -                          |
| reference_price          | -                                       | numeric                     | -             | -                          |
| renewable                | -                                       | smallint                    | -             | -                          |
| renewable_after_inactive | -                                       | bigint                      | -             | -                          |
| status                   | Status of pass                          | character varying (60.0)    | -             | -                          |
| title                    | Title of pass                           | character varying (150.0)   | -             | au and nz id are different |
| updated_at               | Timestamp of when row was updated       | timestamp without time zone | -             | -                          |
| updated_by               | User id of admin who updated            | bigint                      | -             | -                          |



# password_reset

|            | definition                               | data_type                   | related_key   | remark   |
|:-----------|:-----------------------------------------|:----------------------------|:--------------|:---------|
| created_at | Timestamp of when the row was created at | timestamp without time zone | -             | -        |
| email      | email used                               | character varying (450.0)   | -             | -        |
| id         | Unique ID for the table                  | integer                     | -             | -        |
| status     | Status of the password reset process     | character varying (60.0)    | -             | -        |
| token      | Unique Token given                       | character varying (300.0)   | -             | -        |
| updated_at | Timestamp of when the row was updated at | timestamp without time zone | -             | -        |



# payment_transaction_mapping

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| created_at     | -            | timestamp without time zone | -             | -        |
| id             | -            | bigint                      | -             | -        |
| transaction_id | -            | bigint                      | -             | -        |
| type           | -            | character varying (96.0)    | -             | -        |
| value          | -            | bigint                      | -             | -        |



# promo_code

|              | definition                                                            | data_type                   | related_key         | remark   |
|:-------------|:----------------------------------------------------------------------|:----------------------------|:--------------------|:---------|
| coupon_name  | -                                                                     | character varying (768.0)   | -                   | -        |
| created_at   | Timestamp of when row was created                                     | timestamp without time zone | -                   | -        |
| created_by   | Unique ID of administrator (creator)                                  | bigint                      | neuron_user.user.id | -        |
| currency     | Type of currency                                                      | character varying (192.0)   | -                   | -        |
| expired_at   | Expiry date of promo code                                             | timestamp without time zone | -                   | -        |
| id           | Unique ID for the table                                               | bigint                      | -                   | -        |
| name         | Name of promo coe                                                     | character varying (384.0)   | -                   | -        |
| parent       | ID of promo code if the new promo code is based on existing promotion | bigint                      | -                   | -        |
| redeem_limit | Limit of promo code redemption                                        | integer                     | -                   | -        |
| remark       | Text remark of promo code                                             | character varying (3072.0)  | -                   | -        |
| scope        | Applicability of promo code (what promo can be used on)               | character varying (45.0)    | -                   | -        |
| status       | -                                                                     | character varying (60.0)    | -                   | -        |
| type         | Type of promo code for                                                | character varying (192.0)   | -                   | -        |
| updated_at   | Timestamp of when row was updated                                     | timestamp without time zone | -                   | -        |
| updated_by   | Unique ID of administrator (creator)                                  | bigint                      | neuron_user.user.id | -        |
| valid_at     | Release date of promo code                                            | timestamp without time zone | -                   | -        |
| valid_time   | Validity of promo code (days)                                         | integer                     | -                   | -        |
| value        | Monetary value of promo code                                          | double precision            | -                   | -        |



# purchase_intent

|                   | definition                                                            | data_type                   | related_key                                                            | remark                                               |
|:------------------|:----------------------------------------------------------------------|:----------------------------|:-----------------------------------------------------------------------|:-----------------------------------------------------|
| account           | Country attached to account                                           | character varying (96.0)    | -                                                                      | -                                                    |
| amount            | Amount payable for pass buy, trip payment or card authorisation       | numeric                     | neuron_order.amount, pass.price (if applicable)                        | -                                                    |
| client_ip         | User’s mobile IP when user make the purchase/generate the transaction | character varying (96.0)    | -                                                                      | From user's device, null if it's from Stripe webhook |
| created_at        | Timestamp of when row was created                                     | timestamp without time zone | -                                                                      | -                                                    |
| currency          | Currency used in payment                                              | character varying (48.0)    | -                                                                      | -                                                    |
| id                | Unique ID for the table                                               | bigint                      | -                                                                      | -                                                    |
| order_id          | Unique order ID                                                       | bigint                      | order_detail.order_id / neuron_order.id                                | -                                                    |
| pass_id           | Pass ID if applicable                                                 | bigint                      | pass.id                                                                | -                                                    |
| payment_intent_id | Unique ID of payment intent (Stripe Data)                             | character varying (384.0)   | Stripe Payment Intent table                                            | -                                                    |
| platform          | Platform used (stripe)                                                | character varying (96.0)    | -                                                                      | -                                                    |
| status            | Denotes status of transaction                                         | character varying (96.0)    | refer [here](https://stripe.com/docs/payments/intents#intent-statuses) | -                                                    |
| trip_deposit_id   | -                                                                     | bigint                      | -                                                                      | -                                                    |
| type              | Type of transaction                                                   | character varying (96.0)    | -                                                                      | -                                                    |
| updated_at        | Timestamp of when row was updated                                     | timestamp without time zone | -                                                                      | -                                                    |
| user_id           | Unique user ID                                                        | bigint                      | neuron_user.user.id                                                    | -                                                    |



# qrtz_blob_triggers

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| blob_data     | -            | character varying (65535.0) | -             | -        |
| sched_name    | -            | character varying (360.0)   | -             | -        |
| trigger_group | -            | character varying (600.0)   | -             | -        |
| trigger_name  | -            | character varying (600.0)   | -             | -        |



# qrtz_calendars

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| calendar      | -            | character varying (65535.0) | -             | -        |
| calendar_name | -            | character varying (600.0)   | -             | -        |
| sched_name    | -            | character varying (360.0)   | -             | -        |



# qrtz_cron_triggers

|                 | definition   | data_type                 | related_key   | remark   |
|:----------------|:-------------|:--------------------------|:--------------|:---------|
| cron_expression | -            | character varying (360.0) | -             | -        |
| sched_name      | -            | character varying (360.0) | -             | -        |
| time_zone_id    | -            | character varying (240.0) | -             | -        |
| trigger_group   | -            | character varying (600.0) | -             | -        |
| trigger_name    | -            | character varying (600.0) | -             | -        |



# qrtz_fired_triggers

|                   | definition   | data_type                 | related_key   | remark   |
|:------------------|:-------------|:--------------------------|:--------------|:---------|
| entry_id          | -            | character varying (285.0) | -             | -        |
| fired_time        | -            | bigint                    | -             | -        |
| instance_name     | -            | character varying (600.0) | -             | -        |
| is_nonconcurrent  | -            | character varying (15.0)  | -             | -        |
| job_group         | -            | character varying (600.0) | -             | -        |
| job_name          | -            | character varying (600.0) | -             | -        |
| priority          | -            | integer                   | -             | -        |
| requests_recovery | -            | character varying (15.0)  | -             | -        |
| sched_name        | -            | character varying (360.0) | -             | -        |
| sched_time        | -            | bigint                    | -             | -        |
| state             | -            | character varying (48.0)  | -             | -        |
| trigger_group     | -            | character varying (600.0) | -             | -        |
| trigger_name      | -            | character varying (600.0) | -             | -        |



# qrtz_job_details

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| description       | -            | character varying (750.0)   | -             | -        |
| is_durable        | -            | character varying (15.0)    | -             | -        |
| is_nonconcurrent  | -            | character varying (15.0)    | -             | -        |
| is_update_data    | -            | character varying (15.0)    | -             | -        |
| job_class_name    | -            | character varying (750.0)   | -             | -        |
| job_data          | -            | character varying (65535.0) | -             | -        |
| job_group         | -            | character varying (600.0)   | -             | -        |
| job_name          | -            | character varying (600.0)   | -             | -        |
| requests_recovery | -            | character varying (15.0)    | -             | -        |
| sched_name        | -            | character varying (360.0)   | -             | -        |



# qrtz_locks

|            | definition   | data_type                 | related_key   | remark   |
|:-----------|:-------------|:--------------------------|:--------------|:---------|
| lock_name  | -            | character varying (120.0) | -             | -        |
| sched_name | -            | character varying (360.0) | -             | -        |



# qrtz_paused_trigger_grps

|               | definition   | data_type                 | related_key   | remark   |
|:--------------|:-------------|:--------------------------|:--------------|:---------|
| sched_name    | -            | character varying (360.0) | -             | -        |
| trigger_group | -            | character varying (600.0) | -             | -        |



# qrtz_scheduler_state

|                   | definition   | data_type                 | related_key   | remark   |
|:------------------|:-------------|:--------------------------|:--------------|:---------|
| checkin_interval  | -            | bigint                    | -             | -        |
| instance_name     | -            | character varying (600.0) | -             | -        |
| last_checkin_time | -            | bigint                    | -             | -        |
| sched_name        | -            | character varying (360.0) | -             | -        |



# qrtz_simple_triggers

|                 | definition   | data_type                 | related_key   | remark   |
|:----------------|:-------------|:--------------------------|:--------------|:---------|
| repeat_count    | -            | bigint                    | -             | -        |
| repeat_interval | -            | bigint                    | -             | -        |
| sched_name      | -            | character varying (360.0) | -             | -        |
| times_triggered | -            | bigint                    | -             | -        |
| trigger_group   | -            | character varying (600.0) | -             | -        |
| trigger_name    | -            | character varying (600.0) | -             | -        |



# qrtz_simprop_triggers

|               | definition   | data_type                  | related_key   | remark   |
|:--------------|:-------------|:---------------------------|:--------------|:---------|
| bool_prop_1   | -            | character varying (3.0)    | -             | -        |
| bool_prop_2   | -            | character varying (3.0)    | -             | -        |
| dec_prop_1    | -            | numeric                    | -             | -        |
| dec_prop_2    | -            | numeric                    | -             | -        |
| int_prop_1    | -            | integer                    | -             | -        |
| int_prop_2    | -            | integer                    | -             | -        |
| long_prop_1   | -            | bigint                     | -             | -        |
| long_prop_2   | -            | bigint                     | -             | -        |
| sched_name    | -            | character varying (360.0)  | -             | -        |
| str_prop_1    | -            | character varying (1536.0) | -             | -        |
| str_prop_2    | -            | character varying (1536.0) | -             | -        |
| str_prop_3    | -            | character varying (1536.0) | -             | -        |
| trigger_group | -            | character varying (600.0)  | -             | -        |
| trigger_name  | -            | character varying (600.0)  | -             | -        |



# qrtz_triggers

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| calendar_name  | -            | character varying (600.0)   | -             | -        |
| description    | -            | character varying (750.0)   | -             | -        |
| end_time       | -            | bigint                      | -             | -        |
| job_data       | -            | character varying (65535.0) | -             | -        |
| job_group      | -            | character varying (600.0)   | -             | -        |
| job_name       | -            | character varying (600.0)   | -             | -        |
| misfire_instr  | -            | smallint                    | -             | -        |
| next_fire_time | -            | bigint                      | -             | -        |
| prev_fire_time | -            | bigint                      | -             | -        |
| priority       | -            | integer                     | -             | -        |
| sched_name     | -            | character varying (360.0)   | -             | -        |
| start_time     | -            | bigint                      | -             | -        |
| trigger_group  | -            | character varying (600.0)   | -             | -        |
| trigger_name   | -            | character varying (600.0)   | -             | -        |
| trigger_state  | -            | character varying (48.0)    | -             | -        |
| trigger_type   | -            | character varying (24.0)    | -             | -        |



# rebalance

|                  | definition                               | data_type                   | related_key          | remark                                                                                                                                    |
|:-----------------|:-----------------------------------------|:----------------------------|:---------------------|:------------------------------------------------------------------------------------------------------------------------------------------|
| city             | Location where rebalance was completed   | character varying (96.0)    | -                    | -                                                                                                                                         |
| created_at       | Timestamp of when row was created        | timestamp without time zone | -                    | -                                                                                                                                         |
| created_by       | Unique ID of administrator (creator)     | bigint                      | user.id              | -                                                                                                                                         |
| end_city         | -                                        | character varying (96.0)    | -                    | -                                                                                                                                         |
| end_station_id   | Unique ID of end station                 | bigint                      | station.id           | -                                                                                                                                         |
| end_zone         | -                                        | character varying (150.0)   | -                    | -                                                                                                                                         |
| finish_latitude  | Latitude where rebalance was completed   | numeric                     | -                    | -                                                                                                                                         |
| finish_longitude | Longitude where rebalance  was completed | numeric                     | -                    | -                                                                                                                                         |
| from_scan        | Flag if rebalance is done via scan       | smallint                    | -                    | 1 = yes, 0 = no                                                                                                                           |
| id               | Unique ID for the table                  | bigint                      | -                    | -                                                                                                                                         |
| scooter_id       | Unique ID of scooter                     | bigint                      | scooter_inventory.id | -                                                                                                                                         |
| start_city       | -                                        | character varying (96.0)    | -                    | -                                                                                                                                         |
| start_latitude   | Latitude where rebalance started         | numeric                     | -                    | -                                                                                                                                         |
| start_longitude  | Longitude where rebalance started        | numeric                     | -                    | -                                                                                                                                         |
| start_station_id | Unique ID of start station               | bigint                      | station.id           | -                                                                                                                                         |
| start_zone       | -                                        | character varying (150.0)   | -                    | -                                                                                                                                         |
| status           | Status of rebalance                      | character varying (48.0)    | -                    | ONGOING = retrieved but not deployed yet; FINISHED = retrieved and deployed successfully; CANCELLED = retrieved but then was admin locked |
| updated_at       | Timestamp of when row was updated        | timestamp without time zone | -                    | -                                                                                                                                         |
| updated_by       | Unique ID of administrator (creator)     | bigint                      | user.id              | -                                                                                                                                         |
| user_gps_time    | Timestamp of when user used GPS          | timestamp without time zone | -                    | user's gps when the scooter is deployed                                                                                                   |
| vehicle_type     | Type of Vehicle                          | character varying (96.0)    | -                    | -                                                                                                                                         |



# receipt

|            | definition                                         | data_type                   | related_key   | remark    |
|:-----------|:---------------------------------------------------|:----------------------------|:--------------|:----------|
| created_at | Timestamp of when the row was created at           | timestamp without time zone | -             | -         |
| id         | Unique ID for the table                            | bigint                      | -             | -         |
| sent_num   | Number of times the user requested for the receipt | integer                     | -             | Maximum 5 |
| serial_num | Receipt serial number                              | character varying (60.0)    | -             | -         |
| source     | Order id                                           | bigint                      | order_id      | -         |
| type       | Type of transaction                                | character varying (60.0)    | -             | -         |



# rtk_station

|                | definition                               | data_type                   | related_key   | remark             |
|:---------------|:-----------------------------------------|:----------------------------|:--------------|:-------------------|
| city           | City where scooter is in                 | character varying (96.0)    | -             | -                  |
| created_at     | Timestamp of when the row was created at | timestamp without time zone | -             | -                  |
| imei           | Unique ID of GPS device                  | character varying (96.0)    | -             | gps_inventory.imei |
| latitude       | Station's latitude                       | numeric                     | -             | -                  |
| longitude      | Station's longitude                      | numeric                     | -             | -                  |
| role           | -                                        | character varying (30.0)    | -             | -                  |
| rtk_data       | Data corresponding to the rtk            | character varying (65535.0) | -             | -                  |
| switch_role_at | -                                        | timestamp without time zone | -             | -                  |
| updated_at     | Timestamp of when the row was   updated  | timestamp without time zone | -             | -                  |



# sagemaker_training_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| feature    | -            | character varying (1536.0)  | -             | -        |
| id         | -            | bigint                      | -             | -        |
| raw        | -            | character varying (64512.0) | -             | -        |
| result     | -            | character varying (1536.0)  | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| type       | -            | character varying (96.0)    | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# scan_record

|                   | definition                            | data_type                   | related_key              | remark                                               |
|:------------------|:--------------------------------------|:----------------------------|:-------------------------|:-----------------------------------------------------|
| app_version       | User's app version                    | character varying (96.0)    | -                        | -                                                    |
| city              | City where scan was made              | character varying (96.0)    | -                        | -                                                    |
| created_at        | Timestamp of when row was created     | timestamp without time zone | -                        | -                                                    |
| device_id         | Unique scooter ID                     | character varying (384.0)   | scooter_inventory.id     | -                                                    |
| error_code        | Unique error code                     | character varying (96.0)    | -                        | refer [here](https://www.twilio.com/docs/api/errors) |
| id                | Unique ID for the table               | bigint                      | -                        | -                                                    |
| imei              | Unique ID of GPS device               | character varying (96.0)    | -                        | gps_inventory.imei                                   |
| lpwr_flag         | -                                     | smallint                    | -                        | -                                                    |
| platform          | Type of platform used                 | character varying (96.0)    | -                        | Device platform (IOS or ANDROID)                     |
| remaining_battery | Amount of battery left, from 0 - 1    | double precision            | -                        | -                                                    |
| scooter_id        | Unique ID of scooter                  | bigint                      | -                        | -                                                    |
| scooter_status    | Status of scooter                     | character varying (150.0)   | scooter_inventory.status | -                                                    |
| station_id        | Unique ID of station                  | bigint                      | station.id               | -                                                    |
| trip_id           | Unique Trip ID                        | bigint                      | trip.id                  | -                                                    |
| updated_at        | Timestamp of when row was updated     | timestamp without time zone | -                        | -                                                    |
| user_gps_time     | Timestamp of when GPS was last active | timestamp without time zone | -                        | -                                                    |
| user_id           | Unique ID of user                     | bigint                      | user.id                  | -                                                    |
| user_latitude     | y-coordinate of the user              | double precision            | -                        | -                                                    |
| user_longitude    | x-coordinate of the user              | double precision            | -                        | -                                                    |
| vehicle_type      | Type of Vehicle                       | character varying (96.0)    | -                        | -                                                    |
| vin               | -                                     | double precision            | -                        | -                                                    |



# scooter_assemble_record

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| created_at     | -            | timestamp without time zone | -             | -        |
| deck_id        | -            | character varying (90.0)    | -             | -        |
| id             | -            | bigint                      | -             | -        |
| imei           | -            | character varying (105.0)   | -             | -        |
| licence        | -            | character varying (105.0)   | -             | -        |
| mac            | -            | character varying (90.0)    | -             | -        |
| opt_lock_no    | -            | character varying (105.0)   | -             | -        |
| qr_code        | -            | character varying (48.0)    | -             | -        |
| scooter_id     | -            | bigint                      | -             | -        |
| scooter_status | -            | character varying (60.0)    | -             | -        |
| sticker        | -            | character varying (90.0)    | -             | -        |
| type           | -            | character varying (60.0)    | -             | -        |
| user_id        | -            | bigint                      | -             | -        |



# scooter_check_record

|                | definition                           | data_type                   | related_key            | remark                                                                                                                                                        |
|:---------------|:-------------------------------------|:----------------------------|:-----------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| city           | -                                    | character varying (96.0)    | -                      | -                                                                                                                                                             |
| content        | Scooter check content in JSON format | character varying (65535.0) | -                      | [JSON extract](https://bitbucket.org/neuroncn/data-dictionary/src/79f68395e27daa55f2c97f901959938fe07b17f7/JSON_extract/scooter_check_content.json?at=master) |
| created_at     | Timestamp of when row was created    | timestamp without time zone | -                      | -                                                                                                                                                             |
| created_by     | Unique ID of administrator           | bigint                      | -                      | -                                                                                                                                                             |
| device_id      | Unique scooter ID                    | bigint                      | scooter_inventory.id   | -                                                                                                                                                             |
| id             | Unique ID for the table              | bigint                      | -                      | -                                                                                                                                                             |
| maintenance_id | Unique ID for maintenance job        | bigint                      | scooter_maintenance.id | -                                                                                                                                                             |
| status         | Status of check                      | character varying (60.0)    | -                      | -                                                                                                                                                             |
| type           | Type of check                        | character varying (60.0)    | -                      | refer to JSON extract                                                                                                                                         |
| updated_at     | Timestamp of when row was updated    | timestamp without time zone | -                      | -                                                                                                                                                             |
| updated_by     | Unique ID of administrator           | bigint                      | -                      | -                                                                                                                                                             |



# scooter_city_record

|            | definition                       | data_type                   | related_key    | remark                                 |
|:-----------|:---------------------------------|:----------------------------|:---------------|:---------------------------------------|
| city       | City where scooter is in         | character varying (60.0)    | -              | -                                      |
| created_at | Timestamp of when row is created | timestamp without time zone | -              | -                                      |
| id         | Unique ID for the table          | bigint                      | -              | -                                      |
| pre_city   | -                                | character varying (60.0)    | -              | -                                      |
| pre_zone   | -                                | character varying (90.0)    | -              | -                                      |
| scooter_id | Scooter ID                       | bigint                      | trip.device_id | -                                      |
| zone       | Zone where the scooter is in     | character varying (60.0)    | -              | New record when there is a zone change |



# scooter_cmd_exec

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| cmd_record_list | -            | character varying (64512.0) | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| deleted         | -            | smallint                    | -             | -        |
| id              | -            | bigint                      | -             | -        |
| imei            | -            | character varying (300.0)   | -             | -        |
| status          | -            | character varying (30.0)    | -             | -        |
| task_id         | -            | bigint                      | -             | -        |
| task_type       | -            | character varying (60.0)    | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |



# scooter_extend_info

|                                 | definition   | data_type                   | related_key   | remark   |
|:--------------------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at                      | -            | timestamp without time zone | -             | -        |
| id                              | -            | bigint                      | -             | -        |
| last_rebalance_start_time       | -            | timestamp without time zone | -             | -        |
| last_road_test_time             | -            | timestamp without time zone | -             | -        |
| last_sanitize_time              | -            | timestamp without time zone | -             | -        |
| last_scooter_status_record_time | -            | timestamp without time zone | -             | -        |
| last_warehouse_drop_off_time    | -            | timestamp without time zone | -             | -        |
| latest_rebalance_finish_time    | -            | timestamp without time zone | -             | -        |
| latest_trip_end_time            | -            | timestamp without time zone | -             | -        |
| latest_trip_start_time          | -            | timestamp without time zone | -             | -        |
| scooter_id                      | -            | bigint                      | -             | -        |
| updated_at                      | -            | timestamp without time zone | -             | -        |



# scooter_inventory

|                    | definition                                                                                                          | data_type                   | related_key                | remark                                                                                            |
|:-------------------|:--------------------------------------------------------------------------------------------------------------------|:----------------------------|:---------------------------|:--------------------------------------------------------------------------------------------------|
| city               | City where scooter is                                                                                               | character varying (60.0)    | -                          | -                                                                                                 |
| created_at         | Timestamp of when row was created                                                                                   | timestamp without time zone | -                          | -                                                                                                 |
| deck_id            | Unique deck ID                                                                                                      | character varying (60.0)    | -                          | -                                                                                                 |
| deleted            | Flag if scooter is 'deleted'                                                                                        | smallint                    | -                          | 1 = yes, 0 = no                                                                                   |
| generation         | Unique scooter version                                                                                              | character varying (48.0)    | -                          | -                                                                                                 |
| helmet_lock        | Helmet lock in place                                                                                                | smallint                    | -                          | 1 = yes; 0 = no                                                                                   |
| ic_card            | RFID card ID designed for the docking system, used to recognize the scooter when   we push the scooter to the dock. | character varying (765.0)   | lock_response.card_no      | -                                                                                                 |
| id                 | Unique ID of scooter                                                                                                | bigint                      | -                          | -                                                                                                 |
| image_id           | Unique image ID taken by user                                                                                       | character varying (192.0)   | neuron_file.id             | -                                                                                                 |
| imei               | Unique ID of GPS device                                                                                             | character varying (300.0)   | gps_inventory.imei         | -                                                                                                 |
| last_sanitize_time | Timestamp of when the scooter was last sanitized                                                                    | timestamp without time zone | -                          | -                                                                                                 |
| last_test_time     | Timestamp of when the scooter was last tested                                                                       | timestamp without time zone | -                          | -                                                                                                 |
| licence            | Deprecated                                                                                                          | character varying (105.0)   | -                          | -                                                                                                 |
| mac                | Unique mac address                                                                                                  | character varying (60.0)    | -                          | -                                                                                                 |
| motor_number       | -                                                                                                                   | character varying (150.0)   | -                          | -                                                                                                 |
| qr_code            | Unique QR code of each scooter                                                                                      | character varying (48.0)    | -                          | -                                                                                                 |
| station_id         | Unique ID of station                                                                                                | bigint                      | station.id                 | -                                                                                                 |
| status             | Status of scooter                                                                                                   | character varying (60.0)    | scan_record.scooter_status | -                                                                                                 |
| sticker            | Sticker number                                                                                                      | character varying (90.0)    | -                          | -                                                                                                 |
| sub_status         | -                                                                                                                   | character varying (150.0)   | -                          | -                                                                                                 |
| type               | Type of Vehicle                                                                                                     | character varying (60.0)    | -                          | -                                                                                                 |
| updated_at         | Timestamp of when row was updated                                                                                   | timestamp without time zone | -                          | -                                                                                                 |
| version            | -                                                                                                                   | integer                     | -                          | Backend database optimize lock; used in the backend server to avoid the concurrency data updates. |
| zone               | Pricing zone                                                                                                        | character varying (60.0)    | zone.code                  | -                                                                                                 |



# scooter_issue

|                        | definition                        | data_type                   | related_key            | remark   |
|:-----------------------|:----------------------------------|:----------------------------|:-----------------------|:---------|
| city                   | Scooter's city                    | character varying (96.0)    | -                      | -        |
| content                | Scooter issue comment made by ops | character varying (65535.0) | -                      | -        |
| create_user_name       | -                                 | character varying (768.0)   | -                      | -        |
| create_user_role       | -                                 | character varying (150.0)   | -                      | -        |
| created_at             | Timestamp of when row was created | timestamp without time zone | -                      | -        |
| created_by             | User ID that created the row      | bigint                      | -                      | -        |
| device_id              | Scooter ID                        | bigint                      | scooter_inventory.id   | -        |
| high_risk              | -                                 | smallint                    | -                      | -        |
| id                     | Unique ID for the table           | bigint                      | -                      | -        |
| maintenance_id         | Unique maintenance work id        | bigint                      | scooter_maintenance.id | -        |
| muted_users            | -                                 | character varying (65535.0) | -                      | -        |
| scooter_latitude       | -                                 | numeric                     | -                      | -        |
| scooter_longitude      | -                                 | numeric                     | -                      | -        |
| status                 | Status of issue                   | character varying (60.0)    | -                      | -        |
| tag                    | -                                 | character varying (768.0)   | -                      | -        |
| type                   | Type of issue                     | character varying (300.0)   | -                      | -        |
| update_user_name       | -                                 | character varying (768.0)   | -                      | -        |
| update_user_role       | -                                 | character varying (150.0)   | -                      | -        |
| updated_at             | Timestamp of when row was updated | timestamp without time zone | -                      | -        |
| updated_by             | User ID that updated the row      | bigint                      | -                      | -        |
| users_marked_important | -                                 | character varying (65535.0) | -                      | -        |
| vehicle_type           | -                                 | character varying (96.0)    | -                      | -        |



# scooter_issue_extend_info

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| id            | -            | bigint                      | -             | -        |
| issue_id      | -            | bigint                      | -             | -        |
| lang          | -            | character varying (60.0)    | -             | -        |
| need_callback | -            | smallint                    | -             | -        |
| phone         | -            | character varying (150.0)   | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |



# scooter_issue_image

|              | definition                        | data_type                   | related_key      | remark   |
|:-------------|:----------------------------------|:----------------------------|:-----------------|:---------|
| created_at   | Timestamp of when row was created | timestamp without time zone | -                | -        |
| id           | Unique ID for the table           | bigint                      | -                | -        |
| image_id     | Unique ID of image                | character varying (150.0)   | neuron_file.id   | -        |
| issue_id     | Unique ID of issue                | bigint                      | scooter_issue.id | -        |
| issue_log_id | -                                 | bigint                      | -                | -        |



# scooter_issue_log

|                       | definition   | data_type                   | related_key   | remark   |
|:----------------------|:-------------|:----------------------------|:--------------|:---------|
| action                | -            | character varying (1500.0)  | -             | -        |
| action_template       | -            | character varying (1500.0)  | -             | -        |
| action_template_param | -            | character varying (1500.0)  | -             | -        |
| comment               | -            | character varying (3000.0)  | -             | -        |
| created_at            | -            | timestamp without time zone | -             | -        |
| created_by            | -            | bigint                      | -             | -        |
| id                    | -            | bigint                      | -             | -        |
| issue_id              | -            | bigint                      | -             | -        |
| type                  | -            | character varying (150.0)   | -             | -        |



# scooter_issue_notification

|            | definition                        | data_type                   | related_key      | remark   |
|:-----------|:----------------------------------|:----------------------------|:-----------------|:---------|
| created_at | Timestamp of when row was created | timestamp without time zone | -                | -        |
| id         | Unique ID for the table           | bigint                      | -                | -        |
| issue_id   | Unique ID of issue                | bigint                      | scooter_issue.id | -        |
| message    | Message sent                      | character varying (768.0)   | -                | -        |
| updated_at | Timestamp of when row was updated | timestamp without time zone | -                | -        |



# scooter_maintenance

|              | definition                                              | data_type                   | related_key          | remark   |
|:-------------|:--------------------------------------------------------|:----------------------------|:---------------------|:---------|
| comment      | Additional comment made by ops                          | character varying (65535.0) | -                    | -        |
| created_at   | Timestamp of when row was created                       | timestamp without time zone | -                    | -        |
| created_by   | User ID that created the row                            | bigint                      | -                    | -        |
| device_id    | Unique scooter id                                       | bigint                      | scooter_inventory.id | -        |
| id           | Unique ID for the table                                 | bigint                      | -                    | -        |
| station_id   | Unique station(warehouse) id where maintenance happened | bigint                      | -                    | -        |
| status       | Status of maintenance                                   | character varying (150.0)   | -                    | -        |
| updated_at   | Timestamp of when row was updated                       | timestamp without time zone | -                    | -        |
| updated_by   | User ID that updated the row                            | bigint                      | -                    | -        |
| vehicle_type | -                                                       | character varying (96.0)    | -                    | -        |



# scooter_maintenance_status_record

|                | definition                        | data_type                   | related_key            | remark   |
|:---------------|:----------------------------------|:----------------------------|:-----------------------|:---------|
| created_at     | Timestamp of when row was created | timestamp without time zone | -                      | -        |
| created_by     | User ID that created the row      | bigint                      | -                      | -        |
| device_id      | Unique scooter id                 | bigint                      | scooter_inventory.id   | -        |
| id             | Unique ID for the table           | bigint                      | -                      | -        |
| maintenance_id | Unique maintenance id             | bigint                      | scooter_maintenance.id | -        |
| status         | Status of maintenance             | character varying (150.0)   | -                      | -        |



# scooter_missing_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (96.0)    | -             | -        |
| comment    | -            | character varying (1500.0)  | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| photos     | -            | character varying (1500.0)  | -             | -        |
| reason     | -            | character varying (750.0)   | -             | -        |
| scooter_id | -            | bigint                      | -             | -        |



# scooter_motor_number

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| id            | -            | bigint                      | -             | -        |
| produce_date  | -            | timestamp without time zone | -             | -        |
| serial_number | -            | character varying (150.0)   | -             | -        |



# scooter_reservation

|              | definition                           | data_type                   | related_key          | remark   |
|:-------------|:-------------------------------------|:----------------------------|:---------------------|:---------|
| city         | -                                    | character varying (60.0)    | -                    | -        |
| created_at   | Timestamp of when row was created    | timestamp without time zone | -                    | -        |
| created_by   | User ID that created the reservation | bigint                      | -                    | -        |
| device_id    | Scooter ID                           | bigint                      | scooter_inventory.id | -        |
| id           | Unique ID for table                  | bigint                      | -                    | -        |
| status       | Current status of the reservation    | character varying (60.0)    | -                    | -        |
| updated_at   | Timestamp of when row was updated    | timestamp without time zone | -                    | -        |
| updated_by   | User ID that updated the reservation | bigint                      | -                    | -        |
| user_id      | -                                    | bigint                      | -                    | -        |
| valid_count  | -                                    | smallint                    | -                    | -        |
| vehicle_type | -                                    | character varying (96.0)    | -                    | -        |
| zone         | -                                    | character varying (60.0)    | -                    | -        |



# scooter_retired_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (96.0)    | -             | -        |
| comment    | -            | character varying (1500.0)  | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| photos     | -            | character varying (1500.0)  | -             | -        |
| reason     | -            | character varying (750.0)   | -             | -        |
| scooter_id | -            | bigint                      | -             | -        |
| type       | -            | character varying (150.0)   | -             | -        |



# scooter_sanitize_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (150.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| scooter_id | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# scooter_speed_record

|            | definition                            | data_type                   | related_key   | remark      |
|:-----------|:--------------------------------------|:----------------------------|:--------------|:------------|
| created_at | Timestamp of when the row was created | timestamp without time zone | -             | -           |
| id         | Unique ID for the table               | bigint                      | -             | -           |
| imei       | Unique imei of scooter                | character varying (96.0)    | -             | -           |
| mode       | Speed mode                            | character varying (48.0)    | -             | -           |
| speed      | Speed of scooter                      | smallint                    | -             | units: km/h |
| trip_id    | Unique trip ID                        | bigint                      | -             | -           |
| user_id    | Unique user ID                        | bigint                      | -             | -           |



# scooter_status_record

|                            | definition                                     | data_type                   | related_key              | remark                                                                         |
|:---------------------------|:-----------------------------------------------|:----------------------------|:-------------------------|:-------------------------------------------------------------------------------|
| battery                    | Battery level of scooter                       | double precision            | -                        | -                                                                              |
| city                       | City where scooter is located at               | character varying (96.0)    | -                        | -                                                                              |
| created_at                 | Timestamp of when row was created              | timestamp without time zone | -                        | -                                                                              |
| device_id                  | Unique ID of scooters                          | bigint                      | -                        | -                                                                              |
| id                         | Unique ID for the table                        | bigint                      | -                        | -                                                                              |
| object_id                  | Trip ID if the status change from x to in_trip | bigint                      | -                        | Mainly required by MDS API. Future: Can reuse field to connect to rebalance.id |
| previous_status            | Prior status                                   | character varying (60.0)    | -                        | -                                                                              |
| previous_status_created_at | -                                              | timestamp without time zone | -                        | -                                                                              |
| previous_sub_status        | -                                              | character varying (150.0)   | -                        | -                                                                              |
| previous_zone              | -                                              | character varying (90.0)    | -                        | -                                                                              |
| scooter_latitude           | Scooter latitude based on GPS                  | double precision            | -                        | -                                                                              |
| scooter_longitude          | Scooter longitude based on GPS                 | double precision            | -                        | -                                                                              |
| status                     | Status of scooter                              | character varying (60.0)    | scooter_inventory.status | -                                                                              |
| sub_status                 | -                                              | character varying (150.0)   | -                        | -                                                                              |
| vehicle_type               | -                                              | character varying (96.0)    | -                        | -                                                                              |
| zone                       | -                                              | character varying (60.0)    | -                        | -                                                                              |



# shop

|               | definition                              | data_type                   | related_key   | remark   |
|:--------------|:----------------------------------------|:----------------------------|:--------------|:---------|
| address       | Shop's address                          | character varying (768.0)   | -             | -        |
| category      | Category of Shop                        | character varying (60.0)    | -             | -        |
| city          | City of shop                            | character varying (60.0)    | -             | -        |
| contact_email | Shop's contact email                    | character varying (768.0)   | -             | -        |
| contact_name  | Shop's contact number                   | character varying (768.0)   | -             | -        |
| created_at    | Timestamp of when the row was   created | timestamp without time zone | -             | -        |
| created_by    | User ID that created the row            | bigint                      | -             | -        |
| deleted       | Whether the shop is deleted             | smallint                    | -             | -        |
| disabled      | Whether the shop is disabled            | smallint                    | -             | -        |
| id            | Unique ID of table                      | character varying (48.0)    | -             | -        |
| image         | -                                       | character varying (768.0)   | -             | -        |
| latitude      | Shop's latitude                         | numeric                     | -             | -        |
| longitude     | Shop's longitude                        | numeric                     | -             | -        |
| name          | Name of Shop                            | character varying (768.0)   | -             | -        |
| phone         | Shop's phone number                     | character varying (60.0)    | -             | -        |
| suburb        | -                                       | character varying (90.0)    | -             | -        |
| updated_at    | Timestamp of when the row was   updated | timestamp without time zone | -             | -        |
| updated_by    | User ID that updated the row            | bigint                      | -             | -        |
| website       | Shop's website                          | character varying (1536.0)  | -             | -        |



# shop_offer

|              | definition                              | data_type                   | related_key   | remark   |
|:-------------|:----------------------------------------|:----------------------------|:--------------|:---------|
| created_at   | Timestamp of when the row was   created | timestamp without time zone | -             | -        |
| created_by   | User ID that created the row            | bigint                      | -             | -        |
| deleted      | Whether offer is deleted                | smallint                    | -             | -        |
| detail       | Details of offer                        | character varying (65535.0) | -             | -        |
| disabled     | Whether offer is disabled               | smallint                    | -             | -        |
| frequency    | Discount Frequency                      | character varying (60.0)    | -             | -        |
| id           | Unique ID of table                      | bigint                      | -             | -        |
| shop_id      | Unique ID of shop                       | character varying (48.0)    | shop.id       | -        |
| title        | -                                       | character varying (150.0)   | -             | -        |
| total_number | Count of offer used                     | bigint                      | -             | -        |
| updated_at   | Timestamp of when the row was   updated | timestamp without time zone | -             | -        |
| updated_by   | User ID that updated the row            | bigint                      | -             | -        |



# shop_offer_record

|            | definition                            | data_type                   | related_key   | remark   |
|:-----------|:--------------------------------------|:----------------------------|:--------------|:---------|
| created_at | Timestamp of when the row was created | timestamp without time zone | -             | -        |
| id         | Unique ID of table                    | bigint                      | -             | -        |
| offer_id   | Unique ID of offer                    | bigint                      | shop_offer.id | -        |
| user_id    | Unique ID of user                     | bigint                      | user.id       | -        |



# sign_up_record

|              | definition                             | data_type                   | related_key   | remark                                                     |
|:-------------|:---------------------------------------|:----------------------------|:--------------|:-----------------------------------------------------------|
| app_version  | -                                      | character varying (192.0)   | -             | -                                                          |
| country_code | Country code                           | character varying (48.0)    | -             | -                                                          |
| created_at   | Timestamp of when row was created      | timestamp without time zone | -             | -                                                          |
| device_id    | -                                      | character varying (192.0)   | -             | -                                                          |
| email        | -                                      | character varying (300.0)   | -             | -                                                          |
| geo_city     | Geographical city of sign-up           | character varying (300.0)   | -             | Calculated based on Mapbox API based on user's GPS   point |
| id           | Unique ID for the table                | bigint                      | -             | -                                                          |
| ip_city      | City where IP address used for sign-up | character varying (300.0)   | -             | -                                                          |
| latitude     | y-coordinate of sign-up                | double precision            | -             | -                                                          |
| longitude    | x-coordinate of sign-up                | double precision            | -             | -                                                          |
| mobile       | Mobile number                          | character varying (150.0)   | -             | -                                                          |
| remote_ip    | Unique remote IP address               | character varying (150.0)   | -             | -                                                          |
| social_id    | -                                      | character varying (300.0)   | -             | -                                                          |
| social_type  | -                                      | character varying (96.0)    | -             | -                                                          |
| system       | -                                      | character varying (192.0)   | -             | -                                                          |
| user_city    | -                                      | character varying (96.0)    | -             | -                                                          |
| user_id      | Unique user ID                         | bigint                      | user.id       | -                                                          |



# sms_delivery_record

|              | definition                          | data_type                   | related_key   | remark                                                                                                                                         |
|:-------------|:------------------------------------|:----------------------------|:--------------|:-----------------------------------------------------------------------------------------------------------------------------------------------|
| country_code | Country code                        | character varying (24.0)    | -             | -                                                                                                                                              |
| created_at   | Timestamp of when row was created   | timestamp without time zone | -             | -                                                                                                                                              |
| error_code   | Unique error code                   | integer                     | -             | refer [here](https://www.twilio.com/docs/api/errors)                                                                                           |
| error_msg    | Message content for each error code | character varying (1536.0)  | -             | -                                                                                                                                              |
| id           | Unique ID for the table             | bigint                      | -             | -                                                                                                                                              |
| mobile       | Mobile number                       | character varying (96.0)    | -             | -                                                                                                                                              |
| remote_ip    | User IP address                     | character varying (96.0)    | -             | -                                                                                                                                              |
| status       | Status of SMS delivery              | character varying (96.0)    | -             | refer [here](https://support.twilio.com/hc/en-us/articles/223134347-What-are-the-Possible-SMS-and-MMS-Message-Statuses-and-What-do-They-Mean-) |
| type         | Type of SMS delivered               | character varying (48.0)    | -             | -                                                                                                                                              |



# station

|                | definition                                                      | data_type                   | related_key                  | remark                                           |
|:---------------|:----------------------------------------------------------------|:----------------------------|:-----------------------------|:-------------------------------------------------|
| city           | City in which station is at                                     | character varying (60.0)    | -                            | -                                                |
| created_at     | Timestamp of when row was created                               | timestamp without time zone | -                            | -                                                |
| deleted        | Flag if station is deleted                                      | smallint                    | -                            | -                                                |
| description    | Further description of station                                  | character varying (1500.0)  | -                            | -                                                |
| end_date       | -                                                               | character varying (60.0)    | -                            | -                                                |
| end_time       | -                                                               | character varying (30.0)    | -                            | -                                                |
| gps_radius     | Radius of GPS range (m)                                         | double precision            | -                            | -                                                |
| guide          | Deprecated                                                      | character varying (1536.0)  | -                            | -                                                |
| ibeacon_radius | Deprecated                                                      | double precision            | -                            | -                                                |
| id             | Unique IDs of station                                           | bigint                      | scooter_inventory.station_id | -                                                |
| latitude       | y-coordinate of station                                         | double precision            | -                            | -                                                |
| longitude      | x-coordinate station                                            | double precision            | -                            | -                                                |
| name           | Name of station                                                 | character varying (765.0)   | -                            | -                                                |
| optimal_number | Inactive                                                        | integer                     | -                            | -                                                |
| photo          | Deprecated                                                      | character varying (1500.0)  | -                            | -                                                |
| qr_code        | Station specific QR code that user can scan to park the scooter | character varying (60.0)    | -                            | Maintained by local ops team via admin dashboard |
| start_date     | -                                                               | character varying (60.0)    | -                            | -                                                |
| start_time     | -                                                               | character varying (30.0)    | -                            | -                                                |
| type           | Type of station                                                 | character varying (90.0)    | -                            | -                                                |
| updated_at     | Timestamp of when row was updated                               | timestamp without time zone | -                            | -                                                |
| vehicle_type   | -                                                               | character varying (96.0)    | -                            | -                                                |
| weekdays       | -                                                               | character varying (96.0)    | -                            | -                                                |
| zone           | Zone where station is located in                                | character varying (60.0)    | -                            | -                                                |



# station_images

|            | definition                        | data_type                   | related_key    | remark                                                                                        |
|:-----------|:----------------------------------|:----------------------------|:---------------|:----------------------------------------------------------------------------------------------|
| created_at | Timestamp of when row was created | timestamp without time zone | -              | -                                                                                             |
| id         | Unique ID for the table           | bigint                      | -              | -                                                                                             |
| image_id   | Unique image ID                   | character varying (192.0)   | neuron_file.id | -                                                                                             |
| sort       | To determine what is shown first  | smallint                    | -              | numerical numbers are assigned and are shown in ascending orders; only 1 is shown on user app |
| station_id | Unique station ID                 | bigint                      | station.id     | -                                                                                             |



# station_location_request

|            | definition                        | data_type                   | related_key   | remark   |
|:-----------|:----------------------------------|:----------------------------|:--------------|:---------|
| comment    | Comment made by requestor         | character varying (65535.0) | -             | -        |
| created_at | Timestamp of when row was created | timestamp without time zone | -             | -        |
| email      | email of requestor                | character varying (300.0)   | -             | -        |
| id         | Unique ID for the table           | bigint                      | -             | -        |
| latitude   | Latitude of suggested station     | double precision            | -             | -        |
| longitude  | Longitude of suggested station    | double precision            | -             | -        |
| name       | Name of requestor                 | character varying (300.0)   | -             | -        |



# station_optimal

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| capacity   | -            | integer                     | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| end_time   | -            | character varying (30.0)    | -             | -        |
| id         | -            | bigint                      | -             | -        |
| start_time | -            | character varying (30.0)    | -             | -        |
| station_id | -            | bigint                      | -             | -        |
| weekday    | -            | character varying (30.0)    | -             | -        |



# status

|                       | definition   | data_type                   | related_key   | remark   |
|:----------------------|:-------------|:----------------------------|:--------------|:---------|
| battery_percentage    | -            | double precision            | -             | -        |
| communication_failure | -            | smallint                    | -             | -        |
| controller_failure    | -            | smallint                    | -             | -        |
| created_at            | -            | timestamp without time zone | -             | -        |
| current               | -            | integer                     | -             | -        |
| device_id             | -            | bigint                      | -             | -        |
| id                    | -            | bigint                      | -             | -        |
| motor_phase           | -            | smallint                    | -             | -        |
| speed                 | -            | double precision            | -             | -        |
| turn_brushless        | -            | smallint                    | -             | -        |
| voltage               | -            | numeric                     | -             | -        |
| voltage_protection    | -            | smallint                    | -             | -        |



# stripe_exception_record

|              | definition                        | data_type                   | related_key     | remark                                                             |
|:-------------|:----------------------------------|:----------------------------|:----------------|:-------------------------------------------------------------------|
| charge_id    | Unique ID of Stripe charge        | character varying (384.0)   | -               | -                                                                  |
| created_at   | Timestamp of when row was created | timestamp without time zone | -               | -                                                                  |
| created_by   | Unique ID of user                 | bigint                      | user.id         | -                                                                  |
| decline_code | Decline reason                    | character varying (768.0)   | -               | -                                                                  |
| id           | Unique ID for the table           | bigint                      | -               | -                                                                  |
| message      | Text message of Stripe exception  | character varying (3072.0)  | -               | -                                                                  |
| order_id     | Unique ID of the order            | bigint                      | neuron_order.id | -                                                                  |
| pass_id      | Unique ID of pass used            | bigint                      | pass.id         | -                                                                  |
| pay_method   | Method of payment                 | character varying (96.0)    | -               | Refer to [possible values](https://stripe.com/docs/declines/codes) |
| platform     | Platform used                     | character varying (96.0)    | -               | -                                                                  |
| type         | Type of business action made      | character varying (48.0)    | -               | -                                                                  |
| user_id      | Unique ID of user                 | bigint                      | user.id         | -                                                                  |



# top_up_card

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| code        | -            | character varying (60.0)    | -             | -        |
| created_at  | -            | timestamp without time zone | -             | -        |
| created_by  | -            | bigint                      | -             | -        |
| id          | -            | bigint                      | -             | -        |
| redeemed_by | -            | bigint                      | -             | -        |
| status      | -            | character varying (48.0)    | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| value       | -            | numeric                     | -             | -        |



# trip

|                     | definition                                                                                                                                              | data_type                   | related_key                      | remark                                                                                             |
|:--------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|:----------------------------|:---------------------------------|:---------------------------------------------------------------------------------------------------|
| app_version         | App version of user                                                                                                                                     | character varying (60.0)    | -                                | -                                                                                                  |
| auto_end            | Flag if trip ended automatically, which happens when scooter is inactive for more than 10 minutes                                                       | smallint                    | -                                | yes = 1; no = 0                                                                                    |
| battery_no          | Unique number attached to the battery                                                                                                                   | character varying (192.0)   | battery_inventory.battery_number | -                                                                                                  |
| bt_unlock           | Flag if trip is unlocked by app bluetooth instead of 3G                                                                                                 | smallint                    | -                                | yes = 1; no = 0                                                                                    |
| campaign_id         | -                                                                                                                                                       | bigint                      | -                                | -                                                                                                  |
| city                | City where scooter trip is                                                                                                                              | character varying (60.0)    | -                                | -                                                                                                  |
| device_id           | Unique ID of scooter device                                                                                                                             | bigint                      | scooter_inventory.id             | -                                                                                                  |
| email               | User's email                                                                                                                                            | character varying (300.0)   | -                                | -                                                                                                  |
| end_latitude        | Trip end latitude                                                                                                                                       | double precision            | -                                | -                                                                                                  |
| end_longitude       | Trip end longitude                                                                                                                                      | double precision            | -                                | -                                                                                                  |
| end_no_riding       | User left scooter in no riding area                                                                                                                     | smallint                    | -                                | Scooter either force end by CS or auto_end                                                         |
| end_time            | Timestamp of end of trip                                                                                                                                | timestamp without time zone | -                                | -                                                                                                  |
| end_zone            | -                                                                                                                                                       | character varying (60.0)    | -                                | -                                                                                                  |
| engine_off_minutes  | Total minutes where the user was in the no riding   area.                                                                                               | integer                     | -                                | User is not charged during this period                                                             |
| force_end           | Flag if trip is forcefully ended, by customer service                                                                                                   | smallint                    | -                                | yes = 1; no = 0                                                                                    |
| gold_trip           | DEPRECATED                                                                                                                                              | smallint                    | -                                | yes = 1; no = 0                                                                                    |
| gps_id              | GPS ID attached to scooter                                                                                                                              | bigint                      | -                                | -                                                                                                  |
| group_id            | Unique group ID for group rides                                                                                                                         | character varying (192.0)   | -                                | -                                                                                                  |
| helmet_returned     | Flag if returned                                                                                                                                        | smallint                    | -                                | yes = 1; no = 0                                                                                    |
| helmet_wore         | Flag if helmet unlock successfully                                                                                                                      | smallint                    | -                                | yes = 1; no = null                                                                                 |
| id                  | Unique ID for the table                                                                                                                                 | bigint                      | *.trip_id                        | -                                                                                                  |
| imei                | Unique id of GPS device                                                                                                                                 | character varying (96.0)    | -                                | -                                                                                                  |
| is_short_trip       | (i) trip.min < 60 seconds and 1 short trip at most for 1 scooter in 24 hours, (ii) 5 short trips at most for 1 user in 24 hours (on different scooters) | smallint                    | -                                | yes = 1; no = 0                                                                                    |
| is_vip              | Flag if trip is taken by vip members                                                                                                                    | smallint                    | -                                | yes = 1; no = 0                                                                                    |
| mx_user_id          | DEPRECATED                                                                                                                                              | character varying (300.0)   | -                                | -                                                                                                  |
| positions           | -                                                                                                                                                       | character varying (65535.0) | -                                | -                                                                                                  |
| require_helmet      | Records intent to wear helmet                                                                                                                           | smallint                    | -                                | null = helmet not on scooter, 1 = user chose to unlock helmet, 0 = user chose not to unlock helmet |
| returned_station_id | Unique ID of returned station                                                                                                                           | bigint                      | station.id                       | -                                                                                                  |
| ride_mile           | -                                                                                                                                                       | bigint                      | -                                | -                                                                                                  |
| start_latitude      | Trip start latitude                                                                                                                                     | double precision            | -                                | -                                                                                                  |
| start_longitude     | Trip start longitude                                                                                                                                    | double precision            | -                                | -                                                                                                  |
| start_no_riding     | User starts trip in no riding area                                                                                                                      | smallint                    | -                                | Scooter status will change from NO_RIDING to IN_TRIP, ENGINE_OFF                                   |
| start_station_id    | Unique ID of start station                                                                                                                              | bigint                      | station.id                       | -                                                                                                  |
| start_time          | Timestamp of start of trip                                                                                                                              | timestamp without time zone | -                                | -                                                                                                  |
| start_zone          | -                                                                                                                                                       | character varying (60.0)    | -                                | -                                                                                                  |
| temp_lock           | Flag if scooter is temporarily locked by user (DEPRECATED as of user app version 4.6.0)                                                                 | smallint                    | -                                | yes = 1; no = 0                                                                                    |
| total_mileage       | Total mileage of trip (From scooter GPS, includes no riding/out of service area distance)                                                               | double precision            | -                                | Unit is Metres                                                                                     |
| total_minutes       | Total minutes of trip that is chargeable                                                                                                                | integer                     | -                                | -                                                                                                  |
| transfer_ride       | User starts a consecutive trip within a minute. Basic fee is not charged                                                                                | smallint                    | -                                | yes = 1; no = 0                                                                                    |
| trip_status         | Status of trip                                                                                                                                          | character varying (48.0)    | -                                | -                                                                                                  |
| user_id             | Unique ID of user                                                                                                                                       | bigint                      | user.id                          | -                                                                                                  |
| user_name           | User's username                                                                                                                                         | character varying (300.0)   | -                                | -                                                                                                  |
| vehicle_type        | Type of vehicle                                                                                                                                         | character varying (96.0)    | -                                | -                                                                                                  |
| zone                | Zone where user starts trip                                                                                                                             | character varying (60.0)    | -                                | -                                                                                                  |



# trip_alert

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| alert_time    | -            | timestamp without time zone | -             | -        |
| complete_time | -            | timestamp without time zone | -             | -        |
| duration      | -            | bigint                      | -             | -        |
| id            | -            | numeric                     | -             | -        |
| is_read       | -            | smallint                    | -             | -        |
| scooter_id    | -            | bigint                      | -             | -        |
| status        | -            | character varying (75.0)    | -             | -        |
| trip_id       | -            | bigint                      | -             | -        |
| type          | -            | character varying (75.0)    | -             | -        |
| user_id       | -            | bigint                      | -             | -        |



# trip_deposit

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| amount          | -            | double precision            | -             | -        |
| captured_amount | -            | double precision            | -             | -        |
| charge_id       | -            | character varying (192.0)   | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| id              | -            | bigint                      | -             | -        |
| job_id          | -            | character varying (96.0)    | -             | -        |
| status          | -            | character varying (96.0)    | -             | -        |
| trip_id         | -            | bigint                      | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |
| user_id         | -            | bigint                      | -             | -        |



# trip_end_record

|                     | definition                                       | data_type                   | related_key          | remark   |
|:--------------------|:-------------------------------------------------|:----------------------------|:---------------------|:---------|
| created_at          | Timestamp of when row was created                | timestamp without time zone | -                    | -        |
| gps_id              | Unique GPS ID                                    | bigint                      | gps_inventory.id     | -        |
| id                  | Unique ID of table                               | bigint                      | -                    | -        |
| return_station_id   | Scooter return location                          | bigint                      | -                    | -        |
| scooter_id          | Unique Scooter ID                                | bigint                      | scooter_inventory.id | -        |
| scooter_latitude    | Latitude of scooter's position                   | numeric                     | -                    | -        |
| scooter_longitude   | Longitude of scooter's position                  | numeric                     | -                    | -        |
| station_source_type | Trip finish source                               | character varying (90.0)    | -                    | -        |
| trip_id             | Unique Trip ID                                   | bigint                      | trip.id              | -        |
| user_gps_time       | Timestamp of when user's position   was recorded | timestamp without time zone | -                    | -        |
| user_id             | Unique user ID                                   | bigint                      | user.id              | -        |
| user_latitude       | Latitude of user's position                      | numeric                     | -                    | -        |
| user_longitude      | Longitude of user's position                     | numeric                     | -                    | -        |



# trip_error_record

|               | definition                        | data_type                   | related_key          | remark                                      |
|:--------------|:----------------------------------|:----------------------------|:---------------------|:--------------------------------------------|
| created_at    | Timestamp of when row was created | timestamp without time zone | -                    | -                                           |
| device_id     | Unique ID of scooter device       | bigint                      | scooter_inventory.id | -                                           |
| error_code    | Type of error                     | character varying (150.0)   | -                    | -                                           |
| error_message | Further information on error      | character varying (765.0)   | -                    | -                                           |
| id            | Unique ID for the table           | bigint                      | -                    | -                                           |
| station_id    | Unique ID of station              | bigint                      | station.id           | -                                           |
| trip_id       | Unique ID of trip                 | bigint                      | trip.id              | missing values to be fixed (as of 9/4/2020) |
| user_id       | Unique ID of user                 | bigint                      | user.id              | -                                           |



# trip_event_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| trip_event | -            | character varying (150.0)   | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| trip_info  | -            | character varying (765.0)   | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# trip_finish_source

|                    | definition                                                                    | data_type                   | related_key          | remark                                      |
|:-------------------|:------------------------------------------------------------------------------|:----------------------------|:---------------------|:--------------------------------------------|
| bluetooth_locked   | Flag if bluetooth is locked                                                   | smallint                    | -                    | This is a legacy feature, ignore this field |
| created_at         | Timestamp of when row was created                                             | timestamp without time zone | -                    | -                                           |
| device_active_time | -                                                                             | timestamp without time zone | -                    | -                                           |
| device_latitude    | y-coordinate of scooter gps                                                   | double precision            | -                    | -                                           |
| device_longitude   | x-coordinate of scooter gps                                                   | double precision            | -                    | -                                           |
| id                 | Unique ID for the table                                                       | bigint                      | -                    | -                                           |
| image_id           | Unique image ID taken by user                                                 | character varying (192.0)   | neuron_file.id       | -                                           |
| scooter_id         | Unique ID of scooter                                                          | bigint                      | scooter_inventory.id | -                                           |
| scooter_locked     | Flag if scooter is locked                                                     | smallint                    | -                    | yes = 1; no = 0                             |
| station_id         | Unique ID of station                                                          | bigint                      | station.id           | -                                           |
| station_latitude   | y-coordinate of station                                                       | double precision            | -                    | -                                           |
| station_longitude  | x-coordinate of station                                                       | double precision            | -                    | -                                           |
| trip_id            | Unique ID of trip                                                             | bigint                      | trip.id              | -                                           |
| type               | Type of end-trip record (how scooter trip's end was detected and recorded by) | character varying (150.0)   | -                    | -                                           |
| updated_at         | Timestamp of when row was updated                                             | timestamp without time zone | -                    | -                                           |
| user_gps_time      | Timestamp of user gps last update time                                        | timestamp without time zone | -                    | -                                           |
| user_id            | Unique ID of user                                                             | bigint                      | user.id              | -                                           |
| user_latitude      | y-coordinate of the user gps                                                  | double precision            | -                    | -                                           |
| user_longitude     | x-coordinate of the user gps                                                  | double precision            | -                    | -                                           |



# trip_helmet

|            | definition                               | data_type                   | related_key    | remark          |
|:-----------|:-----------------------------------------|:----------------------------|:---------------|:----------------|
| checked    | Whether it is checked and verified by CS | smallint                    | -              | yes = 1; no = 0 |
| city       | -                                        | character varying (48.0)    | -              | -               |
| created_at | Timestamp of when row was created        | timestamp without time zone | -              | -               |
| created_by | ID of user who uploaded the image        | bigint                      | user.id        | -               |
| id         | Unique ID for the table                  | bigint                      | -              | -               |
| image_id   | Unique image ID                          | character varying (192.0)   | neuron_file.id | -               |
| method     | -                                        | character varying (192.0)   | -              | -               |
| trip_id    | Unique trip ID                           | bigint                      | trip.id        | -               |
| updated_at | Timestamp of when row was updated        | timestamp without time zone | -              | -               |
| updated_by | ID of CS admin who verified the image    | bigint                      | -              | -               |



# trip_parking_photo

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| check_answers | -            | character varying (1500.0)  | -             | -        |
| checked       | -            | smallint                    | -             | -        |
| city          | -            | character varying (60.0)    | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| duration      | -            | bigint                      | -             | -        |
| id            | -            | bigint                      | -             | -        |
| image_id      | -            | character varying (192.0)   | -             | -        |
| reminded      | -            | smallint                    | -             | -        |
| status        | -            | character varying (60.0)    | -             | -        |
| trip_id       | -            | bigint                      | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| user_id       | -            | bigint                      | -             | -        |
| vehicle_id    | -            | bigint                      | -             | -        |



# trip_quartz_checker

|                          | definition   | data_type                   | related_key   | remark   |
|:-------------------------|:-------------|:----------------------------|:--------------|:---------|
| auto_end_job_id          | -            | character varying (600.0)   | -             | -        |
| created_at               | -            | timestamp without time zone | -             | -        |
| dangerous_driving_job_id | -            | character varying (600.0)   | -             | -        |
| id                       | -            | bigint                      | -             | -        |
| sidewalk_job_id          | -            | character varying (600.0)   | -             | -        |
| trip_id                  | -            | bigint                      | -             | -        |
| user_id                  | -            | bigint                      | -             | -        |



# trip_rating

|             | definition                        | data_type                   | related_key   | remark         |
|:------------|:----------------------------------|:----------------------------|:--------------|:---------------|
| app_version | User's app version                | character varying (48.0)    | -             | -              |
| comment     | User's comment                    | character varying (6000.0)  | -             | -              |
| created_at  | Timestamp of when row was created | timestamp without time zone | -             | -              |
| id          | Unique ID for the table           | bigint                      | -             | -              |
| rating      | Rating given by user              | integer                     | -             | -              |
| system      | User's System                     | character varying (48.0)    | -             | IOS or Android |
| trip_id     | Unique trip ID                    | bigint                      | trip.id       | -              |
| user_id     | Unique user ID                    | bigint                      | user.id       | -              |



# trip_report

|                       | definition   | data_type                   | related_key   | remark   |
|:----------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at            | -            | timestamp without time zone | -             | -        |
| id                    | -            | bigint                      | -             | -        |
| is_viewed             | -            | smallint                    | -             | -        |
| trip_helmet_id        | -            | bigint                      | -             | -        |
| trip_id               | -            | bigint                      | -             | -        |
| trip_parking_photo_id | -            | bigint                      | -             | -        |
| trip_sidewalk_id      | -            | bigint                      | -             | -        |
| updated_at            | -            | timestamp without time zone | -             | -        |
| user_id               | -            | bigint                      | -             | -        |



# trip_rider_info

|                        | definition   | data_type                   | related_key   | remark   |
|:-----------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at             | -            | timestamp without time zone | -             | -        |
| driver_license         | -            | character varying (768.0)   | -             | -        |
| driver_license_country | -            | character varying (48.0)    | -             | -        |
| email                  | -            | character varying (384.0)   | -             | -        |
| first_name             | -            | character varying (192.0)   | -             | -        |
| full_name              | -            | character varying (65535.0) | -             | -        |
| id                     | -            | bigint                      | -             | -        |
| is_account_holder      | -            | smallint                    | -             | -        |
| last_name              | -            | character varying (192.0)   | -             | -        |
| phone                  | -            | character varying (48.0)    | -             | -        |
| trip_id                | -            | bigint                      | -             | -        |



# trip_section

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| end_time   | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| mileage    | -            | double precision            | -             | -        |
| start_time | -            | timestamp without time zone | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |



# trip_sidewalk

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| result     | -            | smallint                    | -             | -        |
| source     | -            | character varying (192.0)   | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# trip_trace

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| id            | -            | bigint                      | -             | -        |
| positions     | -            | character varying (65535.0) | -             | -        |
| raw_positions | -            | character varying (64512.0) | -             | -        |
| trip_id       | -            | bigint                      | -             | -        |



# user_card

|             | definition                        | data_type                   | related_key   | remark          |
|:------------|:----------------------------------|:----------------------------|:--------------|:----------------|
| created_at  | Timestamp of when row was created | timestamp without time zone | -             | -               |
| deleted     | Card deleted from app             | smallint                    | -             | yes = 1; no = 0 |
| fingerprint | Card's fingerprint                | character varying (192.0)   | -             | -               |
| id          | Unique ID for the table           | bigint                      | -             | -               |
| updated_at  | Timestamp of when row was updated | timestamp without time zone | -             | -               |
| updated_by  | User ID that updated row          | character varying (192.0)   | user.id       | -               |
| user_id     | Unique user ID                    | bigint                      | user.id       | -               |
| verified    | Card verified by CS               | smallint                    | -             | yes = 1; no = 0 |



# user_city_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (60.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_concession_evidence

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| audited_at    | -            | timestamp without time zone | -             | -        |
| audited_by    | -            | bigint                      | -             | -        |
| check_answers | -            | character varying (1500.0)  | -             | -        |
| city          | -            | character varying (60.0)    | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| full_name     | -            | character varying (150.0)   | -             | -        |
| id            | -            | bigint                      | -             | -        |
| remark        | -            | character varying (1500.0)  | -             | -        |
| status        | -            | character varying (60.0)    | -             | -        |
| type          | -            | character varying (150.0)   | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| user_id       | -            | bigint                      | -             | -        |



# user_concession_evidence_image

|                        | definition   | data_type                 | related_key   | remark   |
|:-----------------------|:-------------|:--------------------------|:--------------|:---------|
| concession_evidence_id | -            | bigint                    | -             | -        |
| id                     | -            | bigint                    | -             | -        |
| image_id               | -            | character varying (192.0) | -             | -        |



# user_concession_evidence_request

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (60.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| full_name  | -            | character varying (150.0)   | -             | -        |
| id         | -            | bigint                      | -             | -        |
| type       | -            | character varying (150.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| updated_by | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_coupon

|               | definition                          | data_type                   | related_key   | remark                                                                                                                                                   |
|:--------------|:------------------------------------|:----------------------------|:--------------|:---------------------------------------------------------------------------------------------------------------------------------------------------------|
| balance       | Balance of coupon                   | numeric                     | -             | -                                                                                                                                                        |
| code          | Code for promotion                  | character varying (768.0)   | -             | -                                                                                                                                                        |
| created_at    | Timestamp of when row was created   | timestamp without time zone | -             | -                                                                                                                                                        |
| created_by    | Unique ID of user                   | bigint                      | user.id       | -                                                                                                                                                        |
| currency      | Currency used                       | character varying (48.0)    | -             | -                                                                                                                                                        |
| discount      | -                                   | smallint                    | -             | -                                                                                                                                                        |
| expired_at    | Date when coupon expires            | timestamp without time zone | -             | -                                                                                                                                                        |
| id            | Unique ID for the table             | bigint                      | -             | -                                                                                                                                                        |
| is_read       | -                                   | smallint                    | -             | -                                                                                                                                                        |
| pass_days     | -                                   | smallint                    | -             | -                                                                                                                                                        |
| pass_id       | -                                   | bigint                      | -             | -                                                                                                                                                        |
| promo_code_id | Unique ID of promo_code             | bigint                      | promo_code.id | -                                                                                                                                                        |
| scope         | Deprecated                          | character varying (45.0)    | -             | -                                                                                                                                                        |
| source        | Source of code                      | character varying (768.0)   | -             | -                                                                                                                                                        |
| status        | Status of coupon                    | character varying (192.0)   | -             | -                                                                                                                                                        |
| updated_at    | Timestamp of when row was updated   | timestamp without time zone | -             | -                                                                                                                                                        |
| updated_by    | Unique ID of user                   | bigint                      | user.id       | -                                                                                                                                                        |
| user_id       | Unique ID of user (coupon receiver) | bigint                      | user.id       | source=‘REFER_SENDER’,The user id is who share their refer code out;source=‘REFER_RECEIVER’, user id is the new user who was invited by the step 1 user. |



# user_device_info

|             | definition                        | data_type                   | related_key   | remark   |
|:------------|:----------------------------------|:----------------------------|:--------------|:---------|
| app_version | App version used                  | character varying (90.0)    | -             | -        |
| brand       | Brand of user device              | character varying (150.0)   | -             | -        |
| created_at  | Timestamp of when row was created | timestamp without time zone | -             | -        |
| device_id   | Unique ID of user device          | character varying (192.0)   | -             | -        |
| id          | Unique ID for the table           | bigint                      | -             | -        |
| lang        | Language of user device           | character varying (30.0)    | -             | -        |
| model       | Model of user device              | character varying (150.0)   | -             | -        |
| sys_version | Version of user device system     | character varying (90.0)    | -             | -        |
| system      | System of user device             | character varying (60.0)    | -             | -        |
| type        | Account permission type           | character varying (90.0)    | -             | -        |
| updated_at  | Timestamp of when row was updated | timestamp without time zone | -             | -        |
| user_id     | Unique ID of user                 | bigint                      | user.id       | -        |



# user_device_record

|            | definition                        | data_type                   | related_key                | remark   |
|:-----------|:----------------------------------|:----------------------------|:---------------------------|:---------|
| created_at | Timestamp of when row was created | timestamp without time zone | -                          | -        |
| device_id  | Unique ID of user's device        | character varying (384.0)   | user_device_info.device_id | -        |
| id         | Unique ID for the table           | bigint                      | -                          | -        |
| platform   | -                                 | character varying (96.0)    | -                          | -        |
| user_id    | Unique ID of the user             | bigint                      | user.id                    | -        |



# user_gps_tracing

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| accuracy   | -            | numeric                     | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| fixed_time | -            | timestamp without time zone | -             | -        |
| id         | -            | numeric                     | -             | -        |
| latitude   | -            | numeric                     | -             | -        |
| longitude  | -            | numeric                     | -             | -        |
| speed      | -            | numeric                     | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_identification

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| id            | -            | bigint                      | -             | -        |
| issue_country | -            | character varying (96.0)    | -             | -        |
| type          | -            | character varying (96.0)    | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| user_id       | -            | bigint                      | -             | -        |



# user_identification_file

|                        | definition   | data_type                 | related_key   | remark   |
|:-----------------------|:-------------|:--------------------------|:--------------|:---------|
| file_id                | -            | character varying (384.0) | -             | -        |
| file_type              | -            | character varying (96.0)  | -             | -        |
| id                     | -            | bigint                    | -             | -        |
| user_identification_id | -            | bigint                    | -             | -        |



# user_issue_notification_config

|            | definition              | data_type                   | related_key   | remark   |
|:-----------|:------------------------|:----------------------------|:--------------|:---------|
| config     | Preference config       | character varying (3000.0)  | -             | -        |
| id         | Unique ID for the table | bigint                      | -             | -        |
| mute_date  | -                       | timestamp without time zone | -             | -        |
| mute_type  | -                       | character varying (60.0)    | -             | -        |
| updated_at | -                       | timestamp without time zone | -             | -        |
| user_id    | Unique user ID          | bigint                      | user.id       | -        |



# user_license

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at      | -            | timestamp without time zone | -             | -        |
| data            | -            | character varying (65535.0) | -             | -        |
| file_id         | -            | character varying (192.0)   | -             | -        |
| first_name      | -            | character varying (192.0)   | -             | -        |
| full_name       | -            | character varying (65535.0) | -             | -        |
| id              | -            | bigint                      | -             | -        |
| last_name       | -            | character varying (192.0)   | -             | -        |
| licence_country | -            | character varying (96.0)    | -             | -        |
| licence_number  | -            | character varying (192.0)   | -             | -        |
| type            | -            | character varying (48.0)    | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |
| user_id         | -            | bigint                      | -             | -        |
| verified        | -            | smallint                    | -             | -        |



# user_license_scan_record

|                    | definition   | data_type                   | related_key   | remark   |
|:-------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at         | -            | timestamp without time zone | -             | -        |
| data               | -            | character varying (65535.0) | -             | -        |
| encrypted_raw_data | -            | character varying (65535.0) | -             | -        |
| full_name          | -            | character varying (765.0)   | -             | -        |
| id                 | -            | bigint                      | -             | -        |
| license_country    | -            | character varying (60.0)    | -             | -        |
| license_number     | -            | character varying (150.0)   | -             | -        |
| scan_result_detail | -            | character varying (65535.0) | -             | -        |
| scan_status        | -            | character varying (60.0)    | -             | -        |
| user_id            | -            | bigint                      | -             | -        |



# user_location_reader

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| browser         | -            | character varying (3072.0)  | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| id              | -            | bigint                      | -             | -        |
| ip              | -            | character varying (192.0)   | -             | -        |
| share_record_id | -            | bigint                      | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |



# user_location_share

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| created_at   | -            | timestamp without time zone | -             | -        |
| id           | -            | bigint                      | -             | -        |
| share_status | -            | smallint                    | -             | -        |
| token        | -            | character varying (192.0)   | -             | -        |
| trip_id      | -            | bigint                      | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| user_id      | -            | bigint                      | -             | -        |



# user_mpu

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at      | -            | timestamp without time zone | -             | -        |
| device_time     | -            | timestamp without time zone | -             | -        |
| id              | -            | bigint                      | -             | -        |
| sensor_time     | -            | bigint                      | -             | -        |
| user_id         | -            | bigint                      | -             | -        |
| x_acceleration  | -            | double precision            | -             | -        |
| x_rotation_rate | -            | double precision            | -             | -        |
| y_acceleration  | -            | double precision            | -             | -        |
| y_rotation_rate | -            | double precision            | -             | -        |
| z_acceleration  | -            | double precision            | -             | -        |
| z_rotation_rate | -            | double precision            | -             | -        |



# user_notification

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| description  | -            | character varying (3072.0)  | -             | -        |
| id           | -            | bigint                      | -             | -        |
| name         | -            | character varying (384.0)   | -             | -        |
| trigger_corn | -            | character varying (96.0)    | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| updated_by   | -            | bigint                      | -             | -        |



# user_pass

|                          | definition                                                 | data_type                   | related_key               | remark                                                |
|:-------------------------|:-----------------------------------------------------------|:----------------------------|:--------------------------|:------------------------------------------------------|
| created_at               | Timestamp of when row was created                          | timestamp without time zone | -                         | -                                                     |
| created_by               | Unique ID of the person who created the record             | bigint                      | -                         | -                                                     |
| expired_at               | Timestamp of when the pass expired                         | timestamp without time zone | -                         | -                                                     |
| future_pass_id           | Future pass type (if any)                                  | bigint                      | -                         | -                                                     |
| future_subscribe         | Flag if auto renew the future pass                         | smallint                    | -                         | yes = 1; no = 0                                       |
| id                       | Unique pass ID assigned                                    | bigint                      | order_detail.user_pass_id | -                                                     |
| job_id                   | Internal cron job ID                                       | character varying (192.0)   | -                         | -                                                     |
| organization_discount_id | Organization that user is afiliated to (if any)            | integer                     | organization_discount.id  | -                                                     |
| pass_id                  | ID based on pass type                                      | bigint                      | pass.id                   | -                                                     |
| purchase_city            | -                                                          | character varying (96.0)    | -                         | -                                                     |
| purchase_price           | Amount that user paid                                      | numeric                     | -                         | -                                                     |
| refund                   | Refund given                                               | numeric                     | -                         | -                                                     |
| started_at               | Timestamp of when the pass started                         | timestamp without time zone | -                         | -                                                     |
| subscribe                | Flag if user wants to auto-renew the current active   pass | smallint                    | -                         | yes = 1; no = 0                                       |
| updated_at               | Timestamp of when row was updated                          | timestamp without time zone | -                         | Can be triggered by auto-renew selection or CS refund |
| updated_by               | Unique ID of the person who updated the record             | bigint                      | -                         | -                                                     |
| user_coupon_id           | -                                                          | bigint                      | -                         | -                                                     |
| user_id                  | Unique ID of user                                          | bigint                      | -                         | -                                                     |



# user_pass_period

|              | definition                               | data_type                   | related_key   | remark   |
|:-------------|:-----------------------------------------|:----------------------------|:--------------|:---------|
| balance      | Minutes left (per day)                   | integer                     | -             | -        |
| created_at   | Timestamp of when row was created        | timestamp without time zone | -             | -        |
| end_time     | The ending time of that 24 hour period   | timestamp without time zone | -             | -        |
| id           | Unique ID for the table                  | bigint                      | -             | -        |
| start_time   | The starting time of that 24 hour period | timestamp without time zone | -             | -        |
| updated_at   | Timestamp of when row was updated        | timestamp without time zone | -             | -        |
| user_pass_id | ID in user_pass table                    | bigint                      | user_pass.id  | -        |



# user_promo_coupon

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| code          | -            | character varying (30.0)    | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| email         | -            | character varying (765.0)   | -             | -        |
| id            | -            | bigint                      | -             | -        |
| promo_code_id | -            | bigint                      | -             | -        |
| reaction_time | -            | double precision            | -             | -        |
| scope         | -            | character varying (60.0)    | -             | -        |
| type          | -            | character varying (60.0)    | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| user_id       | -            | bigint                      | -             | -        |
| value         | -            | numeric                     | -             | -        |



# user_referral

|             | definition                         | data_type                   | related_key   | remark   |
|:------------|:-----------------------------------|:----------------------------|:--------------|:---------|
| code        | Referral code                      | character varying (48.0)    | -             | -        |
| created_at  | Timestamp of when row was created  | timestamp without time zone | -             | -        |
| id          | Unique ID for the table            | bigint                      | -             | -        |
| referrer_id | Unique ID of referrer              | bigint                      | user.id       | -        |
| updated_at  | Timestamp of when row was updated  | timestamp without time zone | -             | -        |
| user_id     | Unique ID of user who was referred | bigint                      | user.id       | -        |



# user_setting

|                      | definition   | data_type                   | related_key   | remark   |
|:---------------------|:-------------|:----------------------------|:--------------|:---------|
| beginner_mode        | -            | smallint                    | -             | -        |
| created_at           | -            | timestamp without time zone | -             | -        |
| has_finished_trip    | -            | smallint                    | -             | -        |
| id                   | -            | bigint                      | -             | -        |
| updated_at           | -            | timestamp without time zone | -             | -        |
| user_agree_insurance | -            | smallint                    | -             | -        |
| user_agree_pic       | -            | smallint                    | -             | -        |
| user_agree_tos       | -            | smallint                    | -             | -        |
| user_id              | -            | bigint                      | -             | -        |



# user_status_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| status     | -            | character varying (150.0)   | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# wallet

|            | definition                        | data_type                   | related_key   | remark   |
|:-----------|:----------------------------------|:----------------------------|:--------------|:---------|
| balance    | DEPRECATED                        | numeric                     | -             | -        |
| cash       | Cash balance                      | numeric                     | -             | -        |
| created_at | Timestamp of when row was created | timestamp without time zone | -             | -        |
| credit     | Credit balance                    | numeric                     | -             | -        |
| currency   | Currency used                     | character varying (48.0)    | -             | -        |
| id         | Unique ID for the table           | bigint                      | -             | -        |
| updated_at | Timestamp of when row was updated | timestamp without time zone | -             | -        |
| user_id    | Unique ID of user                 | bigint                      | user.id       | -        |



# zone

|            | definition                        | data_type                   | related_key            | remark          |
|:-----------|:----------------------------------|:----------------------------|:-----------------------|:----------------|
| base_fee   | Base price                        | numeric                     | -                      | -               |
| city_code  | City zone is located in           | character varying (60.0)    | -                      | -               |
| code       | Zone code                         | character varying (60.0)    | scooter_inventory.zone | -               |
| created_at | Timestamp of when row was created | timestamp without time zone | -                      | -               |
| created_by | Unique ID of user who created     | bigint                      | user.id                | -               |
| deleted    | Flag if zone is deleted           | smallint                    | -                      | yes = 1; no = 0 |
| name       | Name of zone                      | character varying (765.0)   | -                      | -               |
| unit_fee   | Unit price                        | numeric                     | -                      | -               |
| updated_at | Timestamp of when row was updated | timestamp without time zone | -                      | -               |
| updated_by | Unique ID of user who updated     | bigint                      | user.id                | -               |

