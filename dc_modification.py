import dc_mainFunctions as Main


# this script is to copy the past catalog (new) to the current one (ori)
# the output includes a json and a markdown file

def update_dc():
    ori     = 'catalog-neuron_user-editor.json'
    new     = 'past_neuron_user.csv'
    json    = 'catalog-neuron_user-rujin.json'
    md      = 'catalog-neuron_user-rujin.md'

    # ori     = 'catalog-neuron_fiveCountries-editor.json'
    # new     = 'past_neuron_australia-and-neuron_newzealand.csv'
    # json    = 'catalog-neuron_fiveCountries-rujin.json'
    # md      = 'catalog-neuron_fiveCountries-rujin.md'

    Main.update_catalog(ori, new, json, md)


update_dc()
