[TOC]

# active_scooter_location

|                      | definition   | data_type                   | related_key   | remark   |
|:---------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at           | -            | timestamp without time zone | -             | -        |
| id                   | -            | bigint                      | -             | -        |
| imei                 | -            | character varying (96.0)    | -             | -        |
| is_no_parking        | -            | smallint                    | -             | -        |
| is_no_riding         | -            | smallint                    | -             | -        |
| is_outside_service   | -            | smallint                    | -             | -        |
| last_no_parking      | -            | bigint                      | -             | -        |
| last_no_riding       | -            | bigint                      | -             | -        |
| last_outside_service | -            | bigint                      | -             | -        |
| latitude             | -            | double precision            | -             | -        |
| longitude            | -            | double precision            | -             | -        |
| trip_id              | -            | bigint                      | -             | -        |
| updated_at           | -            | timestamp without time zone | -             | -        |
| user_id              | -            | bigint                      | -             | -        |



# activity

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at        | -            | timestamp without time zone | -             | -        |
| description       | -            | character varying (600.0)   | -             | -        |
| id                | -            | bigint                      | -             | -        |
| last_active_at    | -            | timestamp without time zone | -             | -        |
| related_device_id | -            | bigint                      | -             | -        |
| type              | -            | character varying (150.0)   | -             | -        |



# alarm_status_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| alarm_on   | -            | smallint                    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |



# app_config

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| data       | -            | character varying (1500.0)  | -             | -        |
| name       | -            | character varying (150.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# app_version

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| created_at   | -            | timestamp without time zone | -             | -        |
| force_update | -            | smallint                    | -             | -        |
| id           | -            | bigint                      | -             | -        |
| platform     | -            | character varying (150.0)   | -             | -        |
| update_log   | -            | character varying (765.0)   | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| version_code | -            | integer                     | -             | -        |
| version_name | -            | character varying (150.0)   | -             | -        |



# attractions_feedback

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| comment    | -            | character varying (900.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| rating     | -            | smallint                    | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# battery_info_record

|                               | definition   | data_type                   | related_key   | remark   |
|:------------------------------|:-------------|:----------------------------|:--------------|:---------|
| battery_number                | -            | character varying (192.0)   | -             | -        |
| bms_hardware_version          | -            | integer                     | -             | -        |
| bms_software_version          | -            | integer                     | -             | -        |
| charge_mos                    | -            | smallint                    | -             | -        |
| charge_overcurrent_protect    | -            | smallint                    | -             | -        |
| charge_overtemp_protect       | -            | smallint                    | -             | -        |
| charge_overtemp_warn          | -            | smallint                    | -             | -        |
| charge_undertemp_protect      | -            | smallint                    | -             | -        |
| charge_undertemp_warn         | -            | smallint                    | -             | -        |
| charger_connected             | -            | smallint                    | -             | -        |
| charging                      | -            | smallint                    | -             | -        |
| created_at                    | -            | timestamp without time zone | -             | -        |
| discharge_lowtemp_warn        | -            | smallint                    | -             | -        |
| discharge_mos                 | -            | smallint                    | -             | -        |
| discharge_overcurrent_protect | -            | smallint                    | -             | -        |
| discharge_overtemp_protect    | -            | smallint                    | -             | -        |
| discharge_overtemp_warn       | -            | smallint                    | -             | -        |
| discharge_undertemp_protect   | -            | smallint                    | -             | -        |
| discharging                   | -            | smallint                    | -             | -        |
| full_capacity                 | -            | integer                     | -             | -        |
| gps_id                        | -            | bigint                      | -             | -        |
| id                            | -            | bigint                      | -             | -        |
| milli_current                 | -            | integer                     | -             | -        |
| milli_voltage                 | -            | integer                     | -             | -        |
| overvoltage_protect           | -            | smallint                    | -             | -        |
| overvoltage_warn              | -            | smallint                    | -             | -        |
| powerlow_warn                 | -            | smallint                    | -             | -        |
| remaining_capacity            | -            | integer                     | -             | -        |
| remaining_percentage          | -            | integer                     | -             | -        |
| shortout                      | -            | smallint                    | -             | -        |
| temperature_1                 | -            | integer                     | -             | -        |
| temperature_2                 | -            | integer                     | -             | -        |
| undervoltage_protect          | -            | smallint                    | -             | -        |
| voltage_1                     | -            | integer                     | -             | -        |
| voltage_10                    | -            | integer                     | -             | -        |
| voltage_2                     | -            | integer                     | -             | -        |
| voltage_3                     | -            | integer                     | -             | -        |
| voltage_4                     | -            | integer                     | -             | -        |
| voltage_5                     | -            | integer                     | -             | -        |
| voltage_6                     | -            | integer                     | -             | -        |
| voltage_7                     | -            | integer                     | -             | -        |
| voltage_8                     | -            | integer                     | -             | -        |
| voltage_9                     | -            | integer                     | -             | -        |
| work_cycle                    | -            | integer                     | -             | -        |
| working_status                | -            | smallint                    | -             | -        |



# battery_inspection

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| battery_number | -            | character varying (90.0)    | -             | -        |
| city           | -            | character varying (96.0)    | -             | -        |
| created_at     | -            | timestamp without time zone | -             | -        |
| id             | -            | integer                     | -             | -        |
| status         | -            | character varying (60.0)    | -             | -        |
| user_id        | -            | bigint                      | -             | -        |



# battery_inventory

|                    | definition   | data_type                   | related_key   | remark   |
|:-------------------|:-------------|:----------------------------|:--------------|:---------|
| battery_number     | -            | character varying (192.0)   | -             | -        |
| city               | -            | character varying (60.0)    | -             | -        |
| created_at         | -            | timestamp without time zone | -             | -        |
| electricity        | -            | double precision            | -             | -        |
| full_capacity      | -            | smallint                    | -             | -        |
| id                 | -            | bigint                      | -             | -        |
| remaining_capacity | -            | smallint                    | -             | -        |
| scooter_id         | -            | bigint                      | -             | -        |
| status             | -            | character varying (90.0)    | -             | -        |
| updated_at         | -            | timestamp without time zone | -             | -        |
| work_cycle         | -            | smallint                    | -             | -        |



# battery_status_record

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| battery_no      | -            | character varying (150.0)   | -             | -        |
| city            | -            | character varying (96.0)    | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| id              | -            | bigint                      | -             | -        |
| previous_city   | -            | character varying (96.0)    | -             | -        |
| previous_status | -            | character varying (150.0)   | -             | -        |
| status          | -            | character varying (150.0)   | -             | -        |



# battery_swap

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| battery_in_level  | -            | numeric                     | -             | -        |
| battery_in_no     | -            | character varying (192.0)   | -             | -        |
| battery_out_level | -            | numeric                     | -             | -        |
| battery_out_no    | -            | character varying (192.0)   | -             | -        |
| battery_swap_time | -            | timestamp without time zone | -             | -        |
| city              | -            | character varying (96.0)    | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| id                | -            | bigint                      | -             | -        |
| latitude          | -            | numeric                     | -             | -        |
| longitude         | -            | numeric                     | -             | -        |
| scooter_id        | -            | bigint                      | -             | -        |
| user_id           | -            | bigint                      | -             | -        |
| vehicle_type      | -            | character varying (96.0)    | -             | -        |



# battery_swap_history

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| battery_no      | -            | character varying (192.0)   | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| current_battery | -            | double precision            | -             | -        |
| id              | -            | bigint                      | -             | -        |
| scooter_id      | -            | bigint                      | -             | -        |
| status          | -            | character varying (60.0)    | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |
| user_id         | -            | bigint                      | -             | -        |



# blacklist

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| ip         | -            | character varying (192.0)   | -             | -        |



# campaign

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| active          | -            | smallint                    | -             | -        |
| base_fee        | -            | double precision            | -             | -        |
| city            | -            | character varying (765.0)   | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| created_by      | -            | bigint                      | -             | -        |
| deleted         | -            | smallint                    | -             | -        |
| end_date        | -            | character varying (48.0)    | -             | -        |
| end_time        | -            | character varying (48.0)    | -             | -        |
| end_timestamp   | -            | timestamp without time zone | -             | -        |
| id              | -            | bigint                      | -             | -        |
| name            | -            | character varying (765.0)   | -             | -        |
| start_date      | -            | character varying (48.0)    | -             | -        |
| start_time      | -            | character varying (48.0)    | -             | -        |
| start_timestamp | -            | timestamp without time zone | -             | -        |
| type            | -            | character varying (48.0)    | -             | -        |
| unit_fee        | -            | double precision            | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |
| updated_by      | -            | bigint                      | -             | -        |
| vehicle_type    | -            | character varying (96.0)    | -             | -        |
| weekdays        | -            | character varying (48.0)    | -             | -        |



# campaign_pass

|         | definition   | data_type                 | related_key   | remark   |
|:--------|:-------------|:--------------------------|:--------------|:---------|
| id      | -            | bigint                    | -             | -        |
| pass_id | -            | bigint                    | -             | -        |
| price   | -            | double precision          | -             | -        |
| source  | -            | character varying (150.0) | -             | -        |



# campaign_trip

|          | definition   | data_type        | related_key   | remark   |
|:---------|:-------------|:-----------------|:--------------|:---------|
| base_fee | -            | double precision | -             | -        |
| id       | -            | bigint           | -             | -        |
| unit_fee | -            | double precision | -             | -        |



# cmd_task

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| cmd_list   | -            | character varying (64512.0) | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| deleted    | -            | smallint                    | -             | -        |
| id         | -            | bigint                      | -             | -        |
| rule       | -            | character varying (600.0)   | -             | -        |
| task_type  | -            | character varying (60.0)    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| valid_time | -            | timestamp without time zone | -             | -        |



# command

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| content    | -            | character varying (600.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| name       | -            | character varying (150.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# command_log

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| command    | -            | character varying (3072.0)  | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| succeed    | -            | smallint                    | -             | -        |



# command_response

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| message    | -            | character varying (765.0)   | -             | -        |



# coupon_task_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| coupon_id  | -            | bigint                      | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | character varying (192.0)   | -             | -        |
| remark     | -            | character varying (3072.0)  | -             | -        |
| type       | -            | character varying (96.0)    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# deposit

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| amount     | -            | double precision            | -             | -        |
| charge_id  | -            | character varying (300.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| refunded   | -            | smallint                    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# device_blacklist

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| comment    | -            | character varying (768.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| device_id  | -            | character varying (192.0)   | -             | -        |
| id         | -            | bigint                      | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| updated_by | -            | bigint                      | -             | -        |



# dft_quiz_result

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| quiz_id    | -            | smallint                    | -             | -        |
| response   | -            | character varying (300.0)   | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# email_verification

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| email      | -            | character varying (765.0)   | -             | -        |
| id         | -            | bigint                      | -             | -        |
| status     | -            | character varying (60.0)    | -             | -        |
| token      | -            | character varying (150.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# feedback

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (60.0)    | -             | -        |
| content    | -            | character varying (65535.0) | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| status     | -            | character varying (150.0)   | -             | -        |
| subject    | -            | character varying (150.0)   | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# flyway_schema_history

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| checksum       | -            | integer                     | -             | -        |
| description    | -            | character varying (600.0)   | -             | -        |
| execution_time | -            | integer                     | -             | -        |
| installed_by   | -            | character varying (300.0)   | -             | -        |
| installed_on   | -            | timestamp without time zone | -             | -        |
| installed_rank | -            | integer                     | -             | -        |
| script         | -            | character varying (3000.0)  | -             | -        |
| success        | -            | smallint                    | -             | -        |
| type           | -            | character varying (60.0)    | -             | -        |
| version        | -            | character varying (150.0)   | -             | -        |



# geofence

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| city            | -            | character varying (60.0)    | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| description     | -            | character varying (65535.0) | -             | -        |
| end_date        | -            | character varying (60.0)    | -             | -        |
| end_time        | -            | character varying (30.0)    | -             | -        |
| hidden          | -            | smallint                    | -             | -        |
| id              | -            | bigint                      | -             | -        |
| max_speed       | -            | integer                     | -             | -        |
| name            | -            | character varying (450.0)   | -             | -        |
| sidewalk_action | -            | integer                     | -             | -        |
| start_date      | -            | character varying (60.0)    | -             | -        |
| start_time      | -            | character varying (30.0)    | -             | -        |
| station_id      | -            | bigint                      | -             | -        |
| status          | -            | character varying (60.0)    | -             | -        |
| type            | -            | character varying (96.0)    | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |
| vehicle_type    | -            | character varying (96.0)    | -             | -        |
| weekdays        | -            | character varying (96.0)    | -             | -        |
| zone            | -            | character varying (60.0)    | -             | -        |



# geofence_alert

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at        | -            | timestamp without time zone | -             | -        |
| geo_trigger_event | -            | character varying (300.0)   | -             | -        |
| geofence_id       | -            | bigint                      | -             | -        |
| id                | -            | bigint                      | -             | -        |
| imei              | -            | character varying (96.0)    | -             | -        |
| source            | -            | character varying (30.0)    | -             | -        |
| status            | -            | character varying (48.0)    | -             | -        |
| trip_id           | -            | bigint                      | -             | -        |
| updated_at        | -            | timestamp without time zone | -             | -        |
| user_id           | -            | bigint                      | -             | -        |



# geofence_position

|           | definition   | data_type                | related_key   | remark   |
|:----------|:-------------|:-------------------------|:--------------|:---------|
| fence_id  | -            | bigint                   | -             | -        |
| id        | -            | numeric                  | -             | -        |
| latitude  | -            | numeric                  | -             | -        |
| longitude | -            | numeric                  | -             | -        |
| type      | -            | character varying (48.0) | -             | -        |



# getui_user_client

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| client_id  | -            | character varying (1536.0)  | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| type       | -            | character varying (48.0)    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# gps_brake

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| brake      | -            | smallint                    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| gps_id     | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |



# gps_inventory

|                      | definition   | data_type                   | related_key   | remark   |
|:---------------------|:-------------|:----------------------------|:--------------|:---------|
| alarm_on             | -            | smallint                    | -             | -        |
| apn                  | -            | character varying (300.0)   | -             | -        |
| blt_password         | -            | character varying (384.0)   | -             | -        |
| bluetooth_version    | -            | character varying (384.0)   | -             | -        |
| bt_mac               | -            | character varying (96.0)    | -             | -        |
| created_at           | -            | timestamp without time zone | -             | -        |
| dashboard_version    | -            | character varying (384.0)   | -             | -        |
| dbu_hw               | -            | character varying (30.0)    | -             | -        |
| engine_off           | -            | smallint                    | -             | -        |
| gps_battery_level    | -            | integer                     | -             | -        |
| gps_external_power   | -            | smallint                    | -             | -        |
| hdop                 | -            | double precision            | -             | -        |
| helmet_attached      | -            | smallint                    | -             | -        |
| iccid                | -            | character varying (300.0)   | -             | -        |
| id                   | -            | bigint                      | -             | -        |
| imei                 | -            | character varying (300.0)   | -             | -        |
| iot_version          | -            | character varying (150.0)   | -             | -        |
| is_connected         | -            | smallint                    | -             | -        |
| latitude             | -            | double precision            | -             | -        |
| local_geo_status     | -            | character varying (64512.0) | -             | -        |
| local_geofence       | -            | smallint                    | -             | -        |
| location_source      | -            | character varying (48.0)    | -             | -        |
| longitude            | -            | double precision            | -             | -        |
| lpwr_flag            | -            | smallint                    | -             | -        |
| mode                 | -            | smallint                    | -             | -        |
| motor_version        | -            | character varying (384.0)   | -             | -        |
| no_parking_light     | -            | smallint                    | -             | -        |
| normal_speed         | -            | smallint                    | -             | -        |
| position_active_time | -            | timestamp without time zone | -             | -        |
| protocol             | -            | character varying (90.0)    | -             | -        |
| remaining_battery    | -            | double precision            | -             | -        |
| remaining_range      | -            | numeric                     | -             | -        |
| ride_mile            | -            | bigint                      | -             | -        |
| sport_speed          | -            | smallint                    | -             | -        |
| status               | -            | character varying (60.0)    | -             | -        |
| topple               | -            | smallint                    | -             | -        |
| updated_at           | -            | timestamp without time zone | -             | -        |
| version              | -            | character varying (765.0)   | -             | -        |
| version_list         | -            | character varying (64512.0) | -             | -        |
| vin                  | -            | double precision            | -             | -        |
| voltage              | -            | numeric                     | -             | -        |
| wheel_size           | -            | smallint                    | -             | -        |
| white_noise          | -            | smallint                    | -             | -        |
| wrench_light         | -            | smallint                    | -             | -        |



# gps_region

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| country    | -            | character varying (48.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| imei       | -            | character varying (105.0)   | -             | -        |



# gps_self_check

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| 4gstat       | -            | smallint                    | -             | -        |
| accelfault   | -            | smallint                    | -             | -        |
| baro         | -            | integer                     | -             | -        |
| bat_vs       | -            | smallint                    | -             | -        |
| batstat      | -            | smallint                    | -             | -        |
| batulck      | -            | smallint                    | -             | -        |
| brakefault   | -            | smallint                    | -             | -        |
| btu_sw       | -            | character varying (150.0)   | -             | -        |
| btu_vs       | -            | smallint                    | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| ctle         | -            | smallint                    | -             | -        |
| cyc          | -            | integer                     | -             | -        |
| dbstat       | -            | smallint                    | -             | -        |
| dbu_vs       | -            | smallint                    | -             | -        |
| description  | -            | character varying (768.0)   | -             | -        |
| fall         | -            | smallint                    | -             | -        |
| gps_id       | -            | bigint                      | -             | -        |
| gps_protocol | -            | character varying (24.0)    | -             | -        |
| hlu_vs       | -            | smallint                    | -             | -        |
| id           | -            | bigint                      | -             | -        |
| imei         | -            | character varying (96.0)    | -             | -        |
| iot_sw       | -            | character varying (150.0)   | -             | -        |
| mode         | -            | smallint                    | -             | -        |
| mpu          | -            | smallint                    | -             | -        |
| pitch        | -            | integer                     | -             | -        |
| roll         | -            | integer                     | -             | -        |
| rssi         | -            | integer                     | -             | -        |
| scu_vs       | -            | smallint                    | -             | -        |
| soc          | -            | smallint                    | -             | -        |
| status       | -            | character varying (96.0)    | -             | -        |
| sv_unum      | -            | smallint                    | -             | -        |
| temp         | -            | smallint                    | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| v18          | -            | integer                     | -             | -        |
| v50          | -            | integer                     | -             | -        |
| v90          | -            | integer                     | -             | -        |
| vbat         | -            | integer                     | -             | -        |
| vin          | -            | integer                     | -             | -        |
| yaw          | -            | integer                     | -             | -        |



# helmet_unlock_record

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at      | -            | timestamp without time zone | -             | -        |
| helmet_attached | -            | smallint                    | -             | -        |
| id              | -            | bigint                      | -             | -        |
| result          | -            | smallint                    | -             | -        |
| scooter_online  | -            | smallint                    | -             | -        |
| stage           | -            | character varying (60.0)    | -             | -        |
| trip_id         | -            | bigint                      | -             | -        |
| unlock_channel  | -            | character varying (30.0)    | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |
| user_id         | -            | bigint                      | -             | -        |



# ibeacon

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| major      | -            | character varying (48.0)    | -             | -        |
| minor      | -            | character varying (48.0)    | -             | -        |
| station_id | -            | bigint                      | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| uuid       | -            | character varying (150.0)   | -             | -        |



# incident_log

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| aware_time    | -            | timestamp without time zone | -             | -        |
| city          | -            | character varying (90.0)    | -             | -        |
| content       | -            | character varying (65535.0) | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| id            | -            | bigint                      | -             | -        |
| incident_time | -            | timestamp without time zone | -             | -        |
| risk_level    | -            | character varying (90.0)    | -             | -        |
| status        | -            | character varying (90.0)    | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |



# incident_log_image

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| image_id   | -            | character varying (150.0)   | -             | -        |
| log_id     | -            | bigint                      | -             | -        |
| type       | -            | character varying (90.0)    | -             | -        |



# incident_mechanic_report

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| comment           | -            | character varying (65535.0) | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| created_by        | -            | bigint                      | -             | -        |
| created_user      | -            | character varying (768.0)   | -             | -        |
| created_user_role | -            | character varying (768.0)   | -             | -        |
| deck_id           | -            | character varying (300.0)   | -             | -        |
| id                | -            | bigint                      | -             | -        |
| report_id         | -            | bigint                      | -             | -        |
| updated_at        | -            | timestamp without time zone | -             | -        |
| updated_by        | -            | bigint                      | -             | -        |
| updated_user      | -            | character varying (768.0)   | -             | -        |
| updated_user_role | -            | character varying (768.0)   | -             | -        |



# incident_report

|                      | definition   | data_type                   | related_key   | remark   |
|:---------------------|:-------------|:----------------------------|:--------------|:---------|
| aware_time           | -            | timestamp without time zone | -             | -        |
| business_risk_level  | -            | character varying (300.0)   | -             | -        |
| category             | -            | character varying (90.0)    | -             | -        |
| city                 | -            | character varying (90.0)    | -             | -        |
| created_at           | -            | timestamp without time zone | -             | -        |
| created_by           | -            | bigint                      | -             | -        |
| created_user         | -            | character varying (768.0)   | -             | -        |
| created_user_role    | -            | character varying (768.0)   | -             | -        |
| description          | -            | character varying (65535.0) | -             | -        |
| edm_sent             | -            | smallint                    | -             | -        |
| id                   | -            | bigint                      | -             | -        |
| incident_time        | -            | timestamp without time zone | -             | -        |
| latitude             | -            | numeric                     | -             | -        |
| location             | -            | character varying (768.0)   | -             | -        |
| longitude            | -            | numeric                     | -             | -        |
| parties              | -            | character varying (65535.0) | -             | -        |
| platform             | -            | character varying (300.0)   | -             | -        |
| police               | -            | smallint                    | -             | -        |
| reason               | -            | character varying (765.0)   | -             | -        |
| remarks              | -            | character varying (768.0)   | -             | -        |
| risk_level           | -            | character varying (90.0)    | -             | -        |
| short_description    | -            | character varying (3000.0)  | -             | -        |
| source               | -            | character varying (384.0)   | -             | -        |
| status               | -            | character varying (90.0)    | -             | -        |
| third_party_involved | -            | character varying (300.0)   | -             | -        |
| ticket_url           | -            | character varying (768.0)   | -             | -        |
| type                 | -            | character varying (90.0)    | -             | -        |
| updated_at           | -            | timestamp without time zone | -             | -        |
| updated_by           | -            | bigint                      | -             | -        |
| updated_user         | -            | character varying (768.0)   | -             | -        |
| updated_user_role    | -            | character varying (768.0)   | -             | -        |
| user_involved        | -            | character varying (300.0)   | -             | -        |
| users                | -            | character varying (65535.0) | -             | -        |
| vehicle_property     | -            | character varying (65535.0) | -             | -        |
| version              | -            | integer                     | -             | -        |



# incident_report_history

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at        | -            | timestamp without time zone | -             | -        |
| created_by        | -            | bigint                      | -             | -        |
| created_user      | -            | character varying (768.0)   | -             | -        |
| created_user_role | -            | character varying (768.0)   | -             | -        |
| current           | -            | character varying (65535.0) | -             | -        |
| id                | -            | bigint                      | -             | -        |
| platform          | -            | character varying (300.0)   | -             | -        |
| previous          | -            | character varying (65535.0) | -             | -        |
| report_id         | -            | bigint                      | -             | -        |
| source            | -            | character varying (384.0)   | -             | -        |
| updated_fields    | -            | character varying (1500.0)  | -             | -        |
| version           | -            | integer                     | -             | -        |



# incident_report_image

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| image_id   | -            | character varying (150.0)   | -             | -        |
| report_id  | -            | bigint                      | -             | -        |
| type       | -            | character varying (90.0)    | -             | -        |
| version    | -            | integer                     | -             | -        |



# incident_report_snapshot

|                      | definition   | data_type                   | related_key   | remark   |
|:---------------------|:-------------|:----------------------------|:--------------|:---------|
| aware_time           | -            | timestamp without time zone | -             | -        |
| business_risk_level  | -            | character varying (300.0)   | -             | -        |
| city                 | -            | character varying (90.0)    | -             | -        |
| created_at           | -            | timestamp without time zone | -             | -        |
| created_by           | -            | bigint                      | -             | -        |
| created_user         | -            | character varying (768.0)   | -             | -        |
| created_user_role    | -            | character varying (768.0)   | -             | -        |
| description          | -            | character varying (65535.0) | -             | -        |
| id                   | -            | bigint                      | -             | -        |
| incident_time        | -            | timestamp without time zone | -             | -        |
| location             | -            | character varying (768.0)   | -             | -        |
| parties              | -            | character varying (65535.0) | -             | -        |
| platform             | -            | character varying (300.0)   | -             | -        |
| police               | -            | smallint                    | -             | -        |
| reason               | -            | character varying (765.0)   | -             | -        |
| remarks              | -            | character varying (768.0)   | -             | -        |
| report_id            | -            | bigint                      | -             | -        |
| risk_level           | -            | character varying (90.0)    | -             | -        |
| short_description    | -            | character varying (3000.0)  | -             | -        |
| source               | -            | character varying (384.0)   | -             | -        |
| status               | -            | character varying (90.0)    | -             | -        |
| third_party_involved | -            | character varying (300.0)   | -             | -        |
| ticket_url           | -            | character varying (768.0)   | -             | -        |
| type                 | -            | character varying (90.0)    | -             | -        |
| user_involved        | -            | character varying (300.0)   | -             | -        |
| users                | -            | character varying (65535.0) | -             | -        |
| vehicle_property     | -            | character varying (65535.0) | -             | -        |
| version              | -            | integer                     | -             | -        |



# incident_report_timeline

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| content           | -            | character varying (65535.0) | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| created_by        | -            | bigint                      | -             | -        |
| created_user      | -            | character varying (768.0)   | -             | -        |
| created_user_role | -            | character varying (768.0)   | -             | -        |
| email_address     | -            | character varying (1500.0)  | -             | -        |
| email_template    | -            | character varying (384.0)   | -             | -        |
| id                | -            | bigint                      | -             | -        |
| local_time        | -            | smallint                    | -             | -        |
| report_id         | -            | bigint                      | -             | -        |
| touch_time        | -            | timestamp without time zone | -             | -        |
| type              | -            | character varying (150.0)   | -             | -        |
| updated           | -            | smallint                    | -             | -        |



# issue_subscribe_operator

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| created_at  | -            | timestamp without time zone | -             | -        |
| created_by  | -            | bigint                      | -             | -        |
| id          | -            | bigint                      | -             | -        |
| important   | -            | smallint                    | -             | -        |
| issue_id    | -            | bigint                      | -             | -        |
| mute        | -            | smallint                    | -             | -        |
| operator_id | -            | bigint                      | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| updated_by  | -            | bigint                      | -             | -        |



# location_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | character varying (1536.0)  | -             | -        |
| id         | -            | bigint                      | -             | -        |
| latitude   | -            | double precision            | -             | -        |
| longitude  | -            | double precision            | -             | -        |
| type       | -            | character varying (150.0)   | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# lock_response

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| card_no    | -            | character varying (300.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| locked     | -            | smallint                    | -             | -        |
| succeed    | -            | smallint                    | -             | -        |



# maintenance_work

|       | definition   | data_type                 | related_key   | remark   |
|:------|:-------------|:--------------------------|:--------------|:---------|
| id    | -            | integer                   | -             | -        |
| title | -            | character varying (765.0) | -             | -        |



# maintenance_work_item

|          | definition   | data_type                 | related_key   | remark   |
|:---------|:-------------|:--------------------------|:--------------|:---------|
| deleted  | -            | smallint                  | -             | -        |
| group_id | -            | integer                   | -             | -        |
| id       | -            | integer                   | -             | -        |
| title    | -            | character varying (765.0) | -             | -        |



# maintenance_work_performed

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| action            | -            | character varying (765.0)   | -             | -        |
| comment           | -            | character varying (6000.0)  | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| created_by        | -            | bigint                      | -             | -        |
| id                | -            | bigint                      | -             | -        |
| maintenance_id    | -            | bigint                      | -             | -        |
| performed_item_id | -            | integer                     | -             | -        |
| quantity          | -            | smallint                    | -             | -        |
| updated_at        | -            | timestamp without time zone | -             | -        |
| updated_by        | -            | bigint                      | -             | -        |



# mechanic_report_image

|                    | definition   | data_type                   | related_key   | remark   |
|:-------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at         | -            | timestamp without time zone | -             | -        |
| id                 | -            | bigint                      | -             | -        |
| image_id           | -            | character varying (150.0)   | -             | -        |
| mechanic_report_id | -            | bigint                      | -             | -        |



# mobile_verification

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| country_code | -            | character varying (48.0)    | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| mobile       | -            | character varying (150.0)   | -             | -        |
| remote_ip    | -            | character varying (192.0)   | -             | -        |
| sms_code     | -            | character varying (30.0)    | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |



# mqtt_btu

|                            | definition   | data_type                   | related_key   | remark   |
|:---------------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at                 | -            | timestamp without time zone | -             | -        |
| enabled                    | -            | smallint                    | -             | -        |
| gps_id                     | -            | bigint                      | -             | -        |
| hw                         | -            | character varying (96.0)    | -             | -        |
| id                         | -            | bigint                      | -             | -        |
| receive_failure_bytes      | -            | integer                     | -             | -        |
| receive_send_total_failure | -            | integer                     | -             | -        |
| receive_success_bytes      | -            | integer                     | -             | -        |
| send_failure_bytes         | -            | integer                     | -             | -        |
| send_success_bytes         | -            | integer                     | -             | -        |
| sn                         | -            | character varying (96.0)    | -             | -        |
| sw                         | -            | character varying (96.0)    | -             | -        |
| valid_count                | -            | integer                     | -             | -        |
| valid_status               | -            | smallint                    | -             | -        |
| working_status             | -            | smallint                    | -             | -        |



# mqtt_dbu

|                            | definition   | data_type                   | related_key   | remark   |
|:---------------------------|:-------------|:----------------------------|:--------------|:---------|
| accelerate                 | -            | smallint                    | -             | -        |
| bell_on                    | -            | smallint                    | -             | -        |
| braking                    | -            | smallint                    | -             | -        |
| created_at                 | -            | timestamp without time zone | -             | -        |
| gear                       | -            | smallint                    | -             | -        |
| gps_id                     | -            | bigint                      | -             | -        |
| headlight_on               | -            | smallint                    | -             | -        |
| hw                         | -            | character varying (96.0)    | -             | -        |
| id                         | -            | bigint                      | -             | -        |
| receive_failure_bytes      | -            | integer                     | -             | -        |
| receive_send_total_failure | -            | integer                     | -             | -        |
| receive_success_bytes      | -            | integer                     | -             | -        |
| screen_on                  | -            | smallint                    | -             | -        |
| send_failure_bytes         | -            | integer                     | -             | -        |
| send_success_bytes         | -            | integer                     | -             | -        |
| sn                         | -            | character varying (96.0)    | -             | -        |
| sw                         | -            | character varying (96.0)    | -             | -        |
| working_status             | -            | smallint                    | -             | -        |



# mqtt_hlu

|                            | definition   | data_type                   | related_key   | remark   |
|:---------------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at                 | -            | timestamp without time zone | -             | -        |
| gps_id                     | -            | bigint                      | -             | -        |
| helmet_locked              | -            | smallint                    | -             | -        |
| hw                         | -            | character varying (96.0)    | -             | -        |
| id                         | -            | bigint                      | -             | -        |
| physical_lock              | -            | smallint                    | -             | -        |
| receive_failure_bytes      | -            | integer                     | -             | -        |
| receive_send_total_failure | -            | integer                     | -             | -        |
| receive_success_bytes      | -            | integer                     | -             | -        |
| send_failure_bytes         | -            | integer                     | -             | -        |
| send_success_bytes         | -            | integer                     | -             | -        |
| sn                         | -            | character varying (96.0)    | -             | -        |
| sw                         | -            | character varying (96.0)    | -             | -        |
| working_status             | -            | smallint                    | -             | -        |



# mqtt_iot

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| baro              | -            | integer                     | -             | -        |
| bat_vc            | -            | smallint                    | -             | -        |
| bat_vs            | -            | smallint                    | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| dangerous_driving | -            | smallint                    | -             | -        |
| dbu_vc            | -            | smallint                    | -             | -        |
| dbu_vs            | -            | smallint                    | -             | -        |
| flow              | -            | integer                     | -             | -        |
| gps_id            | -            | bigint                      | -             | -        |
| hlu_vc            | -            | smallint                    | -             | -        |
| hlu_vs            | -            | smallint                    | -             | -        |
| hw                | -            | character varying (96.0)    | -             | -        |
| id                | -            | bigint                      | -             | -        |
| low_power_mode    | -            | smallint                    | -             | -        |
| lte_vc            | -            | smallint                    | -             | -        |
| lte_vs            | -            | smallint                    | -             | -        |
| mpu_status        | -            | smallint                    | -             | -        |
| ride_mile         | -            | integer                     | -             | -        |
| scu_vc            | -            | smallint                    | -             | -        |
| scu_vs            | -            | smallint                    | -             | -        |
| sn                | -            | character varying (96.0)    | -             | -        |
| sw                | -            | character varying (96.0)    | -             | -        |
| temperature       | -            | numeric                     | -             | -        |
| topple            | -            | smallint                    | -             | -        |
| total_mile        | -            | integer                     | -             | -        |
| uptime            | -            | integer                     | -             | -        |
| v18               | -            | integer                     | -             | -        |
| v50               | -            | integer                     | -             | -        |
| v90               | -            | integer                     | -             | -        |
| vbat              | -            | integer                     | -             | -        |
| vin               | -            | integer                     | -             | -        |
| water_leak        | -            | smallint                    | -             | -        |
| working_status    | -            | smallint                    | -             | -        |



# mqtt_lte

|                               | definition   | data_type                   | related_key   | remark   |
|:------------------------------|:-------------|:----------------------------|:--------------|:---------|
| ber                           | -            | smallint                    | -             | -        |
| brand                         | -            | character varying (192.0)   | -             | -        |
| chan                          | -            | character varying (192.0)   | -             | -        |
| channel                       | -            | character varying (192.0)   | -             | -        |
| connection_count              | -            | integer                     | -             | -        |
| created_at                    | -            | timestamp without time zone | -             | -        |
| dial_count                    | -            | integer                     | -             | -        |
| gps_id                        | -            | bigint                      | -             | -        |
| hw                            | -            | character varying (96.0)    | -             | -        |
| iccid                         | -            | character varying (96.0)    | -             | -        |
| id                            | -            | bigint                      | -             | -        |
| imsi                          | -            | character varying (192.0)   | -             | -        |
| join_count                    | -            | integer                     | -             | -        |
| join_status                   | -            | smallint                    | -             | -        |
| network_receive_failure_count | -            | integer                     | -             | -        |
| network_receive_success_count | -            | integer                     | -             | -        |
| network_send_failure_count    | -            | integer                     | -             | -        |
| network_send_success_count    | -            | integer                     | -             | -        |
| online_count                  | -            | integer                     | -             | -        |
| operator                      | -            | character varying (192.0)   | -             | -        |
| receive_failure_bytes         | -            | integer                     | -             | -        |
| receive_send_total_failure    | -            | integer                     | -             | -        |
| receive_success_bytes         | -            | integer                     | -             | -        |
| reset_count                   | -            | integer                     | -             | -        |
| rssi                          | -            | smallint                    | -             | -        |
| send_failure_bytes            | -            | integer                     | -             | -        |
| send_success_bytes            | -            | integer                     | -             | -        |
| sn                            | -            | character varying (96.0)    | -             | -        |
| sw                            | -            | character varying (96.0)    | -             | -        |
| tech                          | -            | character varying (192.0)   | -             | -        |
| working_status                | -            | smallint                    | -             | -        |



# mqtt_mpu

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| accelerate1 | -            | numeric                     | -             | -        |
| accelerate2 | -            | numeric                     | -             | -        |
| accelerate3 | -            | numeric                     | -             | -        |
| created_at  | -            | timestamp without time zone | -             | -        |
| gps_id      | -            | bigint                      | -             | -        |
| gyro1       | -            | numeric                     | -             | -        |
| gyro2       | -            | numeric                     | -             | -        |
| gyro3       | -            | numeric                     | -             | -        |
| id          | -            | bigint                      | -             | -        |
| pitch_angle | -            | numeric                     | -             | -        |
| roll_angle  | -            | numeric                     | -             | -        |
| ts          | -            | integer                     | -             | -        |
| yaw_angle   | -            | numeric                     | -             | -        |



# mqtt_position

|                  | definition   | data_type                   | related_key   | remark   |
|:-----------------|:-------------|:----------------------------|:--------------|:---------|
| altitude         | -            | numeric                     | -             | -        |
| course           | -            | numeric                     | -             | -        |
| created_at       | -            | timestamp without time zone | -             | -        |
| device_time      | -            | timestamp without time zone | -             | -        |
| gps_id           | -            | bigint                      | -             | -        |
| hdop             | -            | numeric                     | -             | -        |
| id               | -            | bigint                      | -             | -        |
| inview_st_signal | -            | character varying (384.0)   | -             | -        |
| inview_st_sn     | -            | character varying (384.0)   | -             | -        |
| latitude         | -            | numeric                     | -             | -        |
| longitude        | -            | numeric                     | -             | -        |
| mode             | -            | smallint                    | -             | -        |
| pdop             | -            | numeric                     | -             | -        |
| speed            | -            | numeric                     | -             | -        |
| status           | -            | smallint                    | -             | -        |
| using_st_num     | -            | smallint                    | -             | -        |
| vdop             | -            | numeric                     | -             | -        |
| visible_st_num   | -            | smallint                    | -             | -        |
| work_status      | -            | smallint                    | -             | -        |



# mqtt_scu

|                            | definition   | data_type                   | related_key   | remark   |
|:---------------------------|:-------------|:----------------------------|:--------------|:---------|
| battery_lock_failure       | -            | smallint                    | -             | -        |
| battery_lock_unlocked      | -            | smallint                    | -             | -        |
| battery_network_failure    | -            | smallint                    | -             | -        |
| controller_failure         | -            | smallint                    | -             | -        |
| created_at                 | -            | timestamp without time zone | -             | -        |
| current                    | -            | integer                     | -             | -        |
| current_limit              | -            | integer                     | -             | -        |
| gear                       | -            | smallint                    | -             | -        |
| gps_id                     | -            | bigint                      | -             | -        |
| hw                         | -            | character varying (96.0)    | -             | -        |
| id                         | -            | bigint                      | -             | -        |
| low_voltage                | -            | smallint                    | -             | -        |
| mhall_failure              | -            | smallint                    | -             | -        |
| mploss_failure             | -            | smallint                    | -             | -        |
| mstall_failure             | -            | smallint                    | -             | -        |
| over_current               | -            | smallint                    | -             | -        |
| receive_failure_bytes      | -            | integer                     | -             | -        |
| receive_send_total_failure | -            | integer                     | -             | -        |
| receive_success_bytes      | -            | integer                     | -             | -        |
| send_failure_bytes         | -            | integer                     | -             | -        |
| send_success_bytes         | -            | integer                     | -             | -        |
| sn                         | -            | character varying (96.0)    | -             | -        |
| speed                      | -            | numeric                     | -             | -        |
| speed_limit1               | -            | integer                     | -             | -        |
| speed_limit2               | -            | integer                     | -             | -        |
| sw                         | -            | character varying (96.0)    | -             | -        |
| unlocked                   | -            | smallint                    | -             | -        |
| voltage                    | -            | integer                     | -             | -        |
| working_status             | -            | smallint                    | -             | -        |



# n3_status

|                         | definition   | data_type                   | related_key   | remark   |
|:------------------------|:-------------|:----------------------------|:--------------|:---------|
| a_phase_current         | -            | smallint                    | -             | -        |
| accelerator             | -            | smallint                    | -             | -        |
| b_phase_current         | -            | smallint                    | -             | -        |
| battery_percentage      | -            | double precision            | -             | -        |
| ble_communication       | -            | smallint                    | -             | -        |
| bms_communication       | -            | smallint                    | -             | -        |
| brake                   | -            | smallint                    | -             | -        |
| c_phase_current         | -            | smallint                    | -             | -        |
| created_at              | -            | timestamp without time zone | -             | -        |
| dashboard_communication | -            | smallint                    | -             | -        |
| dc_current              | -            | smallint                    | -             | -        |
| device_id               | -            | bigint                      | -             | -        |
| electric_hall           | -            | smallint                    | -             | -        |
| electricity             | -            | double precision            | -             | -        |
| helmet_attached         | -            | smallint                    | -             | -        |
| id                      | -            | bigint                      | -             | -        |
| iot_communication       | -            | smallint                    | -             | -        |
| lock_status             | -            | character varying (96.0)    | -             | -        |
| low_battery             | -            | smallint                    | -             | -        |
| low_battery_protection  | -            | smallint                    | -             | -        |
| single_range            | -            | double precision            | -             | -        |
| speed                   | -            | double precision            | -             | -        |
| topple                  | -            | smallint                    | -             | -        |
| voltage                 | -            | double precision            | -             | -        |



# neuron_file

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| audited    | -            | smallint                    | -             | -        |
| bucket     | -            | character varying (150.0)   | -             | -        |
| confidence | -            | double precision            | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | character varying (300.0)   | -             | -        |
| md5        | -            | character varying (300.0)   | -             | -        |
| name       | -            | character varying (765.0)   | -             | -        |
| recognized | -            | smallint                    | -             | -        |
| type       | -            | character varying (60.0)    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| url        | -            | character varying (1500.0)  | -             | -        |



# neuron_order

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| amount        | -            | numeric                     | -             | -        |
| city_base_fee | -            | numeric                     | -             | -        |
| city_unit_fee | -            | numeric                     | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| currency      | -            | character varying (48.0)    | -             | -        |
| id            | -            | bigint                      | -             | -        |
| pass_id       | -            | bigint                      | -             | -        |
| status        | -            | character varying (96.0)    | -             | -        |
| trip_id       | -            | bigint                      | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| user_id       | -            | bigint                      | -             | -        |
| version       | -            | integer                     | -             | -        |



# neuron_sync_queue

|                     | definition   | data_type                   | related_key   | remark   |
|:--------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at          | -            | timestamp without time zone | -             | -        |
| id                  | -            | numeric                     | -             | -        |
| json_body           | -            | character varying (3000.0)  | -             | -        |
| original_created_at | -            | timestamp without time zone | -             | -        |
| original_id         | -            | bigint                      | -             | -        |
| succeed             | -            | smallint                    | -             | -        |
| type                | -            | character varying (150.0)   | -             | -        |
| updated_at          | -            | timestamp without time zone | -             | -        |



# neuron_sync_scooter_zego

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at      | -            | timestamp without time zone | -             | -        |
| id              | -            | numeric                     | -             | -        |
| scooter_id      | -            | bigint                      | -             | -        |
| zego_vehicle_id | -            | bigint                      | -             | -        |



# neuron_transaction

|                  | definition   | data_type                   | related_key   | remark   |
|:-----------------|:-------------|:----------------------------|:--------------|:---------|
| account          | -            | character varying (96.0)    | -             | -        |
| amount           | -            | numeric                     | -             | -        |
| automatic        | -            | smallint                    | -             | -        |
| brand            | -            | character varying (96.0)    | -             | -        |
| city             | -            | character varying (96.0)    | -             | -        |
| client_ip        | -            | character varying (96.0)    | -             | -        |
| created_at       | -            | timestamp without time zone | -             | -        |
| currency         | -            | character varying (48.0)    | -             | -        |
| dispute_amount   | -            | double precision            | -             | -        |
| dispute_status   | -            | character varying (96.0)    | -             | -        |
| id               | -            | bigint                      | -             | -        |
| last4            | -            | character varying (12.0)    | -             | -        |
| method           | -            | character varying (192.0)   | -             | -        |
| my_pass_id       | -            | bigint                      | -             | -        |
| order_id         | -            | bigint                      | -             | -        |
| platform         | -            | character varying (96.0)    | -             | -        |
| stripe_charge_id | -            | character varying (300.0)   | -             | -        |
| stripe_refund_id | -            | character varying (300.0)   | -             | -        |
| trip_deposit_id  | -            | bigint                      | -             | -        |
| type             | -            | character varying (150.0)   | -             | -        |
| updated_at       | -            | timestamp without time zone | -             | -        |
| user_id          | -            | bigint                      | -             | -        |
| user_pass_id     | -            | bigint                      | -             | -        |



# notification

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| content    | -            | character varying (65535.0) | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| data       | -            | character varying (65535.0) | -             | -        |
| id         | -            | bigint                      | -             | -        |
| type       | -            | character varying (150.0)   | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# notification_config

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| city          | -            | character varying (60.0)    | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| end_time      | -            | character varying (30.0)    | -             | -        |
| id            | -            | bigint                      | -             | -        |
| message_id    | -            | character varying (150.0)   | -             | -        |
| message_type  | -            | character varying (60.0)    | -             | -        |
| start_time    | -            | character varying (30.0)    | -             | -        |
| status        | -            | character varying (60.0)    | -             | -        |
| title         | -            | character varying (765.0)   | -             | -        |
| trigger_event | -            | character varying (150.0)   | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| weekdays      | -            | character varying (90.0)    | -             | -        |



# okai_battery_sn

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| battery    | -            | character varying (360.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| sn         | -            | character varying (192.0)   | -             | -        |



# ops_live_location

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| avatar     | -            | character varying (768.0)   | -             | -        |
| city       | -            | character varying (150.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| full_name  | -            | character varying (768.0)   | -             | -        |
| id         | -            | bigint                      | -             | -        |
| latitude   | -            | double precision            | -             | -        |
| longitude  | -            | double precision            | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |
| username   | -            | character varying (768.0)   | -             | -        |



# ops_open_deck

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| battery_no        | -            | character varying (192.0)   | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| id                | -            | bigint                      | -             | -        |
| open_channel      | -            | character varying (60.0)    | -             | -        |
| open_deck_time    | -            | timestamp without time zone | -             | -        |
| remaining_battery | -            | numeric                     | -             | -        |
| scooter_id        | -            | bigint                      | -             | -        |
| status            | -            | character varying (60.0)    | -             | -        |
| user_id           | -            | bigint                      | -             | -        |



# order_detail

|                        | definition   | data_type                   | related_key   | remark   |
|:-----------------------|:-------------|:----------------------------|:--------------|:---------|
| adjust_off             | -            | numeric                     | -             | -        |
| base_fee               | -            | numeric                     | -             | -        |
| convenience_fee        | -            | numeric                     | -             | -        |
| coupon_amount          | -            | numeric                     | -             | -        |
| created_at             | -            | timestamp without time zone | -             | -        |
| credit_amount          | -            | numeric                     | -             | -        |
| currency               | -            | character varying (48.0)    | -             | -        |
| deposit_captured       | -            | numeric                     | -             | -        |
| helmet_replacement_fee | -            | numeric                     | -             | -        |
| id                     | -            | bigint                      | -             | -        |
| is_short_trip          | -            | smallint                    | -             | -        |
| order_id               | -            | bigint                      | -             | -        |
| pass_amount            | -            | numeric                     | -             | -        |
| pass_minutes           | -            | bigint                      | -             | -        |
| scooter_discount       | -            | numeric                     | -             | -        |
| trip_cap_off           | -            | numeric                     | -             | -        |
| trip_fare              | -            | numeric                     | -             | -        |
| trip_mileage           | -            | bigint                      | -             | -        |
| trip_minutes           | -            | bigint                      | -             | -        |
| unit_fee               | -            | numeric                     | -             | -        |
| updated_at             | -            | timestamp without time zone | -             | -        |
| updated_by             | -            | bigint                      | -             | -        |
| user_coupon_id         | -            | bigint                      | -             | -        |
| user_id                | -            | bigint                      | -             | -        |
| user_pass_id           | -            | bigint                      | -             | -        |
| vip                    | -            | smallint                    | -             | -        |
| wallet_amount          | -            | numeric                     | -             | -        |



# organization

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| code         | -            | character varying (150.0)   | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| created_user | -            | character varying (768.0)   | -             | -        |
| deleted      | -            | smallint                    | -             | -        |
| domains      | -            | character varying (1500.0)  | -             | -        |
| name         | -            | character varying (765.0)   | -             | -        |
| type         | -            | character varying (150.0)   | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| updated_by   | -            | bigint                      | -             | -        |
| updated_user | -            | character varying (768.0)   | -             | -        |



# organization_discount

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| deleted    | -            | smallint                    | -             | -        |
| discount   | -            | integer                     | -             | -        |
| id         | -            | integer                     | -             | -        |
| org_code   | -            | character varying (765.0)   | -             | -        |
| pass_days  | -            | integer                     | -             | -        |
| pass_id    | -            | bigint                      | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# organization_email_verification

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| deleted    | -            | smallint                    | -             | -        |
| email      | -            | character varying (765.0)   | -             | -        |
| id         | -            | bigint                      | -             | -        |
| org_code   | -            | character varying (150.0)   | -             | -        |
| status     | -            | character varying (60.0)    | -             | -        |
| token      | -            | character varying (150.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# organization_invitation

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| code       | -            | character varying (96.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| org_code   | -            | character varying (765.0)   | -             | -        |
| status     | -            | character varying (96.0)    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| updated_by | -            | bigint                      | -             | -        |



# organization_secret_key

|                  | definition   | data_type                   | related_key   | remark   |
|:-----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at       | -            | timestamp without time zone | -             | -        |
| created_by       | -            | bigint                      | -             | -        |
| encrypted_secret | -            | character varying (1536.0)  | -             | -        |
| id               | -            | bigint                      | -             | -        |
| org_code         | -            | character varying (765.0)   | -             | -        |
| status           | -            | character varying (96.0)    | -             | -        |
| updated_at       | -            | timestamp without time zone | -             | -        |
| updated_by       | -            | bigint                      | -             | -        |



# organization_user

|                  | definition   | data_type                   | related_key   | remark   |
|:-----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at       | -            | timestamp without time zone | -             | -        |
| deleted          | -            | smallint                    | -             | -        |
| external_user_id | -            | character varying (384.0)   | -             | -        |
| id               | -            | bigint                      | -             | -        |
| org_code         | -            | character varying (765.0)   | -             | -        |
| updated_at       | -            | timestamp without time zone | -             | -        |
| user_id          | -            | bigint                      | -             | -        |



# ota_status_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| imei       | -            | character varying (96.0)    | -             | -        |
| notes      | -            | character varying (300.0)   | -             | -        |
| scooter_id | -            | bigint                      | -             | -        |
| status     | -            | character varying (96.0)    | -             | -        |
| task_id    | -            | bigint                      | -             | -        |



# ota_task

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| description   | -            | character varying (300.0)   | -             | -        |
| end_time      | -            | character varying (48.0)    | -             | -        |
| enum_protocol | -            | character varying (48.0)    | -             | -        |
| firmware      | -            | character varying (384.0)   | -             | -        |
| fm_version    | -            | character varying (48.0)    | -             | -        |
| id            | -            | bigint                      | -             | -        |
| language      | -            | character varying (48.0)    | -             | -        |
| progress_info | -            | character varying (600.0)   | -             | -        |
| protocol      | -            | character varying (90.0)    | -             | -        |
| retry_times   | -            | smallint                    | -             | -        |
| scooter_id    | -            | bigint                      | -             | -        |
| start_time    | -            | character varying (48.0)    | -             | -        |
| status        | -            | character varying (96.0)    | -             | -        |
| type          | -            | character varying (96.0)    | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| upgrade_end   | -            | timestamp without time zone | -             | -        |
| upgrade_start | -            | timestamp without time zone | -             | -        |
| version       | -            | integer                     | -             | -        |
| voice         | -            | character varying (150.0)   | -             | -        |



# pass

|                          | definition   | data_type                   | related_key   | remark   |
|:-------------------------|:-------------|:----------------------------|:--------------|:---------|
| cities                   | -            | character varying (1536.0)  | -             | -        |
| created_at               | -            | timestamp without time zone | -             | -        |
| created_by               | -            | bigint                      | -             | -        |
| currency                 | -            | character varying (48.0)    | -             | -        |
| description              | -            | character varying (65535.0) | -             | -        |
| duration                 | -            | bigint                      | -             | -        |
| free_min                 | -            | bigint                      | -             | -        |
| id                       | -            | bigint                      | -             | -        |
| lang_description         | -            | character varying (6000.0)  | -             | -        |
| lang_title               | -            | character varying (6000.0)  | -             | -        |
| price                    | -            | numeric                     | -             | -        |
| reference_price          | -            | numeric                     | -             | -        |
| renewable                | -            | smallint                    | -             | -        |
| renewable_after_inactive | -            | bigint                      | -             | -        |
| status                   | -            | character varying (60.0)    | -             | -        |
| title                    | -            | character varying (150.0)   | -             | -        |
| updated_at               | -            | timestamp without time zone | -             | -        |
| updated_by               | -            | bigint                      | -             | -        |



# password_reset

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| email      | -            | character varying (450.0)   | -             | -        |
| id         | -            | integer                     | -             | -        |
| status     | -            | character varying (60.0)    | -             | -        |
| token      | -            | character varying (300.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# payment_transaction_mapping

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| created_at     | -            | timestamp without time zone | -             | -        |
| id             | -            | bigint                      | -             | -        |
| transaction_id | -            | bigint                      | -             | -        |
| type           | -            | character varying (96.0)    | -             | -        |
| value          | -            | bigint                      | -             | -        |



# promo_code

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| coupon_name  | -            | character varying (768.0)   | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| currency     | -            | character varying (192.0)   | -             | -        |
| expired_at   | -            | timestamp without time zone | -             | -        |
| id           | -            | bigint                      | -             | -        |
| name         | -            | character varying (384.0)   | -             | -        |
| parent       | -            | bigint                      | -             | -        |
| redeem_limit | -            | integer                     | -             | -        |
| remark       | -            | character varying (3072.0)  | -             | -        |
| scope        | -            | character varying (45.0)    | -             | -        |
| status       | -            | character varying (60.0)    | -             | -        |
| type         | -            | character varying (192.0)   | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| updated_by   | -            | bigint                      | -             | -        |
| valid_at     | -            | timestamp without time zone | -             | -        |
| valid_time   | -            | integer                     | -             | -        |
| value        | -            | double precision            | -             | -        |



# purchase_intent

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| account           | -            | character varying (96.0)    | -             | -        |
| amount            | -            | numeric                     | -             | -        |
| client_ip         | -            | character varying (96.0)    | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| currency          | -            | character varying (48.0)    | -             | -        |
| id                | -            | bigint                      | -             | -        |
| order_id          | -            | bigint                      | -             | -        |
| pass_id           | -            | bigint                      | -             | -        |
| payment_intent_id | -            | character varying (384.0)   | -             | -        |
| platform          | -            | character varying (96.0)    | -             | -        |
| status            | -            | character varying (96.0)    | -             | -        |
| trip_deposit_id   | -            | bigint                      | -             | -        |
| type              | -            | character varying (96.0)    | -             | -        |
| updated_at        | -            | timestamp without time zone | -             | -        |
| user_id           | -            | bigint                      | -             | -        |



# qrtz_blob_triggers

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| blob_data     | -            | character varying (65535.0) | -             | -        |
| sched_name    | -            | character varying (360.0)   | -             | -        |
| trigger_group | -            | character varying (600.0)   | -             | -        |
| trigger_name  | -            | character varying (600.0)   | -             | -        |



# qrtz_calendars

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| calendar      | -            | character varying (65535.0) | -             | -        |
| calendar_name | -            | character varying (600.0)   | -             | -        |
| sched_name    | -            | character varying (360.0)   | -             | -        |



# qrtz_cron_triggers

|                 | definition   | data_type                 | related_key   | remark   |
|:----------------|:-------------|:--------------------------|:--------------|:---------|
| cron_expression | -            | character varying (360.0) | -             | -        |
| sched_name      | -            | character varying (360.0) | -             | -        |
| time_zone_id    | -            | character varying (240.0) | -             | -        |
| trigger_group   | -            | character varying (600.0) | -             | -        |
| trigger_name    | -            | character varying (600.0) | -             | -        |



# qrtz_fired_triggers

|                   | definition   | data_type                 | related_key   | remark   |
|:------------------|:-------------|:--------------------------|:--------------|:---------|
| entry_id          | -            | character varying (285.0) | -             | -        |
| fired_time        | -            | bigint                    | -             | -        |
| instance_name     | -            | character varying (600.0) | -             | -        |
| is_nonconcurrent  | -            | character varying (15.0)  | -             | -        |
| job_group         | -            | character varying (600.0) | -             | -        |
| job_name          | -            | character varying (600.0) | -             | -        |
| priority          | -            | integer                   | -             | -        |
| requests_recovery | -            | character varying (15.0)  | -             | -        |
| sched_name        | -            | character varying (360.0) | -             | -        |
| sched_time        | -            | bigint                    | -             | -        |
| state             | -            | character varying (48.0)  | -             | -        |
| trigger_group     | -            | character varying (600.0) | -             | -        |
| trigger_name      | -            | character varying (600.0) | -             | -        |



# qrtz_job_details

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| description       | -            | character varying (750.0)   | -             | -        |
| is_durable        | -            | character varying (15.0)    | -             | -        |
| is_nonconcurrent  | -            | character varying (15.0)    | -             | -        |
| is_update_data    | -            | character varying (15.0)    | -             | -        |
| job_class_name    | -            | character varying (750.0)   | -             | -        |
| job_data          | -            | character varying (65535.0) | -             | -        |
| job_group         | -            | character varying (600.0)   | -             | -        |
| job_name          | -            | character varying (600.0)   | -             | -        |
| requests_recovery | -            | character varying (15.0)    | -             | -        |
| sched_name        | -            | character varying (360.0)   | -             | -        |



# qrtz_locks

|            | definition   | data_type                 | related_key   | remark   |
|:-----------|:-------------|:--------------------------|:--------------|:---------|
| lock_name  | -            | character varying (120.0) | -             | -        |
| sched_name | -            | character varying (360.0) | -             | -        |



# qrtz_paused_trigger_grps

|               | definition   | data_type                 | related_key   | remark   |
|:--------------|:-------------|:--------------------------|:--------------|:---------|
| sched_name    | -            | character varying (360.0) | -             | -        |
| trigger_group | -            | character varying (600.0) | -             | -        |



# qrtz_scheduler_state

|                   | definition   | data_type                 | related_key   | remark   |
|:------------------|:-------------|:--------------------------|:--------------|:---------|
| checkin_interval  | -            | bigint                    | -             | -        |
| instance_name     | -            | character varying (600.0) | -             | -        |
| last_checkin_time | -            | bigint                    | -             | -        |
| sched_name        | -            | character varying (360.0) | -             | -        |



# qrtz_simple_triggers

|                 | definition   | data_type                 | related_key   | remark   |
|:----------------|:-------------|:--------------------------|:--------------|:---------|
| repeat_count    | -            | bigint                    | -             | -        |
| repeat_interval | -            | bigint                    | -             | -        |
| sched_name      | -            | character varying (360.0) | -             | -        |
| times_triggered | -            | bigint                    | -             | -        |
| trigger_group   | -            | character varying (600.0) | -             | -        |
| trigger_name    | -            | character varying (600.0) | -             | -        |



# qrtz_simprop_triggers

|               | definition   | data_type                  | related_key   | remark   |
|:--------------|:-------------|:---------------------------|:--------------|:---------|
| bool_prop_1   | -            | character varying (3.0)    | -             | -        |
| bool_prop_2   | -            | character varying (3.0)    | -             | -        |
| dec_prop_1    | -            | numeric                    | -             | -        |
| dec_prop_2    | -            | numeric                    | -             | -        |
| int_prop_1    | -            | integer                    | -             | -        |
| int_prop_2    | -            | integer                    | -             | -        |
| long_prop_1   | -            | bigint                     | -             | -        |
| long_prop_2   | -            | bigint                     | -             | -        |
| sched_name    | -            | character varying (360.0)  | -             | -        |
| str_prop_1    | -            | character varying (1536.0) | -             | -        |
| str_prop_2    | -            | character varying (1536.0) | -             | -        |
| str_prop_3    | -            | character varying (1536.0) | -             | -        |
| trigger_group | -            | character varying (600.0)  | -             | -        |
| trigger_name  | -            | character varying (600.0)  | -             | -        |



# qrtz_triggers

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| calendar_name  | -            | character varying (600.0)   | -             | -        |
| description    | -            | character varying (750.0)   | -             | -        |
| end_time       | -            | bigint                      | -             | -        |
| job_data       | -            | character varying (65535.0) | -             | -        |
| job_group      | -            | character varying (600.0)   | -             | -        |
| job_name       | -            | character varying (600.0)   | -             | -        |
| misfire_instr  | -            | smallint                    | -             | -        |
| next_fire_time | -            | bigint                      | -             | -        |
| prev_fire_time | -            | bigint                      | -             | -        |
| priority       | -            | integer                     | -             | -        |
| sched_name     | -            | character varying (360.0)   | -             | -        |
| start_time     | -            | bigint                      | -             | -        |
| trigger_group  | -            | character varying (600.0)   | -             | -        |
| trigger_name   | -            | character varying (600.0)   | -             | -        |
| trigger_state  | -            | character varying (48.0)    | -             | -        |
| trigger_type   | -            | character varying (24.0)    | -             | -        |



# rebalance

|                  | definition   | data_type                   | related_key   | remark   |
|:-----------------|:-------------|:----------------------------|:--------------|:---------|
| city             | -            | character varying (96.0)    | -             | -        |
| created_at       | -            | timestamp without time zone | -             | -        |
| created_by       | -            | bigint                      | -             | -        |
| end_city         | -            | character varying (96.0)    | -             | -        |
| end_station_id   | -            | bigint                      | -             | -        |
| end_zone         | -            | character varying (150.0)   | -             | -        |
| finish_latitude  | -            | numeric                     | -             | -        |
| finish_longitude | -            | numeric                     | -             | -        |
| from_scan        | -            | smallint                    | -             | -        |
| id               | -            | bigint                      | -             | -        |
| scooter_id       | -            | bigint                      | -             | -        |
| start_city       | -            | character varying (96.0)    | -             | -        |
| start_latitude   | -            | numeric                     | -             | -        |
| start_longitude  | -            | numeric                     | -             | -        |
| start_station_id | -            | bigint                      | -             | -        |
| start_zone       | -            | character varying (150.0)   | -             | -        |
| status           | -            | character varying (48.0)    | -             | -        |
| updated_at       | -            | timestamp without time zone | -             | -        |
| updated_by       | -            | bigint                      | -             | -        |
| user_gps_time    | -            | timestamp without time zone | -             | -        |
| vehicle_type     | -            | character varying (96.0)    | -             | -        |



# receipt

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| sent_num   | -            | integer                     | -             | -        |
| serial_num | -            | character varying (60.0)    | -             | -        |
| source     | -            | bigint                      | -             | -        |
| type       | -            | character varying (60.0)    | -             | -        |



# rtk_station

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| city           | -            | character varying (96.0)    | -             | -        |
| created_at     | -            | timestamp without time zone | -             | -        |
| imei           | -            | character varying (96.0)    | -             | -        |
| latitude       | -            | numeric                     | -             | -        |
| longitude      | -            | numeric                     | -             | -        |
| role           | -            | character varying (30.0)    | -             | -        |
| rtk_data       | -            | character varying (65535.0) | -             | -        |
| switch_role_at | -            | timestamp without time zone | -             | -        |
| updated_at     | -            | timestamp without time zone | -             | -        |



# sagemaker_training_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| feature    | -            | character varying (1536.0)  | -             | -        |
| id         | -            | bigint                      | -             | -        |
| raw        | -            | character varying (64512.0) | -             | -        |
| result     | -            | character varying (1536.0)  | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| type       | -            | character varying (96.0)    | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# scan_record

|                   | definition   | data_type                   | related_key   | remark   |
|:------------------|:-------------|:----------------------------|:--------------|:---------|
| app_version       | -            | character varying (96.0)    | -             | -        |
| city              | -            | character varying (96.0)    | -             | -        |
| created_at        | -            | timestamp without time zone | -             | -        |
| device_id         | -            | character varying (384.0)   | -             | -        |
| error_code        | -            | character varying (96.0)    | -             | -        |
| id                | -            | bigint                      | -             | -        |
| imei              | -            | character varying (96.0)    | -             | -        |
| lpwr_flag         | -            | smallint                    | -             | -        |
| platform          | -            | character varying (96.0)    | -             | -        |
| remaining_battery | -            | double precision            | -             | -        |
| scooter_id        | -            | bigint                      | -             | -        |
| scooter_status    | -            | character varying (150.0)   | -             | -        |
| station_id        | -            | bigint                      | -             | -        |
| trip_id           | -            | bigint                      | -             | -        |
| updated_at        | -            | timestamp without time zone | -             | -        |
| user_gps_time     | -            | timestamp without time zone | -             | -        |
| user_id           | -            | bigint                      | -             | -        |
| user_latitude     | -            | double precision            | -             | -        |
| user_longitude    | -            | double precision            | -             | -        |
| vehicle_type      | -            | character varying (96.0)    | -             | -        |
| vin               | -            | double precision            | -             | -        |



# scooter_assemble_record

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| created_at     | -            | timestamp without time zone | -             | -        |
| deck_id        | -            | character varying (90.0)    | -             | -        |
| id             | -            | bigint                      | -             | -        |
| imei           | -            | character varying (105.0)   | -             | -        |
| licence        | -            | character varying (105.0)   | -             | -        |
| mac            | -            | character varying (90.0)    | -             | -        |
| opt_lock_no    | -            | character varying (105.0)   | -             | -        |
| qr_code        | -            | character varying (48.0)    | -             | -        |
| scooter_id     | -            | bigint                      | -             | -        |
| scooter_status | -            | character varying (60.0)    | -             | -        |
| sticker        | -            | character varying (90.0)    | -             | -        |
| type           | -            | character varying (60.0)    | -             | -        |
| user_id        | -            | bigint                      | -             | -        |



# scooter_check_record

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| city           | -            | character varying (96.0)    | -             | -        |
| content        | -            | character varying (65535.0) | -             | -        |
| created_at     | -            | timestamp without time zone | -             | -        |
| created_by     | -            | bigint                      | -             | -        |
| device_id      | -            | bigint                      | -             | -        |
| id             | -            | bigint                      | -             | -        |
| maintenance_id | -            | bigint                      | -             | -        |
| status         | -            | character varying (60.0)    | -             | -        |
| type           | -            | character varying (60.0)    | -             | -        |
| updated_at     | -            | timestamp without time zone | -             | -        |
| updated_by     | -            | bigint                      | -             | -        |



# scooter_city_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (60.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| pre_city   | -            | character varying (60.0)    | -             | -        |
| pre_zone   | -            | character varying (90.0)    | -             | -        |
| scooter_id | -            | bigint                      | -             | -        |
| zone       | -            | character varying (60.0)    | -             | -        |



# scooter_cmd_exec

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| cmd_record_list | -            | character varying (64512.0) | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| deleted         | -            | smallint                    | -             | -        |
| id              | -            | bigint                      | -             | -        |
| imei            | -            | character varying (300.0)   | -             | -        |
| status          | -            | character varying (30.0)    | -             | -        |
| task_id         | -            | bigint                      | -             | -        |
| task_type       | -            | character varying (60.0)    | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |



# scooter_extend_info

|                                 | definition   | data_type                   | related_key   | remark   |
|:--------------------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at                      | -            | timestamp without time zone | -             | -        |
| id                              | -            | bigint                      | -             | -        |
| last_rebalance_start_time       | -            | timestamp without time zone | -             | -        |
| last_road_test_time             | -            | timestamp without time zone | -             | -        |
| last_sanitize_time              | -            | timestamp without time zone | -             | -        |
| last_scooter_status_record_time | -            | timestamp without time zone | -             | -        |
| last_warehouse_drop_off_time    | -            | timestamp without time zone | -             | -        |
| latest_rebalance_finish_time    | -            | timestamp without time zone | -             | -        |
| latest_trip_end_time            | -            | timestamp without time zone | -             | -        |
| latest_trip_start_time          | -            | timestamp without time zone | -             | -        |
| scooter_id                      | -            | bigint                      | -             | -        |
| updated_at                      | -            | timestamp without time zone | -             | -        |



# scooter_inventory

|                    | definition   | data_type                   | related_key   | remark   |
|:-------------------|:-------------|:----------------------------|:--------------|:---------|
| city               | -            | character varying (60.0)    | -             | -        |
| created_at         | -            | timestamp without time zone | -             | -        |
| deck_id            | -            | character varying (60.0)    | -             | -        |
| deleted            | -            | smallint                    | -             | -        |
| generation         | -            | character varying (48.0)    | -             | -        |
| helmet_lock        | -            | smallint                    | -             | -        |
| ic_card            | -            | character varying (765.0)   | -             | -        |
| id                 | -            | bigint                      | -             | -        |
| image_id           | -            | character varying (192.0)   | -             | -        |
| imei               | -            | character varying (300.0)   | -             | -        |
| last_sanitize_time | -            | timestamp without time zone | -             | -        |
| last_test_time     | -            | timestamp without time zone | -             | -        |
| licence            | -            | character varying (105.0)   | -             | -        |
| mac                | -            | character varying (60.0)    | -             | -        |
| motor_number       | -            | character varying (150.0)   | -             | -        |
| qr_code            | -            | character varying (48.0)    | -             | -        |
| station_id         | -            | bigint                      | -             | -        |
| status             | -            | character varying (60.0)    | -             | -        |
| sticker            | -            | character varying (90.0)    | -             | -        |
| sub_status         | -            | character varying (150.0)   | -             | -        |
| type               | -            | character varying (60.0)    | -             | -        |
| updated_at         | -            | timestamp without time zone | -             | -        |
| version            | -            | integer                     | -             | -        |
| zone               | -            | character varying (60.0)    | -             | -        |



# scooter_issue

|                        | definition   | data_type                   | related_key   | remark   |
|:-----------------------|:-------------|:----------------------------|:--------------|:---------|
| city                   | -            | character varying (96.0)    | -             | -        |
| content                | -            | character varying (65535.0) | -             | -        |
| create_user_name       | -            | character varying (768.0)   | -             | -        |
| create_user_role       | -            | character varying (150.0)   | -             | -        |
| created_at             | -            | timestamp without time zone | -             | -        |
| created_by             | -            | bigint                      | -             | -        |
| device_id              | -            | bigint                      | -             | -        |
| high_risk              | -            | smallint                    | -             | -        |
| id                     | -            | bigint                      | -             | -        |
| maintenance_id         | -            | bigint                      | -             | -        |
| muted_users            | -            | character varying (65535.0) | -             | -        |
| scooter_latitude       | -            | numeric                     | -             | -        |
| scooter_longitude      | -            | numeric                     | -             | -        |
| status                 | -            | character varying (60.0)    | -             | -        |
| tag                    | -            | character varying (768.0)   | -             | -        |
| type                   | -            | character varying (300.0)   | -             | -        |
| update_user_name       | -            | character varying (768.0)   | -             | -        |
| update_user_role       | -            | character varying (150.0)   | -             | -        |
| updated_at             | -            | timestamp without time zone | -             | -        |
| updated_by             | -            | bigint                      | -             | -        |
| users_marked_important | -            | character varying (65535.0) | -             | -        |
| vehicle_type           | -            | character varying (96.0)    | -             | -        |



# scooter_issue_extend_info

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| id            | -            | bigint                      | -             | -        |
| issue_id      | -            | bigint                      | -             | -        |
| lang          | -            | character varying (60.0)    | -             | -        |
| need_callback | -            | smallint                    | -             | -        |
| phone         | -            | character varying (150.0)   | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |



# scooter_issue_image

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| created_at   | -            | timestamp without time zone | -             | -        |
| id           | -            | bigint                      | -             | -        |
| image_id     | -            | character varying (150.0)   | -             | -        |
| issue_id     | -            | bigint                      | -             | -        |
| issue_log_id | -            | bigint                      | -             | -        |



# scooter_issue_log

|                       | definition   | data_type                   | related_key   | remark   |
|:----------------------|:-------------|:----------------------------|:--------------|:---------|
| action                | -            | character varying (1500.0)  | -             | -        |
| action_template       | -            | character varying (1500.0)  | -             | -        |
| action_template_param | -            | character varying (1500.0)  | -             | -        |
| comment               | -            | character varying (3000.0)  | -             | -        |
| created_at            | -            | timestamp without time zone | -             | -        |
| created_by            | -            | bigint                      | -             | -        |
| id                    | -            | bigint                      | -             | -        |
| issue_id              | -            | bigint                      | -             | -        |
| type                  | -            | character varying (150.0)   | -             | -        |



# scooter_issue_notification

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| issue_id   | -            | bigint                      | -             | -        |
| message    | -            | character varying (768.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# scooter_maintenance

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| comment      | -            | character varying (65535.0) | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| device_id    | -            | bigint                      | -             | -        |
| id           | -            | bigint                      | -             | -        |
| station_id   | -            | bigint                      | -             | -        |
| status       | -            | character varying (150.0)   | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| updated_by   | -            | bigint                      | -             | -        |
| vehicle_type | -            | character varying (96.0)    | -             | -        |



# scooter_maintenance_status_record

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| created_at     | -            | timestamp without time zone | -             | -        |
| created_by     | -            | bigint                      | -             | -        |
| device_id      | -            | bigint                      | -             | -        |
| id             | -            | bigint                      | -             | -        |
| maintenance_id | -            | bigint                      | -             | -        |
| status         | -            | character varying (150.0)   | -             | -        |



# scooter_missing_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (96.0)    | -             | -        |
| comment    | -            | character varying (1500.0)  | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| photos     | -            | character varying (1500.0)  | -             | -        |
| reason     | -            | character varying (750.0)   | -             | -        |
| scooter_id | -            | bigint                      | -             | -        |



# scooter_motor_number

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| id            | -            | bigint                      | -             | -        |
| produce_date  | -            | timestamp without time zone | -             | -        |
| serial_number | -            | character varying (150.0)   | -             | -        |



# scooter_reservation

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| city         | -            | character varying (60.0)    | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| device_id    | -            | bigint                      | -             | -        |
| id           | -            | bigint                      | -             | -        |
| status       | -            | character varying (60.0)    | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| updated_by   | -            | bigint                      | -             | -        |
| user_id      | -            | bigint                      | -             | -        |
| valid_count  | -            | smallint                    | -             | -        |
| vehicle_type | -            | character varying (96.0)    | -             | -        |
| zone         | -            | character varying (60.0)    | -             | -        |



# scooter_retired_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (96.0)    | -             | -        |
| comment    | -            | character varying (1500.0)  | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| photos     | -            | character varying (1500.0)  | -             | -        |
| reason     | -            | character varying (750.0)   | -             | -        |
| scooter_id | -            | bigint                      | -             | -        |
| type       | -            | character varying (150.0)   | -             | -        |



# scooter_sanitize_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (150.0)   | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| scooter_id | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# scooter_speed_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| imei       | -            | character varying (96.0)    | -             | -        |
| mode       | -            | character varying (48.0)    | -             | -        |
| speed      | -            | smallint                    | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# scooter_status_record

|                            | definition   | data_type                   | related_key   | remark   |
|:---------------------------|:-------------|:----------------------------|:--------------|:---------|
| battery                    | -            | double precision            | -             | -        |
| city                       | -            | character varying (96.0)    | -             | -        |
| created_at                 | -            | timestamp without time zone | -             | -        |
| device_id                  | -            | bigint                      | -             | -        |
| id                         | -            | bigint                      | -             | -        |
| object_id                  | -            | bigint                      | -             | -        |
| previous_status            | -            | character varying (60.0)    | -             | -        |
| previous_status_created_at | -            | timestamp without time zone | -             | -        |
| previous_sub_status        | -            | character varying (150.0)   | -             | -        |
| previous_zone              | -            | character varying (90.0)    | -             | -        |
| scooter_latitude           | -            | double precision            | -             | -        |
| scooter_longitude          | -            | double precision            | -             | -        |
| status                     | -            | character varying (60.0)    | -             | -        |
| sub_status                 | -            | character varying (150.0)   | -             | -        |
| vehicle_type               | -            | character varying (96.0)    | -             | -        |
| zone                       | -            | character varying (60.0)    | -             | -        |



# shop

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| address       | -            | character varying (768.0)   | -             | -        |
| category      | -            | character varying (60.0)    | -             | -        |
| city          | -            | character varying (60.0)    | -             | -        |
| contact_email | -            | character varying (768.0)   | -             | -        |
| contact_name  | -            | character varying (768.0)   | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| deleted       | -            | smallint                    | -             | -        |
| disabled      | -            | smallint                    | -             | -        |
| id            | -            | character varying (48.0)    | -             | -        |
| image         | -            | character varying (768.0)   | -             | -        |
| latitude      | -            | numeric                     | -             | -        |
| longitude     | -            | numeric                     | -             | -        |
| name          | -            | character varying (768.0)   | -             | -        |
| phone         | -            | character varying (60.0)    | -             | -        |
| suburb        | -            | character varying (90.0)    | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| website       | -            | character varying (1536.0)  | -             | -        |



# shop_offer

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| deleted      | -            | smallint                    | -             | -        |
| detail       | -            | character varying (65535.0) | -             | -        |
| disabled     | -            | smallint                    | -             | -        |
| frequency    | -            | character varying (60.0)    | -             | -        |
| id           | -            | bigint                      | -             | -        |
| shop_id      | -            | character varying (48.0)    | -             | -        |
| title        | -            | character varying (150.0)   | -             | -        |
| total_number | -            | bigint                      | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| updated_by   | -            | bigint                      | -             | -        |



# shop_offer_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| offer_id   | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# sign_up_record

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| app_version  | -            | character varying (192.0)   | -             | -        |
| country_code | -            | character varying (48.0)    | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| device_id    | -            | character varying (192.0)   | -             | -        |
| email        | -            | character varying (300.0)   | -             | -        |
| geo_city     | -            | character varying (300.0)   | -             | -        |
| id           | -            | bigint                      | -             | -        |
| ip_city      | -            | character varying (300.0)   | -             | -        |
| latitude     | -            | double precision            | -             | -        |
| longitude    | -            | double precision            | -             | -        |
| mobile       | -            | character varying (150.0)   | -             | -        |
| remote_ip    | -            | character varying (150.0)   | -             | -        |
| social_id    | -            | character varying (300.0)   | -             | -        |
| social_type  | -            | character varying (96.0)    | -             | -        |
| system       | -            | character varying (192.0)   | -             | -        |
| user_city    | -            | character varying (96.0)    | -             | -        |
| user_id      | -            | bigint                      | -             | -        |



# sms_delivery_record

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| country_code | -            | character varying (24.0)    | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| error_code   | -            | integer                     | -             | -        |
| error_msg    | -            | character varying (1536.0)  | -             | -        |
| id           | -            | bigint                      | -             | -        |
| mobile       | -            | character varying (96.0)    | -             | -        |
| remote_ip    | -            | character varying (96.0)    | -             | -        |
| status       | -            | character varying (96.0)    | -             | -        |
| type         | -            | character varying (48.0)    | -             | -        |



# station

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| city           | -            | character varying (60.0)    | -             | -        |
| created_at     | -            | timestamp without time zone | -             | -        |
| deleted        | -            | smallint                    | -             | -        |
| description    | -            | character varying (1500.0)  | -             | -        |
| end_date       | -            | character varying (60.0)    | -             | -        |
| end_time       | -            | character varying (30.0)    | -             | -        |
| gps_radius     | -            | double precision            | -             | -        |
| guide          | -            | character varying (1536.0)  | -             | -        |
| ibeacon_radius | -            | double precision            | -             | -        |
| id             | -            | bigint                      | -             | -        |
| latitude       | -            | double precision            | -             | -        |
| longitude      | -            | double precision            | -             | -        |
| name           | -            | character varying (765.0)   | -             | -        |
| optimal_number | -            | integer                     | -             | -        |
| photo          | -            | character varying (1500.0)  | -             | -        |
| qr_code        | -            | character varying (60.0)    | -             | -        |
| start_date     | -            | character varying (60.0)    | -             | -        |
| start_time     | -            | character varying (30.0)    | -             | -        |
| type           | -            | character varying (90.0)    | -             | -        |
| updated_at     | -            | timestamp without time zone | -             | -        |
| vehicle_type   | -            | character varying (96.0)    | -             | -        |
| weekdays       | -            | character varying (96.0)    | -             | -        |
| zone           | -            | character varying (60.0)    | -             | -        |



# station_images

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| image_id   | -            | character varying (192.0)   | -             | -        |
| sort       | -            | smallint                    | -             | -        |
| station_id | -            | bigint                      | -             | -        |



# station_location_request

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| comment    | -            | character varying (65535.0) | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| email      | -            | character varying (300.0)   | -             | -        |
| id         | -            | bigint                      | -             | -        |
| latitude   | -            | double precision            | -             | -        |
| longitude  | -            | double precision            | -             | -        |
| name       | -            | character varying (300.0)   | -             | -        |



# station_optimal

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| capacity   | -            | integer                     | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| end_time   | -            | character varying (30.0)    | -             | -        |
| id         | -            | bigint                      | -             | -        |
| start_time | -            | character varying (30.0)    | -             | -        |
| station_id | -            | bigint                      | -             | -        |
| weekday    | -            | character varying (30.0)    | -             | -        |



# status

|                       | definition   | data_type                   | related_key   | remark   |
|:----------------------|:-------------|:----------------------------|:--------------|:---------|
| battery_percentage    | -            | double precision            | -             | -        |
| communication_failure | -            | smallint                    | -             | -        |
| controller_failure    | -            | smallint                    | -             | -        |
| created_at            | -            | timestamp without time zone | -             | -        |
| current               | -            | integer                     | -             | -        |
| device_id             | -            | bigint                      | -             | -        |
| id                    | -            | bigint                      | -             | -        |
| motor_phase           | -            | smallint                    | -             | -        |
| speed                 | -            | double precision            | -             | -        |
| turn_brushless        | -            | smallint                    | -             | -        |
| voltage               | -            | numeric                     | -             | -        |
| voltage_protection    | -            | smallint                    | -             | -        |



# stripe_exception_record

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| charge_id    | -            | character varying (384.0)   | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| decline_code | -            | character varying (768.0)   | -             | -        |
| id           | -            | bigint                      | -             | -        |
| message      | -            | character varying (3072.0)  | -             | -        |
| order_id     | -            | bigint                      | -             | -        |
| pass_id      | -            | bigint                      | -             | -        |
| pay_method   | -            | character varying (96.0)    | -             | -        |
| platform     | -            | character varying (96.0)    | -             | -        |
| type         | -            | character varying (48.0)    | -             | -        |
| user_id      | -            | bigint                      | -             | -        |



# top_up_card

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| code        | -            | character varying (60.0)    | -             | -        |
| created_at  | -            | timestamp without time zone | -             | -        |
| created_by  | -            | bigint                      | -             | -        |
| id          | -            | bigint                      | -             | -        |
| redeemed_by | -            | bigint                      | -             | -        |
| status      | -            | character varying (48.0)    | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| value       | -            | numeric                     | -             | -        |



# trip

|                     | definition   | data_type                   | related_key   | remark   |
|:--------------------|:-------------|:----------------------------|:--------------|:---------|
| app_version         | -            | character varying (60.0)    | -             | -        |
| auto_end            | -            | smallint                    | -             | -        |
| battery_no          | -            | character varying (192.0)   | -             | -        |
| bt_unlock           | -            | smallint                    | -             | -        |
| campaign_id         | -            | bigint                      | -             | -        |
| city                | -            | character varying (60.0)    | -             | -        |
| device_id           | -            | bigint                      | -             | -        |
| email               | -            | character varying (300.0)   | -             | -        |
| end_latitude        | -            | double precision            | -             | -        |
| end_longitude       | -            | double precision            | -             | -        |
| end_no_riding       | -            | smallint                    | -             | -        |
| end_time            | -            | timestamp without time zone | -             | -        |
| end_zone            | -            | character varying (60.0)    | -             | -        |
| engine_off_minutes  | -            | integer                     | -             | -        |
| force_end           | -            | smallint                    | -             | -        |
| gold_trip           | -            | smallint                    | -             | -        |
| gps_id              | -            | bigint                      | -             | -        |
| group_id            | -            | character varying (192.0)   | -             | -        |
| helmet_returned     | -            | smallint                    | -             | -        |
| helmet_wore         | -            | smallint                    | -             | -        |
| id                  | -            | bigint                      | -             | -        |
| imei                | -            | character varying (96.0)    | -             | -        |
| is_short_trip       | -            | smallint                    | -             | -        |
| is_vip              | -            | smallint                    | -             | -        |
| mx_user_id          | -            | character varying (300.0)   | -             | -        |
| positions           | -            | character varying (65535.0) | -             | -        |
| require_helmet      | -            | smallint                    | -             | -        |
| returned_station_id | -            | bigint                      | -             | -        |
| ride_mile           | -            | bigint                      | -             | -        |
| start_latitude      | -            | double precision            | -             | -        |
| start_longitude     | -            | double precision            | -             | -        |
| start_no_riding     | -            | smallint                    | -             | -        |
| start_station_id    | -            | bigint                      | -             | -        |
| start_time          | -            | timestamp without time zone | -             | -        |
| start_zone          | -            | character varying (60.0)    | -             | -        |
| temp_lock           | -            | smallint                    | -             | -        |
| total_mileage       | -            | double precision            | -             | -        |
| total_minutes       | -            | integer                     | -             | -        |
| transfer_ride       | -            | smallint                    | -             | -        |
| trip_status         | -            | character varying (48.0)    | -             | -        |
| user_id             | -            | bigint                      | -             | -        |
| user_name           | -            | character varying (300.0)   | -             | -        |
| vehicle_type        | -            | character varying (96.0)    | -             | -        |
| zone                | -            | character varying (60.0)    | -             | -        |



# trip_alert

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| alert_time    | -            | timestamp without time zone | -             | -        |
| complete_time | -            | timestamp without time zone | -             | -        |
| duration      | -            | bigint                      | -             | -        |
| id            | -            | numeric                     | -             | -        |
| is_read       | -            | smallint                    | -             | -        |
| scooter_id    | -            | bigint                      | -             | -        |
| status        | -            | character varying (75.0)    | -             | -        |
| trip_id       | -            | bigint                      | -             | -        |
| type          | -            | character varying (75.0)    | -             | -        |
| user_id       | -            | bigint                      | -             | -        |



# trip_deposit

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| amount          | -            | double precision            | -             | -        |
| captured_amount | -            | double precision            | -             | -        |
| charge_id       | -            | character varying (192.0)   | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| id              | -            | bigint                      | -             | -        |
| job_id          | -            | character varying (96.0)    | -             | -        |
| status          | -            | character varying (96.0)    | -             | -        |
| trip_id         | -            | bigint                      | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |
| user_id         | -            | bigint                      | -             | -        |



# trip_end_record

|                     | definition   | data_type                   | related_key   | remark   |
|:--------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at          | -            | timestamp without time zone | -             | -        |
| gps_id              | -            | bigint                      | -             | -        |
| id                  | -            | bigint                      | -             | -        |
| return_station_id   | -            | bigint                      | -             | -        |
| scooter_id          | -            | bigint                      | -             | -        |
| scooter_latitude    | -            | numeric                     | -             | -        |
| scooter_longitude   | -            | numeric                     | -             | -        |
| station_source_type | -            | character varying (90.0)    | -             | -        |
| trip_id             | -            | bigint                      | -             | -        |
| user_gps_time       | -            | timestamp without time zone | -             | -        |
| user_id             | -            | bigint                      | -             | -        |
| user_latitude       | -            | numeric                     | -             | -        |
| user_longitude      | -            | numeric                     | -             | -        |



# trip_error_record

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| device_id     | -            | bigint                      | -             | -        |
| error_code    | -            | character varying (150.0)   | -             | -        |
| error_message | -            | character varying (765.0)   | -             | -        |
| id            | -            | bigint                      | -             | -        |
| station_id    | -            | bigint                      | -             | -        |
| trip_id       | -            | bigint                      | -             | -        |
| user_id       | -            | bigint                      | -             | -        |



# trip_event_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| trip_event | -            | character varying (150.0)   | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| trip_info  | -            | character varying (765.0)   | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# trip_finish_source

|                    | definition   | data_type                   | related_key   | remark   |
|:-------------------|:-------------|:----------------------------|:--------------|:---------|
| bluetooth_locked   | -            | smallint                    | -             | -        |
| created_at         | -            | timestamp without time zone | -             | -        |
| device_active_time | -            | timestamp without time zone | -             | -        |
| device_latitude    | -            | double precision            | -             | -        |
| device_longitude   | -            | double precision            | -             | -        |
| id                 | -            | bigint                      | -             | -        |
| image_id           | -            | character varying (192.0)   | -             | -        |
| scooter_id         | -            | bigint                      | -             | -        |
| scooter_locked     | -            | smallint                    | -             | -        |
| station_id         | -            | bigint                      | -             | -        |
| station_latitude   | -            | double precision            | -             | -        |
| station_longitude  | -            | double precision            | -             | -        |
| trip_id            | -            | bigint                      | -             | -        |
| type               | -            | character varying (150.0)   | -             | -        |
| updated_at         | -            | timestamp without time zone | -             | -        |
| user_gps_time      | -            | timestamp without time zone | -             | -        |
| user_id            | -            | bigint                      | -             | -        |
| user_latitude      | -            | double precision            | -             | -        |
| user_longitude     | -            | double precision            | -             | -        |



# trip_helmet

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| checked    | -            | smallint                    | -             | -        |
| city       | -            | character varying (48.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| id         | -            | bigint                      | -             | -        |
| image_id   | -            | character varying (192.0)   | -             | -        |
| method     | -            | character varying (192.0)   | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| updated_by | -            | bigint                      | -             | -        |



# trip_parking_photo

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| check_answers | -            | character varying (1500.0)  | -             | -        |
| checked       | -            | smallint                    | -             | -        |
| city          | -            | character varying (60.0)    | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| duration      | -            | bigint                      | -             | -        |
| id            | -            | bigint                      | -             | -        |
| image_id      | -            | character varying (192.0)   | -             | -        |
| reminded      | -            | smallint                    | -             | -        |
| status        | -            | character varying (60.0)    | -             | -        |
| trip_id       | -            | bigint                      | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| user_id       | -            | bigint                      | -             | -        |
| vehicle_id    | -            | bigint                      | -             | -        |



# trip_quartz_checker

|                          | definition   | data_type                   | related_key   | remark   |
|:-------------------------|:-------------|:----------------------------|:--------------|:---------|
| auto_end_job_id          | -            | character varying (600.0)   | -             | -        |
| created_at               | -            | timestamp without time zone | -             | -        |
| dangerous_driving_job_id | -            | character varying (600.0)   | -             | -        |
| id                       | -            | bigint                      | -             | -        |
| sidewalk_job_id          | -            | character varying (600.0)   | -             | -        |
| trip_id                  | -            | bigint                      | -             | -        |
| user_id                  | -            | bigint                      | -             | -        |



# trip_rating

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| app_version | -            | character varying (48.0)    | -             | -        |
| comment     | -            | character varying (6000.0)  | -             | -        |
| created_at  | -            | timestamp without time zone | -             | -        |
| id          | -            | bigint                      | -             | -        |
| rating      | -            | integer                     | -             | -        |
| system      | -            | character varying (48.0)    | -             | -        |
| trip_id     | -            | bigint                      | -             | -        |
| user_id     | -            | bigint                      | -             | -        |



# trip_report

|                       | definition   | data_type                   | related_key   | remark   |
|:----------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at            | -            | timestamp without time zone | -             | -        |
| id                    | -            | bigint                      | -             | -        |
| is_viewed             | -            | smallint                    | -             | -        |
| trip_helmet_id        | -            | bigint                      | -             | -        |
| trip_id               | -            | bigint                      | -             | -        |
| trip_parking_photo_id | -            | bigint                      | -             | -        |
| trip_sidewalk_id      | -            | bigint                      | -             | -        |
| updated_at            | -            | timestamp without time zone | -             | -        |
| user_id               | -            | bigint                      | -             | -        |



# trip_rider_info

|                        | definition   | data_type                   | related_key   | remark   |
|:-----------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at             | -            | timestamp without time zone | -             | -        |
| driver_license         | -            | character varying (768.0)   | -             | -        |
| driver_license_country | -            | character varying (48.0)    | -             | -        |
| email                  | -            | character varying (384.0)   | -             | -        |
| first_name             | -            | character varying (192.0)   | -             | -        |
| full_name              | -            | character varying (65535.0) | -             | -        |
| id                     | -            | bigint                      | -             | -        |
| is_account_holder      | -            | smallint                    | -             | -        |
| last_name              | -            | character varying (192.0)   | -             | -        |
| phone                  | -            | character varying (48.0)    | -             | -        |
| trip_id                | -            | bigint                      | -             | -        |



# trip_section

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| end_time   | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| mileage    | -            | double precision            | -             | -        |
| start_time | -            | timestamp without time zone | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |



# trip_sidewalk

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| result     | -            | smallint                    | -             | -        |
| source     | -            | character varying (192.0)   | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |



# trip_trace

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| id            | -            | bigint                      | -             | -        |
| positions     | -            | character varying (65535.0) | -             | -        |
| raw_positions | -            | character varying (64512.0) | -             | -        |
| trip_id       | -            | bigint                      | -             | -        |



# user_card

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| created_at  | -            | timestamp without time zone | -             | -        |
| deleted     | -            | smallint                    | -             | -        |
| fingerprint | -            | character varying (192.0)   | -             | -        |
| id          | -            | bigint                      | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| updated_by  | -            | character varying (192.0)   | -             | -        |
| user_id     | -            | bigint                      | -             | -        |
| verified    | -            | smallint                    | -             | -        |



# user_city_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (60.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_concession_evidence

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| audited_at    | -            | timestamp without time zone | -             | -        |
| audited_by    | -            | bigint                      | -             | -        |
| check_answers | -            | character varying (1500.0)  | -             | -        |
| city          | -            | character varying (60.0)    | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| full_name     | -            | character varying (150.0)   | -             | -        |
| id            | -            | bigint                      | -             | -        |
| remark        | -            | character varying (1500.0)  | -             | -        |
| status        | -            | character varying (60.0)    | -             | -        |
| type          | -            | character varying (150.0)   | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| user_id       | -            | bigint                      | -             | -        |



# user_concession_evidence_image

|                        | definition   | data_type                 | related_key   | remark   |
|:-----------------------|:-------------|:--------------------------|:--------------|:---------|
| concession_evidence_id | -            | bigint                    | -             | -        |
| id                     | -            | bigint                    | -             | -        |
| image_id               | -            | character varying (192.0) | -             | -        |



# user_concession_evidence_request

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| city       | -            | character varying (60.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| full_name  | -            | character varying (150.0)   | -             | -        |
| id         | -            | bigint                      | -             | -        |
| type       | -            | character varying (150.0)   | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| updated_by | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_coupon

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| balance       | -            | numeric                     | -             | -        |
| code          | -            | character varying (768.0)   | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| currency      | -            | character varying (48.0)    | -             | -        |
| discount      | -            | smallint                    | -             | -        |
| expired_at    | -            | timestamp without time zone | -             | -        |
| id            | -            | bigint                      | -             | -        |
| is_read       | -            | smallint                    | -             | -        |
| pass_days     | -            | smallint                    | -             | -        |
| pass_id       | -            | bigint                      | -             | -        |
| promo_code_id | -            | bigint                      | -             | -        |
| scope         | -            | character varying (45.0)    | -             | -        |
| source        | -            | character varying (768.0)   | -             | -        |
| status        | -            | character varying (192.0)   | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| user_id       | -            | bigint                      | -             | -        |



# user_device_info

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| app_version | -            | character varying (90.0)    | -             | -        |
| brand       | -            | character varying (150.0)   | -             | -        |
| created_at  | -            | timestamp without time zone | -             | -        |
| device_id   | -            | character varying (192.0)   | -             | -        |
| id          | -            | bigint                      | -             | -        |
| lang        | -            | character varying (30.0)    | -             | -        |
| model       | -            | character varying (150.0)   | -             | -        |
| sys_version | -            | character varying (90.0)    | -             | -        |
| system      | -            | character varying (60.0)    | -             | -        |
| type        | -            | character varying (90.0)    | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| user_id     | -            | bigint                      | -             | -        |



# user_device_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| device_id  | -            | character varying (384.0)   | -             | -        |
| id         | -            | bigint                      | -             | -        |
| platform   | -            | character varying (96.0)    | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_gps_tracing

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| accuracy   | -            | numeric                     | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| fixed_time | -            | timestamp without time zone | -             | -        |
| id         | -            | numeric                     | -             | -        |
| latitude   | -            | numeric                     | -             | -        |
| longitude  | -            | numeric                     | -             | -        |
| speed      | -            | numeric                     | -             | -        |
| trip_id    | -            | bigint                      | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_identification

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| created_at    | -            | timestamp without time zone | -             | -        |
| created_by    | -            | bigint                      | -             | -        |
| id            | -            | bigint                      | -             | -        |
| issue_country | -            | character varying (96.0)    | -             | -        |
| type          | -            | character varying (96.0)    | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| updated_by    | -            | bigint                      | -             | -        |
| user_id       | -            | bigint                      | -             | -        |



# user_identification_file

|                        | definition   | data_type                 | related_key   | remark   |
|:-----------------------|:-------------|:--------------------------|:--------------|:---------|
| file_id                | -            | character varying (384.0) | -             | -        |
| file_type              | -            | character varying (96.0)  | -             | -        |
| id                     | -            | bigint                    | -             | -        |
| user_identification_id | -            | bigint                    | -             | -        |



# user_issue_notification_config

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| config     | -            | character varying (3000.0)  | -             | -        |
| id         | -            | bigint                      | -             | -        |
| mute_date  | -            | timestamp without time zone | -             | -        |
| mute_type  | -            | character varying (60.0)    | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_license

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at      | -            | timestamp without time zone | -             | -        |
| data            | -            | character varying (65535.0) | -             | -        |
| file_id         | -            | character varying (192.0)   | -             | -        |
| first_name      | -            | character varying (192.0)   | -             | -        |
| full_name       | -            | character varying (65535.0) | -             | -        |
| id              | -            | bigint                      | -             | -        |
| last_name       | -            | character varying (192.0)   | -             | -        |
| licence_country | -            | character varying (96.0)    | -             | -        |
| licence_number  | -            | character varying (192.0)   | -             | -        |
| type            | -            | character varying (48.0)    | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |
| user_id         | -            | bigint                      | -             | -        |
| verified        | -            | smallint                    | -             | -        |



# user_license_scan_record

|                    | definition   | data_type                   | related_key   | remark   |
|:-------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at         | -            | timestamp without time zone | -             | -        |
| data               | -            | character varying (65535.0) | -             | -        |
| encrypted_raw_data | -            | character varying (65535.0) | -             | -        |
| full_name          | -            | character varying (765.0)   | -             | -        |
| id                 | -            | bigint                      | -             | -        |
| license_country    | -            | character varying (60.0)    | -             | -        |
| license_number     | -            | character varying (150.0)   | -             | -        |
| scan_result_detail | -            | character varying (65535.0) | -             | -        |
| scan_status        | -            | character varying (60.0)    | -             | -        |
| user_id            | -            | bigint                      | -             | -        |



# user_location_reader

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| browser         | -            | character varying (3072.0)  | -             | -        |
| created_at      | -            | timestamp without time zone | -             | -        |
| id              | -            | bigint                      | -             | -        |
| ip              | -            | character varying (192.0)   | -             | -        |
| share_record_id | -            | bigint                      | -             | -        |
| updated_at      | -            | timestamp without time zone | -             | -        |



# user_location_share

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| created_at   | -            | timestamp without time zone | -             | -        |
| id           | -            | bigint                      | -             | -        |
| share_status | -            | smallint                    | -             | -        |
| token        | -            | character varying (192.0)   | -             | -        |
| trip_id      | -            | bigint                      | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| user_id      | -            | bigint                      | -             | -        |



# user_mpu

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at      | -            | timestamp without time zone | -             | -        |
| device_time     | -            | timestamp without time zone | -             | -        |
| id              | -            | bigint                      | -             | -        |
| sensor_time     | -            | bigint                      | -             | -        |
| user_id         | -            | bigint                      | -             | -        |
| x_acceleration  | -            | double precision            | -             | -        |
| x_rotation_rate | -            | double precision            | -             | -        |
| y_acceleration  | -            | double precision            | -             | -        |
| y_rotation_rate | -            | double precision            | -             | -        |
| z_acceleration  | -            | double precision            | -             | -        |
| z_rotation_rate | -            | double precision            | -             | -        |



# user_notification

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| created_at   | -            | timestamp without time zone | -             | -        |
| created_by   | -            | bigint                      | -             | -        |
| description  | -            | character varying (3072.0)  | -             | -        |
| id           | -            | bigint                      | -             | -        |
| name         | -            | character varying (384.0)   | -             | -        |
| trigger_corn | -            | character varying (96.0)    | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| updated_by   | -            | bigint                      | -             | -        |



# user_pass

|                          | definition   | data_type                   | related_key   | remark   |
|:-------------------------|:-------------|:----------------------------|:--------------|:---------|
| created_at               | -            | timestamp without time zone | -             | -        |
| created_by               | -            | bigint                      | -             | -        |
| expired_at               | -            | timestamp without time zone | -             | -        |
| future_pass_id           | -            | bigint                      | -             | -        |
| future_subscribe         | -            | smallint                    | -             | -        |
| id                       | -            | bigint                      | -             | -        |
| job_id                   | -            | character varying (192.0)   | -             | -        |
| organization_discount_id | -            | integer                     | -             | -        |
| pass_id                  | -            | bigint                      | -             | -        |
| purchase_city            | -            | character varying (96.0)    | -             | -        |
| purchase_price           | -            | numeric                     | -             | -        |
| refund                   | -            | numeric                     | -             | -        |
| started_at               | -            | timestamp without time zone | -             | -        |
| subscribe                | -            | smallint                    | -             | -        |
| updated_at               | -            | timestamp without time zone | -             | -        |
| updated_by               | -            | bigint                      | -             | -        |
| user_coupon_id           | -            | bigint                      | -             | -        |
| user_id                  | -            | bigint                      | -             | -        |



# user_pass_period

|              | definition   | data_type                   | related_key   | remark   |
|:-------------|:-------------|:----------------------------|:--------------|:---------|
| balance      | -            | integer                     | -             | -        |
| created_at   | -            | timestamp without time zone | -             | -        |
| end_time     | -            | timestamp without time zone | -             | -        |
| id           | -            | bigint                      | -             | -        |
| start_time   | -            | timestamp without time zone | -             | -        |
| updated_at   | -            | timestamp without time zone | -             | -        |
| user_pass_id | -            | bigint                      | -             | -        |



# user_promo_coupon

|               | definition   | data_type                   | related_key   | remark   |
|:--------------|:-------------|:----------------------------|:--------------|:---------|
| code          | -            | character varying (30.0)    | -             | -        |
| created_at    | -            | timestamp without time zone | -             | -        |
| email         | -            | character varying (765.0)   | -             | -        |
| id            | -            | bigint                      | -             | -        |
| promo_code_id | -            | bigint                      | -             | -        |
| reaction_time | -            | double precision            | -             | -        |
| scope         | -            | character varying (60.0)    | -             | -        |
| type          | -            | character varying (60.0)    | -             | -        |
| updated_at    | -            | timestamp without time zone | -             | -        |
| user_id       | -            | bigint                      | -             | -        |
| value         | -            | numeric                     | -             | -        |



# user_referral

|             | definition   | data_type                   | related_key   | remark   |
|:------------|:-------------|:----------------------------|:--------------|:---------|
| code        | -            | character varying (48.0)    | -             | -        |
| created_at  | -            | timestamp without time zone | -             | -        |
| id          | -            | bigint                      | -             | -        |
| referrer_id | -            | bigint                      | -             | -        |
| updated_at  | -            | timestamp without time zone | -             | -        |
| user_id     | -            | bigint                      | -             | -        |



# user_setting

|                      | definition   | data_type                   | related_key   | remark   |
|:---------------------|:-------------|:----------------------------|:--------------|:---------|
| beginner_mode        | -            | smallint                    | -             | -        |
| created_at           | -            | timestamp without time zone | -             | -        |
| has_finished_trip    | -            | smallint                    | -             | -        |
| id                   | -            | bigint                      | -             | -        |
| updated_at           | -            | timestamp without time zone | -             | -        |
| user_agree_insurance | -            | smallint                    | -             | -        |
| user_agree_pic       | -            | smallint                    | -             | -        |
| user_agree_tos       | -            | smallint                    | -             | -        |
| user_id              | -            | bigint                      | -             | -        |



# user_status_record

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| status     | -            | character varying (150.0)   | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# wallet

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| balance    | -            | numeric                     | -             | -        |
| cash       | -            | numeric                     | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| credit     | -            | numeric                     | -             | -        |
| currency   | -            | character varying (48.0)    | -             | -        |
| id         | -            | bigint                      | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# zone

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| base_fee   | -            | numeric                     | -             | -        |
| city_code  | -            | character varying (60.0)    | -             | -        |
| code       | -            | character varying (60.0)    | -             | -        |
| created_at | -            | timestamp without time zone | -             | -        |
| created_by | -            | bigint                      | -             | -        |
| deleted    | -            | smallint                    | -             | -        |
| name       | -            | character varying (765.0)   | -             | -        |
| unit_fee   | -            | numeric                     | -             | -        |
| updated_at | -            | timestamp without time zone | -             | -        |
| updated_by | -            | bigint                      | -             | -        |

