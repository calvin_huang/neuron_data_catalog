[TOC]

# card_block

|             | definition                                           | data_type                   | related_key   | remark                                              |
|:------------|:-----------------------------------------------------|:----------------------------|:--------------|:----------------------------------------------------|
| account     | Country of account                                   | character varying (24.0)    | -             | -                                                   |
| blocked     | Flag if card is blocked                              | smallint                    | -             | yes = 1; no = 0                                     |
| comment     | Comment made (if any) (webhook/stripeRadar)          | character varying (1536.0)  | -             | Any string to indicate                              |
| count       | Number of times user try to bind or charge this card | integer                     | -             | Inaacurate due to duplicate count (as of 31/3/2020) |
| created_at  | Timestamp of when row was created                    | timestamp without time zone | -             | -                                                   |
| created_by  | User ID that created the row                         | bigint                      | -             | -                                                   |
| fingerprint | Fingerprint that was blocked                         | character varying (192.0)   | -             | -                                                   |
| id          | Unique ID for table                                  | bigint                      | -             | -                                                   |
| reason      | Reason that it was blocked                           | character varying (192.0)   | -             | -                                                   |
| source      | Stripe related eventID                               | character varying (192.0)   | -             | -                                                   |
| updated_at  | Timestamp of when row was updated                    | timestamp without time zone | -             | -                                                   |
| updated_by  | User ID that updated the row                         | bigint                      | -             | -                                                   |



# flyway_schema_history

|                | definition   | data_type                   | related_key   | remark   |
|:---------------|:-------------|:----------------------------|:--------------|:---------|
| checksum       | -            | integer                     | -             | -        |
| description    | -            | character varying (600.0)   | -             | -        |
| execution_time | -            | integer                     | -             | -        |
| installed_by   | -            | character varying (300.0)   | -             | -        |
| installed_on   | -            | timestamp without time zone | -             | -        |
| installed_rank | -            | integer                     | -             | -        |
| script         | -            | character varying (3000.0)  | -             | -        |
| success        | -            | smallint                    | -             | -        |
| type           | -            | character varying (60.0)    | -             | -        |
| version        | -            | character varying (150.0)   | -             | -        |



# password_update_record

|                 | definition   | data_type                   | related_key   | remark   |
|:----------------|:-------------|:----------------------------|:--------------|:---------|
| created_at      | -            | timestamp without time zone | -             | -        |
| created_by      | -            | bigint                      | -             | -        |
| id              | -            | bigint                      | -             | -        |
| password        | -            | character varying (768.0)   | -             | -        |
| password_length | -            | smallint                    | -             | -        |
| user_id         | -            | bigint                      | -             | -        |



# permission

|            | definition                                       | data_type                   | related_key   | remark   |
|:-----------|:-------------------------------------------------|:----------------------------|:--------------|:---------|
| created_at | Timestamp of when row was created                | timestamp without time zone | -             | -        |
| id         | Unique ID for the table                          | bigint                      | -             | -        |
| name       | Name of different permissions for access control | character varying (192.0)   | -             | -        |



# role

|                | definition                        | data_type                   | related_key   | remark   |
|:---------------|:----------------------------------|:----------------------------|:--------------|:---------|
| id             | Unique ID for the table           | bigint                      | -             | -        |
| parent_role_id | Entity creation order ID          | bigint                      | -             | -        |
| role_code      | Different roles                   | character varying (150.0)   | -             | -        |
| updated_at     | Timestamp of when row was created | timestamp without time zone | -             | -        |
| updated_by     | User ID that updated the row      | bigint                      | -             | -        |



# role_permission

|               | definition                         | data_type                   | related_key   | remark   |
|:--------------|:-----------------------------------|:----------------------------|:--------------|:---------|
| created_at    | Timestamp of when row was created  | timestamp without time zone | -             | -        |
| id            | Unique ID for the table            | bigint                      | -             | -        |
| permission_id | Unique permission ID for each role | bigint                      | permission.id | -        |
| role          | Different roles                    | character varying (150.0)   | -             | -        |



# role_resource_permission

|            | definition                         | data_type                   | related_key    | remark   |
|:-----------|:-----------------------------------|:----------------------------|:---------------|:---------|
| id         | Unique ID for the table            | bigint                      | -              | -        |
| permission | Allowed permissions for each role  | character varying (1500.0)  | -              | -        |
| resource   | Specific job scope within the role | character varying (150.0)   | -              | -        |
| role_code  | Different roles                    | character varying (150.0)   | role.role_code | -        |
| updated_at | Timestamp of when row was created  | timestamp without time zone | -              | -        |
| updated_by | User ID that updated the row       | bigint                      | -              | -        |



# user

|                        | definition                                | data_type                   | related_key   | remark          |
|:-----------------------|:------------------------------------------|:----------------------------|:--------------|:----------------|
| address                | -                                         | character varying (768.0)   | -             | -               |
| avatar                 | -                                         | character varying (768.0)   | -             | -               |
| city                   | -                                         | character varying (192.0)   | -             | -               |
| country                | -                                         | character varying (96.0)    | -             | -               |
| country_code           | country code based on mobile              | character varying (24.0)    | -             | -               |
| created_at             | Timestamp of when row was created         | timestamp without time zone | -             | -               |
| deleted                | Flag if user account is deleted           | smallint                    | -             | yes = 1; no = 0 |
| disable_reason         | -                                         | character varying (3072.0)  | -             | -               |
| disabled               | Flag if user account is disabled          | smallint                    | -             | yes = 1; no = 0 |
| driver_license         | -                                         | smallint                    | -             | yes = 1; no = 0 |
| email                  | Email address of user                     | character varying (384.0)   | -             | -               |
| email_marketing        | replace subscribed                        | smallint                    | -             | yes = 1; no = 0 |
| email_verified         | Flag if email account of user is verified | smallint                    | -             | yes = 1; no = 0 |
| full_name              | -                                         | character varying (384.0)   | -             | -               |
| id                     | Unique ID for the table                   | bigint                      | user_id       | -               |
| id_card                | -                                         | character varying (192.0)   | -             | -               |
| is_vip                 | -                                         | smallint                    | -             | -               |
| location_marketing     | -                                         | smallint                    | -             | yes = 1; no = 0 |
| mobile                 | Mobile number of user                     | character varying (96.0)    | -             | -               |
| nickname               | -                                         | character varying (384.0)   | -             | -               |
| notification_marketing | replace notification_subsdribed           | smallint                    | -             | yes = 1; no = 0 |
| password               | Encrypted password of user                | character varying (768.0)   | -             | -               |
| slogan                 | -                                         | character varying (768.0)   | -             | -               |
| sms_marketing          | -                                         | smallint                    | -             | -               |
| updated_at             | Timestamp of when row was updated         | timestamp without time zone | -             | -               |
| updated_by             | -                                         | bigint                      | -             | -               |
| usage_tracking         | -                                         | smallint                    | -             | yes = 1; no = 0 |
| user_protocol          | -                                         | smallint                    | -             | -               |
| username               | Unique username of user                   | character varying (768.0)   | -             | -               |



# user_country

|            | definition                                                                                                                        | data_type                   | related_key   | remark                                                                                         |
|:-----------|:----------------------------------------------------------------------------------------------------------------------------------|:----------------------------|:--------------|:-----------------------------------------------------------------------------------------------|
| city       | Location of user determined by scooter                                                                                            | character varying (192.0)   | -             | Recorded when user scans a scooter and location is tagged to scooter if unknown default to BNE |
| country    | Location of user determined by IP address (for new user) followed by user GPS geo country once user has logged into the home page | character varying (24.0)    | -             | Default to Australia if no relevant info available                                             |
| created_at | Timestamp of when row was created                                                                                                 | timestamp without time zone | -             | -                                                                                              |
| id         | Unique ID for the table                                                                                                           | bigint                      | -             | -                                                                                              |
| is_vip     | Flag if user is VIP                                                                                                               | smallint                    | -             | -                                                                                              |
| role_code  | Role of user                                                                                                                      | character varying (96.0)    | -             | -                                                                                              |
| status     | -                                                                                                                                 | character varying (96.0)    | -             | -                                                                                              |
| updated_at | Timestamp of when row was updated                                                                                                 | timestamp without time zone | -             | -                                                                                              |
| user_id    | Unique user ID                                                                                                                    | bigint                      | user.id       | -                                                                                              |



# user_mfa

|            | definition   | data_type                   | related_key   | remark   |
|:-----------|:-------------|:----------------------------|:--------------|:---------|
| created_at | -            | timestamp without time zone | -             | -        |
| id         | -            | bigint                      | -             | -        |
| secret     | -            | character varying (192.0)   | -             | -        |
| user_id    | -            | bigint                      | -             | -        |



# user_role

|            | definition                             | data_type                   | related_key    | remark   |
|:-----------|:---------------------------------------|:----------------------------|:---------------|:---------|
| city       | Location of user determined by scooter | character varying (60.0)    | -              | -        |
| created_at | Timestamp of when row was created      | timestamp without time zone | -              | -        |
| id         | Unique ID for the table                | bigint                      | -              | -        |
| role_code  | Different roles                        | character varying (150.0)   | role.role_code | -        |
| updated_at | Timestamp of when row was created      | timestamp without time zone | -              | -        |
| updated_by | User ID that updated the row           | bigint                      | -              | -        |
| user_id    | Unique User ID                         | bigint                      | user.id        | -        |



# user_social

|             | definition                        | data_type                   | related_key   | remark             |
|:------------|:----------------------------------|:----------------------------|:--------------|:-------------------|
| created_at  | Timestamp of when row was created | timestamp without time zone | -             | -                  |
| first_name  | -                                 | character varying (150.0)   | -             | -                  |
| id          | Unique ID for the table           | bigint                      | -             | -                  |
| last_name   | -                                 | character varying (150.0)   | -             | -                  |
| name        | -                                 | character varying (150.0)   | -             | -                  |
| social_id   | -                                 | character varying (150.0)   | -             | -                  |
| social_type | Platform                          | character varying (60.0)    | -             | Google or Facebook |
| updated_at  | Timestamp of when row was updated | timestamp without time zone | -             | -                  |
| user_id     | Unique user ID                    | bigint                      | user.id       | -                  |



# user_stripe

|             | definition                        | data_type                   | related_key   | remark        |
|:------------|:----------------------------------|:----------------------------|:--------------|:--------------|
| account     | country tied to the platform      | character varying (96.0)    | -             | AU NZ SG      |
| country     | Country of user                   | character varying (24.0)    | -             | -             |
| created_at  | Timestamp of when row was created | timestamp without time zone | -             | -             |
| customer_id | Unique ID of Stripe customer      | character varying (192.0)   | -             | -             |
| id          | Unique ID for the table           | bigint                      | -             | -             |
| platform    | platform used                     | character varying (96.0)    | -             | stripe paypal |
| primary     | primary account used for payment  | smallint                    | -             | -             |
| updated_at  | Timestamp of when row was updated | timestamp without time zone | -             | -             |
| user_id     | Unique ID of user                 | bigint                      | user.id       | -             |

