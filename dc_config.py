from dc_allFunctions import getCommonValues

# parameters for schema comparison based on similarity rate (sr)
sr_para = {
    'csv_file': db_access['query_outputName'],
    'schemas': [
        'neuron_australia',
        'neuron_canada',
        'neuron_gb',
        'neuron_korea',
        'neuron_newzealand'
    ],
    'num': 3,  # the number of elements for itertools.combinations
    'output_name': 'output-sr.csv',
    'include_col': True  # to compare the column_name or not (takes longer time)
}

oneSchema_para = {
    'csv_file': db_access['query_outputName'],
    'schema_name': ['neuron_user'],  # this should be List
    'md_name': 'output-neuron_user.md',
    'keys': ['user_id'],
    'json_name': 'output-neuron_user'
}

multiSchemas_para = {
    'csv_file': db_access['query_outputName'],
    'schema_names': [
        'neuron_australia',
        'neuron_canada'
    ],
    'md_name': 'output-multiSchemas.md',
    'keys_dict': {
        'neuron_australia': [
            'user_id',
            'trip_id'
        ],
        'neuron_canada': [
            'user_id',
            'trip_id',
            'scooter_id'
        ],
    },
    'json_name': 'output-multiSchemas'
}

combinedSchemas_para = {
    'csv_file': db_access['query_outputName'],
    'schemas_to_combine': [
        'neuron_gb',
        'neuron_australia',
        'neuron_newzealand',
        'neuron_canada',
        'neuron_korea'
    ],
    'md_name': 'output-fiveCountries.md',
    'open_para': 'a',
    'keys': [
        'user_id',
        'trip_id',
    ],
    'removed_table': getCommonValues('RRD_removedTable.csv'),
    'add_comp_table': False,
    'combined_name': 'neuron_fiveCountries',
    'json_name': 'output-fiveCountries'
}
