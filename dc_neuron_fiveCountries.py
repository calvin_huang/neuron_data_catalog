import dc_mainFunctions as Main


myToken = 's.eeuWBR9CnptAMqdP7X8x1N2S'

myQuery = '''
select table_schema, table_name, column_name, data_type, character_maximum_length
from information_schema.columns
where table_schema = 'neuron_gb' 
or table_schema = 'neuron_australia'
or table_schema = 'neuron_newzealand'
or table_schema = 'neuron_canada'
or table_schema = 'neuron_korea'
'''

myOutput = 'query-neuron_fiveCountries.csv'

myPara = {
    'csv_file': myOutput,
    'schemas_to_combine': [
        'neuron_gb',
        'neuron_australia',
        'neuron_newzealand',
        'neuron_canada',
        'neuron_korea'
    ],
    'json_name': 'catalog-neuron_fiveCountries-sys.json',
    'md_name': 'catalog-neuron_fiveCountries-sys.md',
    'open_para': 'w',
    'keys': [
        'user_id',
        'trip_id',
    ],
    # 'removed_table': getCommonValues('RRD_removedTable.csv'),
    'add_comp_table': False,
    'combined_name': 'neuron_fiveCountries',  # shared schema name for the five countries
}


# Database -> raw data in csv
Main.dataAccess(myToken, myQuery, myOutput)

# csv -> system_output in json & md
Main.get_combinedSchemas(myPara)

# user_input left join system_output -> final json & md
ori     = 'catalog-neuron_fiveCountries-latest.json'
new     = 'catalog-neuron_fiveCountries-editor.json'
output_name = 'catalog-neuron_fiveCountries'
Main.update_catalog(ori, new, output_name)
