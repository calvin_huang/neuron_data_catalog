[TOC]

## Schema: neuron_x (where x = australia or newzealand)

## Tables 

### Frequently used tables for data analysis

#### battery_inspection
Purpose: Records battery inspection activities

Status: Active

| Column         | Definition                                              | Related Table & Key                                             | Remarks |
|----------------|---------------------------------------------------------|-----------------------------------------------------------------|---------|
| id             | Unique ID for the table                                 |                                                                 |         |
| user_id        | User_id of the Ops member that performed the inspection | battery_swap.user_id                                            |         |
| battery_number | Unique number attached to battery                       | battery_inventory.battery_number, battery_inspection.battery_no |         |
| status         | Records the success of the activity                     |                                                                 |         |
| created_at     | Timestamp of when row was created                       |                                                                 |         |
| city       	 | City of battery inspection							   |                     											 |         |

| Column Name | Possible Values | Details |
|-------------|-----------------|---------|
| status      | pass            |         |
|             | fail            |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### battery_inventory
Purpose: Records individual battery detail
Remarks: scooter battery gets updated when there is a battery number query (e.g. battery swap completed --> scooter online --> send battery number query)

Status: Active

| Column             | Definition                                                     | Related Table & Key               | Remarks |
|--------------------|----------------------------------------------------------------|-----------------------------------|---------|
| id                 | Unique ID for the table                                        |                                   |         |
| scooter_id         | Id of scooter that the battery is in                           | scooter_inventory.id              |         |
| created_at         | Timestamp of when row was created                              |                                   |         |
| updated_at         | Timestamp of when row was updated                              |                                   |         |
| battery_number     | Unique number attached to battery                              | battery_inspection.battery_number |         |
| remaining_capacity | Remaining battery power                                        |                                   |         |
| full_capacity      | Whether the battery is full                                    |                                   |         |
| work_cycle         | Refers to the number of times the battery have been charged    |                                   |         |
| electricity        | Current                                                        |                                   |         |
| city		         | Location of battery                                            |                                   |         |
| status		     | Status of battery                                              |                                   |         |

| Column Name | Possible Values     | Details                 |
|-------------|---------------------|-------------------------|
| status      | IN_STOCK		    | refer [here](https://neuronmobility.atlassian.net/wiki/spaces/ND/pages/1046249473/Battery+Statuses)  |
|             | IN_USE      		|           			  |
|             | NOT_IN_USE     		|                         |
|             | MISSING      		|           			  |
|             | DECOMMISSIONED 		|                         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### battery_status_record
Purpose: Records changes in battery statuses

Status: Active 

| Column            | Definition                                          | Related Table & Key  | Remarks |
|-------------------|-----------------------------------------------------|----------------------|---------|
| id                | Unique ID for the table                           |                      |         |
| battery_no     | Unique number attached to battery                              | battery_inventory.battery_number |         |
| status | New status of battery                 |                      |         |
| previous_status    | Old status of battery                     |                      |         |
| city        		| Location of battery swap                 |                      |         |
| created_at        | Timestamp of when row was created                 |                      |         |

| Column Name | Possible Values     | Details                 |
|-------------|---------------------|-------------------------|
| status      | IN_STOCK		    | refer [here](https://neuronmobility.atlassian.net/wiki/spaces/ND/pages/1046249473/Battery+Statuses)  |
|             | IN_USE      		|           			  |
|             | NOT_IN_USE     		|                         |
|             | MISSING      		|           			  |
|             | DECOMMISSIONED 		|                         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### battery_swap
Purpose: Records battery swap operation 

Status: Active 

| Column            | Definition                                          | Related Table & Key  | Remarks |
|-------------------|-----------------------------------------------------|----------------------|---------|
| id                | Unique ID for the table                           |                      |         |
| scooter_id        | Unique ID of scooter                                | scooter_inventory.id |         |
| battery_swap_time | Timestamp of battery swap operation                 |                      |         |
| battery_out_no    | Battery number that was removed                     |                      |         |
| battery_out_level | Amount of battery left (the one that   was removed) |                      |         |
| battery_in_no     | New battery number                                  |                      |         |
| battery_in_level  | Amount of battery left (new)                        |                      |         |
| user_id           | Unique ID of the user that performed swap           | user.id              |         |
| created_at        | Timestamp of when row was created                 |                      |         |
| city        		| Location of battery swap                 |                      |         |
| vehicle_type      | Type of vehicle                 |                      |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### battery_swap_history (Changed from battery_swap on 25/5/2020)
Purpose: Records battery swap activities 

Status: Active

| Column          | Definition                                              | Related Table & Key        | Remarks                                   |
|-----------------|---------------------------------------------------------|----------------------------|-------------------------------------------|
| id              | Unique ID of the table                                  |                            |                                           |
| user_id         | User_id of the Ops member that performed the inspection | battery_inspection.user_id |                                           |
| scooter_id      | Unique ID of the scooter                                |                            |                                           |
| created_at      | Timestamp of when the row is created at                 |                            |                                           |
| updated_at      | Timestamp of when the row is updated at                 |                            |                                           |
| status          | Status of swap results                                  |                            |                                           |
| current_battery | Scooter battery after the swap is completed             |                            | new row inserted when a swap is performed |
| battery_no      | Unique number attached to the battery                   |                            |                                           |

| Column Name | Possible Values | Details                                                              |
|-------------|-----------------|----------------------------------------------------------------------|
| status      | BATTERY_UPDATED | Final status if swap performed correctly                             |
|             | OPS_COMPLETED   | Updated when the ops member clicked the 'swap' button in the ops app |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### blacklist
Purpose: Record blocked IP (Integrated with AWS WAF)

Status: Active

| Column     | Definition                        | Related Table & Key | Remarks |
|------------|-----------------------------------|---------------------|---------|
| id         | Unique ID for the table           |                     |         |
| ip         | Blocked IP address                |                     |         |
| created_at | Timestamp of when row was created |                     |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### command
Purpose: Record all possible commands to the scooter 

Remark: Refer to [command detail](https://docs.google.com/spreadsheets/d/1mP-aSGgCfqh5x-MyWQ76sWfZKsBqwOAIhJOYrbZ1hSM/edit#gid=0), applicable to command_log and command_response

Status: Active

| Column     | Definition                            | Related Table & Key | Remarks |
|------------|---------------------------------------|---------------------|---------|
| id         | Unique ID for the table               |                     |         |
| name       | Name of the command                   |                     |         |
| content    | Content of the command to the scooter |                     |         |
| created_at | Timestamp of when row was created     |                     |         |
| updated_at | Timestamp of when row was updated     |                     |         |

| Column Name | Possible Values     | Details                 |
|-------------|---------------------|-------------------------|
| name        | DEVICE_VERSION      | Enquire device version  |
|             | DEVICE_RESTART      | Restart device          |
|             | DEVICE_POSITION     |                         |
|             | DEVICE_LOG_OFF      | Log off device          |
|             | DEVICE_ACTIVATE_LOG |                         |
|             | ENGINE_RESUME       | Resume engine           |
|             | ENGINE_STOP         | Stop engine             |
|             | DOCK_UNLOCK         | Unlock device from dock |
| content     | VERSION#            |                         |
|             | RESET#              |                         |
|             | WHERE#              |                         |
|             | CLOG,OFF#           |                         |
|             | LOG,65535#          |                         |
|             | Relay,0#            |                         |
|             | Relay,1#            |                         |
|             | OUTPUT#             |                         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### email_verification
Purpose: Records user's email verification status

Status: Active

| Column     | Definition                           | Related Table & Key    | Remarks                                                                                               |
|------------|--------------------------------------|------------------------|-------------------------------------------------------------------------------------------------------|
| id         | Unique ID of table                   |                        |                                                                                                       |
| email      | User's email                         | neuron_user.user.email |                                                                                                       |
| token      | Unique API token sent                |                        |                                                                                                       |
| status     | verification status                  |                        |                                                                                                       |
| created_at | Timestamp of when the row is created |                        | when the user clicks 'verify now' this will change as well (anti-pattern of usual created_at columns) |
| updated_at | Timestamp of when the row is updated |                        |                                                                                                       |
| user_id | Unique ID of the user  | user.id |   |

| Column Name | Possible Values | Details                          |
|-------------|-----------------|----------------------------------|
| status      | CREATED         | email recorded in user's details |
|             | VERIFIED        | user verified email              |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### device_blacklist 
Purpose: Records detail of devices that are blacklisted 

Status: Active 

| Column     |                Definition                |     Related Table & Key    | Remarks |
|------------|----------------------------------------|----------------------------|-------|
| id         | Unique ID of table                       |                              |         |
| device_id  | Unique ID of device                      | user_device_record.device_id |         |
| comment    | Comment made when blacklisting           |                              |         |
| created_at | Timestamp of when the row was created  |                              |         |
| created_by | User ID that   created the row           |                              |         |
| updated_at | Timestamp of when the row was updated  |                              |         |
| updated_by | User ID that   updated the row           |                              |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### dft_quiz_result
Purpose: Records detail of quiz results after each trip 

Status: Active 

| Column     |                Definition                |     Related Table & Key    | Remarks |
|------------|----------------------------------------|----------------------------|-------|
| id         | Unique ID of table                       |                              |         |
| quiz_id  | Unique ID of quiz                      |  |         |
| user_id | Unique ID of the user  | user.id |   |
| trip_id         | Unique ID of trip                     | trip.id             |                 |
| response         | User's response to quiz                    |              |                 |
| created_at | Timestamp of when the row was created  |                              |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### feedback
Purpose: Records user's feedback sent in app

Remarks: For subject = 'SCOOTER_BREAKDOWN', it contains information for both EBIKE_DAMAGE and SCOOTER_DAMAGE. When users report damaged parts, these parts are reported in the data as IDs instead of parts. Refer to the [documentation](https://neuronmobility.atlassian.net/wiki/spaces/ND/pages/1520304205/Vehicle+Malfunction+Mapping) 
here for ID to parts mapping.

Status: Active

| Column     | Definition                                                  | Related Table & Key | Remarks                                    |
|------------|-------------------------------------------------------------|---------------------|--------------------------------------------|
| id         | Unique ID for the table                                     |                     |                                            |
| user_id    | Unique ID of the user that provided feedback                | user.id             |                                            |
| subject    | Categorisation of user feedback                             |                     |                                            |
| content    | Updated feedback content in JSON format                     |                     | Refer to [feedback_content](https://bitbucket.org/neuroncn/data-dictionary/src/master/JSON_extract/feedback_content.json) for extract |
| status     | Deprecated column                                           |                     |                                            |
| created_at | Timestamp of when row was created                           |                     |                                            |
| updated_at | Timestamp of when row was updated                           |                     |                                            |
| trip_id    | Unique ID of trip                                           | trip.id             |                                            |
| city       | Taken from user_country.city when the user creates a ticket |                     |                                            |


| Column Name | Possible Values   | Details                                                      |
|-------------|-------------------|--------------------------------------------------------------|
| subject     | OTHER             | Further subcategories can be found in [feedback_content](https://bitbucket.org/neuroncn/data-dictionary/src/master/JSON_extract/feedback_content.json) |
|             | SCOOTER_BREAKDOWN |                                                              |
|             | INCORRECT_CHARGE  |                                                              |
|             | END_TRIP_FAIL     |                                                              |
|             | STOLEN_SCOOTER    |                                                              |
|             | INCIDENT_REPORT   |                                                              |
|             | PARKING_BAD       |                                                              |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### geofence
Purpose: Records [Geofence](https://neuronmobility.atlassian.net/wiki/spaces/IW/pages/196214794/Geofence+Types+and+Zone+Flow) details 

Status: Active

| Column     | Definition                                 | Related Table & Key | Remarks                                                                                                |
|------------|--------------------------------------------|---------------------|--------------------------------------------------------------------------------------------------------|
| id         | Unique ID for the table                    | geofence_position.fence_id  |                                                                                                        |
| name       | Name of the Geofence location              |                     |                                                                                                        |
| created_at | Timestamp of when row was created          |                     |                                                                                                        |
| updated_at | Timestamp of when row was updated          |                     |                                                                                                        |
| city       | Acronym of the city                        |                     |                                                                                                        |
| type       | Type of Geofence                           |                     |                                                                                                        |
| max_speed  | Maximum speed allowed in Geofence          |                     |                                                                                                        |
| zone       | Zone that geofence lies within             | zone.code           |                                                                                                        |
| station_id | Station that geofence is located at        | station.id          |                                                                                                        |
| start_time | Time-based geofence start time             |                     | Local Time                                                                                                       |
| end_time   | Time-based geofence end time               |                     | Local Time                                                                                                       |
| hidden     | Active geofence hidden in user and ops app |                     | [Geofence hidden logic](https://neuronmobility.atlassian.net/browse/NS-1503) |
| weekdays   | Time-based geofence day                    |                     |                                                                                                        |
| status     | Whether Geofence is currently in used      |                     | Active = Geofence is in used    |

| Column Name | Possible Values | Details    |
|-------------|-----------------|------------|
| type        | SERVICE         |            |
|             | NO_PARKING      |            |
|             | SPEED           | max speed  |
|             | NO_RIDING       |            |
|             | PARKING         | station id |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### geofence_alert
Purpose: Records [alert](https://neuronmobility.atlassian.net/wiki/spaces/IW/pages/196182054/Geofence+Speed+and+No-Riding+Control) sent when triggered 

Status: Active

| Column     | Definition                       | Related Table & Key | Remarks |
|------------|----------------------------------|---------------------|---------|
| id         | Unique ID for the table          |                     |         |
| user_id    | User id that received alert      |                     |         |
| trip_id    | Trip id that triggered the alert |                     |         |
| status     | Status of alert                  |                     |         |
| created_at | Timestamp when row was created   |                     |         |
| updated_at | Timestamp when row was updated   |                     |         |
| imei       | Unique id of GPS device          |                     |         |

| Column Name | Possible Values | Details                               |
|-------------|-----------------|---------------------------------------|
| Status      | CANCELED        | when scooter goes back to riding zone |
|             | CREATED         | when violation occurs                 |
|             | ENGINE_OFF      | when engine_off command received      |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### geofence_position 
Purpose: Records Geofence polygon coordinates 

Status: Active 

| Column    | Definition                   | Related Table & Key | Remarks |
|-----------|------------------------------|---------------------|---------|
| id        | Unique ID for the table      |                     |         |
| latitude  | y-coordinate of the Geofence |                     |         |
| longitude | x-coordinate of the Geofence |                     |         |
| fence_id  | Unique ID of Geofence        | geofence.id         |         |
| type      | Representation type          |                     |         |

| Column Name | Possible Values | Details              |
|-------------|-----------------|----------------------|
| type        | CENTER          | Center of Polygon    |
|             | POLYGON         | Point of the Polygon |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### gps_inventory 
Purpose: records GPS inventory details

Status: Active 

| Column               | Definition                                                                                                | Related Table & Key | Remarks                                                                          |
|----------------------|-----------------------------------------------------------------------------------------------------------|---------------------|----------------------------------------------------------------------------------|
| id                   | Unique ID for the table                                                                                   |                     |                                                                                  |
| imei                 | Unique imei ID for the GPS                                                                                |                     |                                                                                  |
| created_at           | Timestamp of when row was created                                                                         |                     |                                                                                  |
| updated_at           | Timestamp of when row was updated                                                                         |                     |                                                                                  |
| remaining_battery    | Amount of battery left, range of 0 - 1                                                                    |                     | Big battery's level                                                              |
| latitude             | y-coordinate of the scooter                                                                               |                     | Updated by scooter-gps, user-phone-gps, operator-phone-gps, or station-location. |
| longitude            | x-coordinate of the scooter                                                                               |                     | Updated by scooter-gps, user-phone-gps, operator-phone-gps, or station-location. |
| engine_off           | Flag if the last received lock/unlock command response is 'LED:0' or 'LED:1'.                             |                     | LED:0 means scooter engine is on. LED: 1 means scooter engine is off.            |
| gps_battery_level    | GPS internal battery level, range of 0 - 6                                                                |                     | 6 = highest, 0 = lowest                                                          |
| gps_external_power   | Denotes if external power is on or off (connected to the external big battery)                            |                     | 1 = connected; 0 = not connected                                                 |
| status               | Inventory status of GPS device                                                                            |                     |                                                                                  |
| alarm_on             | Flag if alarm is turned on                                                                                |                     | 1 = yes; 0 = no                                                                  |
| version              | Version of GPS device                                                                                     |                     |                                                                                  |
| voltage              | Voltage of the external power, used to estimate external power                                            |                     |                                                                                  |
| apn                  | Access Point Name. Every telecommunication carrier has their only APN for people to access their network. |                     |                                                                                  |
| iccid                | Unique ID of the SIM card inside the GPS module.                                                          |                     |                                                                                  |
| location_source      | Indicates how the scooter location is last updated.                                                       |                     |                                                                                  |
| position_active_time | Time when GPS is last active                                                                              |                     |                                                                                  |
| protocol             | V1 = Old IOT, V2 = Current IOT, V3 = ebike                                                                                                |                     |                                                                                  |
| normal_speed         | Allowed Mode 1 speed                                                                                      |                     |                                                                                  |
| sport_speed          | Allowed Mode 2 speed                                                                                      |                     |                                                                                  |
| dashboard_version    | Dashboard version number                                                                                  |                     |                                                                                  |
| motor_version        | Motor version number                                                                                      |                     |                                                                                  |
| bluetooth_version    | Bluetooth version number                                                                                  |                     |                                                                                  |
| helmet_attached      | Whether there is a helmet inserted into the helmet lock                                                   |                     | 1 = yes ; 0 = no                                                                 |
| no_parking_light     | Indicates whether the scooter is in parking area in trip                                                  |                     | 1 = yes (scooter enter zone in trip) ; 0 = no (left zone)                        |
| wrench_light         | Indicates when there is a manual issue (as reported by ops)                                               |                     | 1 = yes ; 0 = no/solved                                                          |
| bt_mac               | The mac id of the bluetooth, unique for each bt                                                           |                     |

| Column Name     | Possible Values     | Details                        |
|-----------------|---------------------|--------------------------------|
| status          | IN_USE              |                                |
|                 | IN_STOCK            |                                |
| location_source | GPS                 | GPS device on scooter          |
|                 | OPS                 | Ops team deploy (operator app) |
|                 | STATION             | Station bluetooth (ibeacon)    |
|                 | USER                | User device                    |
| apn             | wireless.twilio.com |                                |
|                 | live.vodafone.com   |                                |
|                 | telstra.internet    |                                |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### helmet_unlock_record
Purpose: Records the result of helmet unlock command 

Status: Active

Remarks: Rows are updated and inserted (i.e. there might be mulitple rows per trip) 

| Column          | Definition                            | Related Table & Key | Remarks         |
|-----------------|---------------------------------------|---------------------|-----------------|
| id              | Unique ID for the table               |                     |                 |
| user_id         | Unique ID of user                     | user.id             |                 |
| trip_id         | Unique ID of trip                     | trip.id             |                 |
| helmet_attached | Whether helmet is attached to scooter |                     | yes = 1; no = 0 |
| scooter_online  | Whether scooter is online (ping <5min)|                     | yes = 1; no = 0  |
| unlock_channel  | Channel used to unlock                |                     |                 |
| stage           | Stage of Trip                         |                     |                 |
| created_at      | Timestamp of when row was created     |                     |                 |
| updated_at      | Timestamp of when row was updated     |                     |                 |
| result          | Result of unlock command              |                     | yes = 1; no =0  |

| Column Name    | Possible Values | Details                                     |
|----------------|-----------------|---------------------------------------------|
| unlock_channel | CHANNEL_3G      |                                             |
|                | CHANNEL_BT      |                                             |
| stage          | DURING_TRIP     | After start trip                            |
|                | BEFORE_TRIP     | Start Trip + Unlock helmet at the same time |
| result         | 1     		   | Successful request (helmet_wore=1)          |
|                | 0     		   | Unsuccessful request (helmet_wore=0) 		 |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### incident_log 
Purpose: Records details of incidents that have happened

Status: Active 

| Column        |                Definition                | Related Table & Key | Remarks |
|------------|----------------------------------------|----------------------------|-------|
| id            | Unique ID of table                       |                       |         |
| city          | City of shop                             |                       |         |
| risk_level          | Risk level of incident                             |                       |         |
| status      | Status of incident                         |                       |         |
| created_at    | Timestamp of when the row was   created  |                       |         |
| updated_at    | Timestamp of when the row was   updated  |                       |         |
| updated_by    | User ID that updated the row             |                       |         |
| created_by    | User ID that created the row           |                       |         |
| content    | Details of the incident             |                       |         |
| incident_time    | Timestamp of when the incident happened  |                       |         |
| aware_time    | Timestamp of when incident was made aware  |                       |         |

#### incident_log_image 
Purpose: Records images of incidents for incident_log table

Status: Active 

| Column        |                Definition                | Related Table & Key | Remarks |
|------------|----------------------------------------|----------------------------|-------|
| id            | Unique ID of table                       |                       |         |
| type          | Image type                             |                       |         |
| image_id          | Unique ID of image                             |                       | neuron_file.id         |
| log_id      | Unique ID for the incident                         |                       | incident_log.id        |
| created_at    | Timestamp of when the row was created  |                       |         |


#### maintenance_work 
Purpose: Records detail of maintenance work type

Status: Active

| Column   | Definition | Related Table & Key            | Remarks |
|----------|------------|--------------------------------|---------|
| id       | unique id  | group_id.maintenance_work_item |         |
| title    | Group Type |                                |         |

| Column Name | Possible Values   | Details |
|-------------|-------------------|---------|
| title       | Handlebar Group   |         |
|             | Stem Group        |         |
|             | Front Wheel Group |         |
|             | Rear Wheel Group  |         |
|             | Frame Group       |         |
|             | Firmware          |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### maintenance_work_item 
Purpose: Records maintenance work item details

Status: Active 

| Column   | Definition             | Related Table & Key                          | Remarks |
|----------|------------------------|----------------------------------------------|---------|
| id       | unique id              | performed_item_id.maintenance_work_performed |         |
| title    | Group Type             |                                              |         |
| group_id | maintenance work group | id.maintenance_work                          |         |  
| deleted  | whether the item has been removed from records | | yes = 1; no = 1|

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### maintenance_work_performed 
Purpose: Records maintenance work performed 

Status: Active 

| Column            | Definition                               | Related Table & Key      | Remarks |
|-------------------|------------------------------------------|--------------------------|---------|
| id                | Unique id of the table                   |                          |         |
| maintenance_id    | Unique id of maintenance job             | scooter_maintenance.id   |         |
| performed_item_id | Item id                                  | maintenance_work_item.id |         |
| created_at        | Timestamp of when the row was created    |                          |         |
| updated_at        | Timestamp of when the row was updated    |                          |         |
| created_by        | User Id of member who performed the work |                          |         |
| updated_by        | User Id of member who updated (If any)   |                          |         |
| action            | Status of work performed                 |                          |         |

| Column Name | Possible Values | Details       |
|-------------|-----------------|---------------|
| action      | FIXED           | Item fixed    |
|             | REPLACED        | Item replaced |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### mobile_verification 
Purpose: Records detail of mobile verification code sent 

Status: Active 

| Column       | Definition                                           | Related Table & Key | Remarks |
|--------------|------------------------------------------------------|---------------------|---------|
| mobile       | Mobile number                                        |                     |         |
| sms_code     | Unique SMS codes                                     |                     |         |
| created_at   | Timestamp of when row was created                    |                     |         |
| updated_at   | Timestamp of when row was updated                    |                     |         |
| country_code | Country code at which verification is sent to mobile |                     |         |
| remote_ip    | Remote IP address of which verification is sent to   |                     |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### n3_status 
Purpose: Record details of N3 status 

Remark: n3_status data is deleted daily and stored in RDS for ~7 days. Refer to redshift n3_status_history for historical data.

Status: Active 

| Column                  | Definition                                                                                                                                                                                                             | Related Table & Key | Remarks                                                                                                                |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------|------------------------------------------------------------------------------------------------------------------------|
| id                      | Unique ID for the table                                                                                                                                                                                                |                     |                                                                                                                        |
|-------------------------|---------------------------------------|------------------|----------------------------------| 
| electric_hall           |                                                                                                                                                                                                                        |                     |                                                                                                                        |
| a_phase_current         |                                                                                                                                                                                                                        |                     |                                                                                                                        |
| b_phase_current         |                                                                                                                                                                                                                        |                     |                                                                                                                        |
| c_phase_current         |                                                                                                                                                                                                                        |                     |                                                                                                                        |
| dc_current              |                                                                                                                                                                                                                        |                     |                                                                                                                        |
| accelerator             | The definition of these values are based on the IOT documentation from myway team.                                                                                                                                     |                     |                                                                                                                        |
| dashboard_communication | They are flagged when there is an error. (yes = 1; no = 1)                                                                                                                                                             |                     |                                                                                                                        |
| bms_communication       | However, they are merely saved and have yet to be strictly tested or used.                                                                                                                                             |                     |                                                                                                                        |
| brake                   |                                                                                                                                                                                                                        |                     |                                                                                                                        |
| iot_communication       |                                                                                                                                                                                                                        |                     |                                                                                                                        |
| ble_communication       |                                                                                                                                                                                                                        |                     |                                                                                                                        |
| low_battery             |                                                                                                                                                                                                                        |                     |                                                                                                                        |
| low_battery_protection  |                                                                                                                                                                                                                        |                     |                                                                                                                        |
|-------------------------|---------------------------------------|------------------|----------------------------------|
| topple                  | Flag when scooter topple                                                                                                                                                                                               |                     | yes = 1; no = 0                                                                                                        |
| lock_status             | Inaccurate (Bug for firmware below 1200)                                                                                                                                                   |                     | locked = scooter engine is off; unlocked = scooter engine is on                        |
| battery_percentage      | Scooter battery percentage                                                                                                                                                                                             |                     | Refer to [logic](https://neuronmobility.atlassian.net/wiki/spaces/IW/pages/55083009/Scooter+Battery+Calculation+Logic) |
| device_id               | Unique GPS device ID                                                                                                                                                                                                   | gps_inventory.id    |                                                                                                                        |
| created_at              | Timestamp of update                                                                                                                                                                                                    |                     |                                                                                                                        |
| single_range            | Deprecated                                                                                                                                                                                                             |                     | value is inaccurate                                                                                                    |
| speed                   | m/s                                                                                                                                                                                                                    |                     | calculated based on the rotation of the scooter's wheel                                                                                                                       |
| voltage                 | Scooter voltage                                                                                                                                                                                                        |                     |                                                                                                                        |
| electricity             | Scooter electricity                                                                                                                                                                                                    |                     |                                                                                                                        |
| helmet_attached       | Whether a helmet is attached to the scooter |     |  Null (default): scooter did not report, yes = 1; no = 0 |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### neuron_file 
Purpose: Records detail of image sent by users

Status: Active 

| Column     | Definition                                                   | Related Table & Key                                | Remarks                    |
|------------|--------------------------------------------------------------|----------------------------------------------------|----------------------------|
| id         | Unique ID for the table                                      | trip_helmet.image_id ; trip_finish_source.image_id |                            |
| type       | Type of Image                                                |                                                    |                            |
| name       | Name of file                                                 |                                                    |                            |
| url        | URL of file                                                  |                                                    | full url: url + "/" + name |
| md5        | md5 hash                                                     |                                                    |                            |
| recognized | Whether it is recognized by AWS object recognization service |                                                    | 1 = yes, 0 = no            |
| confidence | Level of confidence (recognition)                            |                                                    |                            |
| created_at | Timestamp of when row was created                            |                                                    |                            |
| updated_at | Timestamp of when row was updated                            |                                                    |                            |
| bucket     | Bucket used to store file                                    |                                                    |                            |

| Column Name | Possible Values | Details |
|-------------|-----------------|---------|
| type        | SCOOTER         |         |
|             | HELMET          |         |
|             | STATION         |         |
|             | OTHER           |         |
|             | ISSUE           |         |
|             | FEEDBACK        |         |
| bucket      | neuron_server   |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### neuron_order
Purpose: Record order details of unique trips. The order is generated when the trip is successfully generated. The order has a one-to-one relationship with the trip entity. 

Status: Active

| Column     | Definition                                                                                                                   | Related Table & Key | Remarks | Caveats |
|------------|------------------------------------------------------------------------------------------------------------------------------|---------------------|---------|---------|
| id         | Unique ID of the order                                                                                                       | order_detail.id ; receipt.source ; neuron_transaction.order_id     |         |         |
| trip_id    | Unique ID of the trip                                                                                                        | trip.id             |         |         |
| user_id    | Unique ID of user                                                                                                            | user.id             |         |         |
| amount     | Amount paid for order                                                                                                        |                     |         |         |
| created_at | Timestamp of when row was created                                                                                            |                     |         |         |
| updated_at | Timestamp of when row was updated                                                                                            |                     |         |         |
| pass_id    | Unique ID of type of pass used                                                                                               | pass.id             |         |         |
| status     | Status of order payment                                                                                                      |                     |         |         |
| updated_by | Unique ID of user (admin)                                                                                                    | user.id             |         |         |
| version    | To ensure that the row was not updated by two different clients at the same time, thereby enabling stronger data consistency |                     |         |         |
| currency   | Type of currency used                                                                                                        |                     |         |         |
| city_base_fee   | Base fee pricing of city                                                                                                |                     |         |         |
| city_unit_fee   | Additional fee per unit time (minute)                                                                                   |                     |         |         |

| Column Name | Possible Values | Details |
|-------------|-----------------|---------|
| status      | COMPLETED       |         |
|             | PENDING         |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### neuron_transaction 
Purpose: Record transaction details each time we have a SUCCESSFUL payment or refund action (compared to neuron_order: it's more related to the payment and the refund.)

Status: Active

| Column           | Definition                             | Related Table & Key   | Remarks         |
|------------------|----------------------------------------|-----------------------|-----------------|
| id               | Unique ID for the table                |                       |                 |
| user_id          | Unique ID of user                      | user.id               |                 |
| amount           | Amount of transaction                  |                       |                 |
| currency         | Type of currency used                  |                       |                 |
| type             | Type of transaction made               |                       |                 |
| created_at       | Timestamp of when row was created      |                       |                 |
| order_id         | Unique ID of individual order          | order_detail.order_id |                 |
| stripe_charge_id | Unique ID for Stripe charge            | Stripe Charge table   |                 |
| stripe_refund_id | Unique refund ID for Stripe user       |                       |                 |
| my_pass_id       | DEPRECATED                             |                       |                 |
| method           | Method of transaction                  |                       |                 |
| automatic        | States if deduction was automatic      |                       | yes = 1; no = 0 |
| brand            | Card brand                             |                       |                 |
| last4            | Last 4 digits of card number           |                       |                 |
| platform         | Type of platform used                  |                       |                 |
| account          | Country of account                     |                       |                 |
| user_pass_id     | Unique pass id used                    | user_pass.id          |                 |
| dispute_status   | Payment dispute                        |                       |                 |
| dispute_amount   | Disputed amount                        |                       |                 |
| updated_at       | Timestamp when the row was updated     |                       |                 |
| city             | City of user where transaction occured |                       |                 |
| client_ip | User’s mobile IP when user make the purchase/generate the transaction |   | From user's device, null if it's from Stripe webhook |

| Column Name    | Possible Values    | Details                   |
|----------------|--------------------|---------------------------|
| type           | ORDER_PAY          | From Wallet / Credit Card |
|                | ORDER_REFUND       | To Wallet / Credit Card   |
|                | PASS_BUY           | From Wallet / Credit Card |
|                | PASS_REFUND        | To Wallet / Credit Card   |
|                | PRE_AUTH           |                           |
|                | PRE_AUTH_CANCEL    |                           |
|                | PROMOTION_CREDIT   | To Wallet (Non - Cash)    |
|                | TOP_UP             | From Credit Card          |
|                | TOP_UP_REFUND      | To Credit Card            |
| method         | STRIPE_APPLEPAY    |                           |
|                | STRIPE_CREDIT_CARD |                           |
|                | WALLET             |                           |
|                | PAYPAL             |                           |
| platform       | STRIPE             |                           |
|                | PAYPAL             |                           |
| dispute_status | CREATED            | Dispute raised by user    |
|                | LOST               | Money withdrawn to user   |
|                | WON                | Neuron won the case       |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### notification 
Purpose: Record notifications sent 

Status: Active

| Column   | Definition                                                         | Related Table & Key | Remarks |
|------------|--------------------------------------------------------------------|---------------------|---------|
| id         | Unique ID for the table                                            |                     |         |
| user_id    | Unique ID of user                                                  | user.id             |         |
| content    | Content of notification                                            |                     |         |
| type       | Type of notification                                               |                     |         |
| data       | App uses this field to trigger logic for certain push notification |                     |         |
| created_at | Timestamp of when row was created                                  |                     |         |

| Column Name | Possible Values | Details |
|-------------|-----------------|---------|
| type        | PROMOTION       |         |
|             | REFUND          |         |
|             | REMINDER        |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### notification_config
Purpose: Configure notification from backend (eg Automate time-based geofence notification) 

Status: Active 

| Column        | Definition                           | Related   Table & Key | Remarks |
|---------------|--------------------------------------|-----------------------|---------|
| id            | Unique ID of table                   |                       |         |
| title         | Title of notification                |                       |         |
| start_time    | Start time of notification           |                       |         |
| end_time      | End time of notification             |                       |         |
| weekdays      | Days of notification                 |                       |         |
| status        | Status of notification event         |                       |         |
| created_at    | Timestamp when record was created    |                       |         |
| created_by    | User who created notification        | user.id               |         |
| updated_at    | Timestamp when record was updated    |                       |         |
| updated_by    | User who updated notification        | user.id               |         |
| message_id    | Unique ID of message                 |                       |         |
| city          | City                                 |                       |         |
| trigger_event | Action that triggers   notification  |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### okai_battery_sn
Purpose: Records okai battery records and their corresponding serial numbers

Status: Active 

| Column     | Definition                        | Related   Table & Key | Remarks |
|------------|-----------------------------------|-----------------------|---------|
| id         | Unique ID of table                |                       |         |
| battery    | QR Code of the OKAI battery           | user.id               |         |
| sn       | OKAI battery serial number                 |                       |         |
| created_at | Timestamp when record was created |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### ops_live_location
Purpose: Records the latest locational details of Ops (1 record per user(ops) who enabled location sharing) 

Status: Active 

| Column     | Definition                        | Related   Table & Key | Remarks |
|------------|-----------------------------------|-----------------------|---------|
| id         | Unique ID of table                |                       |         |
| user_id    | Unique ID of user (ops)           | user.id               |         |
| city       | City of operation                 |                       |         |
| latitude   | y-coordinate of user's position   |                       |         |
| longitude  | x-coordinate of user's position   |                       |         |
| created_at | Timestamp when record was created |                       |         |
| updated_at | Timestamp when record was updated |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### ops_open_deck
Purpose: Records the open battery deck operation 

Status: Active 

| Column            | Definition                                 | Related Table & Key               | Remarks               |
|-------------------|--------------------------------------------|-----------------------------------|-----------------------|
| id                | Unique ID for the table                    |                                   |                       |
| user_id           | Unique ID of the user that opened the deck | user.id                           |                       |
| scooter_id        | Unique ID of scooter                       | scooter_inventory.id              |                       |
| battery_no        | Unique number attached to the battery      | battery_inventory.battery_number, |                       |
| remaining_battery | Amount of battery left, from 0 - 1         |                                   |                       |
| open_deck_time    | Timestamp of open command response         |                                   |                       |
| open_channel      | Channel used to open deck                  |                                   | CHANNEL_3G/CHANNEL_BT |
| created_at        | Timestamp of when row was created          |                                   |                       |
| status            | Status of Operation                        |                                   |                       |

| Column Name   | Possible Values | Details             |
|---------------|-----------------|---------------------|
| status        | OPEN            | When deck is opened |
|               | BATT_SWAPED     | Swap Completed      |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### order_detail 
Purpose: Records order details 

Status: Active 

| Column                 | Definition                                                                   | Related Table & Key | Remarks                                                                                                                                                                     |
|------------------------|------------------------------------------------------------------------------|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| id                     | Unique ID for the table                                                      |                     |                                                                                                                                                                             |
| order_id               | Unique ID of the order                                                       | neuron_order.id     |                                                                                                                                                                             |
| user_id                | Unique ID of user                                                            | user.id             |                                                                                                                                                                             |
| trip_minutes           | Minutes of scooter trip                                                      |                     |                                                                                                                                                                             |
| trip_mileage           | Mileage of scooter trip                                                      |                     |                                                                                                                                                                             |
| vip                    | Flag if user is VIP at the start of the trip                                 |                     | 1 = yes, 0 = no                                                                                                                                                             |
| pass_minutes           | trip_minutes, if pass is used (and my_pass_id is not null)                   |                     |                                                                                                                                                                             |
| coupon_amount          | Coupon amount used                                                           |                     |                                                                                                                                                                             |
| wallet_amount          | Wallet amount used (final amount will be deducted from wallet before credit) |                     | 1. If the trip is done by a VIP, final_amount = 0 2. If the trip is not a VIP trip, final_amount = trip_fare + convenience_fee - pass_amount - coupon_amount - adjust_off   |
| credit_amount          | Final amount charged to credit card                                          |                     |                                                                                                                                                                             |
| created_at             | Timestamp of when row was created                                            |                     |                                                                                                                                                                             |
| user_coupon_id         | Unique ID of coupon used, linked to user_coupon table, id column             |                     |                                                                                                                                                                             |
| convenience_fee        | Convenience fee paid                                                         |                     |                                                                                                                                                                             |
| trip_fare              | Total trip fare                                                              |                     |                                                                                                                                                                             |
| pass_amount            | Pass amount used                                                             |                     |                                                                                                                                                                             |
| adjust_off             | Amount adjusted by customer service due to user complaint                    |                     | When a user sees the bill, he may not agree with the final amount, so he may contact customer service to get an adjustment. So adjust_off denotes the amount reduced by CS. |
| updated_at             | Timestamp of when row was updated                                            |                     |                                                                                                                                                                             |
| updated_by             | Unique ID of user                                                            | user.id             |                                                                                                                                                                             |
| currency               | Currency used                                                                |                     |                                                                                                                                                                             |
| scooter_discount       | Amount discounted from using specific scooter                                |                     |                                                                                                                                                                             |
| is_short_trip          | less than 60 seconds                                                         |                     |                                                                                                                                                                             |
| trip_cap_off           | DEPRECATED                                                                   |                     | Maximum trip amount; not configured for AU&NZ                                                                                                                               |
| base_fee               | Base fee for using scooter                                                   |                     |                                                                                                                                                                             |
| unit_fee               | Amount per minute                                                            |                     |                                                                                                                                                                             |
| user_pass_id           | user_pass.id                                                                 |                     | user_pass_id is not null for orders during which users exceed the pass minutes, in another word, it is possible that pass_minutes=0 and user_pass_id is not null            |
| helmet_replacement_fee | helmet replacement amount                                                    |                     |                                                                                                                                                                             |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### organization 
Purpose: Records the details of the organization that have exclusive codes 

Status: Active 

| Column     | Definition                            | Related Table & Key            | Remarks         |
|------------|---------------------------------------|--------------------------------|-----------------|
| code       | Organization code                     | organization_discount.org_code |                 |
| name       | Ogranization name                     |                                |                 |
| type       | Organization type                     |                                |                 |
| domains    | Organization domain                   |                                |                 |
| deleted    | Flag if deleted                       |                                | yes = 1; no = 0 |
| created_at | Timestamp of when the row was created |                                |                 |
| updated_at | Timestamp of when the row was updated |                                |                 |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### organization_discount 
Purpose: Records organization discount details 

Status: Active 

| Column     | Definition                                                   | Related Table & Key | Remarks         |
|------------|--------------------------------------------------------------|---------------------|-----------------|
| id         | Unique table id                                              |                     |                 |
| org_code   | Organization code                                            | organization.code   |                 |
| discount   | % of the usual price the user from the organization pays for |                     |                 |
| pass_days  | number of days pass is applied to                            |                     |                 |
| deleted    | Flag if deleted                                              |                     | yes = 1; no = 0 |
| created_at | Timestamp of when the row was created                        |                     |                 |
| updated_at | Timestamp of when the row was updated                        |                     |                 |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### organization_email_verification
Purpose: Records organization verification details

Status: Active

| Column     | Definition                            | Related Table & Key | Remarks |
|------------|---------------------------------------|---------------------|---------|
| id         | Unique table id                       |                     |         |
| email      | email used for verification           |                     |         |
| token      | Unique token                          |                     |         |
| status     | Verification status                   |                     |         |
| created_at | Timestamp of when the row was created |                     |         |
| updated_at | Timestamp of when the row was updated |                     |         |
| user_id    | User id that is verified              | neuron_user.user.id |         |

| Column Name | Possible Values | Details                                      |
|-------------|-----------------|----------------------------------------------|
| status      | CREATED         |                                              |
|             | VERIFIED        | email verified as affiliated to organisation |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### organization_invitation 
Purpose: Records the invitation details used to identify users

Status: Active 

| Column     | Definition                            | Related Table & Key | Remarks |
|------------|---------------------------------------|---------------------|---------|
| id         | Unique table id                       |                     |         |
| org_code   | organization code                     | organization.code   |         |
| code       | code used for the promotion           |                     |         |
| status     | status of invitation process          |                     |         |
| created_at | Timestamp of when the row was created |                     |         |
| updated_at | Timestamp of when the row was updated |                     |         |
| created_by | User id of admin who created          |                     |         |
| updated_by | User id of admin who updated          |                     |         |

| Column Name | Possible Values | Details |
|-------------|-----------------|---------|
| status      | CREATED         |         |
|             | VALIDATED       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### organization_user 
Purpose: Record the details of users who are affiliated to an organisation

Status: Active 

| Column   | Definition                                                 | Related Table & Key | Remarks |
|------------|------------------------------------------------------------|---------------------|---------|
| id         | Unique table id                                            |                     |         |
| org_code   | organization code                                          | organization.code   |         |
| user_id    | User id that is verified as affiliated to the organization |                     |         |
| created_at | Timestamp of when the row was created                      |                     |         |
| updated_at | Timestamp of when the row was updated                      |                     |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### pass
Purpose: Record details of available passes 

Status: Active 

| Column      | Definition                              | Related Table & Key | Remarks                    |
|-------------|-----------------------------------------|---------------------|----------------------------|
| id          | Unique ID for the table                 |                     |                            |
| title       | Title of pass                           |                     | au and nz id are different |
| duration    | Duration of pass (days)                 |                     |                            |
| free_min    | Free duration for scooter riding (mins) |                     |                            |
| price       | Discounted price of pass                |                     |                            |
| status      | Status of pass                          |                     |                            |
| created_at  | Timestamp of when row was created       |                     |                            |
| updated_at  | Timestamp of when row was updated       |                     |                            |
| currency    | Currency used                           |                     |                            |
| created_by  | User id of admin who created            |                     |                            |
| updated_by  | User id of admin who updated            |                     |                            |
| description | Marketing text used in user's app       |                     |                            |

| Column Name | Possible Values | Details            |
|-------------|-----------------|--------------------|
| title       | 3-Day Pass      |                    |
|             | Weekly Pass     |                    |
|             | Monthly Pass    |                    |
| status      | ACTIVE          | Pass is in use     |
|             | INACTIVE        | Pass is not in use |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### password_reset 
Purpose: Records password reset request from the operator's app

Status: Active

| Column     | Definition                               | Related Table & Key | Remarks |
|------------|------------------------------------------|---------------------|---------|
| id         | Unique ID for the table                  |                     |         |
| email      | email used                               |                     |         |
| token      | Unique Token given                       |                     |         |
| status     | Status of the password reset process     |                     |         |
| created_at | Timestamp of when the row was created at |                     |         |
| updated_at | Timestamp of when the row was updated at |                     |         |

| Column Name | Possible Values | Details                                          |
|-------------|-----------------|--------------------------------------------------|
| status      | VALID           | Valid token                                      |
|             | INVALID         | status changes when password reset is successful |
|             | EXPIRED         | Expired token                                    |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### promo_code 
Purpose: Record details of promo code given to the user 

Status: Active 

| Column       | Definition                                                            | Related Table & Key | Remarks |
|--------------|-----------------------------------------------------------------------|---------------------|---------|
| id           | Unique ID for the table                                               |                     |         |
| name         | Name of promo coe                                                     |                     |         |
| type         | Type of promo code for                                                |                     |         |
| value        | Monetary value of promo code                                          |                     |         |
| expired_at   | Expiry date of promo code                                             |                     |         |
| valid_time   | Validity of promo code (days)                                         |                     |         |
| currency     | Type of currency                                                      |                     |         |
| remark       | Text remark of promo code                                             |                     |         |
| parent       | ID of promo code if the new promo code is based on existing promotion |                     |         |
| created_by   | Unique ID of administrator (creator)                                  | neuron_user.user.id |         |
| created_at   | Timestamp of when row was created                                     |                     |         |
| updated_by   | Unique ID of administrator (creator)                                  | neuron_user.user.id |         |
| updated_at   | Timestamp of when row was updated                                     |                     |         |
| redeem_limit | Limit of promo code redemption                                        |                     |         |
| valid_at     | Release date of promo code                                            |                     |         |
| scope        | Applicability of promo code (what promo can be used on)               |                     |         |
| pass_type    | Pass type that the promo code can be used for                         | pass.type           |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### purchase_intent
Purpose: Stripe Payment intent logs, recording whether a stripe payment is successful (through card)

Status: Active

| Column            | Definition                                                      | Related Table & Key                             | Remarks |
|-------------------|-----------------------------------------------------------------|-------------------------------------------------|---------|
| id                | Unique ID for the table                                         |                                                 |         |
| user_id           | Unique user ID                                                  | neuron_user.user.id                             |         |
| pass_id           | Pass ID if applicable                                           | pass.id                                         |         |
| order_id          | Unique order ID                                                 | order_detail.order_id / neuron_order.id         |         |
| amount            | Amount payable for pass buy, trip payment or card authorisation | neuron_order.amount, pass.price (if applicable) |         |
| currency          | Currency used in payment                                        |                                                 |         |
| status            | Denotes status of transaction                                   | refer [here](https://stripe.com/docs/payments/intents#intent-statuses)                                                |         |
| type              | Type of transaction                                             |                                                 |         |
| payment_intent_id | Unique ID of payment intent (Stripe Data)                       | Stripe Payment Intent table                     |         |
| created_at        | Timestamp of when row was created                               |                                                 |         |
| updated_at        | Timestamp of when row was updated                               |                                                 |         |
| platform          | Platform used (stripe)                                          |                                                 |         |
| account           | Country attached to account                                     |                                                 |         |
| client_ip | User’s mobile IP when user make the purchase/generate the transaction |   | From user's device, null if it's from Stripe webhook |

| Column Name | Possible Values | Details                                                     |
|-------------|-----------------|-------------------------------------------------------------|
| type        | CARD_AUTHORIZE  |                                                             |
|             | ORDER_BUY       |                                                             |
|             | PASS_BUY        |                                                             |
|             | TOP_UP          |                                                             |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### rebalance 
Purpose: Record details of rebalancing scooters during ops

Status: Active

| Column           | Definition                           | Related Table & Key  | Remarks                                 |
|------------------|--------------------------------------|----------------------|-----------------------------------------|
| id               | Unique ID for the table              |                      |                                         |
| created_by       | Unique ID of administrator (creator) | user.id              |                                         |
| scooter_id       | Unique ID of scooter                 | scooter_inventory.id |                                         |
| start_station_id | Unique ID of start station           | station.id           |                                         |
| end_station_id   | Unique ID of end station             | station.id           |                                         |
| created_at       | Timestamp of when row was created    |                      |                                         |
| updated_at       | Timestamp of when row was updated    |                      |                                         |
| status           | Status of rebalance                  |                      |ONGOING = retrieved but not deployed yet; FINISHED = retrieved and deployed successfully; CANCELLED = retrieved but then was admin locked|
| updated_by       | Unique ID of administrator (creator) | user.id              |                                         |
| user_gps_time    | Timestamp of when user used GPS      |                      | user's gps when the scooter is deployed |
| from_scan        | Flag if rebalance is done via scan   |                      | 1 = yes, 0 = no                         |
| start_latitude | Latitude where rebalance started |        |         |
| start_longitude | Longitude where rebalance started |        |         |
| finish_latitude | Latitude where rebalance was completed |        |         |
| finish_longitude | Longitude where rebalance  was completed |        |         |
| city 				| Location where rebalance was completed |        |         |
| vehicle_type     | Type of Vehicle                   	  |                      |										   |

| Column Name | Possible Values | Details |
|-------------|-----------------|---------|
| status      | FINISHED        |         |
|             | ONGOING         |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### receipt 
Purpose: Records the receipt detail

Status: Active 

| Column     | Definition                                         | Related Table & Key | Remarks   |
|------------|----------------------------------------------------|---------------------|-----------|
| id         | Unique ID for the table                            |                     |           |
| type       | Type of transaction                                |                     |           |
| source     | Order id                                           | order_id            |           |
| created_at | Timestamp of when the row was created at           |                     |           |
| serial_num | Receipt serial number                              |                     |           |
| sent_num   | Number of times the user requested for the receipt |                     | Maximum 5 |

| Column Name | Possible Values | Details |
|-------------|-----------------|---------|
| type        | order           |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### rtk_station
Purpose: Records the high accuracy GPS positioning of the scooter

Status: Active 

| Column     | Definition                                         | Related Table & Key | Remarks   |
|------------|----------------------------------------------------|---------------------|-----------|
| imei     | Unique ID of GPS device |                          | gps_inventory.imei                             |
| city       | City where scooter is in         |                       |         |
| rtk_data       | Data corresponding to the rtk         |                       |         |
| latitude      | Station's latitude                          |                       |         |
| longitude     | Station's longitude                         |                       |         |
| created_at | Timestamp of when the row was created at           |                     |           |
| updated_at   | Timestamp of when the row was   updated  |                       |         |


[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scan_record 
Purpose: Record scanning details 

Status: Active

| Column            | Definition                            | Related Table & Key      | Remarks                      |
|-------------------|---------------------------------------|--------------------------|------------------------------|
| id                | Unique ID for the table               |                          |                              |
| user_id           | Unique ID of user                     | user.id                  |                              |
| scooter_id        | Unique ID of scooter                  |                          |                              |
| station_id        | Unique ID of station                  | station.id               |                              |
| scooter_status    | Status of scooter                     | scooter_inventory.status |                              |
| user_latitude     | y-coordinate of the user              |                          |                              |
| user_longitude    | x-coordinate of the user              |                          |                              |
| remaining_battery | Amount of battery left, from 0 - 1    |                          |                              |
| created_at        | Timestamp of when row was created     |                          |                              |
| user_gps_time     | Timestamp of when GPS was last active |                          |                              |
| error_code        | Unique error code					    |                          | refer [here](https://www.twilio.com/docs/api/errors)       |
| trip_id     		| Unique Trip ID 						| trip.id                         |                              |
| updated_at     | Timestamp of when row was updated	    |                          |                              |
| app_version     | User's app version					    |                          |                              |
| platform     | Type of platform used						|                          | Device platform (IOS or ANDROID)               |
| device_id     | Unique scooter ID						    | scooter_inventory.id                         |                              |
| imei     | Unique ID of GPS device |                          | gps_inventory.imei                             |
| city     | City where scan was made |                          |                              |
| vehicle_type     | Type of Vehicle				 |                          |                              |

| Column Name  | Possible Values | Details                                                                                                                    |
|----------------|-------------------|----------------------------------------------------------------------------------------------------------------------------|
| scooter_status | IN_STATION        | refer [here](https://neuronmobility.atlassian.net/wiki/spaces/ND/pages/66256909/Scooter+Metrics+Definitions)       |
|                | IN_TRIP           |                                                                                                                            |
|                | REBALANCING       |                                                                                                                            |
|                | IN_STOCK          |                                                                                                                            |
|                | MISSING           |                                                                                                                            |
|                | NO_RIDING         |                                                                                                                            |
|                | ADMIN_LOCK        |                                                                                                                            |
|                | IN_REPAIR         |                                                                                                                            |
|                | IN_WAREHOUSE      |                                                                                                                            |
|                | IN_RESERVATION    |                                                                                                                            |
|                | OUTSIDE_SERVICE   |                                                                                                                            |


[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_check_record
Purpose: Record for ops inspection 

Status: Active

| Column         | Definition                                    | Related Table & Key           | Remarks                                                                                                            |   |
|----------------|-----------------------------------------------|---------------------------------|--------------------------------------------------------------------------------------------------------------------|---|
| id             | Unique ID for the table                     |                                 |                                                                                                                    |   |
| status         | Status of check                             |                                 |                                                                                                                    |   |
| content        | Scooter check content in JSON format    |                                 | [JSON extract](https://bitbucket.org/neuroncn/data-dictionary/src/79f68395e27daa55f2c97f901959938fe07b17f7/JSON_extract/scooter_check_content.json?at=master)       |   |
| maintenance_id | Unique ID for maintenance job               | scooter_maintenance.id    |                                                                                                                    |   |
| type           | Type of check                               |                                 | refer to JSON extract                                                                                        |   |
| created_at     | Timestamp of when row was created           |                                 |                                                                                                                    |   |
| updated_at     | Timestamp of when row was updated           |                                 |                                                                                                                    |   |
| created_by     | Unique ID of administrator                                              |                                 |                                                                                                                    |   |
| updated_by     | Unique ID of administrator                                              |                                 |                                                                                                                    |   |
| device_id      | Unique scooter ID                                  | scooter_inventory.id      |                                                                                                                    |   |


[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_city_record 
Purpose: Records the city where the scooter is located 

Status: Active

| Column     | Definition                         | Related Table & Key | Remarks |
|------------|------------------------------------|-----------------------|---------|
| id         | Unique ID for the table          |                       |         |
| scooter_id | Scooter ID                       | trip.device_id        |         |
| city       | City where scooter is in         |                       |         |
| created_at | Timestamp of when row is created |                       |         |
| zone       | Zone where the scooter is in     |                       | New record when there is a zone change        |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_inventory
Purpose: Record individual scooter details 

Status: Active 

Remark: For information on [scooter status flow](https://neuronmobility.atlassian.net/wiki/spaces/IW/pages/59211777/Scooter+status+flow)

| Column         | Definition                                                                                                            | Related   Table & Key               | Remarks                                                                                               |
|----------------|-----------------------------------------------------------------------------------------------------------------------|-------------------------------------|-------------------------------------------------------------------------------------------------------|
| id             | Unique ID of scooter                                                                                                |                                     |                                                                                                       |
| imei           | Unique ID of GPS device                                                                                             | gps_inventory.imei                  |                                                                                                       |
| station_id     | Unique ID of station                                                                                                | station.id                          |                                                                                                       |
| qr_code        | Unique QR code of each scooter                                                                                      |                                     |                                                                                                       |
| deck_id        | Unique deck ID                                                                                                      |                                     |                                                                                                       |
| mac            | Unique mac address                                                                                                  |                                     |                                                                                                       |
| ic_card        | RFID card ID designed for the docking system, used to recognize the scooter when   we push the scooter to the dock. | lock_response.card_no               |                                                                                                       |
| status         | Status of scooter                                                                                                   | scan_record.scooter_status    |                                                                                                       |
| city           | City where scooter is                                                                                               |                                     |                                                                                                       |
| created_at     | Timestamp of when row was created                                                                                   |                                     |                                                                                                       |
| updated_at     | Timestamp of when row was updated                                                                                   |                                     |                                                                                                       |
| version        | -                                                                                                                     |                                     | Backend database optimize lock; used in the backend server to avoid the concurrency data updates. |
| deleted        | Flag if scooter is 'deleted'                                                                                        |                                     | 1 = yes, 0 = no                                                                                     |
| image_id       | Unique image ID taken by user                                                                                       | neuron_file.id                      |                                                                                                       |
| generation     | Unique scooter version                                                                                              |                                     |                                                                                                       |
| zone           | Pricing zone                                                                                                        | zone.code                           |                                                                                                       |
| licence        | Deprecated                                                                                                            |                                     |                                                                                                       |
| type           | Type of Vehicle                                                                                                     |                                     |                                                                                                       |
| last_test_time | Timestamp of when the scooter was last tested                                                                   |                                     |                                                                                                       |
| helmet_lock    | Helmet lock in place                                                                                                |                                     | 1 = yes; 0 = no                                                                                     |
| sticker        | Sticker number                                                                                                      |                                     |                                                                                                       |
| last_sanitize_time | Timestamp of when the scooter was last sanitized |   |    |

| Column Name | Possible Values | Details                                                                                                                    |
|---------------|-------------------|----------------------------------------------------------------------------------------------------------------------------|
| status (refer [here](https://neuronmobility.atlassian.net/wiki/spaces/ND/pages/66256909/Scooter+Metrics+Definitions) )        | IN_STATION        |       |
|               | IN_WAREHOUSE      |                                                                                                                            |
|               | MISSING           | Status can change                                                                                                                           |
|               | IN_REPAIR         |                                                                                                                            |
|               | IN_STOCK          |                                                                                                                            |
|               | ADMIN_LOCK        |                                                                                                                            |
|               | NO_RIDING         |                                                                                                                            |
|               | IN_TRIP           |                                                                                                                            |
|               | REBALANCING       |                                                                                                                            |
|               | OUTSIDE_SERVICE   |                                                                                                                            |
|               | BEYOND_REPAIR     | Last status that a scooter can have |
|               | IN_RESERVATION    |    |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_issue
Purpose: Record details of scooter issue

Status: Active

| Column         | Definition                          | Related Table & Key           | Remarks |
|----------------|-------------------------------------|---------------------------------|---------|
| id             | Unique ID for the table           |                                 |         |
| type           | Type of issue                     |                                 |         |
| content        | Scooter issue comment made by ops |                                 |         |
| created_at     | Timestamp of when row was created |                                 |         |
| updated_at     | Timestamp of when row was updated |                                 |         |
| created_by     | User ID that created the row      |                                 |         |
| updated_by     | User ID that updated the row      |                                 |         |
| status         | Status of issue                   |                                 |         |
| device_id      | Scooter ID                        | scooter_inventory.id                                |         |
| maintenance_id | Unique maintenance work id        | scooter_maintenance.id    |         |
| city           | Scooter's city                    |                                 |         |

| Column Name | Possible Values | Details |
|---------------|-------------------|---------|
| type          | ADMIN_LOCK        | For admin lock, the updated_by might be NULL if it was rebalanced and therefore admin unlocked |
|               | HELMET_MISSING    |         |
|               | MUL_3G_PROBLEM    |         |
|               | MUL_ACCELERATOR   |         |
|               | MUL_BRAKE         |         |
|               | MUL_DASHBOARD     |         |
|               | MUL_HANDLEBAR     |         |
|               | MUL_HORN          |         |
|               | MUL_KICKSTAND     |         |
|               | MUL_MUDGUARD      |         |
|               | MUL_OTHER         |         |
|               | MUL_TYRES         |         |
|               | MUL_VANDALISM     |         |
|               | NO_PARKING_AREA   |         |
|               | NO_RIDING_AREA    |         |
|               | OUT_SERVICE_AREA  |         |
|               | SCOOTER_INACTIVE  |         |
|               | SCOOTER_OFFLINE   |         |
|               | SCOOTER_TOPPLED   |         |
|               | MUL_FRONT_LIGHT   |         |
|               | MUL_REAR_LIGHT    |         |
| status        | OUTSTANDING       |         |
|               | RESOLVED          |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_issue_image 
Purpose: Records the images showing the issue 

Status: Active 

| Column     | Definition                          | Related Table & Key | Remarks |
|------------|-------------------------------------|-----------------------|---------|
| id         | Unique ID for the table           |                       |         |
| image_id   | Unique ID of image                | neuron_file.id        |         |
| issue_id   | Unique ID of issue                | scooter_issue.id      |         |
| created_at | Timestamp of when row was created |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_issue_notification 
Purpose: Records detail of scooter issue notification

Status: Active 

| Column     | Definition                          | Related Table & Key | Remarks |
|------------|-------------------------------------|-----------------------|---------|
| id         | Unique ID for the table           |                       |         |
| message    | Message sent                      |                       |         |
| created_at | Timestamp of when row was created |                       |         |
| updated_at | Timestamp of when row was updated |                       |         |
| issue_id   | Unique ID of issue                | scooter_issue.id      |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_maintenance 
Purpose: Records scooter maintenance job details 

Status: Active 

| Column     | Definition                                                         | Related Table & Key | Remarks |
|------------|--------------------------------------------------------------------|-----------------------|---------|
| id         | Unique ID for the table                                          |                       |         |
| device_id  | Unique scooter id                                                | scooter_inventory.id  |         |
| created_at | Timestamp of when row was created                                |                       |         |
| updated_at | Timestamp of when row was updated                                |                       |         |
| created_by | User ID that created the row                                     |                       |         |
| updated_by | User ID that updated the row                                     |                       |         |
| status     | Status of maintenance                                            |                       |         |
| station_id | Unique station(warehouse) id where maintenance happened    |                       |         |
| comment    | Additional comment made by ops                                   |                       |         |

| Column Name | Possible Values | Details |
|---------------|-------------------|---------|
| status        | READY_TO_GO       |         |
|               | AWAITING_PARTS    |         |
|               | AWAITING_REPAIR   |         |
|               | WORK_IN_PROGRESS  |         |
|               | TESTING           |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_maintenance_status_record 
Purpose: Records maintenance status of scooter

Status: Active

| Column         | Definition                          | Related Table & Key           | Remarks |
|----------------|-------------------------------------|---------------------------------|---------|
| id             | Unique ID for the table           |                                 |         |
| device_id      | Unique scooter id                 | scooter_inventory.id            |         |
| maintenance_id | Unique maintenance id             | scooter_maintenance.id    |         |
| status         | Status of maintenance             |                                 |         |
| created_at     | Timestamp of when row was created |                                 |         |
| created_by     | User ID that created the row      |                                 |         |

| Column Name | Possible Values | Details |
|---------------|-------------------|---------|
| status        | AWAITING_REPAIR   |         |
|               | WORK_IN_PROGRESS  |         |
|               | TESTING           |         |
|               | READY_TO_GO       |         |
|               | BEYOND_REPAIR     |         |
|               | AWAITING_PARTS    |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_reservation 
Purpose: Record reservation details of scooters

Status: Active 

| Column     | Definition                              | Related Table & Key | Remarks |
|------------|-----------------------------------------|-----------------------|---------|
| id         | Unique ID for table                       |                       |         |
| status     | Current status of the reservation |                       |         |
| created_at | Timestamp of when row was created |                       |         |
| updated_at | Timestamp of when row was updated   |                       |         |
| created_by | User ID that created the reservation  |                       |         |
| updated_by | User ID that updated the reservation  |                       |         |
| device_id  | Scooter ID                            | scooter_inventory.id  |         |

| Column Name | Possible Values | Details                                                                                                                                                                                              |
|---------------|-------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| status        | created           | When a reservation is first created by the user                                                                                                                                                |
|               | succeed           | When a reservation results in a start trip by the user                                                                                                                                       |
|               | cancelled         | 1. When the reservation does not result in a start trip, status changes to cancelled in 15 minutes, 2. User cancelled reservation, 3. User scanned another scooter       |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_speed_record 
Purpose: Records the speed of the scooter in trip (Row created when the scooter enters an area with different speed regulation) 

Status: Active 

| Column     | Definition                                     | Related Table & Key | Remarks       |
|------------|------------------------------------------------|-----------------------|---------------|
| id         | Unique ID for the table                      |                       |               |
| imei       | Unique imei of scooter                       |                       |               |
| mode       | Speed mode                                   |                       |               |
| user_id    | Unique user ID                               |                       |               |
| trip_id    | Unique trip ID                               |                       |               |
| speed      | Speed of scooter                             |                       | units: km/h |
| created_at | Timestamp of when the row was created    |                       |               |

| Column Name | Possible Values | Details       |
|---------------|-------------------|---------------|
| mode          | SPORT             | mode2 speed |
|               | NORMAL            | mode1 speed |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### scooter_status_record
Purpose: Record status details of scooters

Status: Active 

| Column            | Definition                                       | Related Table & Key    | Remarks                                                                                            |
|-------------------|--------------------------------------------------|--------------------------|----------------------------------------------------------------------------------------------------|
| id                | Unique ID for the table                        |                          |                                                                                                    |
| device_id         | Unique ID of scooters                          |                          |                                                                                                    |
| status            | Status of scooter                              | scooter_inventory.status |                                                                                                    |
| created_at        | Timestamp of when row was created              |                          |                                                                                                    |
| scooter_latitude  | Scooter latitude based on GPS                  |                          |                                                                                                    |
| scooter_longitude | Scooter longitude based on GPS                 |                          |                                                                                                    |
| previous_status   | Prior status                                   |                          |                                                                                                    |
| battery           | Battery level of scooter                       |                          |                                                                                                    |
| object_id         | Trip ID if the status change from x to in_trip |                          | Mainly required by MDS API. Future: Can reuse field to connect to rebalance.id            |
| city              | City where scooter is located at               |                          |                                                                                                    |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### shop
Purpose: Records shop that we collaborate with 

Status: Active 

| Column        |                Definition                | Related Table & Key | Remarks |
|------------|----------------------------------------|----------------------------|-------|
| id            | Unique ID of table                       |                       |         |
| city          | City of shop                             |                       |         |
| name          | Name of Shop                             |                       |         |
| category      | Category of Shop                         |                       |         |
| address       | Shop's address                           |                       |         |
| suburb        |                                          |                       |         |
| website       | Shop's website                           |                       |         |
| phone         | Shop's phone number                      |                       |         |
| contact_name  | Shop's contact number                    |                       |         |
| contact_email | Shop's contact email                     |                       |         |
| image         |                                          |                       |         |
| disabled      | Whether the shop is disabled             |                       |         |
| deleted       | Whether the shop is deleted              |                       |         |
| created_at    | Timestamp of when the row was   created  |                       |         |
| updated_at    | Timestamp of when the row was   updated  |                       |         |
| created_by    | User ID that created the row           |                       |         |
| updated_by    | User ID that updated the row             |                       |         |
| latitude      | Shop's latitude                          |                       |         |
| longitude     | Shop's longitude                         |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### shop_offer 
Purpose: Record the offer that the shop provides 

Status: Active 

| Column       |                Definition                | Related Table & Key | Remarks |
|------------|----------------------------------------|----------------------------|-------|
| id           | Unique ID of table                       |                       |         |
| title        |                                          |                       |         |
| frequency    | Discount Frequency                       |                       |         |
| detail       | Details of offer                         |                       |         |
| disabled     | Whether offer is disabled                |                       |         |
| deleted      | Whether offer is deleted                 |                       |         |
| created_at   | Timestamp of when the row was   created  |                       |         |
| updated_at   | Timestamp of when the row was   updated  |                       |         |
| created_by   | User ID that created the row           |                       |         |
| updated_by   | User ID that updated the row             |                       |         |
| shop_id      | Unique ID of shop                        | shop.id               |         |
| total_number | Count of offer used                      |                       |         |

| Column   Name | Possible Values | Details |
|---------------|-------------------|---------|
| Frequency     | ONCE_DAY          |         |
|               | ONCE_WEEK         |         |
|               | ONE_TIME          |         |
|               | ANY_TIME          |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### shop_offer_record 
Purpose: Record the details of discount used 

Status: Active 

| Column     |                Definition                | Related Table & Key | Remarks |
|------------|----------------------------------------|----------------------------|-------|
| id         | Unique ID of table                       |                       |         |
| user_id    | Unique ID of user                        | user.id               |         |
| offer_id   | Unique ID of offer                       | shop_offer.id         |         |
| created_at | Timestamp of when the row was created  |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### sign_up_record
Purpose: Record sign-up details of individual users (New row recorded when new user (i.e. not in the user table) logged in with correct OTP)

Status: Active

| Column       | Definition                               | Related Table & Key | Remarks                                                                |
|--------------|------------------------------------------|-----------------------|------------------------------------------------------------------------|
| id           | Unique ID for the table                |                       |                                                                        |
| mobile       | Mobile number                          |                       |                                                                        |
| country_code | Country code                           |                       |                                                                        |
| remote_ip    | Unique remote IP address               |                       |                                                                        |
| geo_city     | Geographical city of sign-up           |                       | Calculated based on Mapbox API based on user's GPS   point       |
| ip_city      | City where IP address used for sign-up |                       |                                                                        |
| latitude     | y-coordinate of sign-up                |                       |                                                                        |
| longitude    | x-coordinate of sign-up                |                       |                                                                        |
| created_at   | Timestamp of when row was created      |                       |                                                                        |
| user_id      | Unique user ID                         | user.id               |                                                                        |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### sms_delivery_record 
Purpose: Record SMS delivery details

Status: Active 

| Column       | Definition                            | Related Table & Key | Remarks                                                                                                                                                      |
|--------------|---------------------------------------|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| id           | Unique ID for the table             |                       |                                                                                                                                                              |
| country_code | Country code                        |                       |                                                                                                                                                              |
| mobile       | Mobile number                       |                       |                                                                                                                                                              |
| status       | Status of SMS delivery              |                       | refer [here](https://support.twilio.com/hc/en-us/articles/223134347-What-are-the-Possible-SMS-and-MMS-Message-Statuses-and-What-do-They-Mean-)       |
| type         | Type of SMS delivered               |                       |                                                                                                                                                              |
| created_at   | Timestamp of when row was created   |                       |                                                                                                                                                              |
| error_code   | Unique error code                   |                       | refer [here](https://www.twilio.com/docs/api/errors)                                                                                                   |
| error_msg    | Message content for each error code |                       |                                                                                                                                                              |
| remote_ip    | User IP address                     |                       |                                                                                                                                                              |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### station 
Purpose: Record details of station 

Status: Active

| Column             | Definition                                          | Related Table & Key                  | Remarks |
|--------------------|-----------------------------------------------------|----------------------------------------|---------|
| id                 | Unique IDs of station                             | scooter_inventory.station_id  |         |
| name               | Name of station                                   |                                        |         |
| description        | Further description of station                    |                                        |         |
| latitude           | y-coordinate of station                           |                                        |         |
| longitude          | x-coordinate station                              |                                        |         |
| capacity           | Maximum number of scooters station can accomodate |                                        |         |
| service_start_date | Time when service started                         |                                        |         |
| service_end_date   | Arbitrary time when service could end             |                                        |         |
| charging           | Deprecated                                          |                                        |         |
| solar              | Deprecated                                           |                                        |         |
| photo              | Deprecated                                          |                                        |         |
| created_at         | Timestamp of when row was created                 |                                        |         |
| updated_at         | Timestamp of when row was updated                 |                                        |         |
| ibeacon_radius     | Deprecated                                          |                                        |         |
| gps_radius         | Radius of GPS range (m)                           |                                        |         |
| deleted            | Flag if station is deleted                        |                                        |         |
| optimal_number     | Inactive                                            |                                        |         |
| guide              | Deprecated                                          |                                        |         |
| type               | Type of station                                   |                                        |         |
| city               | City in which station is at                       |                                        |         |
| zone               | Zone where station is located in                  |                                        |         |
| qr_code            | Station specific QR code that user can scan to park the scooter |                 | Maintained by local ops team via admin dashboard |

| Column Name | Possible Values | Details                                          |
|---------------|-------------------|--------------------------------------------------|
| type          | PARKING           | Current parking station                        |
|               | PICKUP            | Deprecated                                       |
|               | STAGING           | Potential parking station for future       |
|               | WAREHOUSE         | Our warehouse                                  |
|               | OPS_ONLY         | Only for OPS, users not allowed to scan         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### station_images 
Purpose: Records the detail of the station images

Status: Active 

| Column     | Definition                          | Related Table & Key | Remarks                                                                                                     |
|------------|-------------------------------------|-----------------------|-------------------------------------------------------------------------------------------------------------|
| id         | Unique ID for the table           |                       |                                                                                                             |
| station_id | Unique station ID                 | station.id            |                                                                                                             |
| image_id   | Unique image ID                   | neuron_file.id        |                                                                                                             |
| sort       | To determine what is shown first  |                       | numerical numbers are assigned and are shown in ascending orders; only 1 is shown on user app       |
| created_at | Timestamp of when row was created |                       |                                                                                                             |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### station_location_request 
Purpose: Requests for new station by users 

Status: Active 

| Column     | Definition                          | Related   Table & Key | Remarks |
|------------|-------------------------------------|-----------------------|---------|
| id         | Unique ID for the table           |                       |         |
| name       | Name of requestor                 |                       |         |
| email      | email of requestor                |                       |         |
| comment    | Comment made by requestor         |                       |         |
| latitude   | Latitude of suggested station     |                       |         |
| longitude  | Longitude of suggested station    |                       |         |
| created_at | Timestamp of when row was created |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### stripe_exception_record
Purpose: Record details of Stripe transactions that were declined

Status: Active

| Column       | Definition                          | Related Table & Key | Remarks                                                                          |
|--------------|-------------------------------------|-----------------------|----------------------------------------------------------------------------------|
| id           | Unique ID for the table           |                       |                                                                                  |
| user_id      | Unique ID of user                 | user.id               |                                                                                  |
| type         | Type of business action made          |                       |                                                                                  |
| order_id     | Unique ID of the order            | neuron_order.id       |                                                                                  |
| pass_id      | Unique ID of pass used            | pass.id               |                                                                                  |
| charge_id    | Unique ID of Stripe charge        |                       |                                                                                  |
| message      | Text message of Stripe exception  |                       |                                                                                  |
| created_by   | Unique ID of user                 | user.id               |                                                                                  |
| created_at   | Timestamp of when row was created |                       |                                                                                  |
| pay_method   | Method of payment                 |                       | Refer to [possible values](https://stripe.com/docs/declines/codes)       |
| decline_code | Decline reason                    |                       |                                                                                  |
| platform     | Platform used                     |                       |                                                                                  |

| Column Name | Possible Values  | Details |
|---------------|--------------------|---------|
| type          | CREATE_CUSTOMER    |         |
|               | UPDATE_CUSTOMER    |         |
|               | LIST_CARD          |         |
|               | DELETE_CARD        |         |
|               | UPDATE_DEFAULT_CAR |         |
|               | TOP_UP             |         |
|               | ORDER_PAY          |         |
|               | PASS_BUY           |         |
|               | ORDER_REFUND       |         |
|               | TOP_UP_REFUND      |         |
|               | REFUND_PRE_AUTH    |         |
|               | PRE_AUTH           |         |
|               | GET_CUSTOMER       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### trip
Purpose: Record details for individual scooter trips

Status: Active

| Column              | Definition                                                                                            | Related   Table & Key | Remarks                                                                                                          |
|---------------------|-------------------------------------------------------------------------------------------------------|-----------------------|------------------------------------------------------------------------------------------------------------------|
| id                  | Unique ID for the table                                                                             | *.trip_id             |                                                                                                                  |
| start_time          | Timestamp of start of trip                                                                          |                       |                                                                                                                  |
| end_time            | Timestamp of end of trip                                                                            |                       |                                                                                                                  |
| user_id             | Unique ID of user                                                                                   | user.id               |                                                                                                                  |
| city                | City where scooter trip is                                                                          |                       |                                                                                                                  |
| device_id           | Unique ID of scooter device                                                                         | scooter_inventory.id  |                                                                                                                  |
| trip_status         | Status of trip                                                                                      |                       |                                                                                                                  |
| total_minutes       | Total minutes of trip that is chargeable                                                            |                       |                                                                                                                  |
| total_mileage       | Total mileage of trip (From scooter GPS, includes no riding/out of service area distance)                      |                       | Unit is Metres                                                                                                 |
| start_station_id    | Unique ID of start station                                                                          | station.id            |                                                                                                                  |
| returned_station_id | Unique ID of returned station                                                                       | station.id            |                                                                                                                  |
| mx_user_id          | DEPRECATED                                                                                            |                       |                                                                                                                  |
| force_end           | Flag if trip is forcefully ended, by customer service                                               |                       | yes = 1; no = 0                                                                                                |
| bt_unlock           | Flag if trip is unlocked by app bluetooth instead of 3G                                             |                       | yes = 1; no = 0                                                                                                 |
| gold_trip           | DEPRECATED                                                                                            |                     | yes = 1; no = 0                                                                                                |
| auto_end            | Flag if trip ended automatically, which happens when scooter is inactive for more than 10 minutes |                       | yes = 1; no = 0                                                                                                |
| temp_lock           | Flag if scooter is temporarily locked by user (DEPRECATED as of user app version 4.6.0)                                                      |                       | yes = 1; no = 0                                                                                                |
| is_short_trip       | (i) trip.min < 60 seconds and 1 short trip at most for 1 scooter in 24 hours, (ii) 5 short trips at most for 1 user in 24 hours (on different scooters)                                                                        |                       | yes = 1; no = 0                                                                                              |
| is_vip              | Flag if trip is taken by vip members                                                                |                       | yes = 1; no = 0                                                                                               |
| zone                | Zone where user starts trip                                                                         |                       |                                                                                                                  |
| group_id            | Unique group ID for group rides                                                                     |                       |                                                                                                                  |
| engine_off_minutes  | Total minutes where the user was in the no riding   area.                                       |                       | User is not charged during this period                                                                     |
| user_name           | User's username                                                                                     |                       |                                                                                                                  |
| email               | User's email                                                                                        |                       |                                                                                                                  |
| gps_id              | GPS ID attached to scooter                                                                          |                       |                                                                                                                  |
| transfer_ride       | User starts a consecutive trip within a minute. Basic fee is not charged                      |                           | yes = 1; no = 0                                                                                              |
| helmet_wore         | Flag if helmet unlock successfully                                                                  |                       | yes = 1; no = null                                                                                             |
| helmet_returned     | Flag if returned                                                                                    |                       | yes = 1; no = 0                                                                                                |
| app_version         | App version of user                                                                                 |                       |                                                                                                                  |
| start_latitude      | Trip start latitude                                                                                 |                       |                                                                                                                  |
| start_longitude     | Trip start longitude                                                                                |                       |                                                                                                                  |
| end_latitude        | Trip end latitude                                                                                   |                       |                                                                                                                  |
| end_longitude       | Trip end longitude                                                                                  |                       |                                                                                                                  |
| require_helmet      | Records intent to wear helmet                                                                       |                       | null = helmet not on scooter, 1 = user chose to unlock helmet, 0 = user chose not to unlock helmet       |
| start_no_riding  | User starts trip in no riding area |     | Scooter status will change from NO_RIDING to IN_TRIP, ENGINE_OFF |
| end_no_riding    | User left scooter in no riding area  |            | Scooter either force end by CS or auto_end  |
| imei   			| Unique id of GPS device  |            | 				  |
| battery_no   			| 	Unique number attached to the battery  | battery_inventory.battery_number           | 				  |
| vehicle_type  			| 	Type of vehicle  |            | 				  |


| Column Name   | Possible Values   | Details |   
|---------------|-------------------|---------|
| trip_status   | CONFIRMED         |         |   
|               | STARTED           |         |   

Notes:

1. Assuming it its not a short trip, Users will not be charged for time spent to LEAVE no riding area 
	(i) Start: no riding area, End: service area - chargeable time starts the moment the user leaves no riding area till end trip
	(ii) Start: no riding area, End: no riding area - chargeable time starts the moment the user leaves no riding area till scooter auto end or force end

2. Users will not be charged unit fee if user remains in no riding area (trip.total_minutes = 0)
	(i) If it is a short trip, user do not need to pay anything 
	(ii) If it is not short trip, user will pay base fee
	nb - trip will auto end in 10 minutes if scooter does not leave no riding zone at all
	
	
[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### trip_end_record
Purpose: Record trip end (when user clicks) 

Status: Active 

| Column              | Definition                    | Related Table & Key | Remarks |
|------------|----------------------------------------|----------------------------|-------|
| id                  | Unique ID of table                               |                       |         |
| scooter_id          | Unique Scooter ID                                | scooter_inventory.id  |         |
| gps_id              | Unique GPS ID                                    | gps_inventory.id      |         |
| user_id             | Unique user ID                                   | user.id               |         |
| trip_id             | Unique Trip ID                                   | trip.id               |         |
| created_at          | Timestamp of when row was created                |                       |         |
| user_latitude       | Latitude of user's position                      |                       |         |
| user_longitude      | Longitude of user's position                     |                       |         |
| user_gps_time       | Timestamp of when user's position   was recorded |                       |         |
| scooter_latitude    | Latitude of scooter's position                   |                       |         |
| scooter_longitude   | Longitude of scooter's position                  |                       |         |
| return_station_id   | Scooter return location                          |                       |         |
| station_source_type | Trip finish source                               |                       |         |

| Column Name       | Possible Values | Details                             |
|---------------------|-------------------|-------------------------------------|
| station_source_type | IBEACON           |                                     |
|                     | USER_LOCATION     |                                     |
|                     | STATION_QRCODE    | QR code at station for user to scan |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### trip_error_record
Purpose: Record trip error details (trip errors occur when 1. user try to unlock 2. user try to lock 3. payment issue)

Remark: Row created per action (i.e. multiple trip_id)

Status: Active

| Column        | Definition                          | Related Table & Key | Remarks |
|---------------|-------------------------------------|-----------------------|---------|
| id            | Unique ID for the table           |                       |         |
| device_id     | Unique ID of scooter device       | scooter_inventory.id  |         |
| user_id       | Unique ID of user                 | user.id               |         |
| station_id    | Unique ID of station              | station.id            |         |
| error_code    | Type of error                     |                       |         |
| error_message | Further information on error      |                       |         |
| created_at    | Timestamp of when row was created |                       |         |
| trip_id       | Unique ID of trip                 | trip.id               | missing values to be fixed (as of 9/4/2020) |

| Column Name | Possible Values          | Details                                                                                                                                                             | Remarks                                                                                                                                                                                                      |
|---------------|----------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| error_code    | DEFAULT                    | Uncategorized, ask Ming with specific examples                                                                                                                    |       Scooter is not in station radius, convenience fee is required                                                                                                                                        |
|               | DEVICE_ADMIN_LOCK          | Scooter in admin lock                                                                                                                                               |                                                                                                                                                                                                              |
|               | DEVICE_LOW_BATTERY         | Battery < 0.3                                                                                                                                                      |                                                                                                                                                                                                              |
|               | DEVICE_NO_RIDING           | Scooter in no riding zone                                                                                                                                         |                                                                                                                                                                                                              |
|               | DEVICE_OCCUPIED            | The previous rider (of the scanned scooter) did not end the trip properly                                                                                         |                                                                                                                                                                                                              |
|               | DEVICE_OFFLINE             | Twilio can't reach scooter for 5 minutes                                                                                                            |                                                                                                                                                                                                              |
|               | DEVICE_OUT_OF_SERVICE_AREA | Scooter in out of service area                                                                                                                                      |                                                                                                                                                                                                              |
|               | DUPLICATE_TRIP             | User tried to use a new scooter before he ends previous trip on another scooter;   or api conoccurence on the same scooter                                        |                                                                                                                                                                                                              |
|               | END_TRIP_NO_PARKING        | User attempted to end trip in no parking area                                                                                                                     |                                                                                                                                                                                                              |
|               | END_TRIP_NO_RIDING         | User attempted to end trip in no riding area                                                                                                                      |                                                                                                                                                                                                              |
|               | END_TRIP_OUT_SERVICE       | User attempted to end trip in outside of service area                                                                                                             |                                                                                                                                                                                                              |
|               | END_TRIP_OUT_STATION       | User attempted to end trip in outside of station area                                                                                                             |                                                                                                                                                                                                              |
|               | IN_RESERVATION             | Scooter reserved for other user                                                                                                                                   |                                                                                                                                                                                                              |
|               | INSUFFICIENT_BALANCE       | After scanning QRcode, server finds that wallet balance <-1                                                                                                       |       normally this is checked at app local, however, there may be some data lag between servers and user apps. In those cases, server will check the balance and send out error code if necessary       |
|               | NO_CREDIT_CARD             | After scanning QRcode, server finds that users don't have enough balance in wallet and have no credit card/paypal/debit card                                      |       normally this is checked at app local, however, there may be some data lag between servers and user apps. In those cases, server will check the balance and send out error code if necessary       |
|               | PENDING_ORDER              | Users have pending orders that they haven't paid                                                                                                                  |                                                                                                                                                                                                              |
|               | STRIPE_PAYMENT             | After ending trip, paypal/stripe payment was rejected (e.g. card security code incorrect, no such customer, card declined, card   insufficient funds...)    |                                                                                                                                                                                                              |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### trip_finish_source
Purpose: Record details of finished trip

Status: Active

| Column            | Definition                                                                      | Related Table & Key | Remarks           |
|-------------------|---------------------------------------------------------------------------------|-----------------------|-------------------|
| id                | Unique ID for the table                                                       |                       |                   |
| trip_id           | Unique ID of trip                                                             | trip.id               |                   |
| station_id        | Unique ID of station                                                          | station.id            |                   |
| user_latitude     | y-coordinate of the user gps                                                  |                       |                   |
| user_longitude    | x-coordinate of the user gps                                                  |                       |                   |
| device_latitude   | y-coordinate of scooter gps                                                   |                       |                   |
| device_longitude  | x-coordinate of scooter gps                                                   |                       |                   |
| type              | Type of end-trip record (how scooter trip's end was detected and recorded by) |                       |                   |
| created_at        | Timestamp of when row was created                                             |                       |                   |
| scooter_locked    | Flag if scooter is locked                                                     |                       | yes = 1; no = 0 |
| station_latitude  | y-coordinate of station                                                       |                       |                   |
| station_longitude | x-coordinate of station                                                       |                       |                   |
| updated_at        | Timestamp of when row was updated                                             |                       |                   |
| bluetooth_locked  | Flag if bluetooth is locked                                                   |                       |This is a legacy feature, ignore this field |
| image_id          | Unique image ID taken by user                                                 | neuron_file.id        |                   |
| scooter_id        | Unique ID of scooter                                                          | scooter_inventory.id  |                   |
| user_id           | Unique ID of user                                                             | user.id               |                   |
| user_gps_time     | Timestamp of user gps last update time                                        |                       |                   |

| Column Name | Possible Values   | Details |
|---------------|---------------------|---------|
| type          | CONVENIENCE_PARKING |         |
|               | IBEACON             |         | 
|               | GEOFENCE_PARKING    |         |
|               | PRIVILEGE_PARKING   |         |
|               | SCOOTER_LOCATION    |         |
|               | USER_LOCATION       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### trip_helmet
Purpose: Record details of helmet photos uploaded 

Status: Active 

| Column     | Definition                                        | Related Table & Key | Remarks           |
|------------|---------------------------------------------------|-----------------------|-------------------|
| id         | Unique ID for the table                         |                       |                   |
| image_id   | Unique image ID                                 | neuron_file.id        |                   |
| trip_id    | Unique trip ID                                  | trip.id               |                   |
| checked    | Whether it is checked and verified by CS    |                       | yes = 1; no = 0 |
| created_at | Timestamp of when row was created               |                       |                   |
| updated_at | Timestamp of when row was updated               |                       |                   |
| created_by | ID of user who uploaded the image               | user.id               |                   |
| updated_by | ID of CS admin who verified the image                     |                       |                   |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### trip_rating 
Purpose: Record user's trip rating details 

Status: Active

| Column      | Definition                          | Related Table & Key | Remarks          |
|-------------|-------------------------------------|-----------------------|------------------|
| id          | Unique ID for the table           |                       |                  |
| trip_id     | Unique trip ID                    | trip.id               |                  |
| user_id     | Unique user ID                    | user.id               |                  |
| rating      | Rating given by user              |                       |                  |
| comment     | User's comment                    |                       |                  |
| system      | User's System                     |                       | IOS or Android |
| app_version | User's app version                |                       |                  |
| created_at  | Timestamp of when row was created |                       |                  |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### user_card 
Purpose: Record the details of user's card 

Status: Active

Remarks: Row is created when the card passes the pre-auth. (1 card can be attached to a maximum of 3 accounts as of 9/4/2020)

| Column      | Definition                          | Related Table & Key | Remarks |
|-------------|-------------------------------------|-----------------------|---------|
| id          | Unique ID for the table           |                       |         |
| user_id     | Unique user ID                    | user.id               |         |
| fingerprint | Card's fingerprint                |                       |         |
| created_at  | Timestamp of when row was created |                       |         |
| updated_at  | Timestamp of when row was updated |                       |         |
| updated_by  | User ID that updated row          | user.id    |          |
| verified  | Card verified by CS         |  | 	yes = 1; no = 0          |
| deleted  | Card deleted from app          |     | 	yes = 1; no = 0         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### user_coupon 
Purpose: Record user coupon details

Status: Active

| Column        | Definition                            | Related Table & Key | Remarks                                                                                                                                                      |
|---------------|---------------------------------------|-----------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|
| id            | Unique ID for the table             |                       |                                                                                                                                                              |
| user_id       | Unique ID of user (coupon receiver) | user.id               | source=‘REFER_SENDER’,The user id is who share their refer code out;source=‘REFER_RECEIVER’, user id is the new user who was invited by the step 1 user. |
| promo_code_id | Unique ID of promo_code             | promo_code.id         |                                                                                                                                                              |
| balance       | Balance of coupon                   |                       |                                                                                                                                                              |
| expired_at    | Date when coupon expires            |                       |                                                                                                                                                              |
| status        | Status of coupon                    |                       |                                                                                                                                                              |
| created_by    | Unique ID of user                   | user.id               |                                                                                                                                                              |
| created_at    | Timestamp of when row was created   |                       |                                                                                                                                                              |
| updated_by    | Unique ID of user                   | user.id               |                                                                                                                                                              |
| updated_at    | Timestamp of when row was updated   |                       |                                                                                                                                                              |
| currency      | Currency used                       |                       |                                                                                                                                                              |
| scope         | Deprecated                            |                       |                                                                                                                                                              |
| pass_type     | Deprecated                            |                       |                                                                                                                                                              |
| source        | Source of code                      |                       |                                                                                                                                                              |
| code          | Code for promotion                  |                       |                                                                                                                                                              |

| Column Name | Possible Values | Details |
|---------------|-------------------|---------|
| source        | PARKING           |         |
|               | LEANPLUM          |         |
|               | HELMET            |         |
|               | MARKETING         |         |
|               | REFER_SENDER      |         |
|               | DASHBOARD         |         |
|               | REFER_RECEIVER    |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### user_device_info
Purpose: Record the details of user's device 

Status: Active 


| Column     | Definition                          | Related Table & Key | Remarks |
|--------------|-------------------------------------|-----------------------|---------|
| id           | Unique ID for the table           |                       |         |
| user_id      | Unique ID of user                 | user.id               |         |
| system       | System of user device             |                       |         |
| sys_version  | Version of user device system     |                       |         |
| app_version  | App version used                  |                       |         |
| brand        | Brand of user device              |                       |         |
| model        | Model of user device              |                       |         |
| created_at   | Timestamp of when row was created |                       |         |
| updated_at   | Timestamp of when row was updated |                       |         |
| device_id    | Unique ID of user device          |                       |         |
| lp_device_id | Unique ID of user device               |                       |       |
| af_device_id | Deprecated                    |                       |         |
| type         | Account permission type            |                       |         |
| lang         | Language of user device            |                       |         |

| Column Name | Possible Values | Details |
|---------------|-------------------|---------|
| type          | USER              |         |
|               | OPS               |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### user_device_record
Purpose: Records user device details

Status: Active

| Column            | Definition                                                                  | Related Table & Key | Remarks |
|-------------------|-----------------------------------------------------------------------------|---------------------|---------|
| id | Unique ID for the table |    |   |
| user_id | Unique ID of the user  | user.id |     |
| device_id | Unique ID of user's device | user_device_info.device_id
| created_at | Timestamp of when row was created  |    |   |

Limitations of device_id (applicable to user_device_info.device_id):

* If reinstall the App in the same device will change the device_id 

* If factory reset the same device will change the device_id 

* If the same user logged in to the different devices, the device_id will vary from each other

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### user_pass
Purpose: Record details of user's pass

Status: Active

| Column                   | Definition                                                          | Related   Table & Key              | Remarks                                                           |
|--------------------------|---------------------------------------------------------------------|------------------------------------|-------------------------------------------------------------------|
| id                       | Unique pass ID assigned                                             | order_detail.user_pass_id    |                                                                   |
| user_id                  | Unique ID of user                                                   |                                    |                                                                   |
| pass_id                  | ID based on pass type                                               | pass.id                            |                                                                   |
| started_at               | Timestamp of when the pass started                                  |                                    |                                                                   |
| expired_at               | Timestamp of when the pass expired                                  |                                    |                                                                   |
| subscribe                | Flag if user wants to auto-renew the current active   pass          |                                    | yes = 1; no = 0                                                  |
| future_pass_id           | Future pass type (if any)                                           |                                    |                                                                   |
| future_subscribe         | Flag if auto renew the future pass                                  |                                    | yes = 1; no = 0                                                 |
| job_id                   | Internal cron job ID                                                |                                    |                                                                   |
| refund                   | Refund given                                                        |                                    |                                                                   |
| created_at               | Timestamp of when row was created                                   |                                    |                                                                   |
| created_by               | Unique ID of the person who created the record                      |                                    |                                                                   |
| updated_at               | Timestamp of when row was updated                                   |                                    | Can be triggered by auto-renew selection or CS refund       |
| updated_by               | Unique ID of the person who updated the record                      |                                    |                                                                   |
| organization_discount_id | Organization that user is afiliated to (if any)                     | organization_discount.id           |                                                                   |
| purchase_price           | Amount that user paid                                               |                                    |                                                                   |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### user_pass_period
Purpose: Record the pass daily balance (minutes) 

Status: Active

| Column       | Definition                               | Related Table & Key   | Remarks |
|--------------|------------------------------------------|-----------------------|---------|
| id           | Unique ID for the table                  |                       |         |
| user_pass_id | ID in user_pass table                    | user_pass.id          |         |
| balance      | Minutes left (per day)                   |                       |         |
| start_time   | The starting time of that 24 hour period |                       |         |
| end_time     | The ending time of that 24 hour period   |                       |         |
| created_at   | Timestamp of when row was created        |                       |         |
| updated_at   | Timestamp of when row was updated        |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### user_referral
Purpose: Record user referral details

Status: Active

| Column      | Definition                         | Related Table & Key   | Remarks |
|-------------|------------------------------------|-----------------------|---------|
| id          | Unique ID for the table            |                       |         |
| user_id     | Unique ID of user who was referred | user.id               |         |
| code        | Referral code                      |                       |         |
| created_at  | Timestamp of when row was created  |                       |         |
| referrer_id | Unique ID of referrer              | user.id               |         |
| updated_at  | Timestamp of when row was updated  |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)


#### user_promo_code
Purpose: Record user code received from game or other activities
Status: Active

| Column        | Definition                        | Related Table & Key | Remarks |
|---------------|-----------------------------------|-----------------------|---------|
| id            | Unique ID for the table           |                       |         |
| code          | Unique ID of code                 |                       |         |
| email         | receiver's email                  |                       |         |
| reaction_time | reaction time for drunk test game |                       |         |
| value         | value of coupon in local dollars  |                       |         |
| scope         | Trip or Pass                      |                       |         |
| user_id       | null if not redeemed              | user.id               |         |
| created_at    | Timestamp of when row was created |                       |         |
| updated_at    | Timestamp of when row was updated |                       |         |
| type          | type of game/activities           |                       |         |


[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

#### wallet
Purpose: Record user wallet details

Status: Active

| Column     | Definition                        | Related Table & Key | Remarks |
|------------|-----------------------------------|-----------------------|---------|
| id         | Unique ID for the table           |                       |         |
| user_id    | Unique ID of user                 | user.id               |         |
| balance    | DEPRECATED                        |                       |         |
| created_at | Timestamp of when row was created |                       |         |
| updated_at | Timestamp of when row was updated |                       |         |
| currency   | Currency used                     |                       |         |
| cash       | Cash balance                      |                       |         |
| credit     | Credit balance                    |                       |         |

Purpose: Record user wallet details

#### zone
Purpose: Zone information 

Status: Active

| Column     | Definition                          | Related Table & Key           | Remarks           |
|------------|-------------------------------------|-------------------------------|-------------------|
| code       | Zone code                           | scooter_inventory.zone        |                   |
| city_code  | City zone is located in             |                               |                   |
| name       | Name of zone                      |                                 |                   |
| base_fee   | Base price                        |                                 |                   |
| unit_fee   | Unit price                        |                                 |                   |
| created_by | Unique ID of user who created     | user.id                         |                   |
| updated_by | Unique ID of user who updated     | user.id                         |                   |
| created_at | Timestamp of when row was created |                                 |                   |
| updated_at | Timestamp of when row was updated |                                 |                   |
| deleted    | Flag if zone is deleted           |                                 | yes = 1; no = 0   | 

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

### Others 

#### flyway_schema_history
Purpose: Track changes to database

Status: Active

#### getui_user_client 
Purpose: Records the relationship for push notification clientID to user_id

Status: Active 

| Column     | Definition                        | Related Table & Key | Remarks |
|------------|-----------------------------------|---------------------|---------|
| id         | Unique ID for the table           |                     |         |
| user_id    | Unique ID of user                 | user.id             |         |
| client_id  | Unique client ID of user          |                     |         |
| created_at | Timestamp of when row was created |                     |         |
| updated_at | Timestamp of when row was updated |                     |         |
| type       | app type                          |                     |         |

| Column Name | Possible Values | Details |
|-------------|-----------------|---------|
| type        | USER            |         |
|             | OPS             |         |

#### ota_task
Purpose: This table is used to configure some scooter OTA (Over The Air) firmware upgrade.

Status: Active 

#### qrtz_*
Status: Active

#### user_issue_notification_config
Purpose: Records notification preference for Ops user

Status: Active

| Column  | Definition                       | Related Table & Key | Remarks |
|---------|----------------------------------|-----------------------|---------|
| id      | Unique ID for the table    |                       |         |
| user_id | Unique user ID                 | user.id               |         |
| config  | Preference config          |                       |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)

### Inactive/Deprecated

#### active_scooter_location 
Status: Empty

#### alarm_status_record
Status: Empty

#### app_config
Status: Empty

#### app_version
Status: Empty

#### deposit
Status: Empty

#### gps_region
Status: Empty
 
#### gps_self_check
Status: Empty/to add columns

#### ibeacon
Status: Empty/Deprecated 

#### lock_response 
Status: Empty/Deprecated

#### scooter_assemble_record
Status: To be deprecated 

#### station_optimal 
Purpose: Records optimal number of scooters, dynamic (based on data team script)

Status: Inactive as of 9/4/2020. refer [here](https://bitbucket.org/neuroncn/neuron-demand-rebalancing/src/master/demand_rebalancing_au.py)

#### status
Status: EMPTY

#### trip_alert
Purpose: Record details for individual scooter trips

Status: Deprecated

#### trip_section 
Status: Deprecated

#### user_city_record 
Status: Deprecated 

#### user_license
Status: Empty

#### user_status_record 
Status: Empty

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_australia-and-neuron_newzealand.md#top)