[TOC]

## Schema: neuron_user

## Tables

### Frequently used tables

#### card_block 
Purpose: Record fingerprints that were blocked 

Status: Active 

| Column      | Definition                                                 | Related Table & Key | Remarks         |
|-------------|------------------------------------------------------------|---------------------|-----------------|
| id          | Unique ID for table                                        |                     |                 |
| fingerprint | Fingerprint that was blocked                               |                     |                 |
| account     | Country of account                                         |                     |                 |
| reason      | Reason that it was blocked                                 |                     |                 |
| comment     | Comment made (if any) (webhook/stripeRadar)                |                     | Any string to indicate   |
| blocked     | Flag if card is blocked                                    |                     | yes = 1; no = 0 |
| count       | Number of times user try to bind or charge this card |                     | Inaacurate due to duplicate count (as of 31/3/2020)                |
| created_at  | Timestamp of when row was created                          |                     |                 |
| created_by  | User ID that created the row                               |                     |                 |
| updated_at  | Timestamp of when row was updated                          |                     |                 |
| updated_by  | User ID that updated the row                               |                     |                 |
| source      | Stripe related eventID                                     |                     |                 |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_user.md#top)

#### user
Purpose: Record details of individual user account  

Status: Active


| Column                  | Definition                                 | Related Table & Key | Remarks         |
|-------------------------|--------------------------------------------|---------------------|-----------------|
| id                      | Unique ID for the table                    | user_id             |                 |
| username                | Unique username of user                    |                     |                 |
| password                | Encrypted password of user                 |                     |                 |
| country_code            | country code based on mobile               |                     |                 |
| mobile                  | Mobile number of user                      |                     |                 |
| email                   | Email address of user                      |                     |                 |
| email_verified          | Flag if email account of user is verified  |                     | yes = 1; no = 0 |
| address                 |                                            |                     |                 |
| full_name               |                                            |                     |                 |
| id_card                 |                                            |                     |                 |
| avatar                  |                                            |                     |                 |
| nickname                |                                            |                     |                 |
| slogan                  |                                            |                     |                 |
| subscribed              | Flag if user subscribed to email           |                     | yes = 1; no = 0, dropped |
| deleted                 | Flag if user account is deleted            |                     | yes = 1; no = 0 |
| disabled                | Flag if user account is disabled           |                     | yes = 1; no = 0 |
| created_at              | Timestamp of when row was created          |                     |                 |
| updated_at              | Timestamp of when row was updated          |                     |                 |
| notification_subscribed | Flag if user subscribed to IA notification |                     | yes = 1; no = 0, dropped|
| driver_license          |                                            |                     | yes = 1; no = 0 |
| email_marketing         | replace subscribed                         |                     | yes = 1; no = 0 |
| notification_marketing  | replace notification_subsdribed            |                     | yes = 1; no = 0|
| location_marketing      |                                            |                     | yes = 1; no = 0 |
| usage_tracking          |                                            |                     | yes = 1; no = 0 |



[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_user.md#top)

#### user_country
Purpose: Records the user's details including locality

Status: Inactive

| Column                  | Definition                                                                                                                         | Related Table & Key | Remarks                                                                                          |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------|---------------------|--------------------------------------------------------------------------------------------------|
| id                      | Unique ID for the table                                                                                                            |                     |                                                                                                  |
| user_id                 | Unique user ID                                                                                                                     | user.id             |                                                                                                  |
| role_code               | Role of user                                                                                                                       |                     |                                                                                                  |
| is_vip                  | Flag if user is VIP                                                                                                                |                     |                                                                                                  |
| country                 | Location of user determined by IP address (for new user), followed by user GPS geo country once user has logged into the home page |                     | Default to Australia if no relevant info available                                               |
| city                    | Location of user determined by scooter                                                                                             |                     | Recorded when user scans a scooter, and location is tagged to scooter, if unknown default to BNE |
| status                  | Status of user account                                                                                                             |                     |                                                                                                  |
| updated_at              | Timestamp of when row was updated                                                                                                  |                     |                                                                                                  |
| created_at              | Timestamp of when row was created                                                                                                  |                     |                                                                                                  |
| id_card                 |                                                                                                                                    |                     |                                                                                                  |
| avatar                  |                                                                                                                                    |                     |                                                                                                  |
| nickname                |                                                                                                                                    |                     |                                                                                                  |
| slogan                  |                                                                                                                                    |                     |                                                                                                  |
| subscribed              | Flag if user subscribed to email                                                                                                   |                     | yes = 1; no = 0                                                                                  |
| deleted                 | Flag if user account is deleted                                                                                                    |                     | yes = 1; no = 0                                                                                  |
| disabled                | Flag if user account is disabled                                                                                                   |                     | yes = 1; no = 0                                                                                  |
| created_at              | Timestamp of when row was created                                                                                                  |                     |                                                                                                  |
| updated_at              | Timestamp of when row was updated                                                                                                  |                     |                                                                                                  |
| notification_subscribed | Flag if user subscribed to IA notification                                                                                         |                     | yes = 1; no = 0                                                                                  |


| Column Name | Possible Values    | Details |
|-------------|--------------------|---------|
| role        | ADMIN              |         |
|             | CUSTOMER_SERVICE   |         |
|             | FACTORY_TESTER     |         |
|             | GROUND_OPERATOR    |         |
|             | MARKETING          |         |
|             | OPERATOR           |         |
|             | SUBSCRIBER         |         |
|             | USER               |         |
|             | WAREHOUSE_OPERATOR |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_user.md#top)

#### user_role
Purpose: Records different permissions for different roles  

Status: Active


| Column        | Definition                            | Related Table & Key | Remarks |
|---------------|---------------------------------------|---------------------|---------|
| id            | Unique ID for the table               |                     |         |
| user_id       | Unique User ID                        | user.id             |         |
| role_code     | Different roles                       | role.role_code      |         |
| resource      | Specific job scope within the role    |                     |         |
| city          | Location of user determined by scooter|                     |         |
| created_at    | Timestamp of when row was created     |                     |         |
| updated_at    | Timestamp of when row was created     |                     |         |
| updated_by    | User ID that updated the row          |                     |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_user.md#top)

#### user_social
Purpose: Records user's social platform used 

Status: Active


| Column      | Definition                        | Related Table & Key | Remarks            |
|-------------|-----------------------------------|---------------------|--------------------|
| id          | Unique ID for the table           |                     |                    |
| user_id     | Unique user ID                    | user.id             |                    |
| social_id   |                                   |                     |                    |
| social_type | Platform                          |                     | Google or Facebook |
| name        |                                   |                     |                    |
| first_name  |                                   |                     |                    |
| last_name   |                                   |                     |                    |
| updated_at  | Timestamp of when row was updated |                     |                    |
| created_at  | Timestamp of when row was created |                     |                    |


#### user_stripe
Purpose: Records user's social platform used 

Status: Active

| Column      | Definition                        | Related Table & Key | Remarks        |
|-------------|-----------------------------------|---------------------|----------------|
| id          | Unique ID for the table           |                     |                |
| user_id     | Unique ID of user                 | user.id             |                |
| customer_id | Unique ID of Stripe customer      |                     |                |
| country     | Country of user                   |                     |                |
| created_at  | Timestamp of when row was created |                     |                |
| platform    | platform used                     |                     | stripe, paypal |
| account     | country tied to the platform      |                     | AU, NZ, SG     |
| primary     | primary account used for payment  |                     |                |
| updated_at  | Timestamp of when row was updated |                     |                |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_user.md#top)

### Others

#### flyway_schema_history
Purpose: Track changes to database

Status: Active

#### permission
Purpose: Records different permissions for acess control 

Status: Inactive


| Column     | Definition                                       | Related Table & Key | Remarks |
|------------|--------------------------------------------------|---------------------|:-------:|
| id         | Unique ID for the table                          |                     |         |
| name       | Name of different permissions for access control |                     |         |
| created_at | Timestamp of when row was created                |                     |         |


| Column Name      | Possible Values    | Details |
|------------------|--------------------|---------|
| name (tag to id) | ADMIN_CONFIG       |         |
|                  | ALERT_READ         |         |
|                  | ALERT_WRITE        |         |
|                  | DASHBOARD          |         |
|                  | DEVICE_CONTROL     |         |
|                  | DEVICE_READ        |         |
|                  | DEVICE_WRITE       |         |
|                  | DOCK               |         |
|                  | FEEDBACK_WRITE     |         |
|                  | FINANCE_WRITE      |         |
|                  | GEOFENCE_READ      |         |
|                  | GEOFENCE_WRITE     |         |
|                  | GPS                |         |
|                  | GPS_TEST           |         |
|                  | HELMET_WRITE       |         |
|                  | ORGANIZATION_READ  |         |
|                  | ORGANIZATION_WRITE |         |
|                  | OTA_READ           |         |
|                  | OTA_WRITE          |         |
|                  | PASS_READ          |         |
|                  | PASS_WRITE         |         |
|                  | PROMOTION_READ     |         |
|                  | PROMOTION_WRITE    |         |
|                  | REBALANCE_WRITE    |         |
|                  | SCOOTER_READ       |         |
|                  | SCOOTER_REPORT     |         |
|                  | SCOOTER_WRITE      |         |
|                  | SET_REGION         |         |
|                  | SETTING            |         |
|                  | STATION_READ       |         |
|                  | STATION_WRITE      |         |
|                  | SWAP_WRITE         |         |
|                  | TRIP_READ          |         |
|                  | TRIP_WRITE         |         |
|                  | USER_EDIT          |         |
|                  | USER_READ          |         |
|                  | USER_WRITE         |         |
|                  | VERSION            |         |
|                  | ZONE_READ          |         |
|                  | ZONE_WRITE         |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_user.md#top)

#### role
Purpose: Records different roles and associated ids

Status: Active


| Column        | Definition                         | Related Table & Key | Remarks |
|---------------|------------------------------------|---------------------|---------|
| id            | Unique ID for the table            |                     |         |
| role_code     | Different roles                    |                     |         |
| parent_role_id| Entity creation order ID           |                     |         |
| updated_at    | Timestamp of when row was created  |                     |         |
| updated_by    | User ID that updated the row       |                     |         |


| Column Name | Possible Values    | Details |
|-------------|--------------------|---------|
| role_code   | SYSTEM_ADMIN       |         |
|             | ADMIN              |         |
|             | CITY_MANAGER       |         |
|             | OPERATIONS_MANAGER |         |
|             | MARKETING_AGENT    |         |
|             | WAREHOUSE_OPERATOR |         |
|             | GROUND_OPERATOR    |         |
|             | SUBSCRIBER         |         |
|             | CS_MANAGER         |         |
|             | CS_TIER_1          |         |
|             | CS_TIER_2          |         |
|             | SUPPLY_MANAGER     |         |
|             | SUPPLY_AGENT       |         |
|             | FACTORY_TESTER     |         |
|             | USER               |         |
|             | WAREHOUSE_MANAGER  |         |
|             | FINANCE_MANAGER    |         |
|             | PRODUCT            |         |



[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_user.md#top)

#### role_permission
Purpose: Records different permissions for different roles  

Status: Inactive


| Column        | Definition                         | Related Table & Key | Remarks |
|---------------|------------------------------------|---------------------|---------|
| id            | Unique ID for the table            |                     |         |
| role          | Different roles                    |                     |         |
| permission_id | Unique permission ID for each role | permission.id       |         |
| created_at    | Timestamp of when row was created  |                     |         |


| Column Name | Possible Values    | Details |
|-------------|--------------------|---------|
| role        | ADMIN              |         |
|             | CUSTOMER_SERVICE   |         |
|             | FACTORY_TESTER     |         |
|             | GROUND_OPERATOR    |         |
|             | HELMET_CHECKER     |         |
|             | MARKETING          |         |
|             | OPERATOR           |         |
|             | PARTNER            |         |
|             | SUBSCRIBER         |         |
|             | USER               |         |
|             | WAREHOUSE_OPERATOR |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_user.md#top)

#### role_resource_permission
Purpose: Records different permissions for different roles  

Status: Active


| Column        | Definition                         | Related Table & Key | Remarks |
|---------------|------------------------------------|---------------------|---------|
| id            | Unique ID for the table            |                     |         |
| role_code     | Different roles                    | role.role_code      |         |
| resource      | Specific job scope within the role |                     |         |
| permission    | Allowed permissions for each role  |                     |         |
| updated_at    | Timestamp of when row was created  |                     |         |
| updated_by    | User ID that updated the row       |                     |         |

[Return to top](https://bitbucket.org/neuroncn/data-dictionary/src/master/neuron_user.md#top)



