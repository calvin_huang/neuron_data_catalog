import hvac
import psycopg2
import pandas as pd
import dc_allFunctions as All
import copy


def dataAccess(token, query, output):
    # copy from https://bitbucket.org/neuroncn/cs-tracking/src/master/cs_tracking.py
    client = hvac.Client(url='https://vault-hq.neuron.sg',
                         token=token)
    redshift_reader_auth = client.secrets.kv.v2.read_secret_version(mount_point='redshift-reader',
                                                                    path='database-reader')
    red_conn = psycopg2.connect(dbname='rds_raw_data',
                                host='redshift-cluster-1.cqvneaepqxlh.ap-southeast-1.redshift.amazonaws.com',
                                port='5439',
                                user=redshift_reader_auth['data']['data']['redshift.reader.username'],
                                password=redshift_reader_auth['data']['data']['redshift.reader.password'])
    # get csv for raw_dara
    tickets = pd.read_sql(query, red_conn)
    tickets.to_csv(output, index=False)
    print("<{}> generated as query output".format(output))


def get_sr():
    para            = config.sr_para
    csv_file        = para['csv_file']
    schemas         = para['schemas']
    num             = para['num']
    output_name     = para['output_name']
    include_col     = para['include_col']

    dc = All.data_catalog_engin(csv_file)
    dc.similarityRateBySchemaNames(dc.raw_df, schemas, num, output_name, include_col)


def get_oneSchema(para):
    csv_file        = para['csv_file']
    schema_name     = para['schema_name']
    json_name       = para['json_name']
    md_name         = para['md_name']
    keys            = para['keys']

    dc = All.data_catalog_engin(csv_file)
    dc.writeJsonSchemas(schema_list=list(schema_name), output_name=json_name)
    with open(md_name, 'w') as f:  # an alternative to overwrite the md file
        f.write("[TOC]")
    All.json_to_md(json_name, md_name)
    # old version
    # dc.oneSchema_to_md(dc.raw_df, schema_name, md_name, keys)


def get_multiSchemas():
    para            = config.multiSchemas_para
    csv_file        = para['csv_file']
    schema_names    = para['schema_names']
    md_name         = para['md_name']
    keys_dict       = para['keys_dict']
    json_name       = para['json_name']

    dc = All.data_catalog_engin(csv_file)
    dc.writeJsonSchemas(schema_list=list(schema_names), output_name=json_name)
    dc.multiSchemas_to_md(dc.raw_df, schema_names, md_name, keys_dict)


def get_combinedSchemas(para):
    csv_file            = para['csv_file']
    schemas_to_combine  = para['schemas_to_combine']
    json_name           = para['json_name']
    md_name             = para['md_name']
    open_para           = para['open_para']
    keys                = para['keys']
    add_comp_table      = para['add_comp_table']
    combined_name       = para['combined_name']
    # removed_table       = para['removed_table']

    dc = All.data_catalog_engin(csv_file)
    dc.writeJsonCombinedSchemas(schemas_to_combine, combined_name, json_name)
    with open(md_name, 'w') as f:  # an alternative to overwrite the md file
        f.write("[TOC]")
    All.json_to_md(json_name, md_name)
    # old version:
    # dc.combinedSchemas_to_md(md_name, dc.raw_df, schemas_to_combine,
    #                          link=None, open_para=open_para, keys=keys,
    #                          removed_table=removed_table, add_comp_table=add_comp_table)


def edit_catalog(ori, new, json, md):

    origin          = ori
    update          = new
    output_json     = json
    output_md       = md

    # input ->
    def get_dict(filename):
        if filename.endswith('.json'):
            return All.getDictFromJson(filename)
        elif filename.endswith('.csv'):
            return All.getDictFromCsv(filename)
        else:
            return print("Error: Please input csv or json.")
    origin_dict = get_dict(origin)
    update_dict = get_dict(update)

    # origin <- update
    new_dict = All.edit_dict(origin=origin_dict, update=update_dict)

    # dict -> json
    All.writeJsonFromDict(new_dict, output_json)

    # json -> markdown
    All.json_to_md(output_json, output_md)


def update_catalog(ori, new, output_name):
    # output_json     = output_name+'.json'
    output_md       = output_name+'-latest.md'

    # input ->
    def get_dict(filename):
        if filename.endswith('.json'):
            return All.getDictFromJson(filename)
        elif filename.endswith('.csv'):
            return All.getDictFromCsv(filename)
        else:
            return print("Error: Please input csv or json.")  # update this error?
    origin_dict = get_dict(ori)
    old_dict = copy.deepcopy(origin_dict)  # important
    update_dict = get_dict(new)
    # origin <- update
    new_dict = All.update_dict(ori=origin_dict, new=update_dict)

    while True:
        check = input("Do you want to make the above changes?\nInput 'y' or 'n': ")
        if check == 'y':
            # save the old version
            All.writeJsonFromDict(old_dict, output_name+'-'+All.now()+'.json')
            # dict -> json
            All.writeJsonFromDict(new_dict, ori)
            All.writeJsonFromDict(new_dict, new)
            # json -> markdown
            with open(output_md, 'w') as f:  # an alternative to overwrite the md file
                f.write("[TOC]")
            All.json_to_md(new, output_md)
            break
        elif check == 'n':
            print("System ends.")
            break
        else:
            print("Wrong input, please input again.")