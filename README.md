[TOC]

# Data Catalog (DC) Documentation

## Python Files
- *dc_dataAccess.py* - connection to database and data query
- *dc_coreFunctions.py* - collective functions for data processing
- *dc_config.py* - collective parameters/configurations
- *dc_main.py* - execution of functions

## Requirements
- Query output: **CSV**
- First 3 columns: schema_name, table_name, column_name
- Other columns: the corresponding data

## Pipeline
### Auto-output from Database
- To collect data from database in json & md format

| Steps                     | Files            | Output    |
|:--------------------------|:-----------------|:----------|
| Edit db_access parameters | dc_config.py     |           |
| Get data                  | dc_dataAccess.py | csv       |
| Select a function "X"     | dc_main.py       |           |
| Edit related parameters   | dc_config.py     |           |
| Run "X"                   | dc_main.py       | json & md |

- Function "X":
  - get_oneSchema()
  - get_multiSchemas()
  - get_combinedSchemas()

### Manual-input from Json or CSV 
- To edit the auto-output using json or csv

| Steps                   | Files        | Output    |
|:------------------------|:-------------|:----------|
| Edit updatedSchema_para | dc_config.py |           |
| Run get_updatedSchema() | dc_main.py   | json & md |


## Detailed information for main functions

### get_oneSchema()
- function: to write a markdown file for ONE selected schema
- input: CSV for query result
- output: MARKDOWN for one schema
### get_multiSchemas()
- function: to write a markdown file for MULTIPLE selected schemas
- input: CSV for query result
- output: MARKDOWN for multiple schemas
### get_combinedSchemas()
- function: to write a SHARED markdown file for multiple selected schemas
- input: CSV for query result
- output: MARKDOWN for similar schemas
- purpose: to combine the schemas containing common tables
### get_sr()
- function: to generate the similarity rate among selected schemas
- input: CSV for query result
- output: CSV or Dataframe for the table of similarity rate
- purpose: to check the necessity of writing a shared markdown
- description:
  - similarity rate = (the number of common tables) / (the number of tables)
  - comparison table can be generated
  - column_name can also be considered, which takes longer time
### getCommonValues()
- function: find the common values among different columns
- input: CSV with several columns
- output: LIST of common values
- purpose: to find the common empty tables among different schemas 
### update_nested_dict()
- function: update a nested dictionary
- input: two dicts
- output: integrated dict
- descriptions:
  - both dictionaries follow '_schemas_: _tables_: _columns_: _other_columns_: _values_'
### getDictFromJson()
- input: JSON file
- output: DICT
### getDictFromCsv()
- input: CSV file
- output: DICT
### writeJsonFromDict()
- input: DICT
- output: JSON file


