import dc_mainFunctions as Main
from dc_allFunctions import now


myToken = 's.eeuWBR9CnptAMqdP7X8x1N2S'
myQuery = '''
select table_schema, table_name, column_name, data_type, character_maximum_length
from information_schema.columns
where table_schema = 'neuron_user' 
'''
myOutput = 'query-neuron_user.csv'
myPara = {
    'csv_file': myOutput,
    'schema_name': ['neuron_user'],  # this should be List
    'json_name': 'catalog-neuron_user-sys.json',
    'md_name': 'catalog-neuron_user-sys.md',
    'keys': ['user_id']
}


# Database -> raw data in csv
Main.dataAccess(myToken, myQuery, myOutput)

# csv -> system_output in json & md
Main.get_oneSchema(myPara)

# user_input left join system_output -> final json & md
ori     = 'catalog-neuron_user-latest.json'
new     = 'catalog-neuron_user-editor.json'
output_name = 'catalog-neuron_user'
Main.update_catalog(ori, new, output_name)
